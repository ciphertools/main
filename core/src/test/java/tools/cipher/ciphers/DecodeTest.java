/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import static tools.cipher.base.test.CipherTestUtils.assertCipherLogic;
import static tools.cipher.base.test.CipherTestUtils.assertContentEquals;
import static tools.cipher.base.test.CipherTestUtils.assertEncodeDecode;
import static tools.cipher.base.test.CipherTestUtils.fullCipherTest;
import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.util.List;

import tools.cipher.ciphers.solitaire.Solitaire;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.QuadKey;
import tools.cipher.base.ciphers.TriKey;
import tools.cipher.ciphers.enigma.EnigmaLib;
import tools.cipher.ciphers.enigma.EnigmaMachine;
import tools.cipher.ciphers.route.RouteCipherType;
import tools.cipher.ciphers.route.Routes;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.ListUtil;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.util.AMSCOType;
import tools.cipher.util.ReadMode;
import tools.cipher.util.VigenereType;

public class DecodeTest {

    @BeforeAll
    public static void setup() {
        //Dictionary.onLoad();
    }

    @Test
    public void testCaesar() {
        CaesarCipher caesarCipher = new CaesarCipher();

        String plainText = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG";
        String cipherText = "QEBNRFZHYOLTKCLUGRJMPLSBOQEBIXWVALD";
        int key = 23;

        assertEncodeDecode(caesarCipher, key, plainText, cipherText);

        fullCipherTest(caesarCipher);
    }

    @Test
    public void testAffine() {
        // http://practicalcryptography.com/ciphers/affine-cipher/
        AffineCipher affineCipher = new AffineCipher();

        assertTrue(affineCipher.isValid(BiKey.of(3, 23)));
        assertTrue(affineCipher.isValid(BiKey.of(19, 2)));
        assertFalse(affineCipher.isValid(BiKey.of(4, 23)));
        assertFalse(affineCipher.isValid(BiKey.of(3, 32)));
        assertFalse(affineCipher.isValid(BiKey.of(-2, 12)));
        assertFalse(affineCipher.isValid(BiKey.of(3, 27)));
        assertFalse(affineCipher.isValid(BiKey.of(13, -2)));

        String plainText = "DEFENDTHEEASTWALLOFTHECASTLE";
        String cipherText = "VGRGBVPNGGOEPWOFFMRPNGKOEPFG";
        BiKey<Integer, Integer> key = BiKey.of(11, 14);

        assertEncodeDecode(affineCipher, key, plainText, cipherText);
        assertEncodeDecode(affineCipher, BiKey.of(5, 8), "AFFINECIPHER", "IHHWVCSWFRCP");

        fullCipherTest(affineCipher);
    }

    @Test
    public void testColumnarTransposition() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/CompleteColTransposition.pdf
        ColumnarTranspositionCipher transpositionCipher = new ColumnarTranspositionCipher();

        String plainText =  "FILLEDBLOCK";
        String cipherText = "IELKLDOFLBC";
        BiKey<Integer[], ReadMode> key = BiKey.of(new Integer[] {2, 0, 1}, ReadMode.DOWN);

        assertEncodeDecode(transpositionCipher, key, plainText, cipherText);

        fullCipherTest(transpositionCipher);
    }

    @Test
    public void testDoubleColumnarTransposition() {
        DoubleColumnarTranspositionCipher transpositionCipher = new DoubleColumnarTranspositionCipher();

        String plainText =  "FILLEDBLOCK";
        String cipherText = "LELOKIFDLBC";
        QuadKey<Integer[], ReadMode, Integer[], ReadMode> key = QuadKey.of(new Integer[] {2, 0, 1}, ReadMode.ACROSS, new Integer[] {1, 0}, ReadMode.DOWN);

        assertEncodeDecode(transpositionCipher, key, plainText, cipherText);

        fullCipherTest(transpositionCipher);
    }

    @Test
    public void testADFGX() {
        // https://en.wikipedia.org/wiki/ADFGVX_cipher
        ADFGXCipher adfgxCipher = new ADFGXCipher();

        String plainText = "ATTACKATONCE";
        String cipherText = "FAXDFADDDGDGFFFAFAXAFAFX";
        TriKey<String, Integer[], ReadMode> key = TriKey.of("BTALPDHOZKQFVSNGICUXMREWY", new Integer[] {1, 0, 4, 2, 3}, ReadMode.DOWN);

        assertEncodeDecode(adfgxCipher, key, plainText, cipherText);

        fullCipherTest(adfgxCipher);
    }


    @Test
    public void testPeriodicGromark() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/PeriodicGromark.pdf
        PeriodicGromarkCipher periodicGromCipher = new PeriodicGromarkCipher();

        String plainText = "WINTRYSHOWERSWILLCONTINUEFORTHENEXTFEWDAYSACCORDINGTOTHEFORECAST";
        String cipherText = "RHNAAXNRUZBNIUARXCRTPATBRLIGDSVCIRCVOYPVRAAZZMUSREQYEVMMURGWTLUD";
        String key = "ENIGMA";

        assertEncodeDecode(periodicGromCipher, key, plainText, cipherText);

        fullCipherTest(periodicGromCipher);
    }

    @Test
    public void testMorbit() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Morbit.pdf
        MorbitCipher morbitCipher = new MorbitCipher();

        String plainText = "ONCE UPON A TIME";
        String cipherText = "27435881512827465679378";
        Integer[] key = new Integer[] {8, 4, 7, 3, 1, 6, 0, 2, 5};

        assertEncodeDecode(morbitCipher, key, plainText, cipherText);

        fullCipherTest(morbitCipher);
    }

    @Test
    public void testRagbaby() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Ragbaby.pdf
        RagbabyCipher ragbabyCipher = new RagbabyCipher();

        String plainText = "WORD DIVISIONS ARE KEPT";
        String cipherText = "YBBL HNGQDUFGL DEF HFYR";
        String key = "GROSBEAKCDFHILMNPQTUVWYZ";

        assertEncodeDecode(ragbabyCipher, key, plainText, cipherText);

        fullCipherTest(ragbabyCipher);
    }

    @Test
    public void testSolitaire() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Ragbaby.pdf
        SolitaireCipher solitaireCipher = new SolitaireCipher();

        String plainText = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG";
        String cipherText = "XEOOCHUQFYIJGNESEMKZSNSDZWWADMVQYUT";
        Integer[] key = ArrayUtil.createRangeInteger(54);

        assertEncodeDecode(solitaireCipher, key, plainText, cipherText);

        fullCipherTest(solitaireCipher);

        Integer[] keyNinja = {18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,1,16,52,10,11,15,53,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,2,3,4,5,6,7,8,9,51,12,13,14,0,17,33};
        Integer[] keyG = {8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,1,2,3,4,5,6,7,0};
        Integer[] keySolitaire = {48,49,50,2,3,4,5,6,7,17,10,11,12,13,16,51,18,36,52,43,44,26,27,14,15,8,9,53,19,0,22,23,24,25,21,28,29,30,31,32,33,34,35,1,38,39,40,41,42,20,45,46,47,37};
        assertArrayEquals(Solitaire.createCardOrder("NINJA"), keyNinja);
        assertArrayEquals(Solitaire.createCardOrder("G"), keyG);
        assertArrayEquals(Solitaire.createCardOrder("SOLITAIRE"), keySolitaire);
    }

    @Test
    public void testHomophonic() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Homophonic.pdf
        HomophonicCipher homophonicCipher = new HomophonicCipher();

        String plainText = "WORDDIVISIONSMAYBEKEPT";
        String cipherText = "16261199694633038879548312063894672404002789";
        String key = "GOLF";

        assertEncodeDecode(homophonicCipher, key, plainText, cipherText);

        fullCipherTest(homophonicCipher);
    }

    @Test
    public void testADFGVX() {
        // https://en.wikipedia.org/wiki/ADFGVX_cipher
        ADFGXCipher adfgvxCipher = new ADFGXCipher(Alphabet.ALL_36_CHARS, "ADFGVX");

        String plainText = "ATTACKAT1200AM";
        String cipherText = "DGDDDAGDDGAFADDFDADVDVFAADVX";
        TriKey<String, Integer[], ReadMode> key = TriKey.of("NA1C3H8TB2OME5WRPD4F6G7I9J0KLQSUVXYZ", new Integer[] {3, 4, 2, 5, 0, 1, 6}, ReadMode.DOWN);

        assertEncodeDecode(adfgvxCipher, key, plainText, cipherText);

        fullCipherTest(adfgvxCipher);
    }

    @Test
    public void testDigrafid() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Digrafid.pdf
        DigrafidCipher digrafidCipher = new DigrafidCipher();

        String plainText = "THISISTHEFORESTPRI";
        String cipherText = "HJMXWSWJADWGFCSPYI";
        TriKey<String, String, Integer> key = TriKey.of("KEYWORDABCFGHIJKMNPQSTUVXYZ#", "VDPEFQRGSTHUIJWCKXAMYLNZBO#", 3);

        assertEncodeDecode(digrafidCipher, key, plainText, cipherText);

        fullCipherTest(digrafidCipher);
    }

    @Test
    public void testRedefence() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Redefence.pdf
        RedefenceCipher redefenceCipher = new RedefenceCipher();

        String plainText = "CIVILWARFIELDCIPHER";
        String cipherText = "IIWRILCPECLFDHVAEIR";
        BiKey<Integer[], Integer> key = BiKey.of(new Integer[] {1, 0, 2}, 0);

        assertEncodeDecode(redefenceCipher, key, plainText, cipherText);

        fullCipherTest(redefenceCipher);
    }

    @Test
    public void testRailfence() throws ParseException {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Railfence.pdf
        RailFenceCipher railfenceCipher = new RailFenceCipher();

        BiKey<Integer, Integer> parsedKey = railfenceCipher.parseKey("2 3");
        assertEquals(BiKey.of(2, 3), parsedKey);

        String plainText = "CIVILWARFIELDCIPHER";
        String cipherText = "CLFDHIIWRILCPEVAEIR";
        BiKey<Integer, Integer> key = new BiKey<>(3, 0);

        assertEncodeDecode(railfenceCipher, key, plainText, cipherText);

        fullCipherTest(railfenceCipher);
    }

    @Test
    public void testAMSCO() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Amsco.pdf
        AMSCOCipher amscoCipher = new AMSCOCipher();

        String plainText = "INCOMPLETECOLUMNARWITHALTERNATINGSINGLELETTERSANDDIGRAPHS";
        String cipherText = "CECRTEGLENPHPLUTNANTEIOMOWIRSITDDSINTNALINESAALEMHATGLRGR";
        BiKey<Integer[], AMSCOType> key = new BiKey<>(new Integer[] {3, 0, 2, 1, 4}, AMSCOType.DOUBLE_FIRST);

        assertEncodeDecode(amscoCipher, key, plainText, cipherText);

        fullCipherTest(amscoCipher);
    }

    @Test
    public void testBifid() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Bifid.pdf
        BifidCipher bifidCipher = new BifidCipher();

        String plainText = "ODDPERIODSAREPOPULAR";
        String cipherText = "MWEINGIMGEOYYRLVEYWY";
        BiKey<String, Integer> key = BiKey.of("EXTRAKLMPOHWZQDGVUSIFCBYN", 7);

        assertEncodeDecode(bifidCipher, key, plainText, cipherText);

        fullCipherTest(bifidCipher);
    }

    @Test
    public void testConjugatedBifid() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/CMBifid.pdf
        ConjugatedBifidCipher cmbifidCipher = new ConjugatedBifidCipher();

        String plainText = "ODDPERIODSAREPOPULAR";
        String cipherText = "FANXZEXFENUKKRBYNKAK";
        TriKey<String, String, Integer> key = TriKey.of("EXTRAKLMPOHWZQDGVUSIFCBYN", "NCDRSOBFQUVAGPWEYHMXLTIKZ", 7);

        assertEncodeDecode(cmbifidCipher, key, plainText, cipherText);

        fullCipherTest(cmbifidCipher);
    }

    @Test
    public void testHutton() {
        HuttonCipher huttonCipher = new HuttonCipher();

        String plainText = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG";
        String cipherText = "DOCBBDFUFVBXWVECHLCAYSKFKUIKKDFFLBH";
        BiKey<String, String> key = new BiKey<>("FEDORA", "JUPITER");

        assertEncodeDecode(huttonCipher, key, plainText, cipherText);

        fullCipherTest(huttonCipher);
    }

    @Test
    public void testKeyword() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Swagman.pdf
        KeywordCipher keywordCipher = new KeywordCipher();

        String plainText = "THISISASECRETMESSAGE";
        String cipherText = "QABPBPKPOYNOQHOPPKDO";
        String key = "KEYWORDABCFGHIJLMNPQRSTUVXZ";

        assertEncodeDecode(keywordCipher, key, plainText, cipherText);

        fullCipherTest(keywordCipher);
    }

    @Test
    public void testSwagman() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Swagman.pdf
        SwagmanCipher swagCipher = new SwagmanCipher();

        String plainText = "DONTBEAFRAIDTOTAKEABIGLEAPIFONEISINDICATEDYOUCANNOTCROSSARIVERORACHASMINTWOSMALLJUMPS";
        String cipherText = "ENDSCMORDANIBOISICTNASTGBLTEWAOAREEFSAIDVPYRMOEAIAFUILRLDOCOTJNRAAENOUNCMITSOAPHSKATI";
        int[] key = new int[] {2,1,0,3,4,0,4,2,1,3,1,3,4,2,0,4,2,3,0,1,3,0,1,4,2};

        assertEncodeDecode(swagCipher, key, plainText, cipherText);

        fullCipherTest(swagCipher);
    }

    @Test
    public void testPollux() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Pollux.pdf
        PolluxCipher polluxCipher = new PolluxCipher();

        String plainText = "LUCK HELPS";
        String cipherText = "086393425702417685963041456234908745360";
        Character[] key = new Character[] {'.', 'X', '-', '.', '.', 'X', '.', '-', '-', 'X'};

        assertEncodeDecode(polluxCipher, key, plainText, cipherText);

        fullCipherTest(polluxCipher);
    }

    @Test
    public void testGrille() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Grille.pdf
        GrilleCipher grilleCipher = new GrilleCipher();

        String plainText = "THETURNINGGRILLE";
        String cipherText = "TILUNRGHGELTENIR";
        Integer[] key = new Integer[] {0, 7, 9, 11};

        assertEncodeDecode(grilleCipher, key, plainText, cipherText);

        fullCipherTest(grilleCipher);
    }

    @Test
    public void testTrifid() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Trifid.pdf
        TrifidCipher trifidCipher = new TrifidCipher();

        String plainText = "TRIFIDSAREFRACTIONATEDCIPHERS";
        String cipherText = "EYMXVUCRYYYYEAYVYOVVXITDPATHE";
        BiKey<String, Integer> key = BiKey.of("EXTRAODINYBCFGHJKLMPQSUVWZ#", 10);

        assertEncodeDecode(trifidCipher, key, plainText, cipherText);

        fullCipherTest(trifidCipher);
    }

    @Test
    public void testFractionatedMorse() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/FractionatedMorse.pdf
        FractionatedMorseCipher fractionatedMorseCipher = new FractionatedMorseCipher();

        String plainText = "COME AT ONCE";
        String cipherText = "CBIILTMHVVFL";
        String key = "ROUNDTABLECFGHIJKMPQSVWXYZ";

        assertEncodeDecode(fractionatedMorseCipher, key, plainText, cipherText);

        fullCipherTest(fractionatedMorseCipher);
    }

    @Test
    public void testBazeries() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Trifid.pdf
        BazeriesCipher bazeriesCipher = new BazeriesCipher();

        String plainText = "SIMPLESUBSTITUTIONPLUSTRANSPOSITION";
        String cipherText = "ACYYUXYMRQKXKCKGCRQIYITNKYXKCYGQGCI";
        Integer key = 3752;

        assertEncodeDecode(bazeriesCipher, key, plainText, cipherText);

        fullCipherTest(bazeriesCipher);
    }

    @Test
    public void testFourSquare() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Foursquare.pdf
        FourSquareCipher fourSquareCipher = new FourSquareCipher();

        String plainText = "COMEQUICKLYWENEEDHELP";
        String cipherText = "LEWIXAFNEXCUDXUVDPGXHZ";
        BiKey<String, String> key = BiKey.of("GRDLUEYFNVOAHPWMBIQXTCKSZ", "LICNVOTDPWGHEQXAMFSYRBKUZ");

        assertEncodeDecode(fourSquareCipher, key, plainText, cipherText);

        fullCipherTest(fourSquareCipher);
    }

    @Test
    public void testPhillips() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Phillips.pdf
        PhillipsCipher phillipsCipher = new PhillipsCipher();

        String plainText = "SQUARESONEANDFIVEAREACTUALLYTHESAMEASARESQUARESTWOANDEIGHTTHEOVERALLPERIODISFORTY";
        String cipherText = "KZWLYTGEDTQETARBTYGTLFXWLPPOXLTYKUTKGKYTKZWLYTGXSEQETIRZQAAQTCITYKPPVBLHEFHGREYXO";
        TriKey<String, Boolean, Boolean> key = TriKey.of(new RouteTranspositionCipher().encode(CipherUtils.genKeySquare("DIAGONALS", Alphabet.ALL_25_CHARS), QuadKey.of(5, 5, Routes.SNAKE_TOPTOBOTTOM_LEFT, Routes.ACROSS_1)), true, false);

        assertEncodeDecode(phillipsCipher, key, plainText, cipherText);

        fullCipherTest(phillipsCipher);
    }

    @Test
    public void testRouteTransposition() {
        RouteTranspositionCipher routeTransCipher = new RouteTranspositionCipher();

        String plainText = "THEREAREMANYROUTES";
        String cipherText = "THARYRESTUAMREEENO";
        QuadKey<Integer, Integer, RouteCipherType, RouteCipherType> key = QuadKey.of(3, 6, Routes.DIAG_ALTER_TOPLEFT, Routes.SPIRAL_CLOCKWISE_TOPLEFT);

        assertEncodeDecode(routeTransCipher, key, plainText, cipherText);

        for (int i = 0; i < 10; i++) {
            int width = RandomUtil.pickRandomInt(1, 100);
            int height = RandomUtil.pickRandomInt(1, 100);
            int size = width * height;

            for (RouteCipherType type : Routes.getRoutes()) {
                int[] pattern = type.createPattern(width, height, size);
                List<Integer> test = ListUtil.randomRange(0, size);
                for (int col = 0; col < size; col++) {
                    assertTrue(test.remove((Integer) pattern[col]), () -> type.getDescription() + " pattern doesn't contain all indices");
                }
            }
        }

//        fullCipherTest(keywordCipher);
    }

    public Integer[] toArray(String ringStr) {
        Integer[] ring = new Integer[ringStr.length()];
        for(int i = 0; i < ring.length; i++)
            ring[i] = ringStr.charAt(i) - 'A';
        return ring;
    }


    @Test
    public void testEnigma() {
        // http://practicalcryptography.com/ciphers/enigma-cipher/
        EnigmaCipher enigmaCipher = new EnigmaCipher(EnigmaLib.ENIGMA_M3);

        String plainText = "NEEDEDFEEBLYDININGOHTALKEDWISDOMOPPOSEATAPPLAUDEDUSEATTEMPTEDSTRANGERSNOWAREMIDDLETONCONCLUDEDHADITISTRIEDNOADDEDPURSESHALLNOONTRUTHPLEASEDANXIOUSORASINBYVIEWINGFORBADEMINUTESPREVENTTOOLEAVEHADTHOSEGETBEINGLEDWEEKSBLIND";
        String cipherText = "TAJKSKVWIKGBZVXLPTZMBKZSZBNDHMWDRXZRYVRWEUKWUJGOFEVUKLKYOSVQWVCPQTVNMQCRSPQAEHRUIINCLWULPHFTZBKEALZLPHGNOKYWPPVCKYRAGGMOFHFFUDUMQWEVRPNCRLMYOTGPWDVWKVOTQBKFXLOEVPVCGYZHETLNXGROLXJSOUQXZRSPZZGCRCZKJFOHBYQRYLRQCJAWNWECWJZ";
        QuadKey<Integer[], Integer[], Integer[], Integer> key = QuadKey.of(toArray("CAR"), toArray("FER"), new Integer[] {2, 1, 0}, 0);

        assertEncodeDecode(enigmaCipher, key, plainText, cipherText);

        for(EnigmaMachine machine : EnigmaLib.MACHINES) {
            EnigmaCipher cipher = new EnigmaCipher(machine);

            fullCipherTest(cipher);
        }
    }

    @Test
    public void testEnigmaPlugboard() {
        // http://practicalcryptography.com/ciphers/enigma-cipher/
        EnigmaPlugboardCipher enigmaCipher = new EnigmaPlugboardCipher(EnigmaLib.ENIGMA_M3);

//        String plainText = "NEEDEDFEEBLYDININGOHTALKEDWISDOMOPPOSEATAPPLAUDEDUSEATTEMPTEDSTRANGERSNOWAREMIDDLETONCONCLUDEDHADITISTRIEDNOADDEDPURSESHALLNOONTRUTHPLEASEDANXIOUSORASINBYVIEWINGFORBADEMINUTESPREVENTTOOLEAVEHADTHOSEGETBEINGLEDWEEKSBLIND";
//        String cipherText = "TAJKSKVWIKGBZVXLPTZMBKZSZBNDHMWDRXZRYVRWEUKWUJGOFEVUKLKYOSVQWVCPQTVNMQCRSPQAEHRUIINCLWULPHFTZBKEALZLPHGNOKYWPPVCKYRAGGMOFHFFUDUMQWEVRPNCRLMYOTGPWDVWKVOTQBKFXLOEVPVCGYZHETLNXGROLXJSOUQXZRSPZZGCRCZKJFOHBYQRYLRQCJAWNWECWJZ";
//        QuadKey<Integer[], Integer[], Integer[], Integer> key = QuadKey.of(toArray("CAR"), toArray("FER"), new Integer[] {2, 1, 0}, 0);
//
//        assertEncodeDecode(enigmaCipher, key, plainText, cipherText);

        for(EnigmaMachine machine : EnigmaLib.MACHINES) {
            EnigmaCipher cipher = new EnigmaCipher(machine);

            fullCipherTest(cipher);
        }
    }

    @Test
    public void testHill() {
        //
        HillCipher hillCipher = new HillCipher();

//        String plainText = "NEEDEDFEEBLYDININGOHTALKEDWISDOMOPPOSEATAPPLAUDEDUSEATTEMPTEDSTRANGERSNOWAREMIDDLETONCONCLUDEDHADITISTRIEDNOADDEDPURSESHALLNOONTRUTHPLEASEDANXIOUSORASINBYVIEWINGFORBADEMINUTESPREVENTTOOLEAVEHADTHOSEGETBEINGLEDWEEKSBLIND";
//        String cipherText = "TAJKSKVWIKGBZVXLPTZMBKZSZBNDHMWDRXZRYVRWEUKWUJGOFEVUKLKYOSVQWVCPQTVNMQCRSPQAEHRUIINCLWULPHFTZBKEALZLPHGNOKYWPPVCKYRAGGMOFHFFUDUMQWEVRPNCRLMYOTGPWDVWKVOTQBKFXLOEVPVCGYZHETLNXGROLXJSOUQXZRSPZZGCRCZKJFOHBYQRYLRQCJAWNWECWJZ";
//        QuadKey<Integer[], Integer[], Integer[], Integer> key = QuadKey.of(toArray("CAR"), toArray("FER"), new Integer[] {2, 1, 0}, 0);
//
//        assertEncodeDecode(hillCipher, key, plainText, cipherText);

          fullCipherTest(hillCipher);
    }

    @Test
    public void testHillExtended() {
        //
        HillExtendedCipher hillCipher = new HillExtendedCipher();

//        String plainText = "NEEDEDFEEBLYDININGOHTALKEDWISDOMOPPOSEATAPPLAUDEDUSEATTEMPTEDSTRANGERSNOWAREMIDDLETONCONCLUDEDHADITISTRIEDNOADDEDPURSESHALLNOONTRUTHPLEASEDANXIOUSORASINBYVIEWINGFORBADEMINUTESPREVENTTOOLEAVEHADTHOSEGETBEINGLEDWEEKSBLIND";
//        String cipherText = "TAJKSKVWIKGBZVXLPTZMBKZSZBNDHMWDRXZRYVRWEUKWUJGOFEVUKLKYOSVQWVCPQTVNMQCRSPQAEHRUIINCLWULPHFTZBKEALZLPHGNOKYWPPVCKYRAGGMOFHFFUDUMQWEVRPNCRLMYOTGPWDVWKVOTQBKFXLOEVPVCGYZHETLNXGROLXJSOUQXZRSPZZGCRCZKJFOHBYQRYLRQCJAWNWECWJZ";
//        QuadKey<Integer[], Integer[], Integer[], Integer> key = QuadKey.of(toArray("CAR"), toArray("FER"), new Integer[] {2, 1, 0}, 0);
//
//        assertEncodeDecode(hillCipher, key, plainText, cipherText);

        fullCipherTest(hillCipher);
    }

    @Test
    public void testHillSubstitution() {
        //
        HillSubstitutionCipher hillCipher = new HillSubstitutionCipher();

//        String plainText = "NEEDEDFEEBLYDININGOHTALKEDWISDOMOPPOSEATAPPLAUDEDUSEATTEMPTEDSTRANGERSNOWAREMIDDLETONCONCLUDEDHADITISTRIEDNOADDEDPURSESHALLNOONTRUTHPLEASEDANXIOUSORASINBYVIEWINGFORBADEMINUTESPREVENTTOOLEAVEHADTHOSEGETBEINGLEDWEEKSBLIND";
//        String cipherText = "TAJKSKVWIKGBZVXLPTZMBKZSZBNDHMWDRXZRYVRWEUKWUJGOFEVUKLKYOSVQWVCPQTVNMQCRSPQAEHRUIINCLWULPHFTZBKEALZLPHGNOKYWPPVCKYRAGGMOFHFFUDUMQWEVRPNCRLMYOTGPWDVWKVOTQBKFXLOEVPVCGYZHETLNXGROLXJSOUQXZRSPZZGCRCZKJFOHBYQRYLRQCJAWNWECWJZ";
//        QuadKey<Integer[], Integer[], Integer[], Integer> key = QuadKey.of(toArray("CAR"), toArray("FER"), new Integer[] {2, 1, 0}, 0);
//
//        assertEncodeDecode(hillCipher, key, plainText, cipherText);

        fullCipherTest(hillCipher);
    }

    @Test
    public void testQuagmireI() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/QuagmireI.pdf
        QuagmireICipher quagmireICipher = new QuagmireICipher();

        String plainText = "THEQUAGONEISAPERIODICCIPHERWITHAKEYEDPLAINALPHABETRUNAGAINSTASTRAIGHTCIPHERALPHABET";
        String cipherText = "QPMGQRBUJUYIFDMPYAIFQYYJJJHJYCJLUUTPIDVWYMFSGAESDWHIZRBLIRVCFCZPELBPZYYJJJHWLJJLPUP";
        TriKey<String, String, Character> key = TriKey.of("SPRINGFEVABCDHJKLMOQTUWXYZ", "FLOWER", 'A');

        assertEncodeDecode(quagmireICipher, key, plainText, cipherText);

        fullCipherTest(quagmireICipher);
    }

    @Test
    public void testQuagmireII() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/QuagmireII.pdf
        QuagmireIICipher quagmireIICipher = new QuagmireIICipher();

        String plainText = "INTHEQUAGTWOASTRAIGHTPLAINALPHABETISRUNAGAINSTAKEYEDCIPHERALPHABET";
        String cipherText = "JICICOSLYKILFVCHEBDXCCORJIOEWAFMWKKTXBGWHRJIBKEDBJWZABUXWHEHUXOXCU";
        TriKey<String, String, Character> key = TriKey.of("SPRINGFEVABCDHJKLMOQTUWXYZ", "FLOWER", 'A');

        assertEncodeDecode(quagmireIICipher, key, plainText, cipherText);

        fullCipherTest(quagmireIICipher);
    }

    @Test
    public void testQuagmireIII() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/QuagmireIII.pdf
        QuagmireIIICipher quagmireIIICipher = new QuagmireIIICipher();

        String plainText = "THESAMEKEYEDALPHABETISUSEDFORPLAINANDCIPHERALPHABETS";
        String cipherText = "KRSLWMITJDVIABMRGQMTMLLIVIFUIXRHTNYONVRHHIIIRMCAOVEI";
        TriKey<String, String, Character> key = TriKey.of("AUTOMBILECDFGHJKNPQRSVWXYZ", "HIGHWAY", 'A');

        assertEncodeDecode(quagmireIIICipher, key, plainText, cipherText);

        fullCipherTest(quagmireIIICipher);
    }

    @Test
    public void testQuagmireIV() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/QuagmireIV.pdf
        QuagmireIVCipher quagmireIVCipher = new QuagmireIVCipher();

        String plainText = "THISONEEMPLOYSTHREEKEYWORDS";
        String cipherText = "VBMRFCYISPMPBRRHEICXRREIGDX";
        QuadKey<String, String, String, Character> key = QuadKey.of("SENORYABCDFGHIJKLMPQTUVWXZ", "PERCTIONABDFGHJKLMQSUVWXYZ", "EXTRA", 'S');

        assertEncodeDecode(quagmireIVCipher, key, plainText, cipherText);

        for(int i = 0; i < 1000; i++) {
            assertCipherLogic(quagmireIVCipher);
        }
    }

    @Test
    public void testNihilistSubstitution() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/NihilistSubstitution.pdf
        NihilistSubstitutionCipher nihilistSubCipher = new NihilistSubstitutionCipher();

        String plainText = "THEEARLYBIRD";
        String cipherText = "655532754365260844345479";
        BiKey<String, String> key = BiKey.of("SIMPLEABCDFGHKNOQRTUVWXYZ", "EASY");

        assertEncodeDecode(nihilistSubCipher, key, plainText, cipherText);

        fullCipherTest(nihilistSubCipher);
    }

    @Test
    public void testNihilistTransposition() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/NihilistTransposition.pdf
        NihilistTranspositionCipher nihilistSubCipher = new NihilistTranspositionCipher();

        String plainText = "SQUARENEEDEDHERE";
        String cipherText = "EQDERSEHNUEREADE";
        String cipherText2 = "ERNEQSUADEEDEHRE";
        BiKey<Integer[], ReadMode> key = BiKey.of(new Integer[] {1, 0, 2, 3}, ReadMode.DOWN);
        BiKey<Integer[], ReadMode> key2 = BiKey.of(new Integer[] {1, 0, 2, 3}, ReadMode.ACROSS);
        assertEncodeDecode(nihilistSubCipher, key, plainText, cipherText);
        assertEncodeDecode(nihilistSubCipher, key2, plainText, cipherText2);

        fullCipherTest(nihilistSubCipher);
    }

    @Test
    public void testMyszkowski() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Myszkowski.pdf
        MyszkowskiCipher myszkowskiCipher = new MyszkowskiCipher();

        String plainText = "INCOMPLETECOLUMNARWITHPATTERNWORDKEYANDLETTERSUNDERSAMENUMBERTAKENOFFBYROWFROMTOPTOBOTTOM";
        String cipherText = "NOPEEOUNRIHATRWRKYNLTESNESMNMETKNFBRWRMOTBTOILLWTOATDEROOTOCMTCMATPENDEDERURAUBAEFYFOPOTM";
        String key = "BANANA";

        assertEncodeDecode(myszkowskiCipher, key, plainText, cipherText);

        fullCipherTest(myszkowskiCipher);
    }

    @Test
    public void testPlayfair() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Playfair.pdf
        PlayfairCipher playfairCipher = new PlayfairCipher();

        // Padding testing
        assertContentEquals("WEEXEXXQ", playfairCipher.normaliseText("WEEEXX", null));
        assertContentEquals("XQXQXQXQXQXQXQXQ", playfairCipher.normaliseText("XXXXXXXX", null));
        assertContentEquals("EXEXQX", playfairCipher.normaliseText("EEXQ", null));
        assertContentEquals("IXIXIXIX", playfairCipher.normaliseText("JIJI", null));

        String plainText = "COMEQUICKLYWENEEDHELP";
        String cipherText = "DLHFSNCNCRZXCQQGFEEQON";
        String key = "LOGARITHMBCDEFKNPQSUVWXYZ";

        assertEncodeDecode(playfairCipher, key, plainText, cipherText);

        fullCipherTest(playfairCipher);
    }

    @Test
    public void testSeriatedPlayfair() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/SeriatedPlayfair.pdf
        SeriatedPlayfairCipher playfairCipher = new SeriatedPlayfairCipher();

        // Padding testing
        assertContentEquals("WEEEXXEXXE", playfairCipher.normaliseText("WEEEEE", new BiKey<>(null, 3)));

        String plainText = "COMEQUICKLYWENEEDHELPIMMEDIATELYTOM";

        String cipherText = "NLBCSPCDFGXZQQCDCMGCGQTBHCFTRHFGWHGB";
        BiKey<String, Integer> key = new BiKey<>("LOGARITHMBCDEFKNPQSUVWXYZ", 6);

        assertEncodeDecode(playfairCipher, key, plainText, cipherText);

        //for(int i = 0; i < 100; i++) {
            //assertCipherLogic(playfairCipher);
       // }
    }

    @Test
    public void testTwoSquare() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/TwoSquare.pdf
        TwoSquareCipher twoSquareCipher = new TwoSquareCipher();

        String plainText = "ANOTHERDIGRAPHICSETUP";
        String cipherText = "IRRTEHMKGIMEQGRUNMMZSV";
        BiKey<String, String> key = BiKey.of("DIALOGUEBCFHKMNPQRSTVWXYZ", "BIOGRAPHYCDEFKLMNQSTUVWXZ");

        assertEncodeDecode(twoSquareCipher, key, plainText, cipherText);

        fullCipherTest(twoSquareCipher);
    }

    @Test
    public void testTriSquare() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Portax.pdf
        TriSquareCipher triSquareCipher = new TriSquareCipher();

        String plainText = "THREEKEYSQUARESUSED";
        String cipherText = "RHLQXRLXOEVZBATXSERXDDIUAAABFZ";
        TriKey<String, String, String> key = TriKey.of("NSFMUOAGPWVBHQXECIRYLDKTZ", "READINGBCFHKLMOPQSTUVWXYZ", "PASTINOQRMLYZUEKXWVBHGFDC");

        assertEncodeDecode(triSquareCipher, key, plainText, cipherText);

        fullCipherTest(triSquareCipher);
    }

    @Test
    public void testPortax() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Portax.pdf
        PortaxCipher portaxCipher = new PortaxCipher();

        String plainText = "THEEARLYBIRDGETSTHEWORM";
        String cipherText = "NIJAMPBGQCWKHQJEUIKYMPAT";
        String key = "EASY";

        assertEncodeDecode(portaxCipher, key, plainText, cipherText);

        fullCipherTest(portaxCipher);
    }

    @Test
    public void testVigenere() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Slidefair.pdf
        VigenereCipher vigenereCipher = new VigenereCipher(VigenereType.VIGENERE);

        String plainText = "INTHEVIGENERECEQUALSKPLUSPWHEREAISZEROBISONEETC";
        String cipherText = "XBEFEGXNEOIKMETEFYLDZWLVWIEJTFPYIDOLRPFBAQCSPRC";
        String key = "POLYALPHABETIC";

        assertEncodeDecode(vigenereCipher, key, plainText, cipherText);

        for(VigenereType type : VigenereType.NORMAL_LIST) {
            VigenereCipher cipher = new VigenereCipher(type);

            fullCipherTest(cipher);
        }
    }

    @Test
    public void testAutoKey() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Autokey.pdf
        AutokeyCipher autokeyCipher = new AutokeyCipher(VigenereType.VIGENERE);

        String plainText = "THEAUTOKEYCANBEUSEDWITHVIGENEREVARIANTBEAUFORTORPORTA";
        String cipherText = "IYMMYKHRIYWTBLISUEQXMNZZLCMGLMMBEEMRROBVIUSHSXOLUCIMO";
        String key = "PRIMER";

        assertEncodeDecode(autokeyCipher, key, plainText, cipherText);

        for(VigenereType type : VigenereType.NORMAL_LIST) {
            ProgressiveCipher cipher = new ProgressiveCipher(type);

            fullCipherTest(autokeyCipher);
        }
    }

    @Test
    public void testProgressiveKey() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/ProgressiveKey.pdf
        ProgressiveCipher progCipher = new ProgressiveCipher(VigenereType.VIGENERE);

        String plainText = "THISCIPHERCANBEUSEDWITHANYOFTHEPERIODICS";
        String cipherText = "ZYIHGNGBMKJSORJAKZMQQMJRTFHBDCNJHJPWXFNO";
        TriKey<String, Integer, Integer> key = TriKey.of("GRAPEFRUIT", 10, 1);

        assertEncodeDecode(progCipher, key, plainText, cipherText);

        for(VigenereType type : VigenereType.NORMAL_LIST) {
            ProgressiveCipher cipher = new ProgressiveCipher(type);

            fullCipherTest(progCipher);
        }
    }

    @Test
    public void testSlidefair() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Slidefair.pdf
        SlidefairCipher slidefairCipher = new SlidefairCipher(VigenereType.VIGENERE);

        String plainText = "THESLIDEFAIRCANBEUSEDWITHVIGENEREVARIANTORBEAUFORT";
        String cipherText = "EWKMCRNUAFCXTJYQMMYYFUTIGWZPKHJMPKBSAIECKVCFMIILCI";
        String key = "DIGRAPH";

        assertEncodeDecode(slidefairCipher, key, plainText, cipherText);

        for(VigenereType type : VigenereType.SLIDEFAIR_LIST) {
            NicodemusCipher cipher = new NicodemusCipher(type);

            fullCipherTest(cipher);
        }
    }

    @Test
    public void testNicodemus() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Nicodemus.pdf
        NicodemusCipher nicCipher = new NicodemusCipher(VigenereType.VIGENERE);

        String plainText = "THEEARLYBIRDGETSTHEWORM";
        String cipherText = "HAYREVGNKIXKUWMTWMUGTAH";
        BiKey<String, Integer> key = BiKey.of("CAT", 5);

        assertEncodeDecode(nicCipher, key, plainText, cipherText);

        for(VigenereType type : VigenereType.NORMAL_LIST) {
            NicodemusCipher cipher = new NicodemusCipher(type);

            fullCipherTest(cipher);
        }
    }

    @Test
    public void testCadenus() {
        // http://www.cryptogram.org/downloads/aca.info/ciphers/Cadenus.pdf
        CadenusCipher cadenusCipher = new CadenusCipher();

        String plainText  = "ASEVERELIMITATIONONTHEUSEFULNESSOFTHECADENUSISTHATEVERYMESSAGEMUSTBEAMULTIPLEOFTWENTYFIVELETTERSLONG";
        String cipherText = "SYSTRETOMTATTLUSOATLEEESFIYHEASDFNMSCHBHNEUVSNPMTOFARENUSEIEEIELTARLMENTIEETOGEVESITFAISLTNGEEUVOWUL";
        String key = "EASY";

        assertEncodeDecode(cadenusCipher, key, plainText, cipherText);

        fullCipherTest(cadenusCipher);
    }
}
