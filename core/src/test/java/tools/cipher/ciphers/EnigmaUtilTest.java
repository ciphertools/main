/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import tools.cipher.ciphers.enigma.EnigmaUtil;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class EnigmaUtilTest {

    @Test
    public void testDisplayPlugboard() {
        Integer[] mapping = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
        assertEquals("[]", EnigmaUtil.displayPlugboard(mapping));

        this.swap(mapping, 0, 1);
        assertEquals("[AB]", EnigmaUtil.displayPlugboard(mapping));

        this.swap(mapping, 1, 2);
        assertThrows(IllegalArgumentException.class, () -> EnigmaUtil.displayPlugboard(mapping));
        this.swap(mapping, 1, 2); // Revert

        this.swap(mapping, 24, 4);
        this.swap(mapping, 25, 17);
        this.swap(mapping, 15, 18);
        assertEquals("[AB EY PS RZ]", EnigmaUtil.displayPlugboard(mapping));
    }

    public void swap(Integer[] mapping, int a, int b) {
        int temp = mapping[a];
        mapping[a] = mapping[b];
        mapping[b] = temp;
    }
}
