/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import static tools.cipher.base.test.CipherTestUtils.assertEncodeDecode;

import org.junit.jupiter.api.Test;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.CipherChain;
import tools.cipher.util.ReadMode;
import tools.cipher.util.VigenereType;

/**
 * @author Alex
 *
 */
public class CipherChainTest {

    @Test
    public void test() {
        CipherChain<String, BiKey<Integer[], ReadMode>, VigenereCipher, ColumnarTranspositionCipher> cipher = CipherChain.chain(new VigenereCipher(VigenereType.VIGENERE), new ColumnarTranspositionCipher());
        String plainText = "THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG";
        String cipherText = "WDGYNXWMMGQLXRJUJEXESNVGIKEYLUPCSZR";
        BiKey<String, BiKey<Integer[], ReadMode>> key = BiKey.of("TEST", BiKey.of(new Integer[] {1, 4, 0, 2, 3}, ReadMode.DOWN));

        assertEncodeDecode(cipher, key, plainText, cipherText);
    }
}
