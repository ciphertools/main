/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.key;

import tools.cipher.base.key.types.PlugboardKeyType;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class PlugboardKeyTypeTests {

    private static final Supplier<Integer[]> PLUGBOARD_AB = () -> new Integer[] {1,0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};
    private static final Supplier<Integer[]> PLUGBOARD_ALL_SWAP = () -> new Integer[] {1,0,3,2,5,4,7,6,9,8,11,10,13,12,15,14,17,16,19,18,21,20,23,22,25,24};

    @Test
    public void testPlugboardParseMethod() throws ParseException {
        PlugboardKeyType handler = PlugboardKeyType.builder().setRange(2, 8).create();
        assertThrows(ParseException.class, () -> handler.parse("AJ-HY-RD-TR"), "Should throw when plugs overlap");
        assertThrows(ParseException.class, () -> handler.parse("ABC-ED"), "Should throw when plug is too long");

        assertDoesNotThrow(() -> {
            assertArrayEquals(PLUGBOARD_AB.get(), handler.parse("AB"));
        });
        assertDoesNotThrow(() -> {
            assertArrayEquals(PLUGBOARD_ALL_SWAP.get(), handler.parse("AB-CD-EF-GH-IJ-KL-MN-OP-QR-ST-UV-WX-YZ"));
        });
        assertDoesNotThrow(() -> {
            assertArrayEquals(new Integer[] {25,1,2,17,4,5,7,6,8,9,10,11,12,13,14,15,16,3,18,24,20,21,22,23,19,0}, handler.parse("AZ-HG-TY-DR"));
        });
    }

    @Test
    public void testPlugboardIsValidMethod() {
        PlugboardKeyType handler = PlugboardKeyType.builder().setRange(2, 8).create();
        assertTrue(handler.isValid(PLUGBOARD_AB.get()));
        assertTrue(handler.isValid(PLUGBOARD_ALL_SWAP.get()));
        assertTrue(handler.isValid(new Integer[] {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25}));
        assertTrue(handler.isValid(new Integer[] {1,0,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,25,18,19,20,21,22,23,24,17}));
        assertFalse(handler.isValid(new Integer[] {1,0,2,3,5,6,4,7,8,9,10,11,12,13,14,15,16,25,18,19,20,21,22,23,24,17}));
        assertFalse(handler.isValid(new Integer[] {0, 1, 2, 3}));
        assertFalse(handler.isValid(new Integer[] {25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25,25}));

        for (int i = 0; i < 10; i++) {
            Integer[] key = handler.randomise();
            System.out.println("Checking if Plugboard is valid: " + Arrays.toString(key));
            assertTrue(handler.isValid(key));
        }
    }
}
