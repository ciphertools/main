/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.key;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import tools.cipher.base.key.types.BooleanKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.base.key.types.SwagmanKeyType;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class KeyType {

    @Test
    public void testBooleanKeyType() throws ParseException {
        BooleanKeyType handler = BooleanKeyType.builder().create();
        assertEquals(true, handler.parse("true"));
        assertEquals(true, handler.parse("True"));
        assertEquals(false, handler.parse("false"));
        assertEquals(false, handler.parse("faLSe"));
        assertEquals(false, handler.parse("0"));
        assertEquals(true, handler.parse("1"));

        assertThrows(ParseException.class, () -> handler.parse("random"));
    }

    @Test
    public void testVariableStringKeyHandler() {
        VariableStringKeyType handler = VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setMax(15).create();
        assertFalse(handler.isValid("BIN1"));
        assertTrue(handler.isValid("FOX"));
        assertFalse(handler.isValid("BAMBOO"));
        assertFalse(handler.isValid("2HATTER"));

        for(int i = 0; i < 10; i++) {
            String key = handler.randomise();
            System.out.println("Checking if Short26 is valid: " + key);
            assertTrue(handler.isValid(key));
        }

        handler = VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setMax(15).setRepeats().create();
        assertFalse(handler.isValid("BIN1"));
        assertTrue(handler.isValid("FOX"));
        assertTrue(handler.isValid("BAMBOO"));
        assertFalse(handler.isValid("2HATTER"));

        for(int i = 0; i < 10; i++) {
            String key = handler.randomise();
            System.out.println("Checking if Short26Repeating is valid: " + key);
            assertTrue(handler.isValid(key));
        }
    }

    @Test
    public void testOrderedIntegerKeyHandler() {
        OrderedIntegerKeyType handler = OrderedIntegerKeyType.builder().setRange(2, 8).create();
        assertTrue(handler.isValid(new Integer[] {2, 1, 0}));
        assertFalse(handler.isValid(new Integer[] {7, 1, 2, 3, 4, 5, 6}));
        assertTrue(handler.isValid(new Integer[] {7, 1, 2, 3, 4, 5, 6, 0}));
        assertFalse(handler.isValid(new Integer[] {0, 0}));
        assertTrue(handler.isValid(new Integer[] {4, 1, 2, 3, 0}));

        for(int i = 0; i < 10; i++) {
            Integer[] key = handler.randomise();
            System.out.println("Checking if OrderInteger is valid: " + Arrays.toString(key));
            assertTrue(handler.isValid(key));
        }
    }

    @Test
    public void testSwagmanKeyHandler() {
        SwagmanKeyType handler = SwagmanKeyType.builder().setRange(2, 5).create();
        assertFalse(handler.isValid(new int[] {1, 0}));
        assertTrue(handler.isValid(new int[] {1, 0,
                                              0, 1}));
        assertTrue(handler.isValid(new int[] {0, 1, 2,
                                              2, 0, 1,
                                              1, 2, 0}));
        assertFalse(handler.isValid(new int[] {0, 1, 2,
                                               2, 3, 1,
                                               1, 2, 0}));
        assertFalse(handler.isValid(new int[] {2, 1, 0,
                                               0, 1, 2,
                                               1, 2, 0}));


        for(int i = 0; i < 10; i++) {
            int[] key = handler.randomise();
            System.out.println("Checking if Swagman is valid: " + Arrays.toString(key));
            assertTrue(handler.isValid(key));
        }

    }
}
