/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class CadenusCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public CadenusCipher() {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 5);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, String key) {
        int blockSize = 25 * key.length();

        if (plainText.length() % blockSize != 0) {
            StringBuilder builder = new StringBuilder(plainText.length() + blockSize - (plainText.length() % blockSize));
            builder.append(plainText);
            while (builder.length() % blockSize != 0) {
                builder.append('X');
            }

            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {

        if (plainText.length() > key.length() * 25) {
            String combinedMulti = "";
            for (int i = 0; i < plainText.length() / (key.length() * 25); i++)
                combinedMulti += encode(plainText.subSequence(i * key.length() * 25, (i + 1) * key.length() * 25), key);
            return combinedMulti;
        } else {
            int keyLength = key.length();

            int[] order = new int[key.length()];

            int p = 0;
            for (char ch = 'A'; ch <= 'Z'; ++ch)
                for (int i = 0; i < order.length; i++)
                    if (ch == key.charAt(i))
                        order[i] = p++;

            // Creates grid
            char[] temp_grid = new char[plainText.length()];

            for (int j = 0; j < 25; j++) {
                for (int i = 0; i < keyLength; i++) {
                    int newColumn = order[i];
                    int newIndex = (j - charValue(key.charAt(i)) + 25) % 25;
                    temp_grid[newIndex * keyLength + newColumn] = plainText.charAt(j * keyLength + i);
                }
            }
            return new String(temp_grid);
        }

    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        int[] order = new int[key.length()];

        int p = 0;
        for (char ch = 'A'; ch <= 'Z'; ++ch) {
            for (int i = 0; i < order.length; i++) {
                if (ch == key.charAt(i)) {
                    order[p++] = i;
                }
            }
        }

        return decode(cipherText, plainText, key, order);
    }

    public static char[] decode(CharSequence cipherText, @Nullable char[] plainText, String key, int[] order) {
        int keyLength = order.length;

        int gridSize = keyLength * 25;

        if (cipherText.length() % gridSize != 0) {
            throw new IllegalArgumentException("Ciphertext must be a multiple of " + gridSize);
        }

        if (cipherText.length() > gridSize) {
            for (int i = 0; i < cipherText.length(); i += gridSize) {
                char[] decodedPart = decode(cipherText.subSequence(i, i + gridSize), new char[gridSize], key, order);
                System.arraycopy(decodedPart, 0, plainText, i, gridSize);
            }
            return plainText;
        }

        for (int j = 0; j < 25; j++) {
            for (int i = 0; i < keyLength; i++) {
                int newColumn = order[i];
                int newIndex = (j + charValue(key.charAt(newColumn))) % 25;
                plainText[newIndex * keyLength + newColumn] = cipherText.charAt(j * keyLength + i);
            }
        }

        return plainText;
    }

    public static int charValue(char character) {
        if (character >= 'W')
            return ('Z' - character + 1) % 25;
        return ('Z' - character) % 25;
    }
}
