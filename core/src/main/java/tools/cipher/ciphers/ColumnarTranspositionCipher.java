/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.interfaces.IKeyType.IKeyBuilder;
import tools.cipher.base.key.types.EnumKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.util.ReadMode;

public class ColumnarTranspositionCipher extends BiKeyCipher<Integer[], ReadMode, OrderedIntegerKeyType.Builder, EnumKeyType.Builder<ReadMode>> {

    public ColumnarTranspositionCipher() {
        super(OrderedIntegerKeyType.builder().setRange(2, Integer.MAX_VALUE),
                EnumKeyType.builder(ReadMode.class).setUniverse(ReadMode.values()));
    }

    @Override
    public IKeyBuilder<Integer[]> limitDomainForFirstKey(OrderedIntegerKeyType.Builder firstKey) {
        return firstKey.setRange(2, 9);
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<Integer[], ReadMode> key) {
        Integer[] order = key.getFirstKey();

        StringBuilder[] columns = ArrayUtil.fill(new StringBuilder[key.getFirstKey().length],
                () -> new StringBuilder((int)Math.ceil((double) plainText.length() / key.getFirstKey().length)));

        int index = 0;
        label: while (true) {
            for (int col = 0; col < order.length; col++) {
                if (index >= plainText.length())
                    break label;

                columns[order[col]].append(plainText.charAt(Math.min(index++, plainText.length() - 1)));
            }
        }
        StringBuilder read = new StringBuilder(plainText.length());

        switch (key.getSecondKey()) {
        case ACROSS:
            int rows = (int) Math.ceil((double) plainText.length() / order.length);
            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < order.length; col++) {
                    if (row < columns[col].length()) {
                        read.append(columns[col].charAt(row));
                    }
                }
            }
            break;
        case DOWN:
            for (int i = 0; i < order.length; i++) {
                read.append(columns[i]);
            }
            break;
        }

        return read;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<Integer[], ReadMode> key) {
        Integer[] order = key.getFirstKey();

        Integer[] orderIndex = ArrayUtil.toIndexedArray(order);
        int period = order.length;
        int rows = (int) Math.ceil(cipherText.length() / (double) period);

        int index = 0;
        switch (key.getSecondKey()) {
        case DOWN:
            for (int col = 0; col < period; col++) {
                int trueColumn = orderIndex[col];
                for (int row = 0; row < rows; row++) {
                    if (row * period + trueColumn >= cipherText.length())
                        continue;

                    if (index >= cipherText.length())
                        break;

                    plainText[row * period + trueColumn] = cipherText.charAt(index++);
                }
            }
            break;
        case ACROSS:
            for (int row = 0; row < rows; row++) {
                for (int col = 0; col < period; col++) { // Swapped is all that needs to happen
                    int trueColumn = orderIndex[col];
                    if (row * period + trueColumn >= cipherText.length())
                        continue;

                    if (index >= cipherText.length())
                        break;

                    plainText[row * period + trueColumn] = cipherText.charAt(index++);
                }
            }
            break;
        }

        return plainText;
    }
}
