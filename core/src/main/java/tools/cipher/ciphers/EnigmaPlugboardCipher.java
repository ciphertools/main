/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.QuinKey;
import tools.cipher.base.ciphers.QuinKeyCipher;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.base.key.types.PlugboardKeyType;
import tools.cipher.ciphers.enigma.EnigmaMachine;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.characters.CharArrayWrapper;

import java.text.ParseException;
import java.util.Locale;

public class EnigmaPlugboardCipher extends QuinKeyCipher<Integer[], Integer[], Integer[], Integer, Integer[], OrderedIntegerKeyType.Builder, OrderedIntegerKeyType.Builder, OrderedIntegerKeyType.Builder, IntegerKeyType.Builder, PlugboardKeyType.Builder> {

    private EnigmaMachine machine;

    public EnigmaPlugboardCipher(EnigmaMachine machine) {
        super(OrderedIntegerKeyType.builder().setSize(3).setEntryRange(26).setRepeats().setDisplay(CipherUtils::displayAsLetters), // indicator
                OrderedIntegerKeyType.builder().setSize(3).setEntryRange(26).setRepeats().setDisplay(CipherUtils::displayAsLetters), // ring
                OrderedIntegerKeyType.builder().setSize(3).setEntryRange(machine.getRotorCount()),
                IntegerKeyType.builder().setRange(0, machine.getReflectorCount() - 1),
                PlugboardKeyType.builder()); // plugboard
        this.machine = machine;
    }

    @Deprecated
    public EnigmaPlugboardCipher setMachine(EnigmaMachine machine) {
        this.machine = machine;
        return this;
    }

    @Override
    public CharSequence encode(CharSequence plainText, QuinKey<Integer[], Integer[], Integer[], Integer, Integer[]> key) {
        return new CharArrayWrapper(this.decodeEfficiently(plainText, key));
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, QuinKey<Integer[], Integer[], Integer[], Integer, Integer[]> key) {
        Integer[] indicator = ArrayUtil.copy(key.getFirstKey());
        Integer[] ring = ArrayUtil.copy(key.getSecondKey());

        int thinRotor = 0;

        //int reflectorSetting = 0;
        //int thinRotorSetting = 0;
        for(int i = 0; i < cipherText.length(); i++) {
            this.nextRotorPosition(key.getThirdKey(), indicator);

            int ch = cipherText.charAt(i) - 'A';

            // Plugboard
            if (key.getFifthKey() != null) {
                ch = nextCharacter(ch, key.getFifthKey());
            }

            for(int r = 2; r >= 0; r--) {
                ch = nextCharacter(ch, this.machine.rotors[key.getThirdKey()[r]], indicator[r] - ring[r]);
            }

            if(this.machine.hasThinRotor()) {
                ch = this.nextCharacter(ch, this.machine.thinRotor[thinRotor]);//, thinRotorSetting);
            }

            ch = nextCharacter(ch, this.machine.reflector[key.getFourthKey()]);//, reflectorSetting);

            if(this.machine.hasThinRotor()) {
                ch = this.nextCharacter(ch, this.machine.thinRotorInverse[thinRotor]);//, thinRotorSetting);
            }

            for(int r = 0; r < 3; r++) {
                ch = nextCharacter(ch, this.machine.rotorsInverse[key.getThirdKey()[r]], indicator[r] - ring[r]);
            }

            // Reverse of plugboard is identical
            if (key.getFifthKey() != null) {
                ch = nextCharacter(ch, key.getFifthKey());
            }

            plainText[i] = (char)(ch + 'A');
        }

        return plainText;
    }

    public void nextRotorPosition(Integer[] rotors, Integer[] indicator) {
        //Next settings
        if(this.machine.getStepping()) { //Ratchet Setting
            Integer[] middleNotches = this.machine.notches[rotors[1]];
            Integer[] endNotches = this.machine.notches[rotors[2]];

            if(ArrayUtil.contains(middleNotches, indicator[1])) {
                indicator[0] += 1;
                indicator[1] += 1;
                if(indicator[0] > 25) indicator[0] = 0;
                if(indicator[1] > 25) indicator[1] = 0;
            }

            if(ArrayUtil.contains(endNotches, indicator[2])) {
                indicator[1] += 1;
                if(indicator[1] > 25) indicator[1] = 0;
            }

            indicator[2] += 1;
            if(indicator[2] > 25) indicator[2] = 0;
        }
        else { //Cog Setting
            Integer[] endNotches = this.machine.notches[rotors[2]];
            if(ArrayUtil.contains(endNotches, indicator[2])) {
                Integer[] middleNotches = this.machine.notches[rotors[1]];

                if(ArrayUtil.contains(middleNotches, indicator[1])) {
                    //TODO need to add non fixed reflector
                    //int[] otherNotches = machine.notches[rotors[0]];

                    //if(ArrayUtil.contains(otherNotches, indicator[0]))
                    //  reflectorSetting = (reflectorSetting + 1) % 26;

                    indicator[0] = (indicator[0] + 1) % 26;
                }
                indicator[1] = (indicator[1] + 1) % 26;
            }
            indicator[2] = (indicator[2] + 1) % 26;
        }
    }

    public int nextCharacter(int ch, Integer[] key) {
        return key[ch];
    }

    public int nextCharacter(int ch, Integer[] key, int offset) {
        if(offset > 0) {
            ch += offset;
            if(ch > 25) ch -= 26;
            ch = nextCharacter(ch, key);
            ch -= offset;
            if(ch < 0) ch += 26;
        }
        else if(offset < 0) {
            ch += offset;
            if(ch < 0) ch += 26;
            ch = nextCharacter(ch, key);
            ch -= offset;
            if(ch > 25) ch -= 26;
        }
        else
            ch = nextCharacter(ch, key);
        return ch;
    }

    @Override
    public QuinKey<Integer[], Integer[], Integer[], Integer, Integer[]> parseKey(String input) throws ParseException {
        String[] parts = input.split(" ");
        if (parts.length != 5) {
            throw new ParseException(input, 0);
        }

        return QuinKey.of(parseLetterToInteger(this.machine, parts[0]), parseLetterToInteger(this.machine, parts[1]), parseCogNumber(this.machine, parts[2]), parseReflector(this.machine, parts[3]), this.getFifthKeyType().parse(parts[4]));
    }

    public static Integer[] parseLetterToInteger(EnigmaMachine machine, String s) throws ParseException {
        if (s.length() != 3) {
            throw new ParseException("Ring/Indicator settings must be 3 letters", 0);
        }

        s = s.toUpperCase(Locale.ROOT);

        if (!CharSequenceUtils.isCharSubset(s, Alphabet.ALL_26_CHARS)) {
            throw new ParseException("Ring/Indicator setting has invalid letters", 0);
        }

        Integer[] key = {s.charAt(0) - 'A', s.charAt(1) - 'A', s.charAt(2) - 'A'};

        return key;
    }

    public static Integer[] parseCogNumber(EnigmaMachine machine, String s) throws ParseException {
        final String[] MAPPING = {"I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X"};

        String[] elements = s.split(",");
        Integer[] key = new Integer[3];
        int offset = 0;
        for (int i = 0; i < elements.length; i++) {
            int cogIdx = ArrayUtil.indexOf(MAPPING, elements[i]);
            if (cogIdx == -1 || cogIdx >= machine.getRotorCount()) {
                throw new ParseException("Invalid cog roman numeral given", offset);
            }
            if (ArrayUtil.contains(key, 0, i, cogIdx)) {
                throw new ParseException("Duplicate cog used " + elements[i], offset);
            }

            key[i] = cogIdx;
            offset += elements[i].length() + 1;
        }

        return key;
    }

    public static Integer parseReflector(EnigmaMachine machine, String s) throws ParseException {

        int reflectorIdx = ArrayUtil.indexOf(machine.reflectorNames, s);
        if (reflectorIdx == -1) {
            throw new ParseException("Invalid reflector", 0);
        }

        return reflectorIdx;
    }
}
