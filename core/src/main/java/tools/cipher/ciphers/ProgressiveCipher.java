/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.util.VigenereType;

public class ProgressiveCipher extends TriKeyCipher<String, Integer, Integer, VariableStringKeyType.Builder, IntegerKeyType.Builder, IntegerKeyType.Builder> {

    private VigenereType type;

    public ProgressiveCipher(VigenereType typeIn) {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE),
                IntegerKeyType.builder().setRange(2, Integer.MAX_VALUE),
                IntegerKeyType.builder().setRange(1, Integer.MAX_VALUE));
        this.type = typeIn;
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 15);
    }

    @Override
    public IntegerKeyType.Builder limitDomainForSecondKey(IntegerKeyType.Builder firstKey) {
        return firstKey.setRange(2, 15);
    }

    @Override
    public IntegerKeyType.Builder limitDomainForThirdKey(IntegerKeyType.Builder firstKey) {
        return firstKey.setRange(1, 15);
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, Integer, Integer> key) {
        String key2 = key.getFirstKey();
        Character[] cipherText = new Character[plainText.length()];
        int progression = 0;
        int count = 0;
        for (int index = 0; index < plainText.length(); index++) {
            char charIdVig = this.type.encode(plainText.charAt(index), (char) key2.charAt(index % key2.length()));
            cipherText[index] = this.type.encode(charIdVig, (char) (progression + 'A'));

            if (count + 1 == key.getSecondKey()) {
                count = 0;
                progression = (progression + key.getThirdKey()) % 26;
            } else
                count++;

        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, Integer, Integer> key) {
        String key2 = key.getFirstKey();
        int progression = 0;
        int count = 0;
        for (int index = 0; index < cipherText.length(); index++) {
            char charIdProg = this.type.decode(cipherText.charAt(index), (char) (progression + 'A'));
            plainText[index] = this.type.decode(charIdProg, key2.charAt(index % key2.length()));

            if (count + 1 == key.getSecondKey()) {
                count = 0;
                progression = (progression + key.getThirdKey()) % 26;
            } else
                count++;

        }

        return plainText;
    }

    public static char[] decodeBase(char[] cipherText, char[] plainText, int period, int progressiveKey, VigenereType type) {
        int progression = 0;
        int count = 0;
        for (int index = 0; index < cipherText.length; index++) {
            plainText[index] = type.decode(cipherText[index], (char) (progression + 'A'));

            if (count + 1 == period) {
                count = 0;
                progression = (progression + progressiveKey) % 26;
            } else
                count++;

        }

        return plainText;
    }
}
