/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.key.types.FullStringKeyType;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class NihilistSubstitutionCipher extends BiKeyCipher<String, String, FullStringKeyType.Builder, VariableStringKeyType.Builder> {

    public NihilistSubstitutionCipher() {
        super(FullStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS), VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setRange(2, Integer.MAX_VALUE));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForSecondKey(VariableStringKeyType.Builder secondKey) {
        return secondKey.setRange(2, 15);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, BiKey<String, String> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<String, String> key) {
        String cipherText = "";
        for (int i = 0; i < plainText.length(); i++) {
            int no = getNumberValue(plainText.charAt(i), key.getFirstKey()) + getNumberValue(key.getSecondKey().charAt(i % key.getSecondKey().length()), key.getFirstKey());
            if (no >= 100)
                no -= 100;
            String strNo = "" + no;
            if (strNo.length() < 2)
                strNo = "0" + strNo;
            cipherText += strNo;
        }
        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, BiKey<String, String> key) {
        return decodeEfficiently(cipherText, new char[cipherText.length() / 2], key);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<String, String> key) {

        for (int i = 0; i < cipherText.length() / 2; i++) {
            int no = (cipherText.charAt(i * 2) - '0') * 10 + (cipherText.charAt(i * 2 + 1) - '0');
            if (no <= 10)
                no += 100;

            no -= getNumberValue(key.getSecondKey().charAt(i % key.getSecondKey().length()), key.getFirstKey());

            int column = no % 10;

            int row = (no - column) / 10 - 1;

            // TODO if(row * 5 + column - 1 >= key.getFirstKey().length() || row * 5 +
            // column - 1 < 0) return StringTransformer.repeat("Z",
            // plainText.length).toCharArray();
            plainText[i] = key.getFirstKey().charAt(row * 5 + column - 1);
        }

        return plainText;
    }

    private static int getNumberValue(char character, String keysquare) {
        int index = keysquare.indexOf(character);
        int row = (int) (index / 5) + 1;
        int column = index % 5 + 1;
        return row * 10 + column;
    }
}
