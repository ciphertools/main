/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers.enigma;

import java.util.ArrayList;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.StringJoiner;

import com.alexbarter.lib.util.ArrayUtil;

public class EnigmaUtil {

    public static final String POSTER_TEXT = "THEBODYTHATWASHEDUPONTHERIVERBANKBELONGEDTOAYOUNGSCIENTISTCALLEDJAMELIAITBOREALLTHEHALLMARKSOFAPROFESSIONALHITWHICHWOULDHAVEMADESENSEIFSHEWORKEDINNUCLEARPHYSICSORBIOWARFAREBUTSHEDIDNTSHEWORKEDONGRAVITYWAVESANDFORTHELIFEOFMEICOULDNTSEEHOWTHATWOULDHAVEGOTHERKILLED";

    public static List<Integer> useCribInCipherText(String cipherText, String crib) {
        List<Integer> positions = new ArrayList<Integer>();

        nextPos: for (int i = 0; i < cipherText.length() - crib.length() + 1; i++) {
            for (int j = 0; j < crib.length(); j++)
                if (cipherText.charAt(i + j) == crib.charAt(j))
                    continue nextPos;
            positions.add(i);
        }
        return positions;
    }

    public static String displayPlugboard(Integer[] mapping) {
        StringJoiner joiner = new StringJoiner(" ", "[", "]");
        List<Integer> plugsUsed = new ArrayList<>(mapping.length / 2);

        for (int i = 0; i < mapping.length; i++) {
            if (!plugsUsed.contains(i)) {
                int other = ArrayUtil.indexOf(mapping, i);
                if (other == i) continue;
                // If the plug is supposedly paired with another that has already
                // been used then there is a 3 or more way invalid swap
                if (plugsUsed.contains(other)) {
                    throw new IllegalArgumentException(String.format(
                        "Invalid plugboard mapping given involving indices %d, %d, %d",
                        i, other, mapping[i]
                    ));
                }

                joiner.add(String.format("%c%c", (char) (i + 'A'), (char) (other + 'A')));
                plugsUsed.add(i);
                plugsUsed.add(other);
            }
        }

        return joiner.toString();
    }
}
