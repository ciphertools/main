/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.ciphers.solitaire.Solitaire;

public class SolitaireCipher extends UniKeyCipher<Integer[], OrderedIntegerKeyType.Builder> {

    public SolitaireCipher() {
        super(OrderedIntegerKeyType.builder().setRange(54, 54)); // TODO add joker checks
    }

    @Override
    public CharSequence encode(CharSequence plainText, Integer[] key) {
        String cipherText = "";
        int index = 0;

        while (index < plainText.length()) {
            key = Solitaire.nextCardOrder(key);

            int topCard = key[0];
            int keyStreamNumber;

            if (!Solitaire.isJoker(topCard))
                keyStreamNumber = key[topCard + 1];
            else
                keyStreamNumber = key[key.length - 1];

            if (Solitaire.isJoker(keyStreamNumber))
                continue;

            cipherText += (char) (((plainText.charAt(index) - 'A') + (keyStreamNumber + 1)) % 26 + 'A');
            index += 1;
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, Integer[] key) {
        int index = 0;

        for (int i = 0; i < index; i++)
            plainText[i] = cipherText.charAt(i);

        while (index < cipherText.length()) {

            key = Solitaire.nextCardOrder(key);

            int topCard = key[0];
            int keyStreamNumber;
            // System.out.println("Top card" + topCard);
            if (!Solitaire.isJoker(topCard))
                keyStreamNumber = key[topCard + 1];
            else
                keyStreamNumber = key[key.length - 1];

            // System.out.println(keyStreamNumber);
            if (Solitaire.isJoker(keyStreamNumber))
                continue;

            plainText[index] = (char) ((52 + (cipherText.charAt(index) - 'A') - (keyStreamNumber + 1)) % 26 + 'A');
            index += 1;
        }

        return plainText;
    }
}
