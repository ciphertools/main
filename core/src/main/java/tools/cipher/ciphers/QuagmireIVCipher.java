/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.QuadKey;
import tools.cipher.base.ciphers.QuadKeyCipher;
import tools.cipher.base.key.types.FullStringKeyType;
import tools.cipher.base.key.types.ObjectKeyType;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.util.Quagmire;

public class QuagmireIVCipher extends QuadKeyCipher<String, String, String, Character, FullStringKeyType.Builder, FullStringKeyType.Builder, VariableStringKeyType.Builder, ObjectKeyType.Builder<Character>> {

    public QuagmireIVCipher() {
        super(FullStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS),
                FullStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS),
                VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE),
                ObjectKeyType.<Character>builder().setUniverse(CharSequenceUtils.toArray(Alphabet.ALL_26_CHARS)));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForThirdKey(VariableStringKeyType.Builder thirdKey) {
        return thirdKey.setRange(2, 15);
    }

    @Override
    public CharSequence encode(CharSequence plainText, QuadKey<String, String, String, Character> key) {
        return Quagmire.encode(plainText, key.getFirstKey(), key.getSecondKey(), key.getThirdKey(), key.getFourthKey());
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, QuadKey<String, String, String, Character> key) {
        return Quagmire.decode(cipherText, plainText, key.getFirstKey(), key.getSecondKey(), key.getThirdKey(), key.getFourthKey());
    }
}
