/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.key.types.IntegerGenKeyType;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class ConjugatedBifidCipher extends TriKeyCipher<String, String, Integer, SquareStringKeyType.Builder, SquareStringKeyType.Builder, IntegerGenKeyType.Builder> {

    public ConjugatedBifidCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5),
                SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5),
                IntegerGenKeyType.builder().setRange(0, 5000).addFilter(i -> i != 1));
    }

    @Override
    public IntegerGenKeyType.Builder limitDomainForThirdKey(IntegerGenKeyType.Builder thirdKey) {
        return thirdKey.setRange(0, 15);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, TriKey<String, String, Integer> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, String, Integer> key) {
        return BifidCipher.encodeGeneral(plainText, key.getFirstKey(), key.getSecondKey(), key.getThirdKey());
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, String, Integer> key) {
        return BifidCipher.decodeGeneral(cipherText, plainText, key.getFirstKey(), key.getSecondKey(), key.getThirdKey());
    }
}
