/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.key.types.SquareStringKeyType;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.constants.Alphabet;

public class TriSquareCipher extends TriKeyCipher<String, String, String, SquareStringKeyType.Builder, SquareStringKeyType.Builder, SquareStringKeyType.Builder> {

    public TriSquareCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5), SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5), SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, TriKey<String, String, String> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        if (builder.length() % 2 == 1) {
            builder.append('X');
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, String, String> key) {
        StringBuilder cipherText = new StringBuilder(plainText.length() * 3 / 2);
        for (int i = 0; i < plainText.length() / 2; i++) {
            char a = plainText.charAt(i * 2);
            char b = plainText.charAt(i * 2 + 1);
            int column1 = key.getFirstKey().indexOf(a) % 5;
            int row1 = key.getFirstKey().indexOf(a) / 5;
            int row2 = key.getSecondKey().indexOf(b) / 5;
            int column2 = key.getSecondKey().indexOf(b) % 5;
            cipherText.append(key.getFirstKey().charAt(5 * RandomUtil.pickRandomInt(5) + column1));
            cipherText.append(key.getThirdKey().charAt(5 * row1 + column2));
            cipherText.append(key.getSecondKey().charAt(5 * row2 + RandomUtil.pickRandomInt(5)));
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, TriKey<String, String, String> key) {
        return decodeEfficiently(cipherText, new char[cipherText.length() / 3 * 2], key);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, String, String> key) {
        for (int i = 0; i < cipherText.length() / 3; i++) {
            char a = cipherText.charAt(i * 3);
            char b = cipherText.charAt(i * 3 + 1);
            char c = cipherText.charAt(i * 3 + 2);

            int column = key.getFirstKey().indexOf(a) % 5;

            int row = key.getSecondKey().indexOf(c) / 5;

            int index = key.getThirdKey().indexOf(b);
            int columnSort = index % 5;
            int rowSort = index / 5;

            plainText[i * 2] = key.getFirstKey().charAt(rowSort * 5 + column);
            plainText[i * 2 + 1] = key.getSecondKey().charAt(row * 5 + columnSort);
        }

        return plainText;
    }

    @Override
    public boolean deterministic() {
        return false;
    }
}
