/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.GrilleKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.characters.CharArrayWrapper;

public class GrilleCipher extends UniKeyCipher<Integer[], GrilleKeyType.Builder>{

    public GrilleCipher() {
        super(GrilleKeyType.builder().setRange(2, 8));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, Integer[] key) {
        int blockSize = key.length * 4;

        if (plainText.length() % blockSize != 0) {
            StringBuilder builder = new StringBuilder(plainText.length() + blockSize - (plainText.length() % blockSize));
            builder.append(plainText);
            while (builder.length() % blockSize != 0) {
                builder.append('X');
            }

            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, Integer[] key) {
        int blockSize = key.length * 4;
        int[] keyMapping = createFullKey(key, true);

        char[] cipherText = new char[plainText.length()];

        for (int offset = 0; offset < plainText.length(); offset += blockSize) {
            decodeSection(plainText, cipherText, offset, blockSize, keyMapping);
        }

        return new CharArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, Integer[] key) {
        int blockSize = key.length * 4;
        int[] keyIndexed = createFullKey(key, false);

        for (int offset = 0; offset < cipherText.length(); offset += blockSize) {
            decodeSection(cipherText, plainText, offset, blockSize, keyIndexed);
        }

        return plainText;
    }

    private static void decodeSection(CharSequence inputText, char[] outputText, int offset, int blockSize, int[] fullKey) {
        for (int i = 0; i < blockSize; i++) {
            outputText[offset + fullKey[i]] = inputText.charAt(offset + i);
        }
    }

    public static int[] createFullKey(Integer[] key, final boolean encode) {
        final int blockSize = key.length * 4;
        final int middleIndex = key.length * 2; // Same as (size * size - 1) / 2
        final int size = (int) Math.sqrt(blockSize + (key.length + 1) % 2);
        final boolean odd = size % 2 == 1;

        int[] normal = new int[blockSize];
        // Init the Grille
        for (int i = 0; i < key.length; i++) {
            normal[i] = key[i];
        }

        // Rotate the Grille clockwise 3 more times and calculate
        // the position of the slots
        for (int rot = 1; rot < 4; rot++) {
            for (int i = 0; i < key.length; i++) {
                int value = normal[(rot - 1) * key.length + i];
                int row = value / size; // cast to int floors value
                int col = value % size;
                normal[rot * key.length + i] = col * size + (size-1 - row);
            }
        }

        int[] ordered = new int[blockSize];
        int gridPos = 0;
        // Orders each quadrant
        for (int rot = 0; rot < 4; rot++) {
            for (int i = 0; i < size * size; i++) {
                if (ArrayUtil.contains(normal, rot * key.length, (rot + 1) * key.length, i)) {
                    int orderPos = i;
                    // If the
                    if (odd && orderPos > middleIndex) {
                        orderPos--;
                    }

                    if (encode) {
                        ordered[gridPos]  = orderPos;
                    } else {
                        ordered[orderPos] = gridPos;
                    }
                    gridPos++;
                }
            }
        }

        return ordered;
    }
}
