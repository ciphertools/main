/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.interfaces.IKeyType.IKeyBuilder;
import tools.cipher.base.key.types.EnumKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.util.ReadMode;

public class ADFGXCipher extends TriKeyCipher<String, Integer[], ReadMode, SquareStringKeyType.Builder, OrderedIntegerKeyType.Builder, EnumKeyType.Builder<ReadMode>> {

    private final static ColumnarTranspositionCipher transpostion = new ColumnarTranspositionCipher();
    private final String colLabels;

    public ADFGXCipher(String alphabet, String colLabelsIn) {
        super(SquareStringKeyType.builder().setAlphabet(alphabet).setDim(colLabelsIn.length(), colLabelsIn.length()),
                OrderedIntegerKeyType.builder().setMin(1).setMax(Integer.MAX_VALUE),
                EnumKeyType.builder(ReadMode.class).setUniverse(ReadMode.DOWN));
        this.colLabels = colLabelsIn;
    }

    public ADFGXCipher() {
        this(Alphabet.ALL_25_CHARS, "ADFGX");
    }

    @Override
    public IKeyBuilder<Integer[]> limitDomainForSecondKey(OrderedIntegerKeyType.Builder secondKey) {
        return secondKey.setRange(2, 9);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, TriKey<String, Integer[], ReadMode> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public boolean isValid(TriKey<String, Integer[], ReadMode> key) {
        return Math.sqrt(key.getFirstKey().length()) == this.colLabels.length() && super.isValid(key);
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, Integer[], ReadMode> key) {
        int size = this.colLabels.length();

        StringBuilder cipherText = new StringBuilder(plainText.length() * 2);

        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);

            int charIndex = key.getFirstKey().indexOf(c);
            int row = (int) Math.floor((double) charIndex / size);
            int column = charIndex % size;

            cipherText.append(this.colLabels.charAt(row));
            cipherText.append(this.colLabels.charAt(column));
        }

        return transpostion.encode(cipherText, BiKey.of(key.getSecondKey(), key.getThirdKey()));
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, Integer[], ReadMode> key) {
        return ADFGXCipher.decodeTransformed(transpostion.decodeEfficiently(cipherText, plainText, BiKey.of(key.getSecondKey(), key.getThirdKey())), key.getFirstKey(), this.colLabels);
    }

    private static char[] decodeTransformed(char[] untransformedText, String keysquare, String adfgvx) {
        char[] plainText = new char[untransformedText.length / 2];

        for (int i = 0; i < untransformedText.length; i += 2) {
            char c1 = untransformedText[i];
            char c2 = untransformedText[i + 1];

            int row = adfgvx.indexOf(c1);
            int column = adfgvx.indexOf(c2);
            if (row != -1 && column != -1) {
                plainText[i / 2] = keysquare.charAt(row * adfgvx.length() + column);
            }
        }

        return plainText;
    }
}
