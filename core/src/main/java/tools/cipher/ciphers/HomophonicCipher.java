/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class HomophonicCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public HomophonicCipher() {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setRange(4, 4));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, String key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        StringBuilder cipherText = new StringBuilder();

        List<char[]> rows = new ArrayList<char[]>();

        for (int i = 0; i < 4; i++) {
            int charIndex = key.charAt(i) - 'A';

            if (key.charAt(i) >= 'J')
                charIndex--;

            for (int no = 0; no < 25; no++) {
                int num = (i * 25 + (no + 25 - charIndex) % 25 + 1) % 100;
                int digit = num % 10;
                int tens = (num - digit) / 10;
                rows.add(new char[] { (char) (tens + '0'), (char) (digit + '0') });
            }
        }

        for (int i = 0; i < plainText.length(); i++) {
            int charIndex = plainText.charAt(i) - 'A';
            if (charIndex >= 9)
                charIndex--;

            cipherText.append(rows.get(RandomUtil.pickRandomInt(4) * 25 + charIndex));
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, String key) {
        return decodeEfficiently(cipherText, new char[cipherText.length() / 2], key);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {

        int[] rows = new int[100];
        String shortAlpha = "ABCDEFGHIKLMNOPQRSTUVWXYZ";

        for (int i = 0; i < 4; i++) {
            int c = key.charAt(i) - 'A';

            if (key.charAt(i) >= 'J')
                c--;

            for (int no = 0; no < 25; no++) {

                rows[(i * 25 + MathUtil.mod(no - c, 25) + 1) % 100] = no;
            }
        }

        for (int i = 0; i < plainText.length; i++) {
            int col = rows[10 * (cipherText.charAt(i * 2) - '0') + (cipherText.charAt(i * 2 + 1) - '0')];
            plainText[i] = shortAlpha.charAt(col);
        }

        return plainText;
    }

    @Override
    public boolean deterministic() {
        return false;
    }
}
