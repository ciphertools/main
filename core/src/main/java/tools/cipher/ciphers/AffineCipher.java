/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.math.BigInteger;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.key.types.IntegerGenKeyType;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.lib.CipherUtils;
import com.alexbarter.lib.util.MathUtil;

public class AffineCipher extends BiKeyCipher<Integer, Integer, IntegerGenKeyType.Builder, IntegerKeyType.Builder> {

    public AffineCipher() {
        super(IntegerGenKeyType.builder().setRange(0, 25).addFilter(MathUtil::hasInverseMod26), IntegerKeyType.builder().setRange(0, 25));
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<Integer, Integer> key) {
        StringBuilder cipherText = new StringBuilder(plainText.length());

        StringBuilder tempAlphabet = new StringBuilder(26);
        for (int i = 0; i < 26; ++i) {
            tempAlphabet.append((char) ('A' + (key.getFirstKey() * i + key.getSecondKey()) % 26));
        }

        for (int i = 0; i < plainText.length(); i++) {
            byte ch = CipherUtils.getAlphaIndex(plainText.charAt(i));

            if (ch == -1) {
                // if(format)
                // cipherText.charAt(i++] = ch;
            } else {
                char newLetter = (char) (tempAlphabet.charAt(plainText.charAt(i) - 'A'));
                cipherText.append(newLetter);
            }
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<Integer, Integer> key) {

        int multiplicativeInverse = BigInteger.valueOf((int) key.getFirstKey()).modInverse(BigInteger.valueOf(26)).intValue();

        // Runs through all the characters from the array
        for (int i = 0; i < cipherText.length(); i++)
            plainText[i] = (char) (MathUtil.mod(multiplicativeInverse * (cipherText.charAt(i) - 'A' - key.getSecondKey()), 26) + 'A');

        return plainText;
    }
}
