/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.util.VigenereType;

public class AutokeyCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    private VigenereType type;

    public AutokeyCipher(VigenereType type) {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(1, Integer.MAX_VALUE));
        this.type = type;
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 15);
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        Character[] cipherText = new Character[plainText.length()];
        int period = key.length();

        for (int index = 0; index < plainText.length(); index++) {
            char charIdKey;

            if (index < period)
                charIdKey = key.charAt(index);
            else
                charIdKey = plainText.charAt(index - period);

            cipherText[index] = this.type.encode(plainText.charAt(index), charIdKey);
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        int period = key.length();

        for (int index = 0; index < cipherText.length(); index++) {
            char charIdKey;

            // Determines key to use; if at start use normal key, else use decrypted
            // plaintext
            if (index < period)
                charIdKey = key.charAt(index);
            else
                charIdKey = plainText[index - period];

            plainText[index] = this.type.decode(cipherText.charAt(index), charIdKey);
        }

        return plainText;
    }
}
