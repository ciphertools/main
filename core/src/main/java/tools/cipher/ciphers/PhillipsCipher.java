/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Map;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.key.types.BooleanKeyType;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.constants.Alphabet;

public class PhillipsCipher extends TriKeyCipher<String, Boolean, Boolean, SquareStringKeyType.Builder, BooleanKeyType.Builder, BooleanKeyType.Builder> {

    public static int[][] rows = new int[][] { { 0, 1, 2, 3, 4 }, { 1, 0, 2, 3, 4 }, { 1, 2, 0, 3, 4 }, { 1, 2, 3, 0, 4 }, { 1, 2, 3, 4, 0 }, { 2, 1, 3, 4, 0 }, { 2, 3, 1, 4, 0 }, { 2, 3, 4, 1, 0 } };
    public static int[][] rowsIndex = new int[][] { { 0, 1, 2, 3, 4 }, { 1, 0, 2, 3, 4 }, { 2, 0, 1, 3, 4 }, { 3, 0, 1, 2, 4 }, { 4, 0, 1, 2, 3 }, { 4, 1, 0, 2, 3 }, { 4, 2, 0, 1, 3 }, { 4, 3, 0, 1, 2 } };

    public PhillipsCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5),
                BooleanKeyType.builder(), BooleanKeyType.builder()); // TODO
                                                                                                                                                       // one
                                                                                                                                                       // of
                                                                                                                                                       // the
                                                                                                                                                       // booleans
                                                                                                                                                       // must
                                                                                                                                                       // be
                                                                                                                                                       // true
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, TriKey<String, Boolean, Boolean> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, Boolean, Boolean> key) {
        StringBuilder cipherText = new StringBuilder(plainText.length());

        for (int i = 0; i < plainText.length(); i++) {
            int squareIndex = ((int) (i / 5) % rows.length);

            int[] order = rows[squareIndex];
            int[] orderIndex = rowsIndex[squareIndex];

            char ch = plainText.charAt(i);

            int index = key.getFirstKey().indexOf(ch);

            int row = index / 5;
            int column = index % 5;

            int newRow;
            int newColumn;

            if (key.getSecondKey())
                newRow = order[(orderIndex[row] + 1) % 5];
            else
                newRow = (row + 1) % 5;

            if (key.getThirdKey())
                newColumn = order[(orderIndex[column] + 1) % 5];
            else
                newColumn = (column + 1) % 5;

            cipherText.append(key.getFirstKey().charAt(newRow * 5 + newColumn));
        }

        return new String(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, Boolean, Boolean> key) {

        Map<Character, Integer> keyIndex = CipherUtils.createCharacterIndexMapping(key.getFirstKey());

        for (int i = 0; i < cipherText.length(); i++) {
            int squareIndex = ((int) (i / 5) % rows.length);

            int[] order = rows[squareIndex];
            int[] orderIndex = rowsIndex[squareIndex];

            int index = keyIndex.get((char) cipherText.charAt(i));

            int row = index / 5;
            int column = index % 5;

            int newRow;
            int newColumn;

            if (key.getSecondKey())
                newRow = order[(orderIndex[row] + 4) % 5];
            else
                newRow = (row + 4) % 5;

            if (key.getThirdKey())
                newColumn = order[(orderIndex[column] + 4) % 5];
            else
                newColumn = (column + 4) % 5;

            plainText[i] = key.getFirstKey().charAt(newRow * 5 + newColumn);
        }

        return plainText;
    }
}
