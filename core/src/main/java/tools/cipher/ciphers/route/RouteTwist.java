/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers.route;

import com.alexbarter.lib.util.MathUtil;

public class RouteTwist extends RouteCipherType {

    public int twistSize;

    public RouteTwist(int twistSize) {
        super("Twist across step %d.", twistSize);
        this.twistSize = twistSize;
    }

    @Override
    public int[] createPattern(int width, int height, int totalSize) {
        int[] grid = new int[totalSize];
        int index = 0;

        for (int h = 0; h < height; h++) {
            int cut = MathUtil.mod(width - h * this.twistSize, width);
            for (int w = 0; w < width; w++)
                grid[index++] = h * width + (cut + w) % width;
        }

        return grid;
    }

}
