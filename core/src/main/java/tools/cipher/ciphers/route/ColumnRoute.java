/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers.route;

public class ColumnRoute {

    public static class TopLeft extends RouteCipherType {
        public TopLeft() {
            super("Columns starting top left.");
        }

        @Override
        public int[] createPattern(int width, int height, int totalSize) {
            int[] grid = new int[totalSize];
            int index = 0;
            for (int c = 0; c < width; c++)
                for (int r = 0; r < height; r++)
                    grid[index++] = r * width + c;

            return grid;
        }
    }

    public static class TopRight extends RouteCipherType {
        public TopRight() {
            super("Columns starting top right.");
        }

        @Override
        public int[] createPattern(int width, int height, int totalSize) {
            int[] grid = new int[totalSize];
            int index = 0;
            for (int c = width - 1; c >= 0; c--)
                for (int r = 0; r < height; r++)
                    grid[index++] = r * width + c;

            return grid;
        }
    }

    public static class BottomRight extends RouteCipherType {
        public BottomRight() {
            super("Columns starting bottom right.");
        }

        @Override
        public int[] createPattern(int width, int height, int totalSize) {
            int[] grid = new int[totalSize];
            int index = 0;
            for (int c = width - 1; c >= 0; c--)
                for (int r = height - 1; r >= 0; r--)
                    grid[index++] = r * width + c;

            return grid;
        }
    }

    public static class BottomLeft extends RouteCipherType {
        public BottomLeft() {
            super("Columns starting bottom left.");
        }

        @Override
        public int[] createPattern(int width, int height, int totalSize) {
            int[] grid = new int[totalSize];
            int index = 0;
            for (int c = 0; c < width; c++)
                for (int r = height - 1; r >= 0; r--)
                    grid[index++] = r * width + c;

            return grid;
        }
    }
}
