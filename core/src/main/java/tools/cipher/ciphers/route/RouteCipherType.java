/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers.route;

import java.util.HashMap;

import com.alexbarter.lib.Pair;

public abstract class RouteCipherType {

    private String description;
    public HashMap<Pair<Integer, Integer>, int[]> cache = new HashMap<>();

    public RouteCipherType() {
        this("No description");
    }

    public RouteCipherType(String description) {
        this(description, true);
    }

    public RouteCipherType(String description, Object... obj) {
        this(String.format(description, obj));
    }

    public RouteCipherType(String description, boolean add) {
        this.description = description;
        if (add)
            Routes.ROUTES.add(this);
    }

    public String getDescription() {
        return this.description;
    }

    public boolean canCache() {
        return true;
    }

    public final int[] getPattern(int width, int height, int totalSize) {
        if (!this.canCache())
            return this.createPattern(width, height, totalSize);

        Pair<Integer, Integer> key = new Pair<>(width, height);
        if (this.cache.containsKey(key))
            return this.cache.get(key);

        int[] grid = this.createPattern(width, height, totalSize);
        this.cache.put(key, grid);

        return grid;
    }

    public abstract int[] createPattern(int width, int height, int totalSize);
}
