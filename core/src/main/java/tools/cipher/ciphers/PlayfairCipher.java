/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Map;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class PlayfairCipher extends UniKeyCipher<String, SquareStringKeyType.Builder> {

    private int size;

    public PlayfairCipher() {
        this(Alphabet.ALL_25_CHARS);
    }

    public PlayfairCipher(CharSequence alphabet) {
        this(alphabet, (int) Math.sqrt(alphabet.length()));
    }

    private PlayfairCipher(CharSequence alphabet, int size) {
        super(SquareStringKeyType.builder().setAlphabet(alphabet).setDim(size, size));
        this.size = size;
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, String key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        int i = 0;
        for (; plainText.length() - i >= 2; i += 2) {
            char a = plainText.charAt(i);
            char b = plainText.charAt(i + 1);
            if (a == 'J')
                a = 'I';
            if (b == 'J')
                b = 'I';

            builder.append(a);

            if (a == b) {
                builder.append(a == 'X' ? 'Q' : 'X');
                i--;
            } else {
                builder.append(b);
            }
        }

        if (i < plainText.length()) {
            char c = plainText.charAt(plainText.length() - 1);
            if (c == 'J')
                c = 'I';
            builder.append(c);
        }

        if (builder.length() % 2 == 1) {
            int f = builder.charAt(builder.length() - 1);
            builder.append(f == 'X' ? 'Q' : 'X');
        }

        return builder.toString();
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        Character[] cipherText = new Character[plainText.length()];

        for (int i = 0; i < plainText.length(); i += 2) {
            char a = plainText.charAt(i);
            char b = plainText.charAt(i + 1);
            int i1 = key.indexOf(a);
            int i2 = key.indexOf(b);
            int row1 = (int) Math.floor(i1 / size);
            int col1 = i1 % size;
            int row2 = (int) Math.floor(i2 / size);
            int col2 = i2 % size;

            char c, d;

            if (row1 == row2) {
                c = key.charAt(row1 * size + (col1 + 1) % size);
                d = key.charAt(row2 * size + (col2 + 1) % size);
            } else if (col1 == col2) {
                c = key.charAt((row1 + 1) % size * size + col1);
                d = key.charAt((row2 + 1) % size * size + col2);
            } else {
                c = key.charAt(row1 * size + col2);
                d = key.charAt(row2 * size + col1);
            }

            cipherText[i] = c;
            cipherText[i + 1] = d;
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        Map<Character, Integer> keyIndex = CipherUtils.createCharacterIndexMapping(key);

        for (int i = 0; i < cipherText.length(); i += 2) {
            int i1 = keyIndex.get(cipherText.charAt(i));
            int i2 = keyIndex.get(cipherText.charAt(i + 1));
            int row1 = i1 / size;
            int col1 = i1 % size;
            int row2 = i2 / size;
            int col2 = i2 % size;

            if (row1 == row2) {
                plainText[i] = key.charAt(row1 * size + (col1 + size - 1) % size);
                plainText[i + 1] = key.charAt(row2 * size + (col2 + size - 1) % size);
            } else if (col1 == col2) {
                plainText[i] = key.charAt(((row1 + size - 1) % size) * size + col1);
                plainText[i + 1] = key.charAt(((row2 + size - 1) % size) * size + col2);
            } else {
                plainText[i] = key.charAt(row1 * size + col2);
                plainText[i + 1] = key.charAt(row2 * size + col1);
            }
        }

        return plainText;
    }
}
