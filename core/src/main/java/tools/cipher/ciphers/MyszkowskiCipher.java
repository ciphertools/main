/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class MyszkowskiCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public MyszkowskiCipher() {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE).setRepeats());
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 15);
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        StringBuilder cipherText = new StringBuilder(plainText.length());

        int rows = (int) Math.ceil(plainText.length() / (double) key.length());

        for (char c = 'A'; c <= 'Z'; c++) {
            if (key.indexOf(c) == -1)
                continue;

            for (int row = 0; row < rows; row++) {
                for (int i = 0; i < key.length(); i++) {
                    if (c == key.charAt(i)) {
                        if (row * key.length() + i < plainText.length()) {
                            cipherText.append(plainText.charAt(row * key.length() + i));
                        }
                    }
                }
            }
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {

        int rows = (int) Math.ceil(cipherText.length() / (double) key.length());

        int index = 0;
        for (char c = 'A'; c <= 'Z'; c++) {
            if (key.indexOf(c) == -1)
                continue;

            for (int row = 0; row < rows; row++) {
                for (int i = 0; i < key.length(); i++) {
                    if (c == key.charAt(i)) {
                        if (row * key.length() + i < cipherText.length()) {
                            plainText[row * key.length() + i] = cipherText.charAt(index++);
                        }
                    }
                }
            }
        }

        return plainText;
    }

}
