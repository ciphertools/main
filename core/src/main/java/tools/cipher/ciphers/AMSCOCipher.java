/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.interfaces.IKeyType.IKeyBuilder;
import tools.cipher.base.key.types.EnumKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.util.AMSCOType;

public class AMSCOCipher extends BiKeyCipher<Integer[], AMSCOType, OrderedIntegerKeyType.Builder, EnumKeyType.Builder<AMSCOType>> {

    public AMSCOCipher() {
        super(OrderedIntegerKeyType.builder().setRange(1, Integer.MAX_VALUE), EnumKeyType.builder(AMSCOType.class).setUniverse(AMSCOType.values()));
    }

    @Override
    public IKeyBuilder<Integer[]> limitDomainForFirstKey(OrderedIntegerKeyType.Builder firstKey) {
        return firstKey.setRange(2, 9);
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<Integer[], AMSCOType> key) {
        StringBuilder[] columns = ArrayUtil.fill(new StringBuilder[key.getFirstKey().length], () -> new StringBuilder(plainText.length() / key.getFirstKey().length));

        int index = 0;
        int row = 0;
        while (index < plainText.length()) {
            for (int col = 0; col < key.getFirstKey().length; col++) {
                if (index >= plainText.length())
                    break;

                int fromIndex = index;
                // How many characters are in block
                index += ((col + row) % 2 == key.getSecondKey().getMod()) ? 2 : 1;

                for (int i = fromIndex; i < Math.min(index, plainText.length()); i++) {
                    columns[key.getFirstKey()[col]].append(plainText.charAt(i));
                }
            }
            row++;
        }

        StringBuilder read = new StringBuilder(plainText.length());
        for (int i = 0; i < key.getFirstKey().length; i++)
            read.append(columns[i]);

        return read;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<Integer[], AMSCOType> key) {
        Integer[] order = key.getFirstKey();
        int period = order.length;

        int[] reversedOrder = new int[order.length];
        for (int i = 0; i < order.length; i++)
            reversedOrder[order[i]] = i;

        int noChar1st = (int) ((period + 1) / 2) * 2 + (int) (period / 2);
        int noChar2nd = (int) ((period + 1) / 2) + (int) (period / 2) * 2;

        int rows = 0;
        int charactersLastRow = 0;

        boolean choose = key.getSecondKey() == AMSCOType.DOUBLE_FIRST;
        int chars = 0;

        do {
            charactersLastRow = chars;
            chars += choose ? noChar1st : noChar2nd;
            rows += 1;
            choose = !choose;
        } while (chars < cipherText.length());

        charactersLastRow = plainText.length - charactersLastRow;

        int noCharColumn1st = (int) ((rows - 1) / 2) + (int) (rows / 2) * 2;
        int noCharColumn2nd = (int) ((rows - 1) / 2) * 2 + (int) (rows / 2);

        int index = 0;

        CharSequence[] grid = new CharSequence[period];

        for (int column = 0; column < period; column++) {
            int realColumn = reversedOrder[column];
            boolean isDoubleLetter = ((realColumn + rows) % 2 == key.getSecondKey().getMod()); // Double letter first

            int length = isDoubleLetter ? noCharColumn1st : noCharColumn2nd;
            int noCharN1st = (int) ((realColumn + 1) / 2) * 2 + (int) (realColumn / 2);
            int noCharN2nd = (int) ((realColumn + 1) / 2) + (int) (realColumn / 2) * 2;

            int left = charactersLastRow - (isDoubleLetter ? noCharN1st : noCharN2nd);
            if (left > 0)
                length += isDoubleLetter ? 1 : Math.min(2, left);

            grid[realColumn] = cipherText.subSequence(index, index + length);
            index += length;
        }

        int[] indexTracker = new int[period];

        int textIndex = 0;
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < period; column++) {
                int number = ((column + row) % 2 == key.getSecondKey().getMod()) ? 2 : 1;

                for (int i = 0; i < number; i++) {
                    if (indexTracker[column] + i >= grid[column].length())
                        break;
                    plainText[textIndex] = grid[column].charAt(indexTracker[column] + i);
                    textIndex++;
                }

                indexTracker[column] += number;
            }
        }

        return plainText;
    }
}
