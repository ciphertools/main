/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Arrays;
import java.util.List;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.lib.MorseCode;

public class MorbitCipher extends UniKeyCipher<Integer[], OrderedIntegerKeyType.Builder> {

    public MorbitCipher() {
        super(OrderedIntegerKeyType.builder().setRange(9, 9));
    }

    @Override
    public CharSequence encode(CharSequence plainText, Integer[] key) {

        String cipherText = "";
        String morseText = "";

        morseText = MorseCode.getMorseEquivalent(plainText);
        if (morseText.length() % 2 != 0)
            morseText += "X";

        List<Character> list = Arrays.asList('.', '-', 'X');
        for (int i = 0; i < morseText.length(); i += 2) {
            int a = list.indexOf(morseText.charAt(i));
            int b = list.indexOf(morseText.charAt(i + 1));
            cipherText += key[a * 3 + b] + 1;
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, Integer[] key) {
        int[] reversedOrder = new int[key.length];
        for (int i = 0; i < key.length; i++)
            reversedOrder[key[i]] = i;

        StringBuilder plainText = new StringBuilder();
        char[] morseText = new char[cipherText.length() * 2];
        char[] list = new char[] { '.', '-', 'X' };

        for (int i = 0; i < cipherText.length(); i++) {
            int a = cipherText.charAt(i) - '0' - 1;

            int index = reversedOrder[a];
            int first = index / 3;
            int second = index % 3;
            morseText[i * 2] = list[first];
            morseText[i * 2 + 1] = list[second];
        }

        int last = 0;
        for (int i = 0; i < morseText.length; i++) {
            char a = morseText[i];
            boolean isX = a == 'X';
            boolean end = i == morseText.length - 1;
            if (isX || end) {
                String code = new String(morseText, last, i - last + (end && !isX ? 1 : 0));

                last = i + 1;
                Character morseChar = MorseCode.getCharFromMorse(code);
                if (morseChar != null) {
                    plainText.append((char) morseChar); // Cast to char is more efficient
                } else {
                    plainText.append(morseText, last, i - last + (end && !isX ? 1 : 0));
                }
            }

        }

        return plainText.toString().toCharArray();
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, char[] unused, Integer[] key) {
        int[] reversedOrder = new int[key.length];
        for (int i = 0; i < key.length; i++)
            reversedOrder[key[i]] = i;

        StringBuilder plainText = new StringBuilder();
        char[] morseText = new char[cipherText.length() * 2];
        char[] list = new char[] { '.', '-', 'X' };

        for (int i = 0; i < cipherText.length(); i++) {
            int a = cipherText.charAt(i) - '0' - 1;

            int index = reversedOrder[a];
            int first = index / 3;
            int second = index % 3;
            morseText[i * 2] = list[first];
            morseText[i * 2 + 1] = list[second];
        }

        int last = 0;
        for (int i = 0; i < morseText.length; i++) {
            char a = morseText[i];
            boolean isX = a == 'X';
            boolean end = i == morseText.length - 1;
            if (isX || end) {
                String code = new String(morseText, last, i - last + (end && !isX ? 1 : 0));

                last = i + 1;
                Character morseChar = MorseCode.getCharFromMorse(code);
                if (morseChar != null) {
                    plainText.append((char) morseChar); // Cast to char is more efficient
                } else {
                    plainText.append(morseText, last, i - last + (end && !isX ? 1 : 0));
                }
            }

        }

        return plainText.toString().toCharArray();
    }

}
