/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.SquareMatrixKeyType;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.matrix.Matrix;

public class HillCipher extends UniKeyCipher<Matrix, SquareMatrixKeyType.Builder> {

    public HillCipher() {
        super(SquareMatrixKeyType.builder());
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, Matrix key) {
        int blockSize = key.size();

        if (plainText.length() % blockSize != 0) {
            StringBuilder builder = new StringBuilder(plainText.length() + blockSize - (plainText.length() % blockSize));
            builder.append(plainText);
            while (builder.length() % blockSize != 0) {
                builder.append('X');
            }

            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, Matrix key) {
        char[] cipherText = new char[plainText.length()];
        int size = key.size();

        for(int i = 0; i < plainText.length(); i += size) {

            Integer[] let = new Integer[size];
            for(int j = 0; j < size; j++) {
                let[j] = ((char)plainText.charAt(i + j) - 'A');
            }

            Matrix plainMatrix = new Matrix(let, size, 1);
            Matrix cipherMatrix = key.multiply(plainMatrix).modular(26);

            for(int j = 0; j < size; j++) {
                cipherText[i + j] = (char)(cipherMatrix.data[j] + 'A');
            }

        }

        return new CharArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, Matrix key) {
        return decodeUsingInverse(cipherText, plainText, key.inverseMod(26));
    }

    public char[] decodeUsingInverse(CharSequence cipherText, char[] plainText, Matrix inverseMatrix)  {
        int size = inverseMatrix.size();
        for(int i = 0; i < cipherText.length(); i += size) {

            Integer[] let = new Integer[size];
            for(int j = 0; j < size; j++)
                let[j] = ((int)cipherText.charAt(i + j) - 'A');

            Matrix cipherMatrix = new Matrix(let, size, 1);
            Matrix plainMatrix = inverseMatrix.multiply(cipherMatrix).modular(26);

            for(int j = 0; j < size; j++) {
                plainText[i + j] = (char)(plainMatrix.data[j] + 'A');
            }

        }

        return plainText;
    }
}
