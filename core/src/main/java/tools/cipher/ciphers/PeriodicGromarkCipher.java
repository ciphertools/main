/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.characters.StringUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class PeriodicGromarkCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public PeriodicGromarkCipher() {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 8);
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {

        /**
         * char[] transBlock = new char[26]; int k = 0; for(; k < key.length(); k++) {
         * transBlock[k] = key.charAt(k); }
         *
         * for(char ch = 'A'; ch <= 'Z'; ++ch) { if(!key.contains(String.valueOf(ch))) {
         * transBlock[k++] = ch; } }
         *
         * System.out.println(Arrays.toString(transBlock));
         **/

        int[] inOrd = new int[key.length()];
        int[] noOrd = new int[key.length()];

        int p = 0;
        for (char ch = 'A'; ch <= 'Z'; ++ch) {
            int keyindex = key.indexOf(ch);
            if (keyindex != -1) {
                inOrd[p++] = keyindex;
                noOrd[keyindex] = p;
            } else
                key += ch;
        }

        String transposedKey = "";
        int[] numericKey = new int[plainText.length()];

        for (int i = 0; i < inOrd.length; i++) {
            transposedKey += StringUtils.getEveryNthChar(key, inOrd[i], inOrd.length);
            numericKey[i] = noOrd[i];
        }

        for (int i = 0; i < numericKey.length - noOrd.length; i++)
            numericKey[i + noOrd.length] = (numericKey[i] + numericKey[i + 1]) % 10;

        StringBuilder cipherText = new StringBuilder(plainText.length());

        for (int i = 0; i < plainText.length(); i++) {
            int keyIndex = (int) (Math.floor((double) i / inOrd.length) % inOrd.length);
            cipherText.append(transposedKey.charAt((transposedKey.indexOf(key.charAt(keyIndex)) + (plainText.charAt(i) - 'A') + numericKey[i]) % 26));
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        int[] inOrd = new int[key.length()];
        int[] noOrd = new int[key.length()];

        char[] keyFull = new char[26];
        int index = 0;
        for (; index < key.length(); index++)
            keyFull[index] = key.charAt(index);

        int p = 0;
        for (char ch = 'A'; ch <= 'Z'; ++ch) {
            int keyindex = key.indexOf(ch);
            if (keyindex != -1) {
                inOrd[p++] = keyindex;
                noOrd[keyindex] = p;
            } else
                keyFull[index++] = ch;
        }

        int[] transposedKeyIndexOf = new int[26];
        int[] numericKey = new int[cipherText.length()];
        int rows = (int) Math.ceil(26D / key.length());
        index = 0;

        for (int i = 0; i < inOrd.length; i++) {
            for (int r = 0; r < rows; r++) {
                if (r * inOrd.length + inOrd[i] >= 26)
                    break;
                transposedKeyIndexOf[keyFull[r * inOrd.length + inOrd[i]] - 'A'] = index++;
            }

            numericKey[i] = noOrd[i];
        }

        for (int i = 0; i < numericKey.length - noOrd.length; i++)
            numericKey[i + noOrd.length] = (numericKey[i] + numericKey[i + 1]) % 10;

        for (int i = 0; i < cipherText.length(); i++) {
            int keyIndex = (int) (Math.floor(i / inOrd.length) % inOrd.length);
            plainText[i] = (char) (MathUtil.mod(transposedKeyIndexOf[cipherText.charAt(i) - 'A'] - transposedKeyIndexOf[key.charAt(keyIndex) - 'A'] - numericKey[i], 26) + 'A');
        }

        return plainText;
    }

}
