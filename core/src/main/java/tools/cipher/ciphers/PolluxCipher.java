/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.PolluxKeyType;
import tools.cipher.lib.MorseCode;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.characters.CharacterArrayWrapper;

public class PolluxCipher extends UniKeyCipher<Character[], PolluxKeyType.Builder> {

    public PolluxCipher() {
        super(PolluxKeyType.builder());
    }

    private static char[] DIGIT_STR = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

    @Override
    public CharSequence encode(CharSequence plainText, Character[] key) {

        String morseText = MorseCode.getMorseEquivalent(plainText);

        morseText = MorseCode.getMorseEquivalent(plainText);

        Character[] cipherText = new Character[morseText.length()];

        for (int i = 0; i < morseText.length(); i++) {
            char a = morseText.charAt(i);
            int no = 0; // Between 0-9 inclusive
            do {
                no = RandomUtil.pickRandomInt(key.length);
            } while (key[no] != a);
            cipherText[i] = DIGIT_STR[no];
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, Character[] key) {
        return decodeEfficiently(cipherText, null, key);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] _unused_, Character[] key) {
        char[] morseText = new char[cipherText.length()];

        for (int i = 0; i < cipherText.length(); i++) {
            morseText[i] = key[cipherText.charAt(i) - '0'];
        }

        StringBuilder builder = new StringBuilder(cipherText.length() / 4);

        int last = 0;
        for (int i = 0; i < morseText.length; i++) {
            char a = morseText[i];
            boolean end = i == morseText.length - 1;
            if (a == 'X' || end) {
                final int len = i - last + (end ? 1 : 0);
                Character morseChar = MorseCode.getCharFromMorse(morseText, last, len);
                if (morseChar != null) {
                    builder.append(morseChar.charValue()); // Cast to char is more efficient
                } else {
                    builder.append(morseText, last, len);
                }
                last = i + 1;
            }
        }

        char[] plainText = new char[builder.length()];
        builder.getChars(0, builder.length(), plainText, 0);
        return plainText;
    }

    @Override
    public boolean deterministic() {
        return false;
    }
}
