/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.SwagmanKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;

public class SwagmanCipher extends UniKeyCipher<int[], SwagmanKeyType.Builder> {

    public SwagmanCipher() {
        super(SwagmanKeyType.builder().setRange(2, Integer.MAX_VALUE));
    }

    @Override
    public SwagmanKeyType.Builder limitDomainForFirstKey(SwagmanKeyType.Builder secondKey) {
        return secondKey.setRange(2, 5);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, int[] key) {
        int size = (int) Math.sqrt(key.length);
        StringBuilder builder = new StringBuilder(plainText.length() + (size - (plainText.length() % size)) % size);
        builder.append(plainText);
        while (builder.length() % size != 0) {
            builder.append('X');
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, int[] key) {
        int size = (int) Math.sqrt(key.length);

        char[] tempText = new char[plainText.length()];
        Character[] cipherText = new Character[plainText.length()];

        int squareMag = key.length;
        int rowLength = plainText.length() / size;
        int noSquares = (int) Math.ceil(plainText.length() / (double) squareMag);

        int[] colInSquare = new int[noSquares];
        for (int i = 0; i < noSquares; i++)
            colInSquare[i] = size;
        if (plainText.length() % squareMag != 0)
            colInSquare[noSquares - 1] = (plainText.length() % squareMag) / size;

        for (int s = 0; s < noSquares; s++)
            for (int r = 0; r < size; r++)
                for (int c = 0; c < colInSquare[s]; c++)
                    tempText[s * squareMag + c + colInSquare[s] * key[r * size + c % size]] = plainText.charAt(s * size + r * rowLength + c);

        for (int s = 0; s < noSquares; s++)
            for (int r = 0; r < size; r++)
                for (int c = 0; c < colInSquare[s]; c++)
                    cipherText[s * squareMag + c * size + r] = tempText[s * squareMag + c + colInSquare[s] * r];

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, int[] key) {
        int size = (int) Math.sqrt(key.length);

        int[] inKey = new int[key.length];
        for (int c = 0; c < size; c++)
            for (int r = 0; r < size; r++)
                inKey[key[r * size + c] * size + c] = r;

        char[] tempText = new char[cipherText.length()];

        int squareMag = (int) Math.pow(size, 2);
        int noSquares = (int) Math.ceil(cipherText.length() / (double) squareMag);

        int[] colInSquare = new int[noSquares];
        for (int i = 0; i < noSquares; i++)
            colInSquare[i] = size;
        if (cipherText.length() % squareMag != 0)
            colInSquare[noSquares - 1] = (cipherText.length() % squareMag) / size;

        for (int s = 0; s < noSquares; s++)
            for (int r = 0; r < size; r++)
                for (int c = 0; c < colInSquare[s]; c++)
                    tempText[s * squareMag + c + colInSquare[s] * inKey[r * size + c % size]] = cipherText.charAt(s * squareMag + c * size + r);

        int i = 0;

        for (int r = 0; r < size; r++)
            for (int s = 0; s < noSquares; s++)
                for (int c = 0; c < colInSquare[s]; c++)
                    plainText[i++] = tempText[s * squareMag + c + r * colInSquare[s]];

        return plainText;
    }

}
