/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.FullStringKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class RagbabyCipher extends UniKeyCipher<String, FullStringKeyType.Builder> {

    public RagbabyCipher() {
        super(FullStringKeyType.builder().setAlphabet(Alphabet.ALL_24_CHARS));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, String key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : (c == 'X' ? 'W' : c));
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        Character[] cipherText = new Character[plainText.length()];

        int word = 2;
        int number = 1;
        for (int i = 0; i < plainText.length(); i++) {
            char character = plainText.charAt(i);

            if (character == ' ') {
                cipherText[i] = ' ';
                number = word++;
            } else {
                cipherText[i] = key.charAt((key.indexOf(character) + number++) % key.length());
            }
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        int word = 1;
        int number = word;
        for (int i = 0; i < cipherText.length(); i++) {
            char character = cipherText.charAt(i);

            if (character == ' ') {
                plainText[i] = ' ';
                word++;
                number = word;
            } else {
                plainText[i] = key.charAt(((key.indexOf(character) - number++) % key.length() + key.length()) % key.length());
            }
        }

        return plainText;
    }
}
