/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.lib.CipherUtils;

public class CaesarCipher extends UniKeyCipher<Integer, IntegerKeyType.Builder> {

    public CaesarCipher() {
        super(IntegerKeyType.builder().setRange(1, 25));
    }

    @Override
    public CharSequence encode(CharSequence plainText, Integer key) {
        StringBuilder cipherText = new StringBuilder(plainText.length());
        int i;
        for (i = 0; i < plainText.length(); i++) {
            byte ch = CipherUtils.getAlphaIndex(plainText.charAt(i));
            if (ch == -1) {
                // if(format)
                // cipherText.charAt(i++] = ch;
            } else {
                char newLetter = (char) (((ch + key) % 26) + 'A');
                cipherText.append(newLetter);
            }
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, Integer key) {
        for (int i = 0; i < cipherText.length(); i++) {
            byte ch = CipherUtils.getAlphaIndex(cipherText.charAt(i));
            if (ch == -1)
                plainText[i] = cipherText.charAt(i);
            else {
                plainText[i] = (char) (((ch + 26 - key) % 26) + 'A');
            }
        }

        return plainText;
    }

}
