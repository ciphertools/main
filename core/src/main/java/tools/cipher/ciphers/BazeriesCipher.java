/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.lib.NumberString;

public class BazeriesCipher extends UniKeyCipher<Integer, IntegerKeyType.Builder> {

    public BazeriesCipher() {
        super(IntegerKeyType.builder().setRange(0, Integer.MAX_VALUE));
    }

    @Override
    public IntegerKeyType.Builder limitDomainForFirstKey(IntegerKeyType.Builder firstKey) {
        return firstKey.setRange(0, 1000000);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, Integer key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, Integer key) {
        String alphabetSquare = "AFLQVBGMRWCHNSXDIOTYEKPUZ";

        String numberSquare = "";
        for (char j : NumberString.convert(key).toCharArray())
            if (numberSquare.indexOf(j) == -1)
                numberSquare += j;

        for (char j : "ABCDEFGHIKLMNOPQRSTUVWXYZ".toCharArray())
            if (numberSquare.indexOf(j) == -1)
                numberSquare += j;

        String s = "" + key;
        String cipherText = "";

        int textPos = 0;
        int count = 0;
        int split = s.charAt(0) - '0';
        while (true) {
            for (int j = textPos + split - 1; j >= textPos; --j) {
                if (j < plainText.length()) {
                    char c = plainText.charAt(j);
                    cipherText += numberSquare.charAt(alphabetSquare.indexOf(c));
                }
            }
            if (textPos + split >= plainText.length())
                break;

            textPos += split;
            count += 1;
            split = s.charAt(count % s.length()) - '0';
        }
        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] unused, Integer key) {

        String alphabetSquare = "AFLQVBGMRWCHNSXDIOTYEKPUZ";

        String numberSquare = "";
        for (char j : NumberString.convert(key).toCharArray())
            if (numberSquare.indexOf(j) == -1)
                numberSquare += j;

        for (char j : "ABCDEFGHIKLMNOPQRSTUVWXYZ".toCharArray())
            if (numberSquare.indexOf(j) == -1)
                numberSquare += j;

        String s = "" + key;
        StringBuilder plainText = new StringBuilder();

        int textPos = 0;
        int count = 0;
        int split = s.charAt(0) - '0';
        while (true) {
            for (int j = textPos + split - 1; j >= textPos; --j) {
                if (j < cipherText.length()) {
                    char c = cipherText.charAt(j);
                    plainText.append(alphabetSquare.charAt(numberSquare.indexOf(c)));
                }
            }
            if (textPos + split >= cipherText.length())
                break;

            textPos += split;
            count += 1;
            split = s.charAt(count % s.length()) - '0';
        }
        return plainText.toString().toCharArray(); // TODO Unchecked
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, Integer key) {
        return decodeEfficiently(cipherText, null, key);
    }
}
