/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers.registry;

import java.util.Locale;
import java.util.Random;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.interfaces.ICipher.Difficulty;
import tools.cipher.ciphers.enigma.EnigmaLib;
import tools.cipher.ciphers.ADFGXCipher;
import tools.cipher.ciphers.AMSCOCipher;
import tools.cipher.ciphers.AffineCipher;
import tools.cipher.ciphers.AutokeyCipher;
import tools.cipher.ciphers.BazeriesCipher;
import tools.cipher.ciphers.BifidCipher;
import tools.cipher.ciphers.CadenusCipher;
import tools.cipher.ciphers.CaesarCipher;
import tools.cipher.ciphers.ColumnarTranspositionCipher;
import tools.cipher.ciphers.ConjugatedBifidCipher;
import tools.cipher.ciphers.DigrafidCipher;
import tools.cipher.ciphers.DoubleColumnarTranspositionCipher;
import tools.cipher.ciphers.EnigmaPlugboardCipher;
import tools.cipher.ciphers.FourSquareCipher;
import tools.cipher.ciphers.FractionatedMorseCipher;
import tools.cipher.ciphers.GrilleCipher;
import tools.cipher.ciphers.HillCipher;
import tools.cipher.ciphers.HillExtendedCipher;
import tools.cipher.ciphers.HillSubstitutionCipher;
import tools.cipher.ciphers.HomophonicCipher;
import tools.cipher.ciphers.HuttonCipher;
import tools.cipher.ciphers.KeywordCipher;
import tools.cipher.ciphers.MorbitCipher;
import tools.cipher.ciphers.MyszkowskiCipher;
import tools.cipher.ciphers.NicodemusCipher;
import tools.cipher.ciphers.NihilistSubstitutionCipher;
import tools.cipher.ciphers.NihilistTranspositionCipher;
import tools.cipher.ciphers.PeriodicGromarkCipher;
import tools.cipher.ciphers.PhillipsCipher;
import tools.cipher.ciphers.PlayfairCipher;
import tools.cipher.ciphers.PolluxCipher;
import tools.cipher.ciphers.PortaxCipher;
import tools.cipher.ciphers.ProgressiveCipher;
import tools.cipher.ciphers.QuagmireICipher;
import tools.cipher.ciphers.QuagmireIICipher;
import tools.cipher.ciphers.QuagmireIIICipher;
import tools.cipher.ciphers.QuagmireIVCipher;
import tools.cipher.ciphers.RagbabyCipher;
import tools.cipher.ciphers.RailFenceCipher;
import tools.cipher.ciphers.RedefenceCipher;
import tools.cipher.ciphers.RouteTranspositionCipher;
import tools.cipher.ciphers.RunningKeyCipher;
import tools.cipher.ciphers.SeriatedPlayfairCipher;
import tools.cipher.ciphers.SlidefairCipher;
import tools.cipher.ciphers.SolitaireCipher;
import tools.cipher.ciphers.SwagmanCipher;
import tools.cipher.ciphers.TriSquareCipher;
import tools.cipher.ciphers.TrifidCipher;
import tools.cipher.ciphers.TwoSquareCipher;
import tools.cipher.ciphers.VigenereCipher;
import tools.cipher.util.VigenereType;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.lib.registry.IRegistry;
import tools.cipher.lib.registry.Registry;

public class EncrypterRegistry {

    public static IRegistry<String, ICipher> RAND_ENCRYPTERS = Registry.builder(ICipher.class).setNamingScheme(cipher -> cipher.getClass().getSimpleName().toLowerCase(Locale.UK).replace("cipher", "")).build();
    private static Random RAND = new Random();

    public static ICipher<?> getFromName(String key) {
        return RAND_ENCRYPTERS.get(key);
    }

    public static int getDifficulty(String name) {
        return RAND_ENCRYPTERS.apply(name, ICipher::getDifficulty).orElse(Difficulty.EASY).getLevel();
    }

    @Nullable
    public static ICipher<?> getEncrypterWith(int maxDifficulty) {
        return RAND_ENCRYPTERS.getValues().stream().filter(v -> v.getDifficulty().isEasierThan(maxDifficulty)).sorted((o1, o2) -> RAND.nextBoolean() ? 1 : -1).findAny().orElse(null);
    }

    public static void registerAll() {
        if (RAND_ENCRYPTERS.frozen()) return;

        RAND_ENCRYPTERS.register(new ADFGXCipher());
        RAND_ENCRYPTERS.register("adfgvx", new ADFGXCipher(Alphabet.ALL_36_CHARS, "ADFGVX"));
        RAND_ENCRYPTERS.registerAll(new AffineCipher(), new AMSCOCipher());
        RAND_ENCRYPTERS.register(new BazeriesCipher());
        RAND_ENCRYPTERS.register(new BifidCipher());
        RAND_ENCRYPTERS.register(new CaesarCipher());
        RAND_ENCRYPTERS.register(new CadenusCipher());
        RAND_ENCRYPTERS.register(new ColumnarTranspositionCipher());
        RAND_ENCRYPTERS.register(new DoubleColumnarTranspositionCipher());
        RAND_ENCRYPTERS.register(new ConjugatedBifidCipher());
        RAND_ENCRYPTERS.register(new DigrafidCipher());
        RAND_ENCRYPTERS.register(new FourSquareCipher());
        RAND_ENCRYPTERS.register(new FractionatedMorseCipher());
        // TODO RAND_ENCRYPTERS.register(new GrilleCipher());
        RAND_ENCRYPTERS.register(new HomophonicCipher());
        RAND_ENCRYPTERS.register(new KeywordCipher());
        RAND_ENCRYPTERS.register(new MorbitCipher());
        RAND_ENCRYPTERS.register(new MyszkowskiCipher());

        for (VigenereType type : VigenereType.NORMAL_LIST) {
            String name = type.getClass().getSimpleName().toLowerCase();
            RAND_ENCRYPTERS.register(name + "_auto_key", new AutokeyCipher(type));
            RAND_ENCRYPTERS.register(name + "_nicodemus", new NicodemusCipher(type));
            RAND_ENCRYPTERS.register(name + "_progressive_key", new ProgressiveCipher(type));
            RAND_ENCRYPTERS.register(name, new VigenereCipher(type));
        }
        for (VigenereType type : VigenereType.SLIDEFAIR_LIST) {
            RAND_ENCRYPTERS.register(type.getClass().getSimpleName().toLowerCase() + "_slidefair", new SlidefairCipher(type));
        }

        RAND_ENCRYPTERS.register(new NihilistSubstitutionCipher());
        RAND_ENCRYPTERS.register(new NihilistTranspositionCipher());
        RAND_ENCRYPTERS.register(new PeriodicGromarkCipher());
        RAND_ENCRYPTERS.register(new PhillipsCipher());
        RAND_ENCRYPTERS.register(new PlayfairCipher());
        RAND_ENCRYPTERS.register("playfair6x6cipher",new PlayfairCipher(Alphabet.ALL_36_CHARS));
        RAND_ENCRYPTERS.register(new PolluxCipher());
        RAND_ENCRYPTERS.register(new PortaxCipher());
        RAND_ENCRYPTERS.registerAll(new QuagmireICipher(), new QuagmireIICipher(), new QuagmireIIICipher(), new QuagmireIVCipher());
        RAND_ENCRYPTERS.registerAll(new RailFenceCipher(), new RedefenceCipher());
        RAND_ENCRYPTERS.register(new RouteTranspositionCipher());
        RAND_ENCRYPTERS.register(new RunningKeyCipher());
        RAND_ENCRYPTERS.register(new SeriatedPlayfairCipher());
        RAND_ENCRYPTERS.register(new SolitaireCipher());
        RAND_ENCRYPTERS.register(new SwagmanCipher());
        RAND_ENCRYPTERS.register(new TrifidCipher());
        RAND_ENCRYPTERS.register(new TwoSquareCipher());
        RAND_ENCRYPTERS.register(new TriSquareCipher());
        RAND_ENCRYPTERS.register(new EnigmaPlugboardCipher(EnigmaLib.ENIGMA_M3));
        RAND_ENCRYPTERS.register("enigmaicipher", new EnigmaPlugboardCipher(EnigmaLib.ENIGMA_I));
        RAND_ENCRYPTERS.register("enigmadcipher", new EnigmaPlugboardCipher(EnigmaLib.ENIGMA_D)); //TODO add all machines
        RAND_ENCRYPTERS.register(new HillCipher());
        RAND_ENCRYPTERS.register(new HillExtendedCipher());
        RAND_ENCRYPTERS.register(new HillSubstitutionCipher());
        RAND_ENCRYPTERS.register(new RagbabyCipher());
        RAND_ENCRYPTERS.register(new GrilleCipher());
        RAND_ENCRYPTERS.register(new HuttonCipher());

        //RAND_ENCRYPTERS.register(new CaesarCipher());
        RAND_ENCRYPTERS.freeze();
    }

    static {
        registerAll();
        System.out.println(RAND_ENCRYPTERS.size());
    }
}
