/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Arrays;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class PortaxCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public PortaxCipher() {
        super(VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(2, Integer.MAX_VALUE));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(2, 15);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, String key) {
        if (plainText.length() % 2 == 1) {
            StringBuilder builder = new StringBuilder(plainText.length() + 1);
            builder.append(plainText);
            builder.append('X');
            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        return decode(plainText, key);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        int period = key.length();

        String[] slidingKey = new String[period];
        Arrays.fill(slidingKey, "");

        for (int i = 0; i < key.length(); i++) {
            char slidingChar = key.charAt(i);
            int slide = (slidingChar - 'A') / 2;
            for (int s = 0; s < 13; s++)
                slidingKey[i] += (char) ((13 + s - slide) % 13 + 'A');
        }

        for (int i = 0; i < cipherText.length(); i += period * 2) {
            int actingPeriod = Math.min((cipherText.length() - i) / 2, period);
            for (int j = 0; j < actingPeriod; j++) {

                char a = cipherText.charAt(i + j);
                char b = cipherText.charAt(i + j + actingPeriod);

                int row = (b - 'A') % 2;
                int column = (b - 'A') / 2;
                char c;
                char d;
                if (a <= 'M') {
                    int aIndex = slidingKey[j].indexOf(a);
                    if (aIndex == column) {
                        c = (char) (column + 'N');
                        d = (char) (aIndex * 2 + (row + 1) % 2 + 'A');
                    } else {
                        c = slidingKey[j].charAt(column);
                        d = (char) (aIndex * 2 + row + 'A');
                    }
                } else {
                    int aIndex = a - 'N';
                    if (aIndex == column) {
                        c = slidingKey[j].charAt(column);
                        d = (char) (aIndex * 2 + (row + 1) % 2 + 'A');
                    } else {
                        c = (char) (column + 'N');
                        d = (char) (aIndex * 2 + row + 'A');
                    }
                }

                plainText[i + j] = c;
                plainText[i + j + actingPeriod] = d;
            }
        }

        return plainText;
    }

}
