/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.QuadKey;
import tools.cipher.base.ciphers.QuadKeyCipher;
import tools.cipher.base.interfaces.IKeyType.IKeyBuilder;
import tools.cipher.base.key.types.EnumKeyType;
import tools.cipher.base.key.types.OrderedIntegerKeyType;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.util.ReadMode;

/**
 * @author Alex
 *
 */
public class DoubleColumnarTranspositionCipher extends QuadKeyCipher<Integer[], ReadMode, Integer[], ReadMode, OrderedIntegerKeyType.Builder, EnumKeyType.Builder<ReadMode>, OrderedIntegerKeyType.Builder, EnumKeyType.Builder<ReadMode>> {

    private ColumnarTranspositionCipher base;

    public DoubleColumnarTranspositionCipher() {
        super(OrderedIntegerKeyType.builder().setRange(2, Integer.MAX_VALUE),
              EnumKeyType.builder(ReadMode.class).setUniverse(ReadMode.values()),
              OrderedIntegerKeyType.builder().setRange(2, Integer.MAX_VALUE),
              EnumKeyType.builder(ReadMode.class).setUniverse(ReadMode.values()));
        this.base = new ColumnarTranspositionCipher();
    }

    @Override
    public IKeyBuilder<Integer[]> limitDomainForFirstKey(OrderedIntegerKeyType.Builder secondKey) {
        return secondKey.setRange(2, 9);
    }

    @Override
    public IKeyBuilder<Integer[]> limitDomainForThirdKey(OrderedIntegerKeyType.Builder secondKey) {
        return secondKey.setRange(2, 9);
    }

    @Override
    public CharSequence encode(CharSequence plainText, QuadKey<Integer[], ReadMode, Integer[], ReadMode> key) {
        return this.base.encode(
                this.base.encode(plainText, BiKey.of(key.getFirstKey(), key.getSecondKey())),
                BiKey.of(key.getThirdKey(), key.getFourthKey()));
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, char[] plainText, QuadKey<Integer[], ReadMode, Integer[], ReadMode> key) {
        return this.base.decodeEfficiently(new CharArrayWrapper(this.base.decodeEfficiently(cipherText, new char[cipherText.length()], BiKey.of(key.getThirdKey(), key.getFourthKey()))), plainText, BiKey.of(key.getFirstKey(), key.getSecondKey()));
    }
}
