/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.QuadKey;
import tools.cipher.base.ciphers.QuadKeyCipher;
import tools.cipher.base.interfaces.IKeyType.IKeyBuilder;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.base.key.types.ObjectKeyType;
import tools.cipher.ciphers.route.RouteCipherType;
import tools.cipher.ciphers.route.Routes;
import tools.cipher.lib.characters.CharArrayWrapper;

public class RouteTranspositionCipher extends QuadKeyCipher<Integer, Integer, RouteCipherType, RouteCipherType, IntegerKeyType.Builder, IntegerKeyType.Builder, ObjectKeyType.Builder<RouteCipherType>, ObjectKeyType.Builder<RouteCipherType>> {

    public RouteTranspositionCipher() {
        super(IntegerKeyType.builder().setRange(1, Integer.MAX_VALUE),
                IntegerKeyType.builder().setRange(1, Integer.MAX_VALUE),
                ObjectKeyType.<RouteCipherType>builder().setUniverse(Routes.getRoutes().toArray(new RouteCipherType[0])),
                ObjectKeyType.<RouteCipherType>builder().setUniverse(Routes.getRoutes().toArray(new RouteCipherType[0])));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, QuadKey<Integer, Integer, RouteCipherType, RouteCipherType> key) {
        int blockSize = key.getFirstKey() * key.getSecondKey();

        if (plainText.length() % blockSize != 0) {
            StringBuilder builder = new StringBuilder(plainText.length() + blockSize - (plainText.length() % blockSize));
            builder.append(plainText);
            while (builder.length() % blockSize != 0) {
                builder.append('X');
            }

            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public IKeyBuilder<Integer> limitDomainForFirstKey(IntegerKeyType.Builder firstKey) {
        return firstKey.setRange(1, 100);
    }

    @Override
    public IKeyBuilder<Integer> limitDomainForSecondKey(IntegerKeyType.Builder secondKey) {
        return secondKey.setRange(1, 100);
    }

    @Override
    public CharSequence encode(CharSequence plainText, QuadKey<Integer, Integer, RouteCipherType, RouteCipherType> key) {
        // Create pattern
        int[] gridWrite = key.getThirdKey().getPattern(key.getFirstKey(), key.getSecondKey(), plainText.length());
        int[] gridRead = key.getFourthKey().getPattern(key.getFirstKey(), key.getSecondKey(), plainText.length());
        // Reads across the grid
        char[] gridString = new char[plainText.length()];
        for (int i = 0; i < plainText.length(); i++)
            gridString[gridWrite[i]] = plainText.charAt(i);

        char[] gridString2 = new char[plainText.length()];
        for (int i = 0; i < plainText.length(); i++)
            gridString2[i] = gridString[gridRead[i]];

        return new CharArrayWrapper(gridString2);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, QuadKey<Integer, Integer, RouteCipherType, RouteCipherType> key) {
        // Create pattern
        int[] gridWrite = key.getThirdKey().getPattern(key.getFirstKey(), key.getSecondKey(), cipherText.length());
        int[] gridRead = key.getFourthKey().getPattern(key.getFirstKey(), key.getSecondKey(), cipherText.length());

        char[] gridString = new char[cipherText.length()];
        for (int i = 0; i < cipherText.length(); i++)
            gridString[gridRead[i]] = cipherText.charAt(i);

        for (int i = 0; i < cipherText.length(); i++)
            plainText[i] = gridString[gridWrite[i]];

        return plainText;
    }
}
