/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.key.types.IntegerKeyType;
import com.alexbarter.lib.util.RandomUtil;

public class RailFenceCipher implements ICipher<BiKey<Integer, Integer>> {

    // TODO Add read off diagonals mode

    protected final IntegerKeyType firstType;
    private IntegerKeyType firstTypeLimit;
    private final IntegerKeyType.Builder firstKeyBuilder;

    public RailFenceCipher() {
        IntegerKeyType.Builder firstKey = IntegerKeyType.builder().setRange(2, Integer.MAX_VALUE / 2 - 2);
        this.firstType = firstKey.create();
        this.firstTypeLimit = firstKey.setRange(2, 50).create();
        this.firstKeyBuilder = firstKey;
    }

    @Override
    public boolean isValid(BiKey<Integer, Integer> key) {
        return this.firstType.isValid(key.getFirstKey()) && 0 <= key.getSecondKey() && key.getSecondKey() < (key.getFirstKey() - 1) * 2 ;
    }

    @Override
    public BiKey<Integer, Integer> randomiseKey() {
        int rails = this.firstTypeLimit.randomise();
        return BiKey.of(rails, RandomUtil.pickRandomInt(0, (rails - 1) * 2 - 1));
    }

    @Override
    public boolean iterateKeys(Function<BiKey<Integer, Integer>, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(f -> {
            for (int s = 0; s < (f - 1) * 2; s++) {
                if (!consumer.apply(BiKey.of(f, s))) {
                    return false;
                }
            }
            return true;
        });
    }

    @Override
    public BiKey<Integer, Integer> alterKey(BiKey<Integer, Integer> key, double temp, int count) {
        return key;
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ZERO;
        for (int i = this.firstTypeLimit.getMin(); i <= this.firstTypeLimit.getMax(); i++) {
            total = total.add(BigInteger.valueOf(i).subtract(BigInteger.ONE).multiply(BigInteger.valueOf(2)));
        }

        return total;
    }

    @Override
    public String prettifyKey(BiKey<Integer, Integer> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), String.valueOf(key.getSecondKey()));
    }

    @Override
    public BiKey<Integer, Integer> parseKey(String input) throws ParseException {
        String[] split = input.split(" ");
        if (split.length != 2) { throw new ParseException(input, 0); }

        return BiKey.of(this.firstType.parse(split[0]), this.firstType.parse(split[1]));
    }

    public void limitDomainForFirstKey(Function<IntegerKeyType.Builder, IntegerKeyType.Builder> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
    }

    public IntegerKeyType getFirstKeyType() {
        return this.firstTypeLimit;
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<Integer, Integer> key) {
        return RedefenceCipher.encodeGeneral(plainText, key.getFirstKey(), key.getSecondKey(), i -> i);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<Integer, Integer> key) {
        return RedefenceCipher.decodeGeneral(cipherText, plainText, key.getFirstKey(), key.getSecondKey(), i -> i);
    }
}
