/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.ciphers.TriKeyCipher;
import tools.cipher.base.key.types.IntegerKeyType;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class DigrafidCipher extends TriKeyCipher<String, String, Integer, SquareStringKeyType.Builder, SquareStringKeyType.Builder, IntegerKeyType.Builder> {

    public DigrafidCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_27_CHARS).setDim(3, 9), SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_27_CHARS).setDim(9, 3), IntegerKeyType.builder().setRange(2, Integer.MAX_VALUE)); // period                                                                                                                                                                                                                      // 0
    }

    @Override
    public IntegerKeyType.Builder limitDomainForThirdKey(IntegerKeyType.Builder thirdKey) {
        return thirdKey.setRange(2, 15);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, TriKey<String, String, Integer> key) {
        if (plainText.length() % 2 == 1) {
            StringBuilder builder = new StringBuilder(plainText.length() + 1);
            builder.append(plainText);
            builder.append('X');
            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, TriKey<String, String, Integer> key) {
        int fractionation = key.getThirdKey();
        if (fractionation == 0)
            fractionation = plainText.length() / 2; // I believe this will work

        int period = fractionation * 2;

        int[] numberText = new int[plainText.length() * 3 / 2];
        int blocks = (int) Math.ceil(plainText.length() / (double) period);

        int index = 0;

        Character[] cipherText = new Character[plainText.length()];

        for (int b = 0; b < blocks; b++) {
            int min = Math.min(fractionation, (plainText.length() - b * period) / 2);

            for (int f = 0; f < min; f++) {
                int cTIndex = b * period + f * 2;
                int index1 = key.getFirstKey().indexOf(plainText.charAt(cTIndex));
                int index2 = key.getSecondKey().indexOf(plainText.charAt(cTIndex + 1));

                numberText[b * fractionation * 3 + f] = index1 % 9;
                numberText[b * fractionation * 3 + min + f] = (index1 / 9) * 3 + (index2 % 3);
                numberText[b * fractionation * 3 + min * 2 + f] = index2 / 3;
            }

            for (int f = 0; f < min; f++) {
                int n1 = numberText[b * fractionation * 3 + f * 3];
                int n2 = numberText[b * fractionation * 3 + f * 3 + 1];
                int n3 = numberText[b * fractionation * 3 + f * 3 + 2];
                cipherText[index++] = key.getFirstKey().charAt(n1 + (int) (n2 / 3) * 9);
                cipherText[index++] = key.getSecondKey().charAt(n3 * 3 + n2 % 3);
            }
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, TriKey<String, String, Integer> key) {
        int fractionation = key.getThirdKey();
        byte[] numberText = new byte[cipherText.length() * 3 / 2]; // TODO Use resuseable one
        if (fractionation == 0)
            fractionation = cipherText.length() / 2; // I believe this will work

        int period = fractionation * 2;

        int blocks = (int) Math.ceil(cipherText.length() / (double) period);

        int indexNo = 0;
        int index = 0;

        for (int b = 0; b < blocks; b++) {
            int min = Math.min(fractionation, (cipherText.length() - b * period) / 2);

            for (int f = 0; f < min; f++) {
                int cTIndex = b * period + f * 2;
                int index1 = key.getFirstKey().indexOf(cipherText.charAt(cTIndex));
                int index2 = key.getSecondKey().indexOf(cipherText.charAt(cTIndex + 1));

                numberText[indexNo++] = (byte) (index1 % 9);
                numberText[indexNo++] = (byte) ((index1 / 9) * 3 + (index2 % 3));
                numberText[indexNo++] = (byte) (index2 / 3);
            }

            for (int f = 0; f < min; f++) {
                int n1 = numberText[b * fractionation * 3 + f];
                int n2 = numberText[b * fractionation * 3 + min + f];
                int n3 = numberText[b * fractionation * 3 + min * 2 + f];
                plainText[index++] = key.getFirstKey().charAt(n1 + (int) (n2 / 3) * 9);
                plainText[index++] = key.getSecondKey().charAt(n3 * 3 + n2 % 3);
            }
        }

        return plainText;
    }
}
