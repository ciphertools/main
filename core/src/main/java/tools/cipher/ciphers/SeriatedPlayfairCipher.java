/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Map;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.key.types.IntegerGenKeyType;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class SeriatedPlayfairCipher extends BiKeyCipher<String, Integer, SquareStringKeyType.Builder, IntegerGenKeyType.Builder> {

    public SeriatedPlayfairCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5),
                IntegerGenKeyType.builder().setRange(0, 5000).addFilter(i -> i != 1));
    }

    @Override
    public IntegerGenKeyType.Builder limitDomainForSecondKey(IntegerGenKeyType.Builder secondKey) {
        return secondKey.setRange(0, 15);
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, BiKey<String, Integer> key) {
        StringBuilder builder = new StringBuilder(plainText);

        // TODO Improve
        int period = key.getSecondKey();
        while (true) {
            boolean valid = true;

            label: for (int i = 0; i < builder.length() && valid; i += period * 2) {
                int min = Math.min(period, (builder.length() - i + 1) / 2);

                for (int j = 0; j < Math.min(min, (builder.length() - i) / 2); j++) {
                    char a = builder.charAt(i + j);
                    char b = builder.charAt(i + j + min);
                    if (a == b) {
                        char nullChar = 'X';
                        if (a == 'X')
                            nullChar = 'Q';
                        builder.insert(i + min + j, nullChar);

                        valid = false;
                        break label;
                    }
                }
            }

            if (valid)
                break;
        }

        if (builder.length() % 2 != 0)
            builder.append('X');

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<String, Integer> key) {
        int period = key.getSecondKey();
        if (period == 0)
            period = plainText.length() / 2;

        Character[] cipherText = new Character[plainText.length()];

        for (int i = 0; i < plainText.length(); i += period * 2) {
            int min = Math.min(period, (int) Math.ceil((double) (plainText.length() - i) / 2));

            for (int j = 0; j < min; j++) {
                char a = plainText.charAt(i + j);
                char b = plainText.charAt(i + j + min);

                int i1 = key.getFirstKey().indexOf(a);
                int i2 = key.getFirstKey().indexOf(b);
                int row1 = (int) Math.floor((double) i1 / 5);
                int col1 = i1 % 5;
                int row2 = (int) Math.floor((double) i2 / 5);
                int col2 = i2 % 5;

                char c, d;

                if (row1 == row2) {
                    c = key.getFirstKey().charAt(row1 * 5 + MathUtil.mod(col1 + 1, 5));
                    d = key.getFirstKey().charAt(row2 * 5 + MathUtil.mod(col2 + 1, 5));
                } else if (col1 == col2) {
                    c = key.getFirstKey().charAt(MathUtil.mod(row1 + 1, 5) * 5 + col1);
                    d = key.getFirstKey().charAt(MathUtil.mod(row2 + 1, 5) * 5 + col2);
                } else {
                    c = key.getFirstKey().charAt(row1 * 5 + col2);
                    d = key.getFirstKey().charAt(row2 * 5 + col1);
                }

                if (c == d) {
                    System.out.println("ERROR " + a + "" + b + " " + c + "" + d);
                }

                cipherText[i + j] = c;
                cipherText[i + j + min] = d;
            }
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<String, Integer> key) {
        int period = key.getSecondKey();
        if (period == 0)
            period = cipherText.length() / 2;

        Map<Character, Integer> keyIndex = CipherUtils.createCharacterIndexMapping(key.getFirstKey());

        for (int i = 0; i < cipherText.length(); i += period * 2) {
            int min = Math.min(period, (int) Math.ceil((double) (plainText.length - i) / 2));

            for (int j = 0; j < min; j++) {
                int i1 = keyIndex.get(cipherText.charAt(i + j));
                int i2 = keyIndex.get(cipherText.charAt(i + j + min));
                int row1 = i1 / 5;
                int col1 = i1 % 5;
                int row2 = i2 / 5;
                int col2 = i2 % 5;

                char c, d;

                if (row1 == row2) {
                    c = key.getFirstKey().charAt(row1 * 5 + MathUtil.mod(col1 - 1, 5));
                    d = key.getFirstKey().charAt(row2 * 5 + MathUtil.mod(col2 - 1, 5));
                } else if (col1 == col2) {
                    c = key.getFirstKey().charAt(MathUtil.mod(row1 - 1, 5) * 5 + col1);
                    d = key.getFirstKey().charAt(MathUtil.mod(row2 - 1, 5) * 5 + col2);
                } else {
                    c = key.getFirstKey().charAt(row1 * 5 + col2);
                    d = key.getFirstKey().charAt(row2 * 5 + col1);
                }

                plainText[i + j] = c;
                plainText[i + j + min] = d;
            }
        }

        return plainText;
    }
}
