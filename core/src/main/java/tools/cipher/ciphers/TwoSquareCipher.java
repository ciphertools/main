/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.key.types.SquareStringKeyType;
import tools.cipher.lib.constants.Alphabet;

public class TwoSquareCipher extends BiKeyCipher<String, String, SquareStringKeyType.Builder, SquareStringKeyType.Builder> {

    public TwoSquareCipher() {
        super(SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5), SquareStringKeyType.builder().setAlphabet(Alphabet.ALL_25_CHARS).setDim(5, 5));
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, BiKey<String, String> key) {
        StringBuilder builder = new StringBuilder(plainText.length());
        for (int i = 0; i < plainText.length(); i++) {
            char c = plainText.charAt(i);
            builder.append(c == 'J' ? 'I' : c);
        }

        if (builder.length() % 2 == 1) {
            builder.append('X');
        }

        return builder;
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<String, String> key) {
        StringBuilder cipherText = new StringBuilder(plainText.length());

        for (int i = 0; i < plainText.length(); i += 2) {
            char a = plainText.charAt(i);
            char b = plainText.charAt(i + 1);
            int aIndex = key.getFirstKey().indexOf(a);
            int bIndex = key.getSecondKey().indexOf(b);
            int aRow = (int) Math.floor(aIndex / 5);
            int bRow = (int) Math.floor(bIndex / 5);
            int aCol = aIndex % 5;
            int bCol = bIndex % 5;

            if (aRow == bRow) {
                cipherText.append(b);
                cipherText.append(a);
            } else {
                cipherText.append(key.getSecondKey().charAt(5 * aRow + bCol));
                cipherText.append(key.getFirstKey().charAt(5 * bRow + aCol));
            }
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<String, String> key) {
        for (int i = 0; i < cipherText.length(); i += 2) {
            char a = cipherText.charAt(i);
            char b = cipherText.charAt(i + 1);
            int aIndex = key.getSecondKey().indexOf(a);
            int bIndex = key.getFirstKey().indexOf(b);
            int aRow = (int) Math.floor(aIndex / 5);
            int bRow = (int) Math.floor(bIndex / 5);
            int aCol = aIndex % 5;
            int bCol = bIndex % 5;

            if (aRow == bRow) {
                plainText[i] = b;
                plainText[i + 1] = a;
            } else {
                plainText[i] = key.getFirstKey().charAt(5 * aRow + bCol);
                plainText[i + 1] = key.getSecondKey().charAt(5 * bRow + aCol);

            }
        }

        return plainText;
    }

}
