/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.BiKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class HuttonCipher extends BiKeyCipher<String, String, VariableStringKeyType.Builder, VariableStringKeyType.Builder> {

    public HuttonCipher() {
        super(VariableStringKeyType.builder().setAlphabet("ABCDEFGHIJKLMNOPQRSTUVWXY").setRange(1, Integer.MAX_VALUE),
                VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(1, Integer.MAX_VALUE));
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForFirstKey(VariableStringKeyType.Builder firstKey) {
        return firstKey.setRange(1, 7);
    }

    @Override
    public VariableStringKeyType.Builder limitDomainForSecondKey(VariableStringKeyType.Builder secondKey) {
        return secondKey.setRange(1, 7);
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<String, String> key) {
        char[] secondKey = new char[26];
        int d = 0;
        for (d = 0; d < key.getSecondKey().length(); d++) {
            secondKey[d] = key.getSecondKey().charAt(d);
        }

        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {
            if (!ArrayUtil.contains(secondKey, 0, d, alpha))
                secondKey[d++] = alpha;
        }

        Character[] cipherText = new Character[plainText.length()];

        for (int i = 0; i < plainText.length(); i++) {
            int keyIndex = ArrayUtil.indexOf(secondKey, plainText.charAt(i));
            int newKeyIndex = (keyIndex + (key.getFirstKey().charAt(i % key.getFirstKey().length()) - 'A' + 1)) % secondKey.length;
            cipherText[i] = secondKey[newKeyIndex];
            secondKey[keyIndex] = cipherText[i];
            secondKey[newKeyIndex] = plainText.charAt(i);
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<String, String> key) {
        char[] secondKey = new char[26];
        int d = 0;
        for (d = 0; d < key.getSecondKey().length(); d++) {
            secondKey[d] = key.getSecondKey().charAt(d);
        }

        for (char alpha = 'A'; alpha <= 'Z'; alpha++) {
            if (!ArrayUtil.contains(secondKey, 0, d, alpha))
                secondKey[d++] = alpha;
        }

        for (int i = 0; i < cipherText.length(); i++) {
            int keyIndex = ArrayUtil.indexOf(secondKey, cipherText.charAt(i));
            int newKeyIndex = (keyIndex - (key.getFirstKey().charAt(i % key.getFirstKey().length()) - 'A' + 1) + secondKey.length) % secondKey.length;
            plainText[i] = secondKey[newKeyIndex];
            secondKey[keyIndex] = (char) plainText[i];
            secondKey[newKeyIndex] = cipherText.charAt(i);
        }

        return plainText;
    }
}
