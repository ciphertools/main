/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.FullStringKeyType;
import tools.cipher.lib.characters.CharacterArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class KeywordCipher extends UniKeyCipher<String, FullStringKeyType.Builder> {

    public KeywordCipher() {
        this(Alphabet.ALL_26_CHARS);
    }

    public KeywordCipher(CharSequence alphabet) {
        super(FullStringKeyType.builder().setAlphabet(alphabet));
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {

        Character[] cipherText = new Character[plainText.length()];

        for (int i = 0; i < plainText.length(); i++) {
            char ch = plainText.charAt(i);
            if (ch >= 'A' && ch <= 'Z')
                cipherText[i] = key.charAt(ch - 'A');
        }

        return new CharacterArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        for (int i = 0; i < cipherText.length(); i++)
            plainText[i] = (char) (key.indexOf(cipherText.charAt(i)) + 'A');

        return plainText;
    }
}
