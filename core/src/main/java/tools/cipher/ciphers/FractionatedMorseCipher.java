/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.FullStringKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.MorseCode;
import tools.cipher.lib.constants.Alphabet;

public class FractionatedMorseCipher extends UniKeyCipher<String, FullStringKeyType.Builder> {

    private static List<Character> list = Arrays.asList('.', '-', 'X');

    public FractionatedMorseCipher() {
        super(FullStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS));
    }

    // TODO read and write xx between words

    @Override
    public CharSequence encode(CharSequence plainText, String key) {

        StringBuilder cipherText = new StringBuilder(plainText.length());

        String morseText = MorseCode.getMorseEquivalent(plainText);
        while (morseText.length() % 3 != 0)
            morseText += "X";

        for (int i = 0; i < morseText.length(); i += 3) {
            int a = list.indexOf(morseText.charAt(i));
            int b = list.indexOf(morseText.charAt(i + 1));
            int c = list.indexOf(morseText.charAt(i + 2));
            cipherText.append(key.charAt(a * 9 + b * 3 + c));
        }

        return cipherText;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, String key) {
        char[] morseText = new char[cipherText.length() * 3];
        plainText = new char[cipherText.length() * 3];
        for (int i = 0; i < cipherText.length(); i++) {
            int index = key.indexOf(cipherText.charAt(i));
            morseText[i * 3] = list.get(index / 9);
            morseText[i * 3 + 1] = list.get((int) (index / 3) % 3);
            morseText[i * 3 + 2] = list.get(index % 3);
        }

        int index = 0;
        int lastX = 0;
        for (int i = 0; i < morseText.length; i++) {

            char morseCh = morseText[i];
            boolean isX = morseCh == 'X';
            boolean end = i == morseText.length - 1;

            if (isX || end) { // When char is X or is at the end of the text
                int length = i - lastX + (end && !isX ? 1 : 0);
                if (length > 0) {
                    Character character = MorseCode.getCharFromMorse(morseText, lastX, length);

                    if (character == null) {
                        for (int j = lastX; j < i; j++) {
                            plainText[index++] = morseText[j];
                        }
                    }
                    else {
                        plainText[index++] = character;
                    }
                }

                if (isX && i < morseText.length - 2 && morseText[i + 1] == 'X') {
                    plainText[index++] = ' ';
                    i++;
                }
                lastX = i + 1;
            }
        }

        return ArrayUtil.copyRange(plainText, 0, index);
    }

}
