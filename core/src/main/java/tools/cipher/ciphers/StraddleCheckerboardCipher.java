/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.key.types.VariableStringKeyType;

public class StraddleCheckerboardCipher extends UniKeyCipher<String, VariableStringKeyType.Builder> {

    public StraddleCheckerboardCipher(VariableStringKeyType.Builder firstKey) {
        super(firstKey);
        // TODO Auto-generated constructor stub
    }

    @Override
    public CharSequence encode(CharSequence plainText, String key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, char[] plainText, String key) {
        // TODO Auto-generated method stub
        return null;
    }

}
