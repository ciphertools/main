/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.ciphers;

import java.math.BigInteger;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.types.SquareMatrixKeyType;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.matrix.Matrix;

public class HillExtendedCipher implements ICipher<BiKey<Matrix, Matrix>> {

    private final int mod;
    protected final SquareMatrixKeyType firstType;
    private SquareMatrixKeyType firstTypeLimit;
    private final SquareMatrixKeyType.Builder firstKeyBuilder;

    public HillExtendedCipher() {
        this(26);
    }

    public HillExtendedCipher(int mod) {
        SquareMatrixKeyType.Builder firstKey = SquareMatrixKeyType.builder().setRange(1, Integer.MAX_VALUE).setMod(mod);
        this.firstType = firstKey.create();
        this.firstTypeLimit = firstKey.setRange(2, 4).create();
        this.firstKeyBuilder = firstKey;
        this.mod = mod;
    }

    @Override
    public boolean isValid(BiKey<Matrix, Matrix> key) {
        return this.firstType.isValid(key.getFirstKey()) && key.getSecondKey().rows == key.getFirstKey().size();
    }

    @Override
    public BiKey<Matrix, Matrix> randomiseKey() {
        Matrix matrix = this.firstTypeLimit.randomise();
        return BiKey.of(matrix, KeyGeneration.createMatrix(matrix.size(), 1, this.mod));
    }

    @Override
    public boolean iterateKeys(Function<BiKey<Matrix, Matrix>, Boolean> consumer) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public BiKey<Matrix, Matrix> alterKey(BiKey<Matrix, Matrix> key, double temp, int count) {
        return key;
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ONE; //TODO
        return total;
    }

    @Override
    public String prettifyKey(BiKey<Matrix, Matrix> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), String.valueOf(key.getSecondKey()));
    }

    public void limitDomainForFirstKey(Function<SquareMatrixKeyType.Builder, SquareMatrixKeyType.Builder> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
    }

    public SquareMatrixKeyType getFirstKeyType() {
        return this.firstTypeLimit;
    }

    @Override
    public CharSequence normaliseText(CharSequence plainText, BiKey<Matrix, Matrix> key) {
        int blockSize = key.getFirstKey().size();

        if (plainText.length() % blockSize != 0) {
            StringBuilder builder = new StringBuilder(plainText.length() + blockSize - (plainText.length() % blockSize));
            builder.append(plainText);
            while (builder.length() % blockSize != 0) {
                builder.append('X');
            }

            return builder;
        } else {
            return plainText;
        }
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<Matrix, Matrix> key) {
        char[] cipherText = new char[plainText.length()];
        int size = key.getFirstKey().size();

        for(int i = 0; i < plainText.length(); i += size) {

            Integer[] let = new Integer[size];
            for(int j = 0; j < size; j++) {
                let[j] = ((char)plainText.charAt(i + j) - 'A');
            }

            Matrix plainMatrix = new Matrix(let, size, 1);
            Matrix cipherMatrix = key.getFirstKey().multiply(plainMatrix).add(key.getSecondKey()).modular(26);

            for(int j = 0; j < size; j++) {
                cipherText[i + j] = (char)(cipherMatrix.data[j] + 'A');
            }

        }

        return new CharArrayWrapper(cipherText);
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, BiKey<Matrix, Matrix> key) {
        return decodeUsingInverse(cipherText, plainText, key.getFirstKey().inverseMod(26), key.getSecondKey());
    }

    public char[] decodeUsingInverse(CharSequence cipherText, char[] plainText, Matrix inverseKey, Matrix secondKey)  {
        int size = inverseKey.size();
        for(int i = 0; i < cipherText.length(); i += size) {

            Integer[] let = new Integer[size];
            for(int j = 0; j < size; j++)
                let[j] = ((int)cipherText.charAt(i + j) - 'A');

            Matrix cipherMatrix = new Matrix(let, size, 1);
            Matrix plainMatrix = inverseKey.multiply(cipherMatrix.subtract(secondKey)).modular(26);

            for(int j = 0; j < size; j++) {
                plainText[i + j] = (char)(plainMatrix.data[j] + 'A');
            }

        }

        return plainText;
    }
}
