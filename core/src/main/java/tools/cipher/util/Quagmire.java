/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.util;

public class Quagmire {

    public static CharSequence encode(CharSequence plainText, String topKey, String gridKey, String indicatorKey, char indicatorBelow) {
        StringBuilder cipherText = new StringBuilder(plainText.length());

        char[][] keyAlpha = new char[indicatorKey.length()][26];
        int indicatorIdx = topKey.indexOf(indicatorBelow);

        for (int i = 0; i < indicatorKey.length(); i++) {
            for (int k = 0; k < 26; k++) {
                keyAlpha[i][k] = gridKey.charAt((26 - indicatorIdx + gridKey.indexOf(indicatorKey.charAt(i)) + k) % 26);
            }
        }

        for (int i = 0; i < plainText.length(); i++) {
            cipherText.append(keyAlpha[i % indicatorKey.length()][topKey.indexOf(plainText.charAt(i))]);
        }

        return cipherText;
    }

    // Assuming the topKey is the ABCDEFGHIJKLMNPQRSTUVWXYZ
    public static char[] decode(CharSequence cipherText, char[] plainText, String gridKey, String indicatorKey, char indicatorBelow) {
        int indicatorIdx = indicatorBelow - 'A';

        int[][] keyAlpha = new int[indicatorKey.length()][26];

        for (int i = 0; i < indicatorKey.length(); i++) {
            for (int k = 0; k < 26; k++) {
                keyAlpha[i][gridKey.charAt((26 - indicatorIdx + gridKey.indexOf(indicatorKey.charAt(i)) + k) % 26) - 'A'] = k;
            }
        }

        for (int i = 0; i < cipherText.length(); i++) {
            plainText[i] = (char) (keyAlpha[i % indicatorKey.length()][cipherText.charAt(i) - 'A'] + 'A');
        }

        return plainText;
    }

    public static char[] decode(CharSequence cipherText, char[] plainText, String topKey, String gridKey, String indicatorKey, char indicatorBelow) {
        int indicatorIndex = topKey.indexOf(indicatorBelow);

        int[][] keyAlpha = new int[indicatorKey.length()][26];

        for (int i = 0; i < indicatorKey.length(); i++) {
            for (int k = 0; k < 26; k++) {
                keyAlpha[i][gridKey.charAt((26 - indicatorIndex + gridKey.indexOf(indicatorKey.charAt(i)) + k) % 26) - 'A'] += k;
            }
        }

        for (int i = 0; i < cipherText.length(); i++) {
            plainText[i] = (char) topKey.charAt(keyAlpha[i % indicatorKey.length()][cipherText.charAt(i) - 'A']);
        }

        return plainText;
    }
}
