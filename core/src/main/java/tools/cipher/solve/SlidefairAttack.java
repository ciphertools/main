/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.IDictionaryAttack;
import tools.cipher.ciphers.SlidefairCipher;

public class SlidefairAttack extends PeriodicKeyAttack<SlidefairCipher> implements IDictionaryAttack<String> {

    public SlidefairAttack(SlidefairCipher cipher, String displayName) {
        super(cipher, displayName);
        this.setAttackMethods(DecryptionMethod.PERIODIC_KEY, DecryptionMethod.DICTIONARY, DecryptionMethod.BRUTE_FORCE);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        // Settings grab
        switch (method) {
        case DICTIONARY:
            DecryptionTracker tracker = new DecryptionTracker(text, app);
            return this.tryDictionaryAttack(tracker);
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public String useWordToGetKey(DecryptionTracker tracker, String word) {
        return this.useStringGetKey(tracker, word);
    }
}
