/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JPanel;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.ciphers.HillExtendedCipher;
import tools.cipher.ciphers.KeywordCipher;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;
import tools.cipher.lib.matrix.Matrix;
import tools.cipher.lib.matrix.MatrixNoInverse;
import tools.cipher.lib.matrix.MatrixNotSquareException;
import tools.cipher.lib.result.DynamicResultList;
import tools.cipher.lib.stats.StatIC;
import tools.cipher.solve.HillAttack.HillSection;

public class HillSubsitutionAttack extends CipherAttack<BiKey<Matrix, Matrix>, HillExtendedCipher> {

    private int[] sizeRange = new int[] { 2, 4 };

    public HillSubsitutionAttack() {
        super(new HillExtendedCipher(), "Hill Subsitution");
        this.setAttackMethods(DecryptionMethod.BRUTE_FORCE, DecryptionMethod.CALCULATED, DecryptionMethod.PERIODIC_KEY, DecryptionMethod.SIMULATED_ANNEALING);
        this.addSetting(SettingTypes.createIntRange("size_range", 2, 4, 2, 100, 1, (values, cipher) -> {HillSubsitutionAttack.this.sizeRange = values;}));
    }

    @Override
    public void createSettingsUI(JDialog dialog, JPanel panel) {
        super.createSettingsUI(dialog, panel);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        HillTracker tracker = new HillTracker(text, app);

        switch (method) {
        case PERIODIC_KEY:
            for (int size = this.sizeRange[0]; size <= this.sizeRange[1]; size++) {
                tracker.resultList.clear();
                if (tracker.getCipherText().length() % size != 0) {
                    this.output(tracker, "Matrix size of %d is not possible, length of text is not a multiple.", size);
                    continue;
                }
                tracker.size = size;
                tracker.lengthSub = tracker.getCipherText().length() / size;

                KeyIterator.iterateIntegerArray(row -> this.iterateMatrixRows(tracker, row), size, 26, true);
                tracker.resultList.sort();

                if (tracker.resultList.size() < size) {
                    this.output(tracker, "Did not find enought key columns that produces good characters %d/%d", tracker.resultList.size(), size);
                } else {
                    this.output(tracker, "Trying all combinations...");
                    this.output(tracker, "Removing trials that have no inverse...");
                    this.output(tracker, "Removing trials with %cIC less than 10...", (char) 916);
                    KeyIterator.iterateObject(row -> this.iteratePossibleRows(tracker, row), size, tracker.resultList.toArray(new HillSection[0]));
                    tracker.bestNext.sort();

                    for (HillSection section : tracker.bestNext) {
                        CipherAttack<String, KeywordCipher> substitutionHack = new CipherAttack<>(new KeywordCipher(Alphabet.ALL_26_CHARS), "Hill Sub").setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING).setIterations(5).mute();
                        IDecryptionTracker keywordTracker = substitutionHack.attemptAttack(new CharArrayWrapper(section.decrypted), DecryptionMethod.SIMULATED_ANNEALING, app);

                        this.updateIfBetterThanBest(tracker, keywordTracker.getBestSolution());
                    }
                }
            }
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public void decryptAndUpdate(IDecryptionTracker tracker, BiKey<Matrix, Matrix> key) {
        try {
            super.decryptAndUpdate(tracker, key);
        } catch (MatrixNoInverse | MatrixNotSquareException e) {
            return;
        }
    }

    public boolean iterateMatrixRows(HillTracker tracker, Integer[] row) {
        if (MathUtil.allDivisibleBy(row, 0, tracker.size, 2, 13)) {
            return true;
        }

        char[] decrypted = new char[tracker.lengthSub];

        for (int i = 0; i < tracker.getCipherText().length(); i += tracker.size) {
            int total = 0;
            for (int s = 0; s < tracker.size; s++)
                total += row[s] * (tracker.getCipherText().charAt(i + s) - 'A');

            decrypted[i / tracker.size] =  (char) (total % 26 + 'A');
        }

        double score = Math.abs(StatIC.calculateMonoIC(decrypted) - tracker.getLanguage().getNormalisedIC()) * 1000;

        if (tracker.resultList.add(new HillSection(score, decrypted, Arrays.copyOf(row, row.length)))) {
            if (score < 10D) {
                this.output(tracker, "%s, %f, %s", Arrays.toString(row), score, new String(decrypted));
            }
        }
        return true;
    }

    public boolean iteratePossibleRows(HillTracker tracker, HillSection[] data) {
        // Create key matrix from data
        Integer[] inverseData = new Integer[tracker.size * tracker.size];
        for (int s = 0; s < tracker.size; s++) {
            HillSection hillResult = data[s];
            for (int n = 0; n < tracker.size; n++) {
                inverseData[s * tracker.size + n] = hillResult.inverseCol[n];
            }
        }

        Matrix inverseMatrix = new Matrix(inverseData, tracker.size);

        if (!inverseMatrix.hasInverseMod(26)) {
            return true;
        }

        char[] combinedDecrypted = new char[tracker.getCipherText().length()];

        for (int s = 0; s < tracker.size; s++) {
            HillSection hillSection = data[s];
            for (int i = 0; i < tracker.lengthSub; i++) {
                combinedDecrypted[i * tracker.size + s] = hillSection.decrypted[i];
            }
        }

        double score = Math.abs(StatIC.calculateMonoIC(combinedDecrypted) - tracker.getLanguage().getNormalisedIC()) * 1000;

        if (score < 10D) {
            HillSection section = new HillSection(score, combinedDecrypted, inverseData);
            if (tracker.bestNext.add(section)) {
                this.output(tracker, section.toString());
            }
        }
        return true;
    }

    public class HillTracker extends DecryptionTracker {

        private int size;
        private int lengthSub;
        private DynamicResultList<HillSection> resultList;
        private DynamicResultList<HillSection> bestNext;

        public HillTracker(CharSequence text, ICipherProgram app) {
            super(text, app);
            this.resultList = new DynamicResultList<HillSection>(8);
            this.bestNext = new DynamicResultList<HillSection>(8);
        }
    }
}
