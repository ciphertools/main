/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.ciphers.KeywordCipher;
import tools.cipher.ciphers.StraddleCheckerboardCipher;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.characters.CharacterCount;
import tools.cipher.lib.constants.Alphabet;

public class StraddleCheckerboardAttack extends CipherAttack<String, StraddleCheckerboardCipher> {

    public StraddleCheckerboardAttack() {
        super(null, "Straddle Checkerboard");
        this.setAttackMethods(DecryptionMethod.CALCULATED);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch (method) {
        case CALCULATED:
            DecryptionTracker tracker = new DecryptionTracker(text, app);
            List<Character> counts = CharacterCount.getOrderedCharacters(tracker.getCipherText());
            this.output(tracker, "Spare positions: %c %c", counts.get(0), counts.get(1));

            char first = counts.get(0);
            char second = counts.get(1);

            List<String> split = new ArrayList<String>();

            // int
            for (int i = 0; i < tracker.getLength(); i++) {
                if (tracker.getCipherText().charAt(i) == first || tracker.getCipherText().charAt(i) == second) {
                    split.add(tracker.getCipherText().charAt(i) + "" + tracker.getCipherText().charAt(i + 1));
                    i++;
                } else
                    split.add("" + tracker.getCipherText().charAt(i));
            }

            app.out().println("" + split);

            char[] tempText = new char[split.size()];
            HashSet<String> nonDupList = new HashSet<String>(split);
            int t = 0;

            for (String nonDup : nonDupList) {
                for (int i = 0; i < split.size(); i++)
                    if (split.get(i).equals(nonDup))
                        tempText[i] = (char) ('A' + t);

                t++;
            }

            CipherAttack<String, KeywordCipher> substitutionHack = new CipherAttack<>(new KeywordCipher(Alphabet.ALL_26_CHARS), "Straddle Checkerboard Sub").setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING).setIterations(10);
            IDecryptionTracker keywordTracker = substitutionHack.attemptAttack(new CharArrayWrapper(tempText), DecryptionMethod.SIMULATED_ANNEALING, app);

            this.updateIfBetterThanBest(tracker, keywordTracker.getBestSolution());
            keywordTracker.getProgress().reset();
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }
}
