/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.IDictionaryAttack;
import tools.cipher.base.solve.IKeySearchAttack;
import tools.cipher.ciphers.NicodemusCipher;

public class NicodemusAttack extends CipherAttack<BiKey<String, Integer>, NicodemusCipher> implements IKeySearchAttack<BiKey<String, Integer>>, IDictionaryAttack<BiKey<String, Integer>> {

    public int[] periodRange;

    public NicodemusAttack(NicodemusCipher cipher, String displayName) {
        super(cipher, displayName);
        this.setAttackMethods(DecryptionMethod.DICTIONARY, DecryptionMethod.BRUTE_FORCE, DecryptionMethod.PERIODIC_KEY);
        this.addSetting(SettingTypes.createIntRange("period_range", 2, 4, 2, 100, 1, (values, cipher2) -> {periodRange = values; cipher2.setFirstKeyDomain(builder -> builder.setRange(values));}));
        this.addSetting(SettingTypes.createIntSpinner("block_size", 5, 1, 100, 1, (values, cipher2) -> {cipher2.setSecondKeyDomain(builder -> builder.setSize(values));}));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        IDecryptionTracker tracker = this.createTracker(text, app);
        switch (method) {
        case PERIODIC_KEY:
            for (int period = this.periodRange[0]; period <= this.periodRange[1]; period++) {
                this.tryKeySearch(tracker, period);
            }
            return tracker;
        case DICTIONARY:
            return this.tryDictionaryAttack(tracker);
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public BiKey<String, Integer> useStringGetKey(IDecryptionTracker tracker, String periodicPart) {
        return this.getCipher().randomiseKey(tracker.getLength()).setFirst(periodicPart);
    }

    @Override
    public BiKey<String, Integer> useWordToGetKey(DecryptionTracker tracker, String word) {
        return this.useStringGetKey(tracker, word);
    }
}
