/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.List;
import java.util.function.Predicate;
import java.util.function.Supplier;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.IDictionaryAttack;
import tools.cipher.ciphers.CadenusCipher;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.MathUtil;
import tools.cipher.lib.result.Solution;

public class CadenusAttack extends CipherAttack<String, CadenusCipher> implements IDictionaryAttack<String> {

    public CadenusAttack() {
        super(new CadenusCipher(), "Cadenus");
        this.setAttackMethods(DecryptionMethod.DICTIONARY, DecryptionMethod.BRUTE_FORCE);
        this.addSetting(SettingTypes.createIntRange("period_range", 2, 8, 2, 100, 1, (values, cipher) -> {cipher.setDomain(builder -> builder.setRange(values));}));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch (method) {
        case DICTIONARY:
            return this.tryDictionaryAttack(new DecryptionTracker(text, app));
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public Supplier<Predicate<String>> getWordFilter(DecryptionTracker tracker) {
        final List<Integer> factors = MathUtil.getFactors(tracker.getCipherText().length() / 25);
        return () -> key -> factors.contains(key.length());
    }

    @Override
    public String useWordToGetKey(DecryptionTracker tracker, String word) {
        return word;
    }

    @Override
    public Solution toSolution(IDecryptionTracker tracker, String key) {
        int blockSize = key.length() * 25;
        char[] plainText = new char[0];
        for (int i = 0; i < tracker.getCipherText().length() / blockSize; i++)
            plainText = ArrayUtil.concat(plainText, CadenusAttack.this.getCipher().decodeEfficiently(tracker.getCipherText().subSequence(i * blockSize, (i + 1) * blockSize), key));

        return new Solution(plainText, tracker.getLanguage());
    }
}
