/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.Arrays;
import java.util.Iterator;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.settings.SettingsCache;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.ciphers.ADFGXCipher;
import tools.cipher.ciphers.ColumnarTranspositionCipher;
import tools.cipher.ciphers.KeywordCipher;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.lib.language.ILanguage;
import tools.cipher.lib.parallel.MasterThread;
import tools.cipher.lib.result.DynamicResultList;
import tools.cipher.lib.result.Result;
import tools.cipher.lib.result.ResultPositive;
import tools.cipher.lib.stats.StatIC;
import tools.cipher.util.ReadMode;

public class ADFGXAttack extends CipherAttack<TriKey<String, Integer[], ReadMode>, ADFGXCipher> {

    public final CharSequence alphabet; //Key square characters
    public final CharSequence charHolders; //ADFGX characters

    public ADFGXAttack() {
        super(new ADFGXCipher(), "ADFGX");
        this.setAttackMethods(DecryptionMethod.PERIODIC_KEY);
        this.alphabet = Alphabet.ALL_25_CHARS;
        this.charHolders = "ADFGX";
        this.addSetting(SettingTypes.createIntRange("period_range", 2, 5, 2, 100, 1, (values, cipher) -> {cipher.setSecondKeyDomain(builder -> builder.setRange(values));}));
        this.addSetting(SettingTypes.createCombo("read_mode", ReadMode.values(), (value, cipher) -> {cipher.setThirdKeyDomain(builder -> builder.setUniverse(value));}));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        ADFGXTracker tracker = new ADFGXTracker(text, app);

        switch (method) {
        case PERIODIC_KEY:
            IKeyType<Integer[]> orderedKey = this.getCipher().getSecondKeyType();

            app.getProgress().addMax(orderedKey.getNumOfKeys());

            if (SettingsCache.useParallel.get()) {
                MasterThread thread = new MasterThread((control) -> {
                    orderedKey.iterateKeys(key -> {
                        Integer[] keyCopy = ArrayUtil.copy(key);
                        Runnable job = () -> {
                            tracker.onPermute(keyCopy);
                        };

                        return !control.tryAddJob(job, 20).end();
                    });
                });
                thread.start();

                thread.waitTillCompleted(tracker::shouldStop);

            } else {
                orderedKey.iterateKeys(tracker::onPermute);
            }
            tracker.getProgress().finish();

            if (tracker.resultsList.size() < 1) {
                this.output(tracker, "No transposition order with good digraph %cIC found.", (char) 916);
                return null;
            }

            this.output(tracker, "Found %d transposition orders with good digraph %cIC.", tracker.resultsList.size(), (char) 916);
            tracker.resultsList.sort();

            int iterSA = 3;
            this.output(tracker, "Running %d SA iterations per result", iterSA);
            Iterator<ADFGXResult> iterator = tracker.resultsList.iterator();

            while (iterator.hasNext()) {
                if (tracker.shouldStop()) { break; }
                ADFGXResult section = iterator.next();

                char[] tempText = new char[section.decrypted.length / 2];

                for (int r = 0; r < this.charHolders.length(); r++) {
                    for (int c = 0; c < this.charHolders.length(); c++) {
                        for (int d = 0; d < section.decrypted.length; d += 2) {
                            if (section.decrypted[d] == this.charHolders.charAt(r) && section.decrypted[d + 1] == this.charHolders.charAt(c)) {
                                tempText[d / 2] = this.alphabet.charAt(r * this.charHolders.length() + c);
                            }
                        }
                    }
                }

                CipherAttack<String, KeywordCipher> substitutionHack = new CipherAttack<>(new KeywordCipher(Alphabet.ALL_25_CHARS), "ADFGX Sub")
                        .setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING)
                        .setIterations(iterSA)
                        .addCallback(this)
                        .mute();
                IDecryptionTracker keywordTracker = substitutionHack.attemptAttack(new CharArrayWrapper(tempText), DecryptionMethod.SIMULATED_ANNEALING, app);

                this.updateIfBetterThanBest(tracker, keywordTracker.getBestSolution());
                keywordTracker.getProgress().reset();
            }
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    public class ADFGXTracker extends DecryptionTracker {

        public final ColumnarTranspositionCipher transCipher;
        private DynamicResultList<ADFGXResult> resultsList;

        public ADFGXTracker(CharSequence text, ICipherProgram app) {
            super(text, app);
            this.resultsList = new DynamicResultList<ADFGXResult>(256);
            this.transCipher = new ColumnarTranspositionCipher();
        }

        public boolean onPermute(Integer[] data) {
            if (this.shouldStop()) { return false; }
            char[] decrypted = new char[this.getOutputTextLength(this.getCipherText().length())];
            ADFGXAttack.this.getCipher().getThirdKeyType().iterateKeys(readMode -> {
                if (this.shouldStop()) { return false; }
                this.transCipher.decodeEfficiently(this.getCipherText(), decrypted, BiKey.of(data, readMode));

                ADFGXResult section = new ADFGXResult(decrypted, this.getLanguage(), Arrays.copyOf(data, data.length));

                if (this.resultsList.add(section)) {
                    if (section.score < 5D) {
                        ADFGXAttack.this.output(this, section.toString());
                    }
                }

                this.getProgress().increment();
                return true;
            });
            return true;
        }
    }

    public static class ADFGXResult extends ResultPositive {

        public char[] decrypted;
        public Integer[] order;

        public ADFGXResult(char[] decrypted, ILanguage language, Integer[] inverseCol) {
            super(Math.abs(StatIC.calculate(new CharArrayWrapper(decrypted), 2, false) - language.getNormalisedIC()) * 1000);
            this.decrypted = decrypted;
            this.order = inverseCol;
        }

        @Override
        public String toString() {
            if (this.decrypted.length > 100) {
                char[] printDecrypt = new char[105];
                for (int i = 0; i < 50; i++)
                    printDecrypt[i] = this.decrypted[i];
                printDecrypt[50] = ' ';
                printDecrypt[51] = '.';
                printDecrypt[52] = '.';
                printDecrypt[53] = '.';
                printDecrypt[54] = ' ';
                for (int i = 0; i < 50; i++)
                    printDecrypt[i + 55] = this.decrypted[this.decrypted.length - 51 + i];

                return String.format("%s, %f, %s", Arrays.toString(this.order), this.score, new String(printDecrypt));
            } else
                return String.format("%s, %f, %s", Arrays.toString(this.order), this.score, new String(this.decrypted));
        }

        @Override
        public int compareTo(Result other) {
            int value = super.compareTo(other);

            return value == 0 ? Integer.compare(this.order.length, ((ADFGXResult)other).order.length) : value;
        }
    }
}
