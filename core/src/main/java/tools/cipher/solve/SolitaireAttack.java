/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.key.types.VariableStringKeyType;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.IDictionaryAttack;
import tools.cipher.ciphers.SolitaireCipher;
import tools.cipher.ciphers.solitaire.DeckParse;
import tools.cipher.ciphers.solitaire.Solitaire;
import tools.cipher.ciphers.solitaire.SolitaireSolver;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.ListUtil;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;
import com.alexbarter.lib.util.MathUtil;
import tools.cipher.lib.swing.DocumentUtils;

public class SolitaireAttack extends CipherAttack<Integer[], SolitaireCipher> implements IDictionaryAttack<Integer[]> {

    public static int[] deck2016 = new int[] { 38, 34, 46, 3, 4, 41, 16, 51, 19, 12, 52, 15, 29, 39, 37, 33, 42, 13, 40, 6, 26, 43, 0, 5, 32, 14, 53, 35, 17, 23, 2, 8, 50, 36, 22, -1, -1, -1, -1, -1, -1, -1, -1, 24, -1, -1, -1, -1, -1, -1, 31, -1, 28, -1 };

    private int[] periodRange;
    private JTextField passKeyStartingOrder;
    private JTextField charactersToDecode;
    private JTextField passKeyIterateOrder;

    public SolitaireAttack() {
        super(new SolitaireCipher(), "Solitaire");
        this.setAttackMethods(DecryptionMethod.BRUTE_FORCE, DecryptionMethod.PERIODIC_KEY, DecryptionMethod.DICTIONARY, DecryptionMethod.CALCULATED);
        this.addSetting(SettingTypes.createIntRange("key_period", 2, 6, 1, 25, 1, (range, cipher) -> this.periodRange = range));
        this.passKeyStartingOrder = new JTextField("0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53");
        this.charactersToDecode = new JTextField("100");
        this.passKeyIterateOrder = new JTextField("0,*,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53");
        String[] args = { "Forwards", "Backwards" };
        this.addSetting(SettingTypes.createCombo("iteration_direction", args, (value, cipher) -> {}));
    }

    @Override
    public void createSettingsUI(JDialog dialog, JPanel panel) {
        super.createSettingsUI(dialog, panel);
        ((AbstractDocument) this.charactersToDecode.getDocument()).setDocumentFilter(DocumentUtils.createTextFilter("[^0-9*,]"));
        ((AbstractDocument) this.passKeyIterateOrder.getDocument()).setDocumentFilter(DocumentUtils.createTextFilter("[^0-9*,]"));
        JLabel suitOrder = new JLabel((char) 0x2663 + " " + (char) 0x25C6 + " " + (char) 0x2665 + " " + (char) 0x2660);
        suitOrder.setFont(suitOrder.getFont().deriveFont(20F));
        panel.add(this.passKeyStartingOrder);
        panel.add(this.charactersToDecode);
        panel.add(this.passKeyIterateOrder);
        panel.add(suitOrder.add(new JLabel("Club | Diamond | Heart | Spade")));
    }

    @Override
    public DecryptionTracker tryBruteForce(IDecryptionTracker trackerIn) {
        SolitaireTracker tracker = (SolitaireTracker) trackerIn;
        DeckParse deck = new DeckParse(this.passKeyIterateOrder.getText());

        this.output(tracker, "Deck Size: %d", deck.order.length);
        this.output(tracker, "Current known order: " + ListUtil.toString(deck.order));
        this.output(tracker, "Current known order: " + ListUtil.toString(deck.order, 1));
        this.output(tracker, "No of unknowns (%d), permutations - %s", deck.countUnknowns(), CipherUtils.formatBigInteger(MathUtil.factorialBig(deck.countUnknowns())));

        if (!deck.isDeckComplete()) {
            tracker.incompleteOrder = deck.order;
            tracker.emptyIndex = deck.emptyIndex;

            this.output(tracker, "Left: %s", Arrays.toString(deck.unknownCards));

            tracker.getProgress().addMax(MathUtil.factorialBig(deck.countUnknowns()));

            KeyIterator.permuteObject(order -> this.decodeIncomplete(tracker, order), deck.unknownCards);
        } else {
            this.output(tracker, "Decrypting...\n%s", this.getCipher().decode(tracker.getCipherText(), deck.order));
        }

        return tracker;
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch (method) {
        case DICTIONARY:
            return this.tryDictionaryAttack(this.createTracker(text, app));
        case PERIODIC_KEY:
            DecryptionTracker tracker = this.createTracker(text, app);
            IKeyType<String> keyGen = VariableStringKeyType.builder().setAlphabet(Alphabet.ALL_26_CHARS).setRange(this.periodRange).create();

            app.getProgress().addMax(keyGen.getNumOfKeys());
            keyGen.iterateKeys(keyword -> {
                this.decryptAndUpdate(tracker, this.useWordToGetKey(tracker, keyword));
                tracker.getProgress().increment();
                return !app.shouldStop();
            });
            return tracker;
        case CALCULATED:
            text = text.subSequence(0, Math.min(100, text.length()));
            SolitaireSolver.startCompleteAttack(text, 7, 256 * 5, new DeckParse(passKeyStartingOrder.getText()), app.out(), 0);
            return null;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    public boolean decodeIncomplete(SolitaireTracker tracker, Integer[] order) {
        for (int i = 0; i < tracker.emptyIndex.length; i++)
            tracker.incompleteOrder[tracker.emptyIndex[tracker.direction ? i : tracker.emptyIndex.length - i - 1]] = order[i];

        //this.lastSolution = new Solution(decode(this.cipherText, tracker.incompleteOrder, this.bestSolution.score, this.getLanguage().getQuadgramData()), this.score);
        this.decryptAndUpdate(tracker, tracker.incompleteOrder);
        return !tracker.shouldStop();
    }

    public char[] decode(char[] cipherText, Integer[] cardOrder, double bestScore, NGramData quadgramData) {
        double score = 0;

        int length = cipherText.length;
        char[] plainText = new char[length];

        int index = 0;

        while (index < length) {

            cardOrder = Solitaire.nextCardOrder(cardOrder);

            int topCard = cardOrder[0];
            int keyStreamNumber;

            if (topCard == Solitaire.JOKER_B)
                topCard = Solitaire.JOKER_A;
            keyStreamNumber = cardOrder[topCard + 1];

            if (Solitaire.isJoker(keyStreamNumber))
                continue;

            plainText[index] = (char) ((52 + (cipherText[index] - 'A') - (keyStreamNumber + 1)) % 26 + 'A');
            index += 1;

            if (index > 3) {
                score += TextFitness.scoreWord(plainText, index - 4, quadgramData);
                if (score < bestScore)
                    break;
            }
        }

        return plainText;
    }

    @Override
    public DecryptionTracker createTracker(CharSequence text, ICipherProgram app) {
        return new SolitaireTracker(text, app);
    }

    public class SolitaireTracker extends DecryptionTracker {

        public Integer[] incompleteOrder;
        public Integer[] emptyIndex;
        public boolean direction;

        public SolitaireTracker(CharSequence text, ICipherProgram app) {
            super(text, app);
        }
    }

    @Override
    public Integer[] useWordToGetKey(DecryptionTracker tracker, String word) {
        return Solitaire.createCardOrder(word);
    }
}
