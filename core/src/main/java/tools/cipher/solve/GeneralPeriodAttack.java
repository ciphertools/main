/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.math.BigInteger;
import java.util.List;
import java.util.StringJoiner;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.settings.SettingsCache;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.result.Solution;

public class GeneralPeriodAttack extends CipherAttack {

    private int period;

    public GeneralPeriodAttack() {
        super(null, "General Period Subsitution");
        this.setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING);
        this.addSetting(SettingTypes.createIntSpinner("period", 5, 1, 100, 1, (value, cipher) -> GeneralPeriodAttack.this.period = value));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch(method) {
        default:
             return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public IDecryptionTracker trySimulatedAnnealing(IDecryptionTracker tracker, int iterations) {
        int rowsMin = tracker.getLength() / this.period;
        int colLeft = tracker.getLength() % this.period;
        int[] height = new int[this.period]; // Number of rows in the 'i'th column
        char[][] keysIndex = new char[this.period][26];

        List<Character> letter = tracker.getLanguage().getLetterLargestFirst();
        // TODO Generate an approximate key based off the letter frequence
        for (int p = 0; p < this.period; p++) {
            height[p] = (rowsMin + (colLeft > p ? 1 : 0)) * this.period;
            keysIndex[p] = KeyGeneration.createLongKey26().toCharArray();
//            String row = StringTransformer.getEveryNthChar(tracker.getCipherText(), p, this.period);
//            ArrayList<LetterCount> d = StringAnalyzer.countLettersInSizeOrder(row);
//
//            for (int i = 0; i < d.size(); i++) {
//                keysIndex[p][i] = d.get(i).ch;
//            }
        }

        char[] editText = tracker.getNewHolder();

        for (int p = 0; p < this.period; p++) {
            for (int i = p; i < height[p]; i += this.period) {
                char ch = tracker.getCipherText().charAt(i);
                editText[i] = (char) (ArrayUtil.indexOf(keysIndex[p], ch) + 'A');
            }
        }
        tracker.getProgress().addMax(BigInteger.valueOf((long) Math.floor((double) SettingsCache.simulatedAnnealing.get().get(0) / SettingsCache.simulatedAnnealing.get().get(1)) + 1).multiply(BigInteger.valueOf(SettingsCache.simulatedAnnealing.get().get(2).intValue())));
        System.out.println(new String(editText));
        for (int p = 0; p < this.period; p++) {
            System.out.println(new String(keysIndex[p]));
        }
        Solution bestMaximaSolution = new Solution(editText, tracker.getLanguage());
        tracker.addSolution(bestMaximaSolution);
        StringJoiner str2 = new StringJoiner(", ");
        for (int j = 0; j < this.period; j++) {
            str2.add(new CharArrayWrapper(keysIndex[j]));
        }
        bestMaximaSolution.setKeyString(str2.toString());
        this.updateIfBetterThanBest(tracker, bestMaximaSolution);

        int ite = 0;
        stop:
        while (true) {
            this.startIteration(tracker);

            double TEMP = SettingsCache.simulatedAnnealing.get().get(0);
            do {
                TEMP = Math.max(0.0D, TEMP - SettingsCache.simulatedAnnealing.get().get(1));

                for (int count = 0; count < SettingsCache.simulatedAnnealing.get().get(2); count++) {
                    if (tracker.shouldStop()) {
                        break stop;
                    }

                    for (int p = 0; p < this.period; p++) {
                        char ch1 = RandomUtil.pickRandomChar("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                        char ch2 = RandomUtil.pickRandomChar("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
                       // System.out.println(ch1 + " " + ch2);
                        for (int i = p; i < height[p]; i += this.period) {
                            if      (editText[i] == ch1) editText[i] = ch2;
                            else if (editText[i] == ch2) editText[i] = ch1;
                        }

                        tracker.setLastSolution(new Solution(editText, tracker.getLanguage()));
                        tracker.addSolution(tracker.getLastSolution());
                        //System.out.println(tracker.lastSolution + " " + bestMaximaSolution);
                        if (this.shouldAcceptSolution(TEMP, tracker.getLastSolution(), bestMaximaSolution)) {
                            bestMaximaSolution = tracker.getLastSolution();

                            char temp = keysIndex[p][ch1 - 'A'];
                            keysIndex[p][ch1 - 'A'] = keysIndex[p][ch2 - 'A'];
                            keysIndex[p][ch2 - 'A'] = temp;

                            StringJoiner str = new StringJoiner(", ");
                            for (int j = 0; j < this.period; j++) {
                                str.add(new CharArrayWrapper(keysIndex[j]));
                            }
                            bestMaximaSolution.setKeyString(str.toString());
                            this.updateIfBetterThanBest(tracker, bestMaximaSolution);

                        } else {
                            for (int i = p; i < height[p]; i += this.period) {
                                if      (editText[i] == ch1) editText[i] = ch2;
                                else if (editText[i] == ch2) editText[i] = ch1;
                            }
                        }
                    }

                    this.onPostIteration(tracker);
                }
            } while (TEMP > 0);

            tracker.getProgress().finish();
            // TODO if(this.iterationTimer)
            // this.output(tracker, "Iteration Time: %f",
            // timer.getTimeRunning(Units.Time.MILLISECOND));
            if (this.endIteration(tracker, tracker.getBestSolution())) {
                break;
            }

            this.output(tracker, "============================ Temperture Reset");
        }

        return tracker;
    }
}
