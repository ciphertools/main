/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import tools.cipher.base.ciphers.TriKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.settings.SettingsCache;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.IKeySearchAttack;
import tools.cipher.ciphers.ProgressiveCipher;

public class ProgressiveKeyAttack extends CipherAttack<TriKey<String, Integer, Integer>, ProgressiveCipher> implements IKeySearchAttack<TriKey<String, Integer, Integer>> {

    private int[] periodRange = new int[] { 2, 15 };

    public ProgressiveKeyAttack(ProgressiveCipher cipher, String displayName) {
        super(cipher, displayName);
        this.setAttackMethods(DecryptionMethod.PERIODIC_KEY);
        this.addSetting(SettingTypes.createIntRange("period_range", 2, 15, 2, 100, 1, (values, cipher2) -> {periodRange = values; cipher2.limitDomainForFirstKey(builder -> builder.setRange(values));}));
        this.addSetting(SettingTypes.createIntRange("prog_period", 1, 30, 1, 100, 1, (values, cipher2) -> {cipher2.setSecondKeyDomain(builder -> builder.setRange(values));}));
        this.addSetting(SettingTypes.createIntRange("prog_key", 1, 25, 1, 100, 1, (values, cipher2) -> {cipher2.setThirdKeyDomain(builder -> builder.setRange(values));}));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch (method) {
        case PERIODIC_KEY:
            // Settings grab
            app.getProgress().setIndeterminate(true);
            DecryptionTracker tracker = new DecryptionTracker(text, app);
            this.getCipher().getSecondKeyType().iterateKeys(progPeriod -> {
                this.getCipher().getThirdKeyType().iterateKeys(progKey -> {
                    this.periodicKey.setSecond(progPeriod);
                    this.periodicKey.setThird(progKey);
                    for (int period = this.periodRange[0]; period <= this.periodRange[1]; period++) {
                        this.tryKeySearch(tracker, period);
                    }
                    return true;
                });
                return true;
            });
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    private TriKey<String, Integer, Integer> periodicKey = TriKey.empty();

    @Override
    public TriKey<String, Integer, Integer> useStringGetKey(IDecryptionTracker tracker, String periodicPart) {
        if (SettingsCache.useParallel.get()) {
            return this.periodicKey.clone().setFirst(periodicPart);
        }
        return this.periodicKey.setFirst(periodicPart);
    }
}
