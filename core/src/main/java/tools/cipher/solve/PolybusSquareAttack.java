/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.ISimulatedAnnealingAttack;
import tools.cipher.ciphers.KeywordCipher;
import tools.cipher.ciphers.PlayfairCipher;
import tools.cipher.lib.characters.CharArrayWrapper;
import tools.cipher.lib.constants.Alphabet;

public class PolybusSquareAttack extends CipherAttack<String, PlayfairCipher> implements ISimulatedAnnealingAttack<String> {

    public PolybusSquareAttack() {
        super(null, "Polybus Square");
        this.setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING);
    }

    @Override
    public IDecryptionTracker trySimulatedAnnealing(IDecryptionTracker tracker, int iterations) {
        List<String> split = new ArrayList<String>();

        // int
        for (int i = 0; i < tracker.getLength(); i += 2) {
            split.add(tracker.getCipherText().charAt(i) + "" + tracker.getCipherText().charAt(i + 1));
        }

        this.output(tracker, split.toString());

        char[] tempText = new char[split.size()];
        HashSet<String> nonDupList = new HashSet<String>(split);
        int t = 0;
        for (String nonDup : nonDupList) {
            for (int i = 0; i < split.size(); i++) {
                if (split.get(i).equals(nonDup)) {
                    tempText[i] = Alphabet.ALL_25_CHARS.charAt(t);
                }
            }

            t++;
        }

        CipherAttack<String, KeywordCipher> substitutionHack = new CipherAttack<>(new KeywordCipher(Alphabet.ALL_25_CHARS), "Polybus Sub").setAttackMethods(DecryptionMethod.SIMULATED_ANNEALING).setIterations(5).mute();
        IDecryptionTracker keywordTracker = substitutionHack.attemptAttack(new CharArrayWrapper(tempText), DecryptionMethod.SIMULATED_ANNEALING, tracker.getApp());

        this.updateIfBetterThanBest(tracker, keywordTracker.getBestSolution());

        return tracker;
    }
}
