/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.util.Arrays;

import javax.swing.JDialog;
import javax.swing.JPanel;

import tools.cipher.base.ciphers.BiKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.ciphers.HillExtendedCipher;
import tools.cipher.lib.fitness.ChiSquared;
import com.alexbarter.lib.util.MathUtil;
import tools.cipher.lib.matrix.Matrix;
import tools.cipher.lib.matrix.MatrixNoInverse;
import tools.cipher.lib.matrix.MatrixNotSquareException;
import tools.cipher.lib.result.DynamicResultList;
import tools.cipher.lib.result.Solution;

public class HillExtendedAttack extends CipherAttack<BiKey<Matrix, Matrix>, HillExtendedCipher> {

    private int[] sizeRange = new int[] { 2, 4 };

    public HillExtendedAttack() {
        super(new HillExtendedCipher(), "Hill Extended");
        this.setAttackMethods(DecryptionMethod.BRUTE_FORCE, DecryptionMethod.PERIODIC_KEY);
        this.addSetting(SettingTypes.createIntRange("size_range", 2, 4, 2, 100, 1, (values, cipher) -> {HillExtendedAttack.this.sizeRange = values;}));
    }

    @Override
    public void createSettingsUI(JDialog dialog, JPanel panel) {
        super.createSettingsUI(dialog, panel);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        HillTracker tracker = new HillTracker(text, app);

        switch (method) {
        case PERIODIC_KEY:
            for (int size = this.sizeRange[0]; size <= this.sizeRange[1]; size++) {
                tracker.resultList.clear();
                if (tracker.getCipherText().length() % size != 0) {
                    this.output(tracker, "Matrix size of %d is not possible, length of text is not a multiple.", size);
                    continue;
                }
                tracker.size = size;
                tracker.lengthSub = tracker.getCipherText().length() / size;

                KeyIterator.iterateIntegerArray(row -> this.iterateMatrixRows(tracker, row), size + 1, 26, true);
                tracker.resultList.sort();

                if (tracker.resultList.size() < size) {
                    this.output(tracker, "Did not find enought key columns that produces good characters %d/%d", tracker.resultList.size(), size);
                } else {
                    KeyIterator.iterateObject(row -> this.iteratePossibleRows(tracker, row), size, tracker.resultList.toArray(new HillAttack.HillSection[0]));
                }
            }
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public void decryptAndUpdate(IDecryptionTracker tracker, BiKey<Matrix, Matrix> key) {
        try {
            super.decryptAndUpdate(tracker, key);
        } catch (MatrixNoInverse | MatrixNotSquareException e) {
            return;
        }
    }

    public boolean iterateMatrixRows(HillTracker tracker, Integer[] row) {
        if (MathUtil.allDivisibleBy(row, 0, tracker.size, 2, 13)) {
            return true;
        }

        char[] decrypted = new char[tracker.lengthSub];

        for (int i = 0; i < tracker.getCipherText().length(); i += tracker.size) {
            int total = 0;
            for (int s = 0; s < tracker.size; s++) {
                total += row[s] * (tracker.getCipherText().charAt(i + s) - 'A');
            }

            decrypted[i / tracker.size] = (char) ((total + (26 - row[tracker.size])) % 26 + 'A');
        }

        double score = ChiSquared.calculate(decrypted, tracker.getLanguage());

        if (tracker.resultList.add(new HillAttack.HillSection(score, decrypted, Arrays.copyOf(row, row.length)))) {
            if (score < 80D) {
                this.output(tracker, "%s, %f, %s", Arrays.toString(row), score, new String(decrypted));
            }
        }
        return true;
    }

    public boolean iteratePossibleRows(HillTracker tracker, HillAttack.HillSection[] data) {
        // Create key matrices from data
        Integer[] inverseData = new Integer[tracker.size * tracker.size];
        Integer[] extendedData = new Integer[tracker.size];
        for (int s = 0; s < tracker.size; s++) {
            HillAttack.HillSection hillResult = data[s];
            for (int n = 0; n < tracker.size; n++) {
                inverseData[s * tracker.size + n] = hillResult.inverseCol[n];
            }
            extendedData[s] = hillResult.inverseCol[tracker.size];
        }

        Matrix inverseMatrix = new Matrix(inverseData, tracker.size);

        if (!inverseMatrix.hasInverseMod(26)) {
            return true;
        }

        for (int s = 0; s < tracker.size; s++) {
            HillAttack.HillSection hillResult = data[s];
            for (int i = 0; i < tracker.lengthSub; i++) {
                tracker.getHolder()[i * tracker.size + s] = hillResult.decrypted[i];
            }
        }

        tracker.setLastSolution(new Solution(tracker.getHolder(), tracker.getLanguage()));

        this.updateIfBetterThanBest(tracker, tracker.getLastSolution(), () -> BiKey.of(inverseMatrix.inverseMod(26), new Matrix(extendedData, tracker.size, 1)));
        return true;
    }

    public class HillTracker extends DecryptionTracker {

        private int size;
        private int lengthSub;
        private DynamicResultList<HillAttack.HillSection> resultList;

        public HillTracker(CharSequence text, ICipherProgram app) {
            super(text, app);
            this.resultList = new DynamicResultList<HillAttack.HillSection>(8);
        }
    }
}
