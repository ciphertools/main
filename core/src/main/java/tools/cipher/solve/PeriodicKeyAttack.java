/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import tools.cipher.base.ciphers.UniKeyCipher;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.key.IRangedKeyType.IRangedKeyBuilder;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.IKeySearchAttack;

public class PeriodicKeyAttack<C extends UniKeyCipher<String, ? extends IRangedKeyBuilder<String>>> extends CipherAttack<String, C> implements IKeySearchAttack<String> {

    public int[] periodRange;
    private int charStep = 1;

    public PeriodicKeyAttack(C cipher, String displayName) {
        super(cipher, displayName);
        this.setAttackMethods(DecryptionMethod.PERIODIC_KEY, DecryptionMethod.BRUTE_FORCE);
        this.addSetting(SettingTypes.createIntRange("period_range", 2, 15, 2, 100, 1, (range, cipher2) -> {periodRange = range; cipher2.setDomain(builder -> builder.setRange(range)); }));
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        IDecryptionTracker tracker = this.createTracker(text, app);
        switch (method) {
        case PERIODIC_KEY:
            for (int period = this.periodRange[0]; period <= this.periodRange[1]; period++) {
                this.tryKeySearch(tracker, period);
            }
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public String useStringGetKey(IDecryptionTracker tracker, String periodicPart) {
        return periodicPart;
    }

    @Override
    public int getCharStep() {
        return this.charStep;
    }

    public PeriodicKeyAttack<C> setCharStep(int charStep) {
        this.charStep = charStep;
        return this;
    }
}
