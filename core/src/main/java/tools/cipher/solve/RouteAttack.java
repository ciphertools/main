/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import tools.cipher.base.ciphers.QuadKey;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.base.solve.ISimulatedAnnealingAttack;
import tools.cipher.ciphers.RouteTranspositionCipher;
import tools.cipher.ciphers.route.RouteCipherType;
import tools.cipher.ciphers.route.Routes;
import tools.cipher.lib.CipherUtils;
import com.alexbarter.lib.util.MathUtil;

//TODO Make it work properly
public class RouteAttack extends CipherAttack<QuadKey<Integer, Integer, RouteCipherType, RouteCipherType>, RouteTranspositionCipher> implements ISimulatedAnnealingAttack<QuadKey<Integer, Integer, RouteCipherType, RouteCipherType>> {

    public RouteAttack() {
        super(new RouteTranspositionCipher(), "Route Transposition");
        this.setAttackMethods(DecryptionMethod.BRUTE_FORCE);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        switch (method) {
        case BRUTE_FORCE:
            return this.trySimulatedAnnealing(new DecryptionTracker(text, app), 1000);
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public IDecryptionTracker tryBruteForce(IDecryptionTracker tracker) {
        BigInteger totalKeys = this.getCipher().getNumOfKeys();
        this.output(tracker, CipherUtils.formatBigInteger(totalKeys) + " Keys to search");
        tracker.getProgress().addMax(totalKeys);

        // Run in parallel if option is enabled and there are more than 1000 keys to test, overhead is not worth it otherwise
        List<Integer> factors = MathUtil.getFactors(tracker.getLength());
        factors.removeAll(Arrays.asList(1, tracker.getLength()));

        this.output(tracker, "Factors - " + factors);
        tracker.getProgress().addMax((factors.size() - 2) * (int) Math.pow(Routes.getRoutes().size(), 2));

        stop: for (int factor : factors) {
            int totalSize = tracker.getLength();
            int width = factor;
            int height = totalSize / width;

            for (RouteCipherType type2 : Routes.getRoutes()) {
                for (RouteCipherType type : Routes.getRoutes()) {
                    if (tracker.shouldStop()) {
                        break stop;
                    }

                    this.decryptAndUpdate(tracker, QuadKey.of(width, height, type2, type));
                    tracker.getProgress().increment();
                }
            }
        }
        tracker.getProgress().finish();
        return tracker;
    }
}
