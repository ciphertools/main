/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.solve;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.settings.ComponentParse;
import tools.cipher.base.settings.SettingTypes;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.base.solve.DecryptionMethod;
import tools.cipher.base.solve.DecryptionTracker;
import tools.cipher.ciphers.HillCipher;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.characters.CharacterCount;
import tools.cipher.lib.characters.StringUtils;
import tools.cipher.lib.file.FileReader;
import tools.cipher.lib.fitness.ChiSquared;
import com.alexbarter.lib.util.MathUtil;
import tools.cipher.lib.math.SimultaneousEquations;
import tools.cipher.lib.matrix.Matrix;
import tools.cipher.lib.matrix.MatrixNoInverse;
import tools.cipher.lib.matrix.MatrixNotSquareException;
import tools.cipher.lib.result.DynamicResultList;
import tools.cipher.lib.result.ResultPositive;
import tools.cipher.lib.result.Solution;
import tools.cipher.lib.swing.JSpinnerUtil;

public class HillAttack extends CipherAttack<Matrix, HillCipher> {

    public JSpinner gramSearchRange;
    public JComboBox<String> trigramSets;
    public final String[][] BI_GRAM = new String[][] { new String[] { "TH", "HE" } };
    public final String[][] TRI_GRAM = new String[][] { new String[] { "ENT", "LES", "ION" }, new String[] { "THE", "ING", "ENT" }, new String[] { "THE", "ING", "ION" }, new String[] { "THE", "ENT", "ION" }, new String[] { "ING", "ENT", "HER" }, new String[] { "ING", "ENT", "FOR" }, new String[] { "ENT", "ION", "HER" }, new String[] { "ING", "ENT", "THA" }, new String[] { "ING", "ENT", "NTH" } };
    // public final String[][] TRI_GRAM = new String[][] {new String[] {"THE",
    // "AND", "ING"}, new String[] {"THE", "ING", "ENT"}, new String[] {"THE",
    // "ING", "ION"}, new String[] {"THE", "ENT", "ION"}, new String[] {"ING",
    // "ENT", "HER"}, new String[] {"ING", "ENT", "FOR"}, new String[] {"ENT",
    // "ION", "HER"}, new String[] {"ING", "ENT", "THA"}, new String[] {"ING",
    // "ENT", "NTH"}};
    public final String[] TRI_GRAM_DISPLAY = new String[] { "THE AND ING", "THE ING ENT", "THE ING ION", "THE ENT ION", "ING ENT HER", "ING ENT FOR", "ENT ION HER", "ING ENT THA", "ING ENT NTH" };
    private int[] sizeRange = new int[] { 2, 4 };

    public HillAttack() {
        super(new HillCipher(), "Hill");
        this.setAttackMethods(DecryptionMethod.BRUTE_FORCE, DecryptionMethod.CALCULATED, DecryptionMethod.PERIODIC_KEY, DecryptionMethod.SIMULATED_ANNEALING);
        this.addSetting(SettingTypes.createIntRange("size_range", 2, 4, 2, 100, 1, (values, cipher) -> {HillAttack.this.sizeRange = values;}));
        this.gramSearchRange = JSpinnerUtil.createSpinner(20, 3, 100, 1);
        this.trigramSets = new JComboBox<String>(this.TRI_GRAM_DISPLAY);
    }

    @Override
    public void createSettingsUI(JDialog dialog, JPanel panel) {
        super.createSettingsUI(dialog, panel);
        panel.add(this.gramSearchRange);
        panel.add(this.trigramSets);
    }

    @Override
    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        HillTracker tracker = new HillTracker(text, app);

        // Settings grab
        int gramSearchRange = ComponentParse.getInteger(this.gramSearchRange);
        String[][] commonGrams = new String[][] { BI_GRAM[0], TRI_GRAM[this.trigramSets.getSelectedIndex()] };

        switch (method) {
        case CALCULATED:
            for (int size = this.sizeRange[0]; size <= this.sizeRange[1]; size++) {
                int sizeF = size;
                Map<String, Integer> chars = CharacterCount.getCount(text, size, size, false);
                List<String> sorted = new ArrayList<String>(chars.keySet());
                Collections.sort(sorted, String.CASE_INSENSITIVE_ORDER.thenComparingInt(key -> chars.get(key)).reversed());
                this.output(tracker, "" + chars.size());
                int[][] pickPattern = this.generatePickPattern(size, Math.min(gramSearchRange, sorted.size()));

                if (size == 2) {
                    for (int i = 0; i < pickPattern.length; i++) {
                        Integer[] matrixData = new Integer[0];
                        for (int k = 0; k < size; k++) {
                            Integer[][] equations = new Integer[size][size + 1];
                            for (int l = 0; l < size; l++)
                                equations[l] = this.createEquationFrom(commonGrams[size - 2][l], sorted.get(pickPattern[i][l]), k);
                            Integer[] solution = SimultaneousEquations.solve(equations, 26);
                            matrixData = ArrayUtil.concatGeneric(matrixData, solution);
                        }

                        this.decryptAndUpdate(tracker, new Matrix(matrixData, size, size));
                    }
                } else if (size == 3) {
                    try {
                        FileReader.read("/resources/commontrigrampairings.txt", line -> {
                            String[] str = StringUtils.splitInto(line, 3);

                            for (int i = 0; i < pickPattern.length; i++) {
                                Integer[] matrixData = new Integer[0];
                                for (int k = 0; k < sizeF; k++) {
                                    Integer[][] equations = new Integer[sizeF][sizeF + 1];
                                    for (int l = 0; l < sizeF; l++)
                                        equations[l] = this.createEquationFrom(str[l], sorted.get(pickPattern[i][l]), k);
                                    Integer[] solution = SimultaneousEquations.solve(equations, 26);
                                    matrixData = ArrayUtil.concatGeneric(matrixData, solution);
                                }

                                this.decryptAndUpdate(tracker, new Matrix(matrixData, sizeF, sizeF));
                            }
                        });
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            return tracker;
        case PERIODIC_KEY:
            for (int size = this.sizeRange[0]; size <= this.sizeRange[1]; size++) {
                tracker.resultList.clear();
                if (tracker.getCipherText().length() % size != 0) {
                    this.output(tracker, "Matrix size of %d is not possible, length of text is not a multiple.", size);
                    continue;
                }
                tracker.size = size;
                tracker.lengthSub = tracker.getCipherText().length() / size;

                KeyIterator.iterateIntegerArray(row -> this.iterateMatrixRows(tracker, row), size, 26, true);
                tracker.resultList.sort();

                if (tracker.resultList.size() < size) {
                    this.output(tracker, "Did not find enought key columns that produces good characters %d/%d", tracker.resultList.size(), size);
                } else {
                    KeyIterator.iterateObject(row -> this.iteratePossibleRows(tracker, row), size, tracker.resultList.toArray(new HillSection[0]));
                }
            }
            return tracker;
        default:
            return super.attemptAttack(text, method, app);
        }
    }

    @Override
    public void decryptAndUpdate(IDecryptionTracker tracker, Matrix key) {
        try {
            super.decryptAndUpdate(tracker, key);
        } catch (MatrixNoInverse | MatrixNotSquareException e) {
            return;
        }
    }

    public int[][] generatePickPattern(int size, int times) { // MathUtil.factorial(times)
        int[][] patterns = new int[(int) Math.pow(times, size)][size];

        int count = 0;

        if (size == 2) {
            for (int i = 0; i < times; i++)
                for (int j = 0; j < times; j++) {
                    if (i == j)
                        continue;
                    patterns[count++] = new int[] { i, j };
                }

            return patterns;
        } else if (size == 3) {
            for (int i = 0; i < times; i++)
                for (int j = 0; j < times; j++) {
                    for (int k = 0; k < times; k++) {
                        if (i == j || i == k || j == k)
                            continue;
                        patterns[count++] = new int[] { i, j, k };
                    }
                }

            return patterns;
        }

        return new int[0][0];
    }


    public boolean iterateMatrixRows(HillTracker tracker, Integer[] row) {
        if (MathUtil.allDivisibleBy(row, 0, tracker.size, 2, 13)) {
            return true;
        }

        char[] decrypted = new char[tracker.lengthSub];

        for (int i = 0; i < tracker.getCipherText().length(); i += tracker.size) {
            int total = 0;
            for (int s = 0; s < tracker.size; s++)
                total += row[s] * (tracker.getCipherText().charAt(i + s) - 'A');

            decrypted[i / tracker.size] = (char) (total % 26 + 'A');
        }

        double score = ChiSquared.calculate(decrypted, tracker.getLanguage());

        if (tracker.resultList.add(new HillSection(score, decrypted, Arrays.copyOf(row, row.length)))) {
            if (score < 80D) {
                this.output(tracker, "%s, %f, %s", Arrays.toString(row), score, new String(decrypted));
            }
        }
        return true;
    }

    public boolean iteratePossibleRows(HillTracker tracker, HillSection[] data) {
        // Create key matrix from data
        Integer[] inverseData = new Integer[tracker.size * tracker.size];
        for (int s = 0; s < tracker.size; s++) {
            HillSection hillResult = data[s];
            for (int n = 0; n < tracker.size; n++) {
                inverseData[s * tracker.size + n] = hillResult.inverseCol[n];
            }
        }

        Matrix inverseMatrix = new Matrix(inverseData, tracker.size);

        if (!inverseMatrix.hasInverseMod(26)) {
            return true;
        }

        for (int s = 0; s < tracker.size; s++) {
            HillSection hillResult = data[s];
            for (int i = 0; i < tracker.lengthSub; i++) {
                tracker.getHolder()[i * tracker.size + s] = hillResult.decrypted[i];
            }
        }

        tracker.setLastSolution(new Solution(tracker.getHolder(), tracker.getLanguage()));

        this.updateIfBetterThanBest(tracker, tracker.getLastSolution(), () -> inverseMatrix.inverseMod(26));
        return true;
    }

    public Integer[] createEquationFrom(String plainText, String cipherText, int index) {
        Integer[] equation = new Integer[plainText.length() + 1];
        for (int i = 0; i < equation.length - 1; i++)
            equation[i] = plainText.charAt(i) - 'A';
        equation[equation.length - 1] = cipherText.charAt(index) - 'A';
        return equation;
    }

    public class HillTracker extends DecryptionTracker {

        private int size;
        private int lengthSub;
        private DynamicResultList<HillSection> resultList;

        public HillTracker(CharSequence text, ICipherProgram app) {
            super(text, app);
            this.resultList = new DynamicResultList<HillSection>(8);
        }
    }

    public static class HillSection extends ResultPositive {
        public char[] decrypted;
        public Integer[] inverseCol;

        public HillSection(double score, char[] decrypted, Integer[] inverseCol) {
            super(score);
            this.decrypted = decrypted;
            this.inverseCol = inverseCol;
        }
    }
}
