/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import static tools.cipher.lib.test.TestUtils.assertContentEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import tools.cipher.base.format.CaseFormat.Type;
import tools.cipher.base.interfaces.IFormat;

public class FormatTest {

    @Test
    public void testBlockFormat() {
        assertContentEquals(new BlockFormat(3).format("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "ABC DEF GHI JKL MNO PQR STU VWX YZ");
        assertContentEquals(new BlockFormat(2, "|").format("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), "AB|CD|EF|GH|IJ|KL|MN|OP|QR|ST|UV|WX|YZ");

        assertThrows(IllegalArgumentException.class, () -> new BlockFormat(-1), "Negative block size should not be allowed.");
    }

    @Test
    public void testJoinedFormats() {
        IFormat format = new CaseFormat(Type.UPPER).then(new BlockFormat(5));
        assertContentEquals(format.format("ajkeUWODmThebCIapw"), "AJKEU WODMT HEBCI APW");
    }
}
