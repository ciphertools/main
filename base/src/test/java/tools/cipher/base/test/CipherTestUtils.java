/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.test;

import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.base.interfaces.ICipher;
import tools.cipher.lib.characters.StringUtils;
import tools.cipher.lib.constants.Alphabet;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

public class CipherTestUtils {

    public static void assertContentEquals(CharSequence expected, CharSequence actual) {
        assertTrue(StringUtils.isContentEqual(expected, actual), () -> getContentErrorMsg(expected, actual));
    }

    private static String getContentErrorMsg(CharSequence expected, CharSequence actual) {
        StringBuilder builder = new StringBuilder(19 + expected.length() + actual.length());
        builder.append("Expected: ");
        builder.append(expected);
        builder.append(' ');
        builder.append("Actual: ");
        builder.append(actual);
        return builder.toString();
    }

    public static <K> void fullCipherTest(ICipher<K> cipher) {
        for (int i = 0; i < 1000; i++) {
            assertCipherLogic(cipher);
        }

        for (int i = 0; i < 1000; i++) {
            assertTrue(cipher.isValid(cipher.randomiseKey()), "Cipher produces a random key that is not valid");
        }

        try  {
            assertKeyCount(cipher);
        } catch (UnsupportedOperationException e) {
            System.out.println(cipher.getClass().getSimpleName() + " has unimplemented methods");
        }
    }

    public static <K> void assertKeyCount(ICipher<K> cipher) throws UnsupportedOperationException {
        BigInteger numKeys = cipher.getNumOfKeys();
        BigInteger maxKeys = BigInteger.valueOf(1000000);

        if (numKeys.compareTo(maxKeys) > 0) {
            System.out.println(cipher.getClass().getSimpleName() + " has too many keys to iterate");
            return;
        }

        long startTime = System.currentTimeMillis();
        Counter count = new Counter();
        boolean completed = cipher.iterateKeys((key) -> {
            count.increase();
            return System.currentTimeMillis() - startTime < 1000;
        });

        if (completed) {
            assertEquals(numKeys, BigInteger.valueOf(count.getValue()), "Number of keys did not match the iterated count.");
        } else {
            System.out.println(cipher.getClass().getSimpleName() + " took too long to iterate it's keys so canceled");
        }
    }

    public static class Counter {
        int count = 0;

        public void increase() {
            this.count++;
        }

        public int getValue() {
            return this.count;
        }
    }

    public static <K> void assertCipherLogic(ICipher<K> cipher, CharSequence plainText) {
        K key = cipher.randomiseKey(plainText.length());
        plainText = cipher.normaliseText(plainText, key);
        CharSequence cipherText = cipher.encode(plainText, key);
        assertContentEquals(plainText, cipher.decode(cipherText, key));
    }

    public static <K> void assertEncodeDecode(ICipher<K> cipher, K key, CharSequence plainText, CharSequence cipherText) {
        final CharSequence normalText = cipher.normaliseText(plainText, key);

        if (cipher.deterministic()) {
            assertContentEquals(cipherText, cipher.encode(normalText, key));
        } else {
            // If the ciphertext could be different everytime then just
            // test that the encryption algorithm does not fail
            assertDoesNotThrow(() -> cipher.encode(normalText, key));
        }

        assertContentEquals(normalText, cipher.decode(cipherText, key));
    }

    public static <K> void assertCipherLogic(ICipher<K> cipher) {
        assertCipherLogic(cipher, 40, 80);
    }

    public static <K> void assertCipherLogic(ICipher<K> cipher, int minLength, int maxLength) {
        assertCipherLogic(cipher, genRandomText(minLength, maxLength));
    }

    public static String genRandomText(int minLength, int maxLength) {
        int length = RandomUtil.pickRandomInt(minLength, maxLength);
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            builder.append(RandomUtil.pickRandomChar(Alphabet.ALL_26_CHARS));
        }

        return builder.toString();
    }
}
