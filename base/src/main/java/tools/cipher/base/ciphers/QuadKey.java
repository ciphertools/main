/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.util.Objects;

public class QuadKey<F, S, T, N> implements Cloneable {

    private F firstKey;
    private S secondKey;
    private T thirdKey;
    private N fourthKey;

    public QuadKey(F firstKey, S secondKey, T thirdKey, N fourthKey) {
        this.firstKey = firstKey;
        this.secondKey = secondKey;
        this.thirdKey = thirdKey;
        this.fourthKey = fourthKey;
    }

    public QuadKey<F, S, T, N> setFirst(F first) {
        this.firstKey = first;
        return this;
    }

    public QuadKey<F, S, T, N> setSecond(S second) {
        this.secondKey = second;
        return this;
    }

    public QuadKey<F, S, T, N> setThird(T third) {
        this.thirdKey = third;
        return this;
    }

    public QuadKey<F, S, T, N> setFourth(N fourth) {
        this.fourthKey = fourth;
        return this;
    }

    public F getFirstKey() {
        return this.firstKey;
    }

    public S getSecondKey() {
        return this.secondKey;
    }

    public T getThirdKey() {
        return this.thirdKey;
    }

    public N getFourthKey() {
        return this.fourthKey;
    }

    public static <F, S, T, N> QuadKey<F, S, T, N> empty() {
        return new QuadKey<>(null, null, null, null);
    }

    public static <F, S, T, N> QuadKey<F, S, T, N> of(F first, S second, T third, N fourth) {
        return new QuadKey<>(first, second, third, fourth);
    }

    @Override
    public QuadKey<F, S, T, N> clone() {
        return new QuadKey<>(this.firstKey, this.secondKey, this.thirdKey, this.fourthKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.firstKey, this.secondKey, this.thirdKey, this.fourthKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof QuadKey)) {
            return false;
        }
        QuadKey other = (QuadKey) obj;

        return Objects.equals(this.firstKey, other.firstKey) &&
                Objects.equals(this.secondKey, other.secondKey) &&
                Objects.equals(this.thirdKey, other.thirdKey) &&
                Objects.equals(this.fourthKey, other.fourthKey);
    }
}
