/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.interfaces.ICipher;

public abstract class UniKeyCipher<T, A extends IKeyType.IKeyBuilder<T>> implements ICipher<T> {

    private final IKeyType<T> firstType;
    private IKeyType<T> firstTypeLimit;
    private final A firstKeyBuilder;

    public UniKeyCipher(A firstKey) {
        this.firstType = firstKey.create();
        this.firstTypeLimit = this.limitDomainForFirstKey(firstKey).create();
        this.firstKeyBuilder = firstKey;
    }

    @Override
    public boolean isValid(T key) {
        return this.firstType.isValid(key);
    }

    @Override
    public T randomiseKey() {
        return this.firstTypeLimit.randomise();
    }

    @Override
    public boolean iterateKeys(Function<T, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(consumer);
    }

    @Override
    public T alterKey(T key, double temp, int count) {
        return this.firstType.alterKey(key);
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.firstTypeLimit.getNumOfKeys();
    }

    @Override
    public String prettifyKey(T key) {
        return this.firstType.prettifyKey(key);
    }

    @Override
    public T parseKey(String input) throws ParseException {
        return this.firstType.parse(input);
    }

    @Nullable
    public String getHelp() {
        return this.firstType.getHelp();
    }

    public IKeyType.IKeyBuilder<T> limitDomainForFirstKey(A firstKey) {
        return firstKey;
    }

    public void setDomain(Function<A, IKeyType.IKeyBuilder<T>> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
    }

    public IKeyType<T> getDomain() {
        return this.firstTypeLimit;
    }
}
