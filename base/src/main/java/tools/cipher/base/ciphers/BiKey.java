/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.util.Objects;

public final class BiKey<F, S> implements Cloneable {

    private F firstKey;
    private S secondKey;

    public BiKey(F firstKey, S secondKey) {
        this.firstKey = firstKey;
        this.secondKey = secondKey;
    }

    public BiKey<F, S> setFirst(F first) {
        this.firstKey = first;
        return this;
    }

    public BiKey<F, S> setSecond(S second) {
        this.secondKey = second;
        return this;
    }

    public F getFirstKey() {
        return this.firstKey;
    }

    public S getSecondKey() {
        return this.secondKey;
    }

    public static <F, S> BiKey<F, S> empty() {
        return new BiKey<>(null, null);
    }

    public static <F, S> BiKey<F, S> of(F first, S second) {
        return new BiKey<>(first, second);
    }

    @Override
    public BiKey<F, S> clone() {
        return new BiKey<>(this.firstKey, this.secondKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.firstKey, this.secondKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof BiKey)) {
            return false;
        }
        BiKey other = (BiKey) obj;

        return Objects.equals(this.firstKey, other.firstKey) &&
                Objects.equals(this.secondKey, other.secondKey);
    }
}
