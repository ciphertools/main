/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.interfaces.ICipher;

public abstract class TriKeyCipher<F, S, T, A extends IKeyType.IKeyBuilder<F>, B extends IKeyType.IKeyBuilder<S>, C extends IKeyType.IKeyBuilder<T>> implements ICipher<TriKey<F, S, T>> {

    private final IKeyType<F> firstType;
    private final IKeyType<S> secondType;
    private final IKeyType<T> thirdType;
    private IKeyType<F> firstTypeLimit;
    private IKeyType<S> secondTypeLimit;
    private IKeyType<T> thirdTypeLimit;
    private final A firstKeyBuilder;
    private final B secondKeyBuilder;
    private final C thirdKeyBuilder;

    public TriKeyCipher(A firstKey, B secondKey, C thirdKey) {
        this.firstType = firstKey.create();
        this.secondType = secondKey.create();
        this.thirdType = thirdKey.create();
        this.firstTypeLimit = this.limitDomainForFirstKey(firstKey).create();
        this.secondTypeLimit = this.limitDomainForSecondKey(secondKey).create();
        this.thirdTypeLimit = this.limitDomainForThirdKey(thirdKey).create();
        this.firstKeyBuilder = firstKey;
        this.secondKeyBuilder = secondKey;
        this.thirdKeyBuilder = thirdKey;
    }

    @Override
    public boolean isValid(TriKey<F, S, T> key) {
        return this.firstType.isValid(key.getFirstKey()) && this.secondType.isValid(key.getSecondKey()) && this.thirdType.isValid(key.getThirdKey());
    }

    @Override
    public TriKey<F, S, T> randomiseKey() {
        return TriKey.of(this.firstTypeLimit.randomise(), this.secondTypeLimit.randomise(), this.thirdTypeLimit.randomise());
    }

    @Override
    public boolean iterateKeys(Function<TriKey<F, S, T>, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(f -> {
            return this.secondTypeLimit.iterateKeys(s -> {
                return this.thirdTypeLimit.iterateKeys(t -> consumer.apply(TriKey.of(f, s, t)));
            });
        });
    }

    @Override
    public TriKey<F, S, T> alterKey(TriKey<F, S, T> key, double temp, int count) {
        return TriKey.of(this.firstType.alterKey(key.getFirstKey()), this.secondType.alterKey(key.getSecondKey()), this.thirdType.alterKey(key.getThirdKey()));
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.firstTypeLimit.getNumOfKeys().multiply(this.secondTypeLimit.getNumOfKeys()).multiply(this.thirdTypeLimit.getNumOfKeys());
    }

    @Override
    public String prettifyKey(TriKey<F, S, T> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), this.secondType.prettifyKey(key.getSecondKey()), this.thirdType.prettifyKey(key.getThirdKey()));
    }

    @Override
    public TriKey<F, S, T> parseKey(String input) throws ParseException {
        String[] parts = input.split(" ");
        if (parts.length != 3) {
            throw new ParseException(input, 0);
        }

        return TriKey.of(this.firstType.parse(parts[0]), this.secondType.parse(parts[1]), this.thirdType.parse(parts[2]));
    }

    @Override
    @Nullable
    public String getHelp() {
        return String.join(" ",  this.firstType.getHelp(), this.secondType.getHelp(), this.thirdType.getHelp());
    }

    public IKeyType.IKeyBuilder<F> limitDomainForFirstKey(A firstKey) {
        return firstKey;
    }

    public IKeyType.IKeyBuilder<S> limitDomainForSecondKey(B secondKey) {
        return secondKey;
    }

    public IKeyType.IKeyBuilder<T> limitDomainForThirdKey(C thirdKey) {
        return thirdKey;
    }

    public IKeyType<F> limitDomainForFirstKey(Function<A, IKeyType.IKeyBuilder<F>> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
        return this.firstTypeLimit;
    }

    public IKeyType<S> setSecondKeyDomain(Function<B, IKeyType.IKeyBuilder<S>> secondKeyFunc) {
        this.secondTypeLimit = secondKeyFunc.apply(this.secondKeyBuilder).create();
        return this.secondTypeLimit;
    }

    public IKeyType<T> setThirdKeyDomain(Function<C, IKeyType.IKeyBuilder<T>> thirdKeyFunc) {
        this.thirdTypeLimit = thirdKeyFunc.apply(this.thirdKeyBuilder).create();
        return this.thirdTypeLimit;
    }

    public IKeyType<F> getFirstKeyType() {
        return this.firstTypeLimit;
    }

    public IKeyType<S> getSecondKeyType() {
        return this.secondTypeLimit;
    }

    public IKeyType<T> getThirdKeyType() {
        return this.thirdTypeLimit;
    }
}
