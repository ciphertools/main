/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.util.Objects;

public class QuinKey<F, S, T, N, Q> implements Cloneable {

    private F firstKey;
    private S secondKey;
    private T thirdKey;
    private N fourthKey;
    private Q fifthKey;

    public QuinKey(F firstKey, S secondKey, T thirdKey, N fourthKey, Q fifthKey) {
        this.firstKey = firstKey;
        this.secondKey = secondKey;
        this.thirdKey = thirdKey;
        this.fourthKey = fourthKey;
        this.fifthKey = fifthKey;
    }

    public QuinKey<F, S, T, N, Q> setFirst(F first) {
        this.firstKey = first;
        return this;
    }

    public QuinKey<F, S, T, N, Q> setSecond(S second) {
        this.secondKey = second;
        return this;
    }

    public QuinKey<F, S, T, N, Q> setThird(T third) {
        this.thirdKey = third;
        return this;
    }

    public QuinKey<F, S, T, N, Q> setFourth(N fourth) {
        this.fourthKey = fourth;
        return this;
    }

    public QuinKey<F, S, T, N, Q> setFifth(Q fifth) {
        this.fifthKey = fifth;
        return this;
    }

    public F getFirstKey() {
        return this.firstKey;
    }

    public S getSecondKey() {
        return this.secondKey;
    }

    public T getThirdKey() {
        return this.thirdKey;
    }

    public N getFourthKey() {
        return this.fourthKey;
    }

    public Q getFifthKey() {
        return this.fifthKey;
    }

    public static <F, S, T, N, Q> QuinKey<F, S, T, N, Q> empty() {
        return new QuinKey<>(null, null, null, null, null);
    }

    public static <F, S, T, N, Q> QuinKey<F, S, T, N, Q> of(F first, S second, T third, N fourth, Q fifth) {
        return new QuinKey<>(first, second, third, fourth, fifth);
    }

    @Override
    public QuinKey<F, S, T, N, Q> clone() {
        return new QuinKey<>(this.firstKey, this.secondKey, this.thirdKey, this.fourthKey, this.fifthKey);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.firstKey, this.secondKey, this.thirdKey, this.fourthKey, this.fifthKey);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof QuinKey)) {
            return false;
        }
        QuinKey other = (QuinKey) obj;

        return Objects.equals(this.firstKey, other.firstKey) &&
                Objects.equals(this.secondKey, other.secondKey) &&
                Objects.equals(this.thirdKey, other.thirdKey) &&
                Objects.equals(this.fourthKey, other.fourthKey) &&
                Objects.equals(this.fifthKey, other.fifthKey);
    }
}
