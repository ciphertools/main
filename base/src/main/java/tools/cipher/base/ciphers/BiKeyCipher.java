/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.interfaces.IKeyType;

public abstract class BiKeyCipher<F, S, A extends IKeyType.IKeyBuilder<F>, B extends IKeyType.IKeyBuilder<S>> implements ICipher<BiKey<F, S>> {

    protected final IKeyType<F> firstType;
    protected final IKeyType<S> secondType;
    private IKeyType<F> firstTypeLimit;
    private IKeyType<S> secondTypeLimit;
    private final A firstKeyBuilder;
    private final B secondKeyBuilder;

    public BiKeyCipher(A firstKey, B secondKey) {
        this.firstType = firstKey.create();
        this.secondType = secondKey.create();
        this.firstTypeLimit = this.limitDomainForFirstKey(firstKey).create();
        this.secondTypeLimit = this.limitDomainForSecondKey(secondKey).create();
        this.firstKeyBuilder = firstKey;
        this.secondKeyBuilder = secondKey;
    }

    @Override
    public boolean isValid(BiKey<F, S> key) {
        return this.firstType.isValid(key.getFirstKey()) && this.secondType.isValid(key.getSecondKey());
    }

    @Override
    public BiKey<F, S> randomiseKey() {
        return BiKey.of(this.firstTypeLimit.randomise(), this.secondTypeLimit.randomise());
    }

    @Override
    public boolean iterateKeys(Function<BiKey<F, S>, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(f ->
            this.secondTypeLimit.iterateKeys(s -> consumer.apply(BiKey.of(f, s)))
        );
    }

    @Override
    public BiKey<F, S> alterKey(BiKey<F, S> key, double temp, int count) {
        return BiKey.of(this.firstType.alterKey(key.getFirstKey()), this.secondType.alterKey(key.getSecondKey()));
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.firstTypeLimit.getNumOfKeys().multiply(this.secondTypeLimit.getNumOfKeys());
    }

    @Override
    public String prettifyKey(BiKey<F, S> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), this.secondType.prettifyKey(key.getSecondKey()));
    }

    @Override
    public BiKey<F, S> parseKey(String input) throws ParseException {
        String[] parts = input.split(" ");
        if (parts.length != 2) {
            throw new ParseException(input, 0);
        }

        return BiKey.of(this.firstType.parse(parts[0]), this.secondType.parse(parts[1]));
    }

    @Nullable
    public String getHelp() {
        return String.join(" ",  this.firstType.getHelp(), this.secondType.getHelp());
    }

    protected IKeyType.IKeyBuilder<F> limitDomainForFirstKey(A firstKey) {
        return firstKey;
    }

    protected IKeyType.IKeyBuilder<S> limitDomainForSecondKey(B secondKey) {
        return secondKey;
    }

    public void setFirstKeyDomain(Function<A, IKeyType.IKeyBuilder<F>> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
    }

    public void setSecondKeyDomain(Function<B, IKeyType.IKeyBuilder<S>> secondKeyFunc) {
        this.secondTypeLimit = secondKeyFunc.apply(this.secondKeyBuilder).create();
    }

    public IKeyType<F> getFirstKeyType() {
        return this.firstTypeLimit;
    }

    public IKeyType<S> getSecondKeyType() {
        return this.secondTypeLimit;
    }
}
