/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.util.function.Function;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.lib.characters.CharArrayWrapper;

/**
 * @author Alex
 *
 */
public class CipherChain<K, H, C extends ICipher<K>, S extends ICipher<H>> implements ICipher<BiKey<K, H>> {

    private C cipher1;
    private S cipher2;

    private CipherChain(C cipher1, S cipher2) {
        this.cipher1 = cipher1;
        this.cipher2 = cipher2;
    }

    public static <K, H, C extends ICipher<K>, S extends ICipher<H>> CipherChain<K, H, C, S> chain(C cipher1, S cipher2) {
        return new CipherChain<>(cipher1, cipher2);
    }

    @Override
    public CharSequence encode(CharSequence plainText, BiKey<K, H> key) {
        return this.cipher2.encode(this.cipher1.encode(plainText, key.getFirstKey()), key.getSecondKey());
    }

    @Override
    public char[] decodeEfficiently(CharSequence cipherText, char[] plainText, BiKey<K, H> key) {
        return this.cipher1.decodeEfficiently(new CharArrayWrapper(this.cipher2.decodeEfficiently(cipherText, new char[cipherText.length()], key.getSecondKey())), plainText, key.getFirstKey());
    }

    @Override
    public BiKey<K, H> randomiseKey() {
        return BiKey.of(this.cipher1.randomiseKey(), this.cipher2.randomiseKey());
    }

    @Override
    public boolean iterateKeys(Function<BiKey<K, H>, Boolean> consumer) {
        return this.cipher1.iterateKeys(k -> {
            return this.cipher2.iterateKeys(h -> { return consumer.apply(BiKey.of(k, h)); });
        });
    }

    @Override
    public boolean isValid(BiKey<K, H> key) {
        return this.cipher1.isValid(key.getFirstKey()) && this.cipher2.isValid(key.getSecondKey());
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.cipher1.getNumOfKeys().multiply(this.cipher2.getNumOfKeys());
    }
}
