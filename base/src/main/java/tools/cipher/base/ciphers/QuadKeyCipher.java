/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.interfaces.IKeyType;

public abstract class QuadKeyCipher<F, S, T, N, A extends IKeyType.IKeyBuilder<F>, B extends IKeyType.IKeyBuilder<S>, C extends IKeyType.IKeyBuilder<T>, D extends IKeyType.IKeyBuilder<N>> implements ICipher<QuadKey<F, S, T, N>> {

    private final IKeyType<F> firstType;
    private final IKeyType<S> secondType;
    private final IKeyType<T> thirdType;
    private final IKeyType<N> fourthType;
    private IKeyType<F> firstTypeLimit;
    private IKeyType<S> secondTypeLimit;
    private IKeyType<T> thirdTypeLimit;
    private IKeyType<N> fourthTypeLimit;
    private final A firstKeyBuilder;
    private final B secondKeyBuilder;
    private final C thirdKeyBuilder;
    private final D fourthKeyBuilder;

    public QuadKeyCipher(A firstKey, B secondKey, C thirdKey, D fourthKey) {
        this.firstType = firstKey.create();
        this.secondType = secondKey.create();
        this.thirdType = thirdKey.create();
        this.fourthType = fourthKey.create();
        this.firstTypeLimit = this.limitDomainForFirstKey(firstKey).create();
        this.secondTypeLimit = this.limitDomainForSecondKey(secondKey).create();
        this.thirdTypeLimit = this.limitDomainForThirdKey(thirdKey).create();
        this.fourthTypeLimit = this.limitDomainForFourthKey(fourthKey).create();
        this.firstKeyBuilder = firstKey;
        this.secondKeyBuilder = secondKey;
        this.thirdKeyBuilder = thirdKey;
        this.fourthKeyBuilder = fourthKey;
    }

    @Override
    public boolean isValid(QuadKey<F, S, T, N> key) {
        return this.firstType.isValid(key.getFirstKey()) && this.secondType.isValid(key.getSecondKey()) && this.thirdType.isValid(key.getThirdKey()) && this.fourthType.isValid(key.getFourthKey());
    }

    @Override
    public QuadKey<F, S, T, N> randomiseKey() {
        return QuadKey.of(this.firstTypeLimit.randomise(), this.secondTypeLimit.randomise(), this.thirdTypeLimit.randomise(), this.fourthTypeLimit.randomise());
    }

    @Override
    public boolean iterateKeys(Function<QuadKey<F, S, T, N>, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(f -> {
            return this.secondTypeLimit.iterateKeys(s -> {
                return this.thirdTypeLimit.iterateKeys(t -> {
                    return this.fourthTypeLimit.iterateKeys(n -> consumer.apply(QuadKey.of(f, s, t, n)));
                });
            });
        });
    }

    @Override
    public QuadKey<F, S, T, N> alterKey(QuadKey<F, S, T, N> key, double temp, int count) {
        return QuadKey.of(this.firstType.alterKey(key.getFirstKey()), this.secondType.alterKey(key.getSecondKey()), this.thirdType.alterKey(key.getThirdKey()), this.fourthType.alterKey(key.getFourthKey()));
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.firstTypeLimit.getNumOfKeys().multiply(this.secondTypeLimit.getNumOfKeys()).multiply(this.thirdTypeLimit.getNumOfKeys()).multiply(this.fourthTypeLimit.getNumOfKeys());
    }

    @Override
    public String prettifyKey(QuadKey<F, S, T, N> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), this.secondType.prettifyKey(key.getSecondKey()), this.thirdType.prettifyKey(key.getThirdKey()), this.fourthType.prettifyKey(key.getFourthKey()));
    }

    @Override
    public QuadKey<F, S, T, N> parseKey(String input) throws ParseException {
        String[] parts = input.split(" ");
        if (parts.length != 4) {
            throw new ParseException(input, 0);
        }

        return QuadKey.of(this.firstType.parse(parts[0]), this.secondType.parse(parts[1]), this.thirdType.parse(parts[2]), this.fourthType.parse(parts[3]));
    }

    @Nullable
    public String getHelp() {
        return String.join(" ",  this.firstType.getHelp(), this.secondType.getHelp(), this.thirdType.getHelp(), this.fourthType.getHelp());
    }

    public IKeyType.IKeyBuilder<F> limitDomainForFirstKey(A firstKey) {
        return firstKey;
    }

    public IKeyType.IKeyBuilder<S> limitDomainForSecondKey(B secondKey) {
        return secondKey;
    }

    public IKeyType.IKeyBuilder<T> limitDomainForThirdKey(C thirdKey) {
        return thirdKey;
    }

    public IKeyType.IKeyBuilder<N> limitDomainForFourthKey(D fourthKey) {
        return fourthKey;
    }

    public IKeyType<F> getFirstKeyType() {
        return this.firstTypeLimit;
    }

    public IKeyType<S> getSecondKeyType() {
        return this.secondTypeLimit;
    }

    public IKeyType<T> getThirdKeyType() {
        return this.thirdTypeLimit;
    }

    public IKeyType<N> getFourthKeyType() {
        return this.fourthTypeLimit;
    }

    public QuadKeyCipher<F, S, T, N, A, B, C, D> limitDomainForFirstKey(Function<A, IKeyType.IKeyBuilder<F>> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
        return this;
    }

    public QuadKeyCipher<F, S, T, N, A, B, C, D> setSecondKeyDomain(Function<B, IKeyType.IKeyBuilder<S>> secondKeyFunc) {
        this.secondTypeLimit = secondKeyFunc.apply(this.secondKeyBuilder).create();
        return this;
    }

    public QuadKeyCipher<F, S, T, N, A, B, C, D> setThirdKeyDomain(Function<C, IKeyType.IKeyBuilder<T>> thirdKeyFunc) {
        this.thirdTypeLimit = thirdKeyFunc.apply(this.thirdKeyBuilder).create();
        return this;
    }

    public QuadKeyCipher<F, S, T, N, A, B, C, D> setFourthKeyDomain(Function<D, IKeyType.IKeyBuilder<N>> fourthKeyFunc) {
        this.fourthTypeLimit = fourthKeyFunc.apply(this.fourthKeyBuilder).create();
        return this;
    }
}
