/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.ciphers;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.interfaces.ICipher;

public abstract class QuinKeyCipher<F, S, T, N, Q, A extends IKeyType.IKeyBuilder<F>, B extends IKeyType.IKeyBuilder<S>, C extends IKeyType.IKeyBuilder<T>, D extends IKeyType.IKeyBuilder<N>, E extends IKeyType.IKeyBuilder<Q>> implements ICipher<QuinKey<F, S, T, N, Q>> {

    private final IKeyType<F> firstType;
    private final IKeyType<S> secondType;
    private final IKeyType<T> thirdType;
    private final IKeyType<N> fourthType;
    private final IKeyType<Q> fifthType;
    private IKeyType<F> firstTypeLimit;
    private IKeyType<S> secondTypeLimit;
    private IKeyType<T> thirdTypeLimit;
    private IKeyType<N> fourthTypeLimit;
    private IKeyType<Q> fifthTypeLimit;
    private final A firstKeyBuilder;
    private final B secondKeyBuilder;
    private final C thirdKeyBuilder;
    private final D fourthKeyBuilder;
    private final E fifthKeyBuilder;

    public QuinKeyCipher(A firstKey, B secondKey, C thirdKey, D fourthKey, E fifthKey) {
        this.firstType = firstKey.create();
        this.secondType = secondKey.create();
        this.thirdType = thirdKey.create();
        this.fourthType = fourthKey.create();
        this.fifthType = fifthKey.create();
        this.firstTypeLimit = this.limitDomainForFirstKey(firstKey).create();
        this.secondTypeLimit = this.limitDomainForSecondKey(secondKey).create();
        this.thirdTypeLimit = this.limitDomainForThirdKey(thirdKey).create();
        this.fourthTypeLimit = this.limitDomainForFourthKey(fourthKey).create();
        this.fifthTypeLimit = this.limitDomainForFifthKey(fifthKey).create();
        this.firstKeyBuilder = firstKey;
        this.secondKeyBuilder = secondKey;
        this.thirdKeyBuilder = thirdKey;
        this.fourthKeyBuilder = fourthKey;
        this.fifthKeyBuilder = fifthKey;
    }

    @Override
    public boolean isValid(QuinKey<F, S, T, N, Q> key) {
        return this.firstType.isValid(key.getFirstKey()) && this.secondType.isValid(key.getSecondKey()) && this.thirdType.isValid(key.getThirdKey()) && this.fourthType.isValid(key.getFourthKey()) && this.fifthType.isValid(key.getFifthKey());
    }

    @Override
    public QuinKey<F, S, T, N, Q> randomiseKey() {
        return QuinKey.of(this.firstTypeLimit.randomise(), this.secondTypeLimit.randomise(), this.thirdTypeLimit.randomise(), this.fourthTypeLimit.randomise(), this.fifthTypeLimit.randomise());
    }

    @Override
    public boolean iterateKeys(Function<QuinKey<F, S, T, N, Q>, Boolean> consumer) {
        return this.firstTypeLimit.iterateKeys(f -> {
            return this.secondTypeLimit.iterateKeys(s -> {
                return this.thirdTypeLimit.iterateKeys(t -> {
                    return this.fourthTypeLimit.iterateKeys(n -> {
                        return this.fifthTypeLimit.iterateKeys(q -> consumer.apply(QuinKey.of(f, s, t, n, q)));
                    });
                });
            });
        });
    }

    @Override
    public QuinKey<F, S, T, N, Q> alterKey(QuinKey<F, S, T, N, Q> key, double temp, int count) {
        return QuinKey.of(this.firstType.alterKey(key.getFirstKey()), this.secondType.alterKey(key.getSecondKey()), this.thirdType.alterKey(key.getThirdKey()), this.fourthType.alterKey(key.getFourthKey()), this.fifthType.alterKey(key.getFifthKey()));
    }

    @Override
    public BigInteger getNumOfKeys() {
        return this.firstTypeLimit.getNumOfKeys().multiply(this.secondTypeLimit.getNumOfKeys()).multiply(this.thirdTypeLimit.getNumOfKeys()).multiply(this.fourthTypeLimit.getNumOfKeys()).multiply(this.fifthTypeLimit.getNumOfKeys());
    }

    @Override
    public String prettifyKey(QuinKey<F, S, T, N, Q> key) {
        return String.join(" ",  this.firstType.prettifyKey(key.getFirstKey()), this.secondType.prettifyKey(key.getSecondKey()), this.thirdType.prettifyKey(key.getThirdKey()), this.fourthType.prettifyKey(key.getFourthKey()), this.fifthType.prettifyKey(key.getFifthKey()));
    }

    @Override
    public QuinKey<F, S, T, N, Q> parseKey(String input) throws ParseException {
        String[] parts = input.split(" ");
        if (parts.length != 5) {
            throw new ParseException(input, 0);
        }

        return QuinKey.of(this.firstType.parse(parts[0]), this.secondType.parse(parts[1]), this.thirdType.parse(parts[2]), this.fourthType.parse(parts[3]), this.fifthType.parse(parts[4]));
    }

    @Override
    @Nullable
    public String getHelp() {
        return String.join(" ",  this.firstType.getHelp(), this.secondType.getHelp(), this.thirdType.getHelp(), this.fourthType.getHelp(), this.fifthType.getHelp());
    }

    public IKeyType.IKeyBuilder<F> limitDomainForFirstKey(A firstKey) {
        return firstKey;
    }

    public IKeyType.IKeyBuilder<S> limitDomainForSecondKey(B secondKey) {
        return secondKey;
    }

    public IKeyType.IKeyBuilder<T> limitDomainForThirdKey(C thirdKey) {
        return thirdKey;
    }

    public IKeyType.IKeyBuilder<N> limitDomainForFourthKey(D fourthKey) {
        return fourthKey;
    }

    public IKeyType.IKeyBuilder<Q> limitDomainForFifthKey(E fifthKey) {
        return fifthKey;
    }

    public IKeyType<F> getFirstKeyType() {
        return this.firstTypeLimit;
    }

    public IKeyType<S> getSecondKeyType() {
        return this.secondTypeLimit;
    }

    public IKeyType<T> getThirdKeyType() {
        return this.thirdTypeLimit;
    }

    public IKeyType<N> getFourthKeyType() {
        return this.fourthTypeLimit;
    }

    public IKeyType<Q> getFifthKeyType() {
        return this.fifthTypeLimit;
    }

    public A limitDomainForFirstKey(Function<A, IKeyType.IKeyBuilder<F>> firstKeyFunc) {
        this.firstTypeLimit = firstKeyFunc.apply(this.firstKeyBuilder).create();
        return this.firstKeyBuilder;
    }

    public B setSecondKeyLimit(Function<B, IKeyType.IKeyBuilder<S>> secondKeyFunc) {
        this.secondTypeLimit = secondKeyFunc.apply(this.secondKeyBuilder).create();
        return this.secondKeyBuilder;
    }

    public C setThirdKeyLimit(Function<C, IKeyType.IKeyBuilder<T>> thirdKeyFunc) {
        this.thirdTypeLimit = thirdKeyFunc.apply(this.thirdKeyBuilder).create();
        return this.thirdKeyBuilder;
    }

    public D setFourthKeyLimit(Function<D, IKeyType.IKeyBuilder<N>> fourthKeyFunc) {
        this.fourthTypeLimit = fourthKeyFunc.apply(this.fourthKeyBuilder).create();
        return this.fourthKeyBuilder;
    }

    public E setFifthKeyLimit(Function<E, IKeyType.IKeyBuilder<Q>> fifthKeyFunc) {
        this.fifthTypeLimit = fifthKeyFunc.apply(this.fifthKeyBuilder).create();
        return this.fifthKeyBuilder;
    }
}
