/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import java.util.ArrayList;
import java.util.List;

import tools.cipher.base.interfaces.IFormat;

public class MaintainFormat implements IFormat {

    private CharSequence originalText;
    public List<Character> maintain;

    public MaintainFormat(CharSequence originalText, List<Character> maintain) {
        this.originalText = originalText;
        this.maintain = maintain;
    }

    public static List<Integer> getIndexOfSpaces(CharSequence text, List<Character> maintain) {
        List<Integer> indices = new ArrayList<>();
        for (int i = 0; i < text.length(); i++) {
            if (maintain.contains(text.charAt(i))) {
                indices.add(i);
            }
        }

        return indices;
    }

    @Override
    public CharSequence format(CharSequence text) {
        if (this.maintain.isEmpty()) {
            return text;
        }

        if (this.originalText.length() < text.length()) {
            System.out.println("Error");
        }

        StringBuilder builder = new StringBuilder(text.length());

        int indexText = 0;
        while (indexText < text.length()) {
            char ch = this.originalText.charAt(builder.length());

            if (this.maintain.contains(ch)) {
                builder.append(ch);
            } else {
                builder.append(text.charAt(indexText));
                indexText++;
            }
        }

        return builder;
    }

    public static final IParseFormat PARSE = (text, args) -> {
        List<Character> maintain = new ArrayList<>(args.length());

        for (int i = 0; i < args.length(); i++) {
            maintain.add(args.charAt(i));
        }

        return new MaintainFormat(text, maintain);
    };
}
