/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import java.util.Locale;

import tools.cipher.base.interfaces.IFormat;
import tools.cipher.base.format.CaseFormat.Type;
import tools.cipher.lib.registry.IRegistry;
import tools.cipher.lib.registry.Registry;

public class Formats {

    public static IFormat BLOCK_5 = new BlockFormat(5);

    public static IRegistry<String, IFormat> FORMATS = Registry.builder(IFormat.class).addValidation((k, v) -> !k.contains(":")).build();
    public static IRegistry<String, IParseFormat> FORMAT_PARSE = Registry.builder(IParseFormat.class).addValidation((k, v) -> !k.contains(":")).build();

    public static void registryFormats() {

        FORMAT_PARSE.register("block", BlockFormat.PARSE);
        FORMAT_PARSE.register("case", CaseFormat.PARSE);
        FORMAT_PARSE.register("pad", PadFormat.PARSE);
        FORMAT_PARSE.register("maintain", MaintainFormat.PARSE);

        for (int i = 1; i < 20; i++) {
            FORMATS.register("block_"+i, new BlockFormat(i));
        }

        for (Type type : Type.values()) {
            FORMATS.register("case_" + type.name().toLowerCase(Locale.UK), new CaseFormat(type));
        }

        FORMATS.register("pad", new PadFormat(5, 'X'));
    }
}
