/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import java.text.ParseException;

import tools.cipher.base.interfaces.IFormat;

public class PadFormat implements IFormat {

    private int multiple;
    private Character pad;

    public PadFormat(int multipleIn, Character padIn) {
        if (multipleIn < 0) {
            throw new IllegalArgumentException();
        }

        this.multiple = multipleIn;
        this.pad = padIn;
    }

    public PadFormat(int sizeIn) {
        this(sizeIn, 'X');
    }

    @Override
    public CharSequence format(CharSequence text) {
        if (text.length() % this.multiple != 0) {
            StringBuilder builder = new StringBuilder(text.length() + this.multiple - (text.length() % this.multiple));
            builder.append(text);
            while (builder.length() % this.multiple != 0) {
                builder.append(this.pad);
            }

            return builder;
        } else {
            return text;
        }
    }

    public static final IParseFormat PARSE = (text, args) -> {
        String[] split = args.split(":");
        int size = 0;
        try {
            size = Integer.valueOf(split[0]);

            if (size > 100) {
                throw new IllegalArgumentException("Size > 100 is too big.");
            }
        } catch(NumberFormatException e) {
            throw new ParseException(split[0], 0);
        }

        char padChar = 'X';

        if (split.length > 1) {
            if (split[1].length() == 1) {
                padChar = split[1].charAt(0);
            } else {
                throw new ParseException("Only one character should be supplied - " + split[1], 1);
            }
        }

        return new PadFormat(size, padChar);
    };
}
