/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import java.text.ParseException;

import tools.cipher.base.interfaces.IFormat;

public class CaseFormat implements IFormat {

    private Type charCase;

    public CaseFormat(Type caseIn) {
        this.charCase = caseIn;
    }

    @Override
    public CharSequence format(CharSequence text) {
        StringBuilder builder = new StringBuilder(text.length());

        for (int i = 0; i < text.length(); i++) {
            builder.append(this.charCase.apply(text, text.charAt(i), i));
        }

        return builder;
    }

    @FunctionalInterface
    public interface CaseFormatLogic {

        public char format(CharSequence text, char ch, int index);
    }

    public enum Type {
        UPPER((t, c, i) -> Character.toUpperCase(c)),
        LOWER((t, c, i) -> Character.toLowerCase(c)),
        START((t, c, i) -> ((i == 0 || t.charAt(i - 1) == ' ') ? Type.UPPER : Type.LOWER).apply(t, c, i));

        private CaseFormatLogic func;

        Type(CaseFormatLogic func) {
            this.func = func;
        }

        public char apply(CharSequence text, char ch, int index) {
            return this.func.format(text, ch, index);
        }
    }

    private static final CaseFormat UPPER_FORMAT = new CaseFormat(Type.UPPER);
    private static final CaseFormat LOWER_FORMAT = new CaseFormat(Type.LOWER);
    private static final CaseFormat START_FORMAT = new CaseFormat(Type.START);

    public static final IParseFormat PARSE = (text, args) -> {
        switch(args) {
        case "upper": return UPPER_FORMAT;
        case "lower": return LOWER_FORMAT;
        case "start": return START_FORMAT;
        }

        throw new ParseException("Unrecognised case type - " + args, 0);
    };
}
