/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.format;

import java.text.ParseException;

import tools.cipher.base.interfaces.IFormat;

public class BlockFormat implements IFormat {

    private final int size;
    private final CharSequence join;

    public BlockFormat(int sizeIn, CharSequence joinIn) {
        if (sizeIn < 0) {
            throw new IllegalArgumentException();
        }

        this.size = sizeIn;
        this.join = joinIn;
    }

    public BlockFormat(int sizeIn) {
        this(sizeIn, " ");
    }

    @Override
    public CharSequence format(CharSequence text) {
        StringBuilder builder = new StringBuilder();

        int count = 0; // The amount of no-space characters we have added
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);

            if (ch != ' ') {
                if (this.size > 0 && count % this.size == 0 && count > 0) {
                    builder.append(this.join);
                }
                builder.append(ch);
                count++;
            }
        }

        return builder;
    }

    public static final IParseFormat PARSE = (text, args) -> {
        int size = 0;
        try {
            size = Integer.parseInt(args);
        } catch(NumberFormatException e) {
            throw new ParseException(args, 0);
        }

        return new BlockFormat(size);
    };
}
