/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.settings;

import java.util.Arrays;
import java.util.List;

import tools.cipher.base.interfaces.ISettings;
import tools.cipher.lib.language.ILanguage;
import tools.cipher.lib.language.Languages;

public class SettingsCache {

    private static boolean loaded = false;

    public static SettingCache<ILanguage> language;
    public static SettingCache<Boolean> checkShift;
    public static SettingCache<Boolean> checkReverse;
    public static SettingCache<Boolean> checkRoutes;
    public static SettingCache<Boolean> useParallel;
    public static SettingCache<Boolean> updateProgress;
    public static SettingCache<Boolean> collectSolutions;
    public static SettingCache<List<Double>> simulatedAnnealing;

    public static SettingCache<Integer> keywordCreationId;

    public static void onLoad(ISettings settings) {
        SettingsCache.language = SettingCache.create(settings, "language", Languages::byName, ILanguage::getName, String.class, Languages.ENGLISH);
        SettingsCache.useParallel = SettingCache.create(settings, "use_parallel", true);
        SettingsCache.updateProgress = SettingCache.create(settings, "update_progress", true);
        SettingsCache.collectSolutions = SettingCache.create(settings, "collect_solutions", false);

        SettingsCache.checkShift = SettingCache.create(settings, "check_shift", true);
        SettingsCache.checkReverse = SettingCache.create(settings, "check_reverse", true);
        SettingsCache.checkRoutes = SettingCache.create(settings, "check_routes", true);
        SettingsCache.simulatedAnnealing = SettingCache.create(settings, "simulated_annealing", Arrays.asList(20.0D, 0.1D, 500.0D));

        SettingsCache.keywordCreationId = SettingCache.create(settings, "keyword_creation_id", 0);

        SettingsCache.loaded = true;
    }
}
