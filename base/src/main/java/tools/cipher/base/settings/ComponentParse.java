/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.settings;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.text.JTextComponent;

public class ComponentParse {

    public static int[] getIntegerRange(JTextComponent textComponent) {
        int[] range = new int[2];

        String text = textComponent.getText().replaceAll("[^-0-9]", "");

        if (!text.contains("-")) {
            range[0] = Integer.valueOf(text);
            range[1] = Integer.valueOf(text);
        } else {
            range[0] = Integer.valueOf(text.split("-")[0]);
            range[1] = Integer.valueOf(text.split("-")[1]);
        }

        return range;
    }

    public static int getInteger(JTextComponent textComponent) {
        String text = textComponent.getText().replaceAll("[^0-9]", "");

        return Integer.valueOf(text);

    }

    public static int[] getIntegerRange(JSpinner[] spinners) {
        return new int[] { ((Number) spinners[0].getValue()).intValue(), ((Number) spinners[1].getValue()).intValue() };
    }

    public static int getInteger(JSpinner spinners) {
        return ((Number) spinners.getValue()).intValue();
    }

    public static boolean getBooleanValue(JComboBox<Boolean> comboBox) {
        return (Boolean) comboBox.getSelectedItem();
    }
}
