/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.settings;

import java.awt.Container;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

import javax.swing.JComboBox;
import javax.swing.JSpinner;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.solve.CipherAttack;
import tools.cipher.lib.swing.JSpinnerUtil;

public class SettingTypes {

    public static <K, C extends ICipher<K>, E> ICipherSettingProvider<K, C> createSpinner(String id, E[] items, BiConsumer<E, C> action) {
        return new ICipherSettingSpinner<K, C, E>(id).set(items).setAction(action);
    }

    public static <K, C extends ICipher<K>> ICipherSettingProvider<K, C> createIntSpinner(String id, int start, int min, int max, int step, BiConsumer<Integer, C> action) {
        return new ICipherSettingIntSpinner<K, C>(id).set(start, min, max, step).setAction(action);
    }

    public static <K, C extends ICipher<K>> ICipherSettingProvider<K, C> createIntRange(String id, int minStart, int maxStart, int min, int max, int step, BiConsumer<int[], C> action) {
        return new ICipherSettingIntRange<K, C>(id).set(minStart, maxStart, min, max, step).setAction(action);
    }

    public static <K, C extends ICipher<K>, E> ICipherSettingProvider<K, C> createCombo(String id, E[] items, BiConsumer<E, C> action) {
        return new ICipherSettingComboBox<K, C, E>(id).set(items).setAction(action);
    }

    public static class ICipherSettingIntSpinner<K, C extends ICipher<K>> implements ICipherSettingProvider<K, C> {

        private String id;
        private int start, min, max, step;
        private BiConsumer<Integer, C> action;

        public ICipherSettingIntSpinner(String id) {
            this.id = id;
        }

        public ICipherSettingIntSpinner<K, C> set(int start, int min, int max, int step) {
            this.start = start;
            this.min = min;
            this.max = max;
            this.step = step;
            return this;
        }

        public ICipherSettingIntSpinner<K, C> setAction(BiConsumer<Integer, C> action) {
            this.action = action;
            return this;
        }

        @Override
        public ICipherSetting<K, C> create() {
            return new ICipherSetting<K, C>() {
                public JSpinner intSpinner = JSpinnerUtil.createSpinner(start, min, max, step);
                @Override
                public void add(Container container) {
                    container.add(this.intSpinner);
                }

                @Override
                public void apply(CipherAttack<K, C> attack) {
                    ICipherSettingIntSpinner.this.action.accept(ComponentParse.getInteger(this.intSpinner), attack.getCipher());
                }

                @Override
                public void save(Map<String, Object> map) {
                    map.put(id, this.intSpinner.getValue());

                }

                @Override
                public void load(Map<String, Object> map) {
                    this.intSpinner.setValue(map.getOrDefault(id, start));
                }
            };
        }
    };

    public static class ICipherSettingIntRange<K, C extends ICipher<K>> implements ICipherSettingProvider<K, C> {

        private String id;
        private int minStart, maxStart, min, max, step;
        private BiConsumer<int[], C> action;

        public ICipherSettingIntRange(String id) {
            this.id = id;
        }

        public ICipherSettingIntRange<K, C> set(int minStart, int maxStart, int min, int max, int step) {
            this.minStart = minStart;
            this.maxStart = maxStart;
            this.min = min;
            this.max = max;
            this.step = step;
            return this;
        }

        public ICipherSettingIntRange<K, C> setAction(BiConsumer<int[], C> action) {
            this.action = action;
            return this;
        }

        @Override
        public ICipherSetting<K, C> create() {
            return new ICipherSetting<K, C>() {
                public JSpinner[] rangeSpinner = JSpinnerUtil.createRangeSpinners(minStart, maxStart, min, max, step);
                @Override
                public void add(Container container) {
                    container.add(this.rangeSpinner[0]);
                    container.add(this.rangeSpinner[1]);
                }

                @Override
                public void apply(CipherAttack<K, C> attack) {
                    ICipherSettingIntRange.this.action.accept(ComponentParse.getIntegerRange(this.rangeSpinner), attack.getCipher());
                }

                @Override
                public void save(Map<String, Object> map) {
                    map.put(id, ComponentParse.getIntegerRange(this.rangeSpinner));

                }

                @Override
                public void load(Map<String, Object> map) {

                    List<Integer> range = (List<Integer>) map.getOrDefault(id, Arrays.asList(minStart, maxStart));
                    this.rangeSpinner[0].setValue(range.get(0));
                    this.rangeSpinner[1].setValue(range.get(1));
                }
            };
        }
    };

    public static class ICipherSettingComboBox<K, C extends ICipher<K>, E> implements ICipherSettingProvider<K, C> {

        private String id;
        private E[] items;
        private BiConsumer<E, C> action;

        public ICipherSettingComboBox(String id) {
            this.id = id;
        }

        public ICipherSettingComboBox<K, C, E> set(E... items) {
            this.items = items;
            return this;
        }

        public ICipherSettingComboBox<K, C, E> setAction(BiConsumer<E, C> action) {
            this.action = action;
            return this;
        }

        @Override
        public ICipherSetting<K, C> create() {
            return new ICipherSetting<K, C>() {
                public JComboBox<E> comboBox = new JComboBox<>(ICipherSettingComboBox.this.items);
                @Override
                public void add(Container container) {
                    container.add(this.comboBox);
                }

                @SuppressWarnings("unchecked")
                @Override
                public void apply(CipherAttack<K, C> attack) {
                    ICipherSettingComboBox.this.action.accept((E) this.comboBox.getSelectedItem(), attack.getCipher());
                }

                @Override
                public void save(Map<String, Object> map) {
                    map.put(id, this.comboBox.getSelectedIndex());

                }

                @Override
                public void load(Map<String, Object> map) {
                    this.comboBox.setSelectedIndex(((Number) map.getOrDefault(id, 0)).intValue());
                }
            };
        }
    };

    public static class ICipherSettingSpinner<K, C extends ICipher<K>, E> implements ICipherSettingProvider<K, C> {

        private String id;
        private E[] items;
        private BiConsumer<E, C> action;

        public ICipherSettingSpinner(String id) {
            this.id = id;
        }

        public ICipherSettingSpinner<K, C, E> set(E... items) {
            this.items = items;
            return this;
        }

        public ICipherSettingSpinner<K, C, E> setAction(BiConsumer<E, C> action) {
            this.action = action;
            return this;
        }

        @Override
        public ICipherSetting<K, C> create() {
            return new ICipherSetting<K, C>() {
                public JSpinner comboBox = JSpinnerUtil.createSpinner(ICipherSettingSpinner.this.items);
                @Override
                public void add(Container container) {
                    container.add(this.comboBox);
                }

                @SuppressWarnings("unchecked")
                @Override
                public void apply(CipherAttack<K, C> attack) {
                    ICipherSettingSpinner.this.action.accept((E) this.comboBox.getValue(), attack.getCipher());
                }

                @Override
                public void save(Map<String, Object> map) {
                    map.put(id, this.comboBox.getValue());

                }

                @Override
                public void load(Map<String, Object> map) {
                    if (items.getClass().getComponentType() == Integer.class) {
                        this.comboBox.setValue(((Number) map.getOrDefault(id, 0)).intValue());
                    } else {
                        this.comboBox.setValue(map.getOrDefault(id, 0));
                    }
                }
            };
        }
    };
}
