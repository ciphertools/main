/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.settings;

import java.awt.Container;
import java.util.Map;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.solve.CipherAttack;

public interface ICipherSetting<K, C extends ICipher<K>> extends ICipherSettingProvider<K, C> {

    public void add(Container container);

    public void apply(CipherAttack<K, C> attack);

    public void save(Map<String, Object> map);

    public void load(Map<String, Object> map);

    @Override
    default ICipherSetting<K, C> create() {
        return this;
    }
}
