/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.settings;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import tools.cipher.base.interfaces.ISettings;
import tools.cipher.lib.Cache;

public class SettingCache<T> extends Cache<T> {

    private final ISettings settings;
    private final String key;
    private final Function<T, ?> reverse;

    private SettingCache(ISettings settings, String key, Supplier<T> loadIn) {
        this(settings, key, null, loadIn);
    }

    private SettingCache(ISettings settings, String key, Function<T, ?> reverse, Supplier<T> loadIn) {
        super(loadIn);
        this.settings = settings;
        this.key = key;
        this.reverse = reverse;
    }

    public void update(T value) {
        this.value = value; // Update the internal cached value
        this.settings.put(this.key, this.reverse != null ? this.reverse.apply(value) : value);
        this.settings.markDirty();
    }

    public static <T> SettingCache<T> create(ISettings settings, String key, T default_) {
        return new SettingCache<>(settings, key, () -> settings.get(key, default_));
    }

    public static <T> SettingCache<List<T>> create(ISettings settings, String key, List<T> default_) {
        return new SettingCache<>(settings, key, () -> (List<T>) settings.getList(key, default_));
    }

    public static <T> SettingCache<Map<String, T>> create(ISettings settings, String key, Map<String, T> default_) {
        return new SettingCache<>(settings, key, () -> (Map<String, T>) settings.getMap(key, default_));
    }

    public static <I, T> SettingCache<T> create(ISettings settings, String key, Function<I, T> function, Function<T, I> reverse, Class<I> type, T default_) {
        return new SettingCache<T>(settings, key, reverse, () -> settings.get(key, function, type, default_));
    }
}
