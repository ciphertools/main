/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.math.BigInteger;

import tools.cipher.base.interfaces.IAttackMethod;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.settings.SettingsCache;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.result.Solution;

public interface ISimulatedAnnealingAttack<K> extends IAttackMethod<K> {

    default IDecryptionTracker trySimulatedAnnealing(IDecryptionTracker tracker, int iterations) {

        tracker.getProgress().addMax(BigInteger.valueOf((long) Math.floor((double) SettingsCache.simulatedAnnealing.get().get(0) / SettingsCache.simulatedAnnealing.get().get(1)) + 1).multiply(BigInteger.valueOf(SettingsCache.simulatedAnnealing.get().get(2).intValue())));
        int ite = 0;

        stop:
        while (iterations < 0 || ite++ < iterations) {
            this.startIteration(tracker);

            K bestMaximaKey = this.generateIntialKey(tracker);
            Solution bestMaximaSolution = this.toSolution(tracker, bestMaximaKey);
            this.updateIfBetterThanBest(tracker, bestMaximaSolution, bestMaximaKey);

            double TEMP = SettingsCache.simulatedAnnealing.get().get(0);
            do {
                TEMP = Math.max(0.0D, TEMP - SettingsCache.simulatedAnnealing.get().get(1));

                for (int count = 0; count < SettingsCache.simulatedAnnealing.get().get(2); count++) {
                    if (tracker.shouldStop()) {
                        break stop;
                    }

                    this.onPreIteration(tracker);
                    K lastKey = this.modifyKey(tracker, bestMaximaKey, TEMP, count);
                    tracker.setLastSolution(this.toSolution(tracker, lastKey));
                    tracker.addSolution(tracker.getLastSolution());

                    if (this.shouldAcceptSolution(TEMP, tracker.getLastSolution(), bestMaximaSolution)) {
                        bestMaximaSolution = tracker.getLastSolution();
                        bestMaximaKey = lastKey;
                        this.updateIfBetterThanBest(tracker, bestMaximaSolution, bestMaximaKey);
                    }

                    this.onPostIteration(tracker);
                }
            } while (TEMP > 0);

            tracker.getProgress().finish();
            // TODO if(this.iterationTimer)
            // this.output(tracker, "Iteration Time: %f",
            // timer.getTimeRunning(Units.Time.MILLISECOND));
            if (this.endIteration(tracker, tracker.getBestSolution())) {
                break;
            }

            this.output(tracker, "============================");
        }

        return tracker;
    }

    default K generateIntialKey(IDecryptionTracker tracker) {
        return this.getCipher().randomiseKey(tracker.getLength());
    }

    default K modifyKey(IDecryptionTracker tracker, K bestMaximaKey, double temp, int count) {
        return this.getCipher().alterKey(bestMaximaKey, temp, count);
    }

    default boolean shouldAcceptSolution(double TEMP, Solution lastSolution, Solution bestSolution) {
        double lastDF = lastSolution.score - bestSolution.score;
        return lastDF >= 0 || (TEMP > 0 && Math.exp(lastDF / TEMP) > RandomUtil.pickDouble());
    }

    default void onPreIteration(IDecryptionTracker tracker) {

    }

    default void onPostIteration(IDecryptionTracker tracker) {
        tracker.getProgress().increment();
    }

    default void startIteration(IDecryptionTracker tracker) {
        tracker.getProgress().set(0);
    }

    default boolean endIteration(IDecryptionTracker tracker, Solution bestSolution) {
        this.output(tracker, bestSolution.toString());
        //TODO NationalCipherUI.BEST_SOULTION = bestSolution.getText();
        return false;
    }
}
