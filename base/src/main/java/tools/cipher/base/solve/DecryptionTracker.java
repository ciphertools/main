/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.interfaces.IProgress;
import tools.cipher.base.interfaces.ISettings;
import tools.cipher.base.settings.SettingsCache;
import com.alexbarter.lib.Pair;
import tools.cipher.lib.fitness.TextFitness;
import tools.cipher.lib.language.ILanguage;
import tools.cipher.lib.result.Solution;

public class DecryptionTracker implements IDecryptionTracker {

    private final CharSequence cipherText;
    private char[] plainText;
    private final ICipherProgram app;
    private double UPPER_ESTIMATE;

    /**
     * Just a place to store the most recent solution
     */
    private Solution bestSolution;
    /**
     * Just a place to store the most recent solution
     */
    @Nullable
    private Solution lastSolution;
    public long iteration;
    private Function<Integer, Integer> outputLength = length -> length;
    private List<ISolutionCallback> callbacks;

    public DecryptionTracker(CharSequence cipherText, ICipherProgram app) {
        this.app = app;
        this.iteration = 1;
        this.bestSolution = Solution.WORST_SOLUTION;

        this.cipherText = cipherText;
        this.callbacks = new ArrayList<>();
        this.UPPER_ESTIMATE = TextFitness.getEstimatedFitness(this.getOutputTextLength(this.cipherText.length()), this.getLanguage().getQuadgramData()) * 1.1;
    }

    public int getOutputTextLength(int inputLength) {
        return this.outputLength.apply(inputLength);
    }

    public DecryptionTracker setOutputLength(Function<Integer, Integer> outputLength) {
        this.outputLength = outputLength;
        return this;
    }

    @Override
    public CharSequence getCipherText() {
        return this.cipherText;
    }

    @Override
    public char[] getHolder() {
        if (this.plainText == null) {
            this.plainText = this.getNewHolder();
        }

        return this.plainText;
    }

    @Override
    public char[] getNewHolder() {
        return new char[this.getOutputTextLength(this.getLength())];
    }

    @Override
    public Pair<char[], Boolean> getVariableHolder() {
        return Pair.of(SettingsCache.useParallel.get() ? this.getNewHolder() : this.getHolder(), SettingsCache.useParallel.get());
    }

    @Override
    public boolean addSolution(Solution solutionIn) {
        // Some quick easy checks
        if (!SettingsCache.collectSolutions.get() || solutionIn.score <= this.UPPER_ESTIMATE)
            return false;

        boolean success = false;

        for (ISolutionCallback c : this.callbacks) {
            success |= c.onSolution(this, solutionIn);
        }

        if (success) {
            solutionIn.bake();
        }

        return success;
    }

    @Override
    public boolean resetSolution() {
        this.bestSolution = Solution.WORST_SOLUTION;
        //NationalCipherUI.topSolutions.reset();
        return true;
    }

    public void resetIteration() {
        this.iteration = 1;
    }

    @Override
    public Solution getBestSolution() {
        return this.bestSolution;
    }

    @Override
    public Solution getLastSolution() {
        return this.lastSolution;
    }

    @Override
    public IDecryptionTracker setBestSolution(Solution solutionIn) {
        this.bestSolution = solutionIn;
        this.callbacks.forEach(c -> c.onChange(this));
        return this;
    }

    @Override
    public IDecryptionTracker setLastSolution(Solution solutionIn) {
        this.lastSolution = solutionIn;
        return this;
    }

    // IApplication methods
    @Override
    public ICipherProgram getApp() {
        return this.app;
    }

    @Override
    public ILanguage getLanguage() {
        return this.app.getLanguage();
    }

    public ISettings getSettings() {
        return this.app.getSettings();
    }

    @Override
    public IProgress getProgress() {
        return this.app.getProgress();
    }

    @Override
    public PrintStream out() {
        return this.app.out();
    }

    @Override
    public boolean shouldStop() {
        return this.app.shouldStop();
    }

    @Override
    public void addCallback(ISolutionCallback callback) {
        this.callbacks.add(callback);
    }
}
