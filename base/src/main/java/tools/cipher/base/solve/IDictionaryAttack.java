/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import tools.cipher.base.interfaces.IAttackMethod;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.lib.language.Dictionary;

public interface IDictionaryAttack<K> extends IAttackMethod<K> {

    default IDecryptionTracker tryDictionaryAttack(IDecryptionTracker tracker) {
        Dictionary dictionary = tracker.getLanguage().getDictionary();

        if (dictionary == null) {
            tracker.out().println("NO dictionary for " + tracker.getLanguage().getName());
            return tracker;
        }

        this.output(tracker, "Found %d words", dictionary.wordCount());
        tracker.getProgress().addMax(dictionary.wordCount());
        //TODO
        /**
        Stream<String> words = CipherUtils.createStream(dictionary.getWords(), tracker);
        Supplier<Predicate<String>> wordFilter = this.getWordFilter(tracker);

        if(wordFilter != null) {
            words = words.filter(wordFilter.get());
        }

        words.forEach(word -> {
            if (!tracker.shouldStop()) {
                this.decryptAndUpdate(tracker, this.useWordToGetKey(tracker, word));

                tracker.increaseIteration();
            }
        });

        tracker.finish();
        **/
        return tracker;
    }

    K useWordToGetKey(DecryptionTracker tracker, String word);

    @Nullable
    default Supplier<Predicate<String>> getWordFilter(DecryptionTracker tracker) {
        return null;
    }
}
