/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import javax.swing.JDialog;
import javax.swing.JPanel;

import tools.cipher.base.settings.ICipherSetting;
import tools.cipher.base.settings.ICipherSettingProvider;
import tools.cipher.base.interfaces.ICipher;
import tools.cipher.base.interfaces.ICipherAttack;
import tools.cipher.base.interfaces.ICipherProgram;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.interfaces.ILoadHook;
import tools.cipher.base.interfaces.ISettings;

public class CipherAttack<K, C extends ICipher<K>> implements IBruteForceAttack<K>, ISimulatedAnnealingAttack<K>, ICipherAttack, ILoadHook {

    private C cipher;
    private String displayName;
    private String saveId;
    private final Set<DecryptionMethod> methods;
    private boolean mute;
    private final List<ICipherSetting<K, C>> settings;
    protected int iterations = 1000;
    private Function<Integer, Integer> outputLength = null;
    private Set<IDecryptionTracker.ISolutionCallback> callbacks;

    public CipherAttack(C cipher, String displayName) {
        this.cipher = cipher;
        this.displayName = displayName;
        this.saveId = "attack_" + displayName;
        this.methods = EnumSet.noneOf(DecryptionMethod.class);
        this.settings = new ArrayList<>();
        this.callbacks = new HashSet<>();
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public CipherAttack<K, C> setAttackMethods(DecryptionMethod... methods) {
        for (DecryptionMethod s : methods) {
            this.methods.add(s);
        }
        return this;
    }

    public CipherAttack<K, C> setIterations(int iterations) {
        this.iterations = iterations;
        return this;
    }

    public CipherAttack<K, C> setOutputLength(Function<Integer, Integer> outputLength) {
        this.outputLength = outputLength;
        return this;
    }

    @SafeVarargs
    public final CipherAttack<K, C> addSetting(ICipherSettingProvider<K, C>... settings) {
        for (ICipherSettingProvider<K, C> s : settings) {
            this.settings.add(s.create());
        }
        return this;
    }

    public boolean hasSettings() {
        return !this.settings.isEmpty();
    }

    @Override
    public C getCipher() {
        return this.cipher;
    }

    @Override
    public void endAttack(IDecryptionTracker tracker, boolean forced) {
        this.callbacks.clear();
    }

    public boolean canBeStopped() {
        return true;
    }

    @Override
    public final IDecryptionTracker startAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        if (!this.methods.contains(method)) {
            throw new UnsupportedOperationException("Decryption method not supported: " + method);
        }

        this.readLatestSettings();

        app.getProgress().start();
        return this.attemptAttack(text, method, app);
    }

    public IDecryptionTracker attemptAttack(CharSequence text, DecryptionMethod method, ICipherProgram app) {
        IDecryptionTracker tracker = this.createTracker(text, app);
        switch (method) {
        case BRUTE_FORCE:
            return this.tryBruteForce(tracker);
        case SIMULATED_ANNEALING:
            return this.trySimulatedAnnealing(tracker, this.iterations);
        default:
            return tracker;
        }
    }

    public IDecryptionTracker createTracker(CharSequence text, ICipherProgram app) {
        DecryptionTracker tracker = new DecryptionTracker(text, app);
        this.populateTracker(tracker);
        return tracker;
    }

    public void populateTracker(DecryptionTracker tracker) {
        if (this.outputLength != null) {
            tracker.setOutputLength(this.outputLength);
        }
        this.callbacks.forEach(tracker::addCallback);
    }

    public void readLatestSettings() {
        this.settings.forEach(setting -> setting.apply(this));
    }

    public void createSettingsUI(JDialog dialog, JPanel panel) {
        this.settings.forEach(setting -> setting.add(panel));
    }

    public final Collection<DecryptionMethod> getAttackMethods() {
        return this.methods == null ? Collections.emptySet() : Collections.unmodifiableSet(this.methods);
    }

    public void writeTo(Map<String, Object> map) {

    }

    public void readFrom(Map<String, Object> map) {

    }

    @Override
    public void save(ISettings settings) {
        Map<String, Object> saveData = new HashMap<>();
        this.settings.forEach(setting -> setting.save(saveData));
        this.writeTo(saveData);
        settings.put(this.saveId, saveData);
    }

    @Override
    public void load(ISettings settings) {
        @SuppressWarnings("unchecked")
        Map<String, Object> saveData = settings.getMap(this.saveId, Collections.EMPTY_MAP);
        this.settings.forEach(setting -> setting.load(saveData));
        this.readFrom(saveData);
    }

    @Override
    public boolean isMuted() {
        return this.mute;
    }

    @Override
    public CipherAttack<K, C> addCallback(IDecryptionTracker.ISolutionCallback callback) {
        this.callbacks.add(callback);
        return this;
    }

    public CipherAttack<K, C> addCallback(CipherAttack<?, ?> callback) {
        this.callbacks.addAll(callback.callbacks);
        return this;
    }

    public CipherAttack<K, C> mute() {
        this.mute = true;
        return this;
    }

    public CipherAttack<K, C> unmute() {
        this.mute = false;
        return this;
    }
}
