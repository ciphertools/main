/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.util.Arrays;

import tools.cipher.base.interfaces.IAttackMethod;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.lib.result.Solution;

/**
 * Used to solve ciphers with a string key where each character in the key
 * independently affects a large proportional of the output text. This is used
 * to attack periodic ciphers such as Vigenere, Beaufort, Porta etc.
 *
 * @author Alex
 *
 */
public interface IKeySearchAttack<K> extends IAttackMethod<K> {

    default IDecryptionTracker tryKeySearch(IDecryptionTracker tracker, int length) {
        tracker.getProgress().setIndeterminate(true);

        char[] parent = new char[length];
        Arrays.fill(parent, 'A');

        Solution currentBestSolution = Solution.WORST_SOLUTION;

        while (true) {
            boolean change = false;
            for (int i = 0; i < length; i++) {
                for (char j = 'A'; j <= 'Z'; j += this.getCharStep()) {
                    if (tracker.shouldStop()) {
                        return tracker;
                    }

                    //TODO if (!this.hasDuplicateLetters() && )

                    char previous = parent[i];
                    parent[i] = j;

                    tracker.setLastSolution(this.toSolution(tracker, this.useStringGetKey(tracker, new String(parent))));
                    tracker.addSolution(tracker.getLastSolution());

                    if (tracker.getLastSolution().compareTo(currentBestSolution) < 0) {
                        currentBestSolution = tracker.getLastSolution();
                        currentBestSolution.bake();
                        change = previous != j;
                    } else { // Last solution is worst so revert key
                        parent[i] = previous;
                    }

                    tracker.getProgress().increment();
                }
            }

            // No change to the key resulted in a better solution so we must have found the solution
            if (!change) {
                break;
            }
        }

        this.updateIfBetterThanBest(tracker, currentBestSolution, () -> this.useStringGetKey(tracker, new String(parent)));

        tracker.getProgress().finish();
        return tracker;
    }

    K useStringGetKey(IDecryptionTracker tracker, String periodicKeyPart);

    // TODO
    default boolean hasDuplicateLetters() {
        return true;
    }

    default int getCharStep() {
        return 1;
    }
}
