/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.solve;

import java.math.BigInteger;

import tools.cipher.base.interfaces.IAttackMethod;
import tools.cipher.base.interfaces.IDecryptionTracker;
import tools.cipher.base.settings.SettingsCache;
import tools.cipher.lib.CipherUtils;
import tools.cipher.lib.parallel.MasterThread;

public interface IBruteForceAttack<K> extends IAttackMethod<K> {

    default IDecryptionTracker tryBruteForce(IDecryptionTracker tracker) {
        BigInteger totalKeys = this.getCipher().getNumOfKeys();
        this.output(tracker, CipherUtils.formatBigInteger(totalKeys) + " Keys to search");
        tracker.getProgress().addMax(totalKeys);

        if (SettingsCache.useParallel.get()) {
            MasterThread thread = new MasterThread((control) -> {
                this.getCipher().iterateKeys(key -> {
                    Runnable job = () -> {
                        this.decryptAndUpdate(tracker, key);
                        tracker.getProgress().increment();
                    };

                    return !control.tryAddJob(job, 20).end();
                });
            }).setErrorHandler(MasterThread.defaultErrorHandler(tracker.out()));

            thread.start();

            thread.waitTillCompleted(tracker::shouldStop);
        } else {
            this.getCipher().iterateKeys(key -> {
                if (tracker.shouldStop()) {
                    return false;
                }

                this.decryptAndUpdate(tracker, key);
                tracker.getProgress().increment();

                return true;
            });
        }

        tracker.getProgress().finish();
        return tracker;
    }
}
