/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.key.IRangedKeyType;
import com.alexbarter.lib.util.RandomUtil;

public class IntegerKeyType implements IRangedKeyType<Integer> {

    // Both inclusive
    private final int min, max;
    private final boolean alterable;

    private IntegerKeyType(int min, int max, boolean alterable) {
        this.min = min;
        this.max = max;
        this.alterable = alterable;
    }

    @Override
    public Integer randomise() {
        return RandomUtil.pickRandomInt(this.min, this.max);
    }

    @Override
    public boolean isValid(Integer key) {
        return this.min <= key && key <= this.max;
    }

    @Override
    public boolean iterateKeys(Function<Integer, Boolean> consumer) {
        for (int i = this.min; i <= this.max; i++) {
            if (!consumer.apply(i)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Integer alterKey(Integer key) {
        return this.alterable ? RandomUtil.pickRandomInt(this.min, this.max) : key;
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.valueOf(this.max).subtract(BigInteger.valueOf(this.min)).add(BigInteger.ONE);
    }

    @Override
    public Integer parse(String input) throws ParseException {
        try {
            return Integer.parseInt(input);
        } catch (NumberFormatException e) {
            throw new ParseException(input, 0);
        }
    }

    @Override
    public String getHelp() {
        StringBuilder builder = new StringBuilder();
        return this.min+ "-"+this.max;
    }

    @Override
    public int getMin() {
        return this.min;
    }

    @Override
    public int getMax() {
        return this.max;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<Integer> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();
        private boolean alterable = false;

        private Builder() {
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        @Override
        public Builder setMin(int supp) {
            this.min = Optional.of(supp);
            return this;
        }

        @Override
        public Builder setMax(int supp) {
            this.max = Optional.of(supp);
            return this;
        }

        public Builder setAlterable() {
            this.alterable = true;
            return this;
        }

        @Override
        public IntegerKeyType create() {
            IntegerKeyType handler = new IntegerKeyType(this.min.orElse(Integer.MIN_VALUE), this.max.orElse(Integer.MAX_VALUE), this.alterable);
            return handler;
        }

    }
}
