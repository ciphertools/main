/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.key.KeyManipulation;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class FullStringKeyType implements IKeyType<String> {

    // Both inclusive
    private final CharSequence alphabet;

    private FullStringKeyType(CharSequence alphabet) {
        this.alphabet = alphabet;
    }

    @Override
    public String randomise() {
        return KeyGeneration.createLongKeyUniversal(this.alphabet);
    }

    @Override
    public boolean isValid(String key) {
        try {
            this.parse(key);
        } catch (ParseException e) {
            return false;
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<String, Boolean> consumer) {
        return KeyIterator.permuteString(consumer, this.alphabet);
    }

    @Override
    public String alterKey(String key) {
        return KeyManipulation.swapTwoCharacters(key);
    }

    @Override
    public BigInteger getNumOfKeys() {
        return MathUtil.factorialBig(BigInteger.valueOf(this.alphabet.length()));
    }

    @Override
    public String parse(String input) throws ParseException {
        input = input.toUpperCase(Locale.ROOT);

        if (input.length() != this.alphabet.length()) {
            throw new ParseException("The key should be of length: " + this.alphabet.length(), 0);
        }

        for (int i = 0; i < this.alphabet.length(); i++) {
            if (!CharSequenceUtils.contains(input, this.alphabet.charAt(i))) {
                throw new ParseException("The key should contain the letter: " + this.alphabet.charAt(i), i);
            }
        }

        return input;
    }

    @Override
    public String getHelp() {
        return "key";
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IKeyBuilder<String> {

        private Optional<CharSequence> alphabet = Optional.empty();

        private Builder() {
        }

        public Builder setAlphabet(CharSequence alphabet) {
            this.alphabet = Optional.of(alphabet);
            return this;
        }

        @Override
        public FullStringKeyType create() {
            FullStringKeyType handler = new FullStringKeyType(this.alphabet.orElse(Alphabet.ALL_26_CHARS));
            return handler;
        }

    }
}
