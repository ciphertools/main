/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.lib.matrix.Matrix;

public class ExtendedMatrixKeyType implements IKeyType<Matrix> {

    private int mod;

    public ExtendedMatrixKeyType(int mod) {
        this.mod = mod;
    }

    @Override
    public Matrix randomise() {
       // int i = ((BiKey<Matrix, Matrix>)partialKey).getFirstKey().squareSize();
        return KeyGeneration.createMatrix(2, 1, this.mod);
    }

    @Override
    public boolean iterateKeys(Function<Matrix, Boolean> consumer) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix alterKey(Matrix key) {
        return key;
    }

    @Override
    public boolean isValid(Matrix key) {
        return true; //TODO
    }

    @Override
    public String prettifyKey(Matrix key) {
        return key.toString();
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ONE; // 0 Plugs
        return total;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IKeyBuilder<Matrix> {

        private Builder() {
        }

        @Override
        public ExtendedMatrixKeyType create() {
            ExtendedMatrixKeyType handler = new ExtendedMatrixKeyType(26);
            return handler;
        }
    }
}
