/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.IRangedKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import com.alexbarter.lib.util.RandomUtil;

public class GrilleKeyType implements IKeyType<Integer[]> {

    // Both inclusive
    private final int min, max;

    private GrilleKeyType(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public Integer[] randomise() {
        return KeyGeneration.createGrilleKey(RandomUtil.pickRandomInt(this.min, this.max));
    }

    @Override
    public boolean isValid(Integer[] key) {
        // TODO
        return true;
    }

    @Override
    public boolean iterateKeys(Function<Integer[], Boolean> consumer) {
        for (int size = this.min; size <= this.max; size++) {
            if (!KeyIterator.iterateGrille(consumer, size)) {
                return false;
            }
        }
        return false;
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.valueOf(59049); //TODO Calculate number
    }

    @Override
    public Integer[] alterKey(Integer[] key) {
        return key;
    }

    @Override
    public String prettifyKey(Integer[] key) {
        return Arrays.toString(key);
    }

    @Override
    public Integer[] parse(String input) throws ParseException {


        throw new UnsupportedOperationException();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyType.IRangedKeyBuilder<Integer[]> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();

        private Builder() {
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        @Override
        public GrilleKeyType create() {
            GrilleKeyType handler = new GrilleKeyType(this.min.orElse(2), this.max.orElse(6));
            return handler;
        }
    }
}
