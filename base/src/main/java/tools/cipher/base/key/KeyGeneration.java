/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.ListUtil;
import com.alexbarter.lib.util.RandomUtil;
import com.alexbarter.lib.Triple;
import tools.cipher.lib.constants.Alphabet;
import tools.cipher.lib.matrix.Matrix;

public class KeyGeneration {

    public static String createRepeatingShortKey26(int length) {
        return createRepeatingShortKeyUniversal(Alphabet.ALL_26_CHARS, length);
    }

    public static String createShortKey26(int length) {
        return createDistinctKeyUniversal(Alphabet.ALL_26_CHARS, length);
    }

    public static String createRepeatingShortKeyUniversal(CharSequence charList, int length) {
        char[] key = new char[length];

        for (int i = 0; i < length; i++) {
            key[i] = RandomUtil.pickRandomChar(charList);
        }

        return new String(key);
    }

    public static String createDistinctKeyUniversal(CharSequence charList, int length) {
        List<Integer> indices = ListUtil.range(0, charList.length() - 1);

        char[] key = new char[length];

        for (int i = 0; i < length; i++) {
            int index = RandomUtil.removeRandomElement(indices);
            key[i] = charList.charAt(index);
        }

        return new String(key);
    }

    public static String createLongKeyUniversal(CharSequence charList) {
        return createDistinctKeyUniversal(charList, charList.length());
    }

    public static String createLongKey25() {
        return createLongKeyUniversal(Alphabet.ALL_25_CHARS);
    }

    public static String createLongKey26() {
        return createLongKeyUniversal(Alphabet.ALL_26_CHARS);
    }

    public static String createLongKey27() {
        return createLongKeyUniversal(Alphabet.ALL_27_CHARS);
    }

    public static String createLongKey36() {
        return createLongKeyUniversal(Alphabet.ALL_36_CHARS);
    }

    public static Integer[] createRepeatingShortOrderKey(int entryRange, int length) {
        Integer[] key = new Integer[length];

        for (int i = 0; i < length; i++) {
            key[i] = RandomUtil.pickRandomInt(0, entryRange - 1);
        }

        return key;
    }

    public static Integer[] createShortOrderKey(int entryRange, int length) {
        List<Integer> integers = ListUtil.range(0, entryRange - 1);

        Integer[] key = new Integer[length];

        for (int i = 0; i < length; i++) {
            key[i] = RandomUtil.pickRandomElement(integers);
            integers.remove(key[i]);
        }

        return key;
    }

    public static Integer[] createOrder(int length) {
        return ArrayUtil.shuffle(ArrayUtil.createRangeInteger(length));
    }

    public static List<Integer> createOrderList(int length) {
        return ListUtil.toList(createOrder(length));
    }

    // Specific key generators

    /**
     * Creates a key that contains at least one of each character from alphabet
     *
     * @param alphabet The characters that can be in the key
     * @param length The length of the key and returned character array
     * @return An array of characters with each element being a char from
     *         alphabet and it contains each char at least once
     */
    public static Character[] createKeyGuaranteed(CharSequence alphabet, int length) {
        if (length < alphabet.length()) {
            throw new IllegalArgumentException("Length of key must be greater or equal to the number of characters in the alphabet");
        }

        List<Integer> indices = ListUtil.range(0, alphabet.length() - 1);

        Character[] key = new Character[length];
        int i = 0;

        // Ensures that there are 3 of each character
        for (; i < alphabet.length(); i++) {
            int index = RandomUtil.removeRandomElement(indices);
            key[i] = alphabet.charAt(index);
        }

        for (; i < key.length; i++) {
            key[i] = RandomUtil.pickRandomChar(alphabet);
        }

        return ArrayUtil.shuffle(key);
    }

    public static int[] createSwagmanKey(int size) {
        int[] key = new int[size * size];
        Arrays.fill(key, -1);

        List<Triple<Integer, Boolean, List<Integer>>> options = new ArrayList<>(size);

        int ite = 0;
        reset: while (ite < size) {
            // System.out.println("Runnings");
            options.clear();

            for (int col = 0; col < ite; col++) {
                options.add(new Triple<>(ite * size + col, true, getOptionsForCell(key, size, ite, col)));
            }

            for (int row = 0; row < ite; row++) {
                options.add(new Triple<>(row * size + ite, false, getOptionsForCell(key, size, row, ite)));
            }

            List<Integer> corner = getOptionsForCell(key, size, ite, ite);

            while (options.size() > 0) {

                // options.sort(Comparator.comparingInt(p -> p.getRight().size()));
                Triple<Integer, Boolean, List<Integer>> pair = options.remove(0);
                Integer random = null;

                do {
                    if (pair.getRight().isEmpty()) {
                        // System.out.println("Reset something went wrong");
                        // Something went wrong reset current row

                        boolean resetAll = true;

                        if (!resetAll) {
                            for (int r = 0; r < size / 2; r++) {
                                if (ite > 0) {
                                    for (int col = 0; col < ite; col++) {
                                        key[ite * size + col] = -1;
                                    }

                                    for (int row = 0; row < ite; row++) {
                                        key[row * size + ite] = -1;
                                    }

                                    key[ite * size + ite] = -1;
                                    ite--;
                                }
                            }
                        } else {

                            ite = 0;
                            Arrays.fill(key, -1);
                        }

                        // System.out.println("Reset something went wrong : " + ite);
                        continue reset;
                    }
                    // System.out.println("Stuck" + pair.getRight());

                    random = RandomUtil.pickRandomElement(pair.getRight());
                    final Integer test = random;

                    if (options.stream().filter(pair::middleEquals).filter(t -> t.getRight().contains(test)).anyMatch(t -> t.getRight().size() <= 1)) {
                        pair.getRight().remove(test);
                        continue;
                    }

                    options.stream().filter(pair::middleEquals).forEach(t -> t.getRight().remove(test));

                    if (!corner.contains(test) || corner.size() > 1) {
                        corner.remove(test);
                        break;
                    } else {
                        pair.getRight().remove(test);
                    }
                } while (true);

                final Integer sucess = random;
                key[pair.getLeft()] = random;
            }

            key[ite * size + ite] = RandomUtil.pickRandomElement(corner);
            ite++;
        }

        return key;
    }

    // Very inefficient requires much more work to be more efficient
    public static int[] createSwagmanKey3(int size) {
        while (true) {
            try {
                int[] key = new int[size * size];
                Arrays.fill(key, -1);

                while (!isFilled(key, size)) {
                    creation: for (int row = 0; row < size; row++) {
                        for (int col = 0; col < size; col++) {
                            boolean change = autoFill(key, size);
                            if (change)
                                break creation;
                            if (key[row * size + col] != -1)
                                continue;

                            List<Integer> options = getOptionsForCell(key, size, row, col);

                            Integer option = RandomUtil.pickRandomElement(options);

                            key[row * size + col] = option;
                            options.remove(option);
                        }
                    }
                }
                return key;
            } catch (Exception e) {
            } // If the key is invalid will attempt again till it gets it gets a valid key
        }
    }

    private static boolean isFilled(int[] key, int size) {
        for (int row = 0; row < size; row++)
            for (int col = 0; col < size; col++)
                if (key[row * size + col] == -1)
                    return false;

        return true;
    }

    private static boolean autoFill(int[] key, int size) {
        boolean finished;
        boolean change = false;

        do {
            finished = true;
            search: for (int row = 0; row < size; row++) {
                for (int col = 0; col < size; col++) {
                    if (key[row * size + col] != -1)
                        continue;

                    List<Integer> options = getOptionsForCell(key, size, row, col);

                    if (options.size() == 1) {
                        key[row * size + col] = options.get(0);
                        finished = false;
                        change = true;
                        break search;
                    }
                }
            }
        } while (!finished);

        return change;
    }

    private static List<Integer> getOptionsForCell(int[] key, int size, int row, int column) {
        List<Integer> validOptions = ListUtil.range(0, size - 1);

        for (int nC = 0; nC < size; nC++)
            validOptions.remove((Integer) key[row * size + nC]);

        for (int nR = 0; nR < size; nR++)
            validOptions.remove((Integer) key[nR * size + column]);

        return validOptions;
    }

    private static Integer[] createGenericGrilleKey(int size) {
        double half = size / 2D;
        int rows = (int) Math.ceil(half);
        int cols = (int) Math.floor(half);
        int keyLength = rows * cols;

        Integer[] key = new Integer[keyLength];
        int count = 0;
        for (int r = 0; r < rows; r++)
            for (int c = 0; c < cols; c++)
                key[count++] = r * size + c;

        return key;
    }

    public static Integer[] createGrilleKey(int size) {
        Integer[] key = createGenericGrilleKey(size);

        for (int i = 0; i < key.length; i++) {
            int quadrant = RandomUtil.pickRandomInt(4);
            int value = key[i];
            for (int rot = 0; rot < quadrant; rot++) {
                int row = value / size;
                int col = value % size;
                value = col * size + (size - row - 1);
            }
            key[i] = value;
        }

        return key;
    }

    // Random key length generator

    public static String createShortKey26(int minLength, int maxLength) {
        int length = RandomUtil.pickRandomInt(minLength, maxLength);
        return createShortKey26(length);
    }

    public static String createRepeatingShortKey26(int minLength, int maxLength) {
        int length = RandomUtil.pickRandomInt(minLength, maxLength);
        return createRepeatingShortKey26(length);
    }

    public static Integer[] createOrder(int minLength, int maxLength) {
        int length = RandomUtil.pickRandomInt(minLength, maxLength);
        return createOrder(length);
    }

    public static Integer[] createGrilleKey(int minLength, int maxLength) {
        int length = RandomUtil.pickRandomInt(minLength, maxLength);
        return createGrilleKey(length);
    }

    public static Matrix createMatrix(int size, int range) {
        return createMatrix(size, size, range);
    }

    public static Matrix createMatrix(int rows, int columns, int range) {
        Matrix matrix = new Matrix(rows, columns);
        for (int i = 0; i < matrix.data.length; i++)
            matrix.data[i] = RandomUtil.pickRandomInt(range);

        return matrix;
    }
}
