/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.BiPredicate;
import java.util.function.Function;

import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.characters.CharacterArrayUtils;
import tools.cipher.lib.matrix.Matrix;

public class KeyIterator {

    public static boolean permuteIntegerOrderedKey(Function<Integer[], Boolean> consumer, int length) {
        return permuteObject(consumer, ArrayUtil.createRangeInteger(length));
    }

    public static boolean permuteString(Function<String, Boolean> consumer, CharSequence str) {
        return permuteString(consumer, CharSequenceUtils.toArray(str));
    }

    public static boolean permuteString(Function<String, Boolean> consumer, Character[] str) {
        return permuteObject(d -> consumer.apply(CharacterArrayUtils.toString(d)), str);
    }

    public static <T> boolean permuteObject(Function<T[], Boolean> consumer, T[] items) {
        return permuteObject(consumer, items, 0);
    }

    private static <T> boolean permuteObject(Function<T[], Boolean> consumer, T[] arr, int pos) {
        if (arr.length - pos == 1)
            return consumer.apply(Arrays.copyOf(arr, arr.length));
        else {
            for (int i = pos; i < arr.length; i++) {
                T h = arr[pos];
                T j = arr[i];
                arr[pos] = j;
                arr[i] = h;

                if (!permuteObject(consumer, arr, pos + 1)) {
                    return false;
                }

                arr[pos] = h;
                arr[i] = j;
            }

            return true;
        }
    }

    public static boolean iterateIntegerArray(Function<Integer[], Boolean> consumer, int length, int range, boolean repeats) {
        return iterateObject(consumer, new Integer[length], ArrayUtil.createRangeInteger(range), repeats ? null : (a, b) -> a == b);
    }

    public static boolean iterateShortCustomKey(Function<String, Boolean> consumer, CharSequence keyAlphabet, int length, boolean repeats) {
        return iterateShortKey(consumer, CharSequenceUtils.toArray(keyAlphabet), length, repeats);
    }

    public static boolean iterateShortKey(Function<String, Boolean> consumer, Character[] characters, int length, boolean repeats) {
        return iterateObject(o -> consumer.apply(CharacterArrayUtils.toString(o)), new Character[length], characters, repeats ? null : (a, b) -> a == b);
    }

    public static boolean iterateMatrix(Function<Matrix, Boolean> consumer, int size) {
        return iterateMatrix(consumer, size, size, 26);
    }

    public static boolean iterateMatrix(Function<Matrix, Boolean> consumer, int noRows, int noColumns, int base) {
        return iterateObject(o -> consumer.apply(new Matrix(o, noRows, noColumns)), new Integer[noRows * noColumns], ArrayUtil.createRangeInteger(base));
    }

    @SuppressWarnings("unchecked")
    public static <T> boolean iterateObject(Function<T[], Boolean> consumer, int length, T[] items) {
        return iterateObject(consumer, (T[]) Array.newInstance(items.getClass().getComponentType(), length), items);
    }

    public static <T> boolean iterateObject(Function<T[], Boolean> consumer, T[] holder, T[] items) {
        return iterateObject(consumer, holder, items, null, 0);
    }

    public static <T> boolean iterateObject(Function<T[], Boolean> consumer, T[] holder, T[] items, BiPredicate<T, T> equalsPred) {
        return iterateObject(consumer, holder, items, equalsPred, 0);
    }

    private static <T> boolean iterateObject(Function<T[], Boolean> consumer, T[] holder, T[] items, BiPredicate<T, T> equalsPred, int pos) {
        if (holder.length - pos == 0)
            return consumer.apply(Arrays.copyOf(holder, holder.length));
        else {
            itemLoop: for (T i : items) {

                // If can't have duplicates
                if (equalsPred != null) {
                    for (int j = 0; j < pos; j++) {
                        if (equalsPred.test(holder[j], i)) {
                            continue itemLoop;
                        }
                    }
                }

                holder[pos] = i;
                if (!iterateObject(consumer, holder, items, equalsPred, pos + 1)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static boolean iterateGrille(Function<Integer[], Boolean> consumer, int size) {
        double halfSize = size / 2D;
        int rows = (int) Math.ceil(halfSize);
        int cols = (int) Math.floor(halfSize);
        int keySize = rows * cols;

        int[] key = new int[keySize];
        int count = 0;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                key[count++] = r * size + c;
            }
        }

        return KeyIterator.iterateIntegerArray(o -> {
            int[] starting = (int[]) key;
            Integer[] next = new Integer[starting.length];

            for (int i = 0; i < key.length; i++) {
                int quadrant = o[i];
                int value = starting[i];
                for (int rot = 0; rot < quadrant; rot++) {
                    int row = value / size;
                    int col = value % size;
                    value = col * size + (size - row - 1);
                }
                next[i] = value;
            }
            return consumer.apply(next);
        }, keySize, 4, true);
    }
}
