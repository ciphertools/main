/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.key.IRangedKeyType;
import tools.cipher.base.key.KeyGeneration;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.matrix.Matrix;

public class SquareMatrixKeyType implements IRangedKeyType<Matrix> {

    private int mod;
    private int min;
    private int max;

    public SquareMatrixKeyType(int mod, int min, int max) {
        this.mod = mod;
        this.min = min;
        this.max = max;
    }

    @Override
    public Matrix randomise() {
        Matrix matrix;
        do {
            matrix = KeyGeneration.createMatrix(RandomUtil.pickRandomInt(this.min, this.max), this.mod);
        }
        while(!matrix.hasInverseMod(this.mod));

        return matrix;
    }

    @Override
    public boolean iterateKeys(Function<Matrix, Boolean> consumer) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Matrix alterKey(Matrix key) {
        return key;
    }

    @Override
    public boolean isValid(Matrix key) {
        return true;
    }

    @Override
    public String prettifyKey(Matrix key) {
        return key.toString();
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ONE; // 0 Plugs
        return total;
    }

    @Override
    public int getMin() {
        return this.min;
    }

    @Override
    public int getMax() {
        return this.max;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<Matrix> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();
        private Optional<Integer> mod = Optional.empty();

        private Builder() {
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        public Builder setMod(int mod) {
            this.mod = Optional.of(mod);
            return this;
        }

        @Override
        public SquareMatrixKeyType create() {
            SquareMatrixKeyType handler = new SquareMatrixKeyType(this.mod.orElse(26), this.min.orElse(2), this.max.orElse(4));
            return handler;
        }
    }
}
