/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.key.KeyManipulation;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class SquareStringKeyType implements IKeyType<String> {

    // Both inclusive
    private final CharSequence alphabet;
    private int rows;
    private int columns;

    private SquareStringKeyType(CharSequence alphabet, int rows, int columns) {
        this.alphabet = alphabet;
        this.rows = rows;
        this.columns = columns;
    }

    @Override
    public String randomise() {
        return KeyGeneration.createLongKeyUniversal(this.alphabet);
    }

    @Override
    public boolean isValid(String key) {
        if (key.length() != this.alphabet.length()) {
            return false;
        }

        for (int i = 0; i < key.length(); i++) {
            if (!CharSequenceUtils.contains(this.alphabet, key.charAt(i))) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<String, Boolean> consumer) {
        return KeyIterator.permuteString(consumer, this.alphabet);
    }

    @Override
    public String alterKey(String key) {
        return KeyManipulation.modifyKeySquare(key, this.columns, this.rows);
    }

    @Override
    public BigInteger getNumOfKeys() {
        return MathUtil.factorialBig(BigInteger.valueOf(this.alphabet.length()));
    }

    @Override
    public String parse(String input) throws ParseException {
        return input;
    }

    @Override
    public String getHelp() {
        return "keysquare"+this.rows+"x"+this.columns;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IKeyBuilder<String> {

        private Optional<CharSequence> alphabet = Optional.empty();
        private Optional<Integer> rows = Optional.empty();
        private Optional<Integer> columns = Optional.empty();

        private Builder() {
        }

        public Builder setAlphabet(CharSequence alphabet) {
            this.alphabet = Optional.of(alphabet);
            return this;
        }

        public Builder setDim(int rows, int columns) {
            this.rows = Optional.of(rows);
            this.columns = Optional.of(columns);
            return this;
        }

        @Override
        public SquareStringKeyType create() {
            CharSequence alphabet = this.alphabet.orElse(Alphabet.ALL_26_CHARS);
            SquareStringKeyType handler = new SquareStringKeyType(alphabet, this.rows.orElse(1), this.columns.orElse(alphabet.length()));
            return handler;
        }

    }
}
