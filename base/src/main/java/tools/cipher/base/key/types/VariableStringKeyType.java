/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import tools.cipher.base.key.IRangedKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.key.KeyManipulation;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.MathUtil;

public class VariableStringKeyType implements IRangedKeyType<String> {

    // Both inclusive
    private final int min, max;
    private final CharSequence alphabet;
    private final boolean repeats;

    private VariableStringKeyType(CharSequence alphabet, int min, int max, boolean repeats) {
        this.alphabet = alphabet;
        this.min = min;
        this.max = max;
        this.repeats = repeats;
    }

    @Override
    public String randomise() {
        BiFunction<CharSequence, Integer, String> func = this.repeats ? KeyGeneration::createRepeatingShortKeyUniversal : KeyGeneration::createDistinctKeyUniversal;

        return func.apply(this.alphabet, RandomUtil.pickRandomInt(this.min, this.max));
    }

    @Override
    public boolean isValid(String key) {
        // Quick check if length bigger than number
        if (!this.repeats && key.length() > this.alphabet.length()) {
            return false;
        }

        for (int i = 0; i < key.length(); i++) {
            if (!CharSequenceUtils.contains(this.alphabet, key.charAt(i))) {
                return false;
            } else if (!this.repeats && i < key.length() - 1) {
                if (CharSequenceUtils.contains(key, i + 1, key.length(), key.charAt(i))) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<String, Boolean> consumer) {
        for (int length = this.min; length <= this.max; length++) {
            if (!KeyIterator.iterateShortCustomKey(consumer, this.alphabet, length, this.repeats)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String alterKey(String key) {
        return new String(KeyManipulation.changeCharacters(key.toCharArray(), this.alphabet, this.repeats)); // TODO decrease
                                                                                                        // copying into
                                                                                                        // new arrays so
                                                                                                        // much
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ZERO;

        for (int length = this.min; length <= this.max; length++) {
            if(this.repeats) {
                total = total.add(BigInteger.valueOf(this.alphabet.length()).pow(length));
            } else {
                total = total.add(MathUtil.factorialLength(BigInteger.valueOf(this.alphabet.length()), BigInteger.valueOf(length)));
            }
        }

        return total;
    }

    @Override
    public String parse(String input) throws ParseException {
        input = input.toUpperCase(Locale.ROOT);

        for (int i = 0; i < input.length(); i++) {
            if (!CharSequenceUtils.contains(this.alphabet, input.charAt(i))) {
                throw new ParseException("The key can not contain the letter: " + input.charAt(i), i);
            }
        }

        return input;
    }

    @Override
    public String getHelp() {
        return "keyword";
    }

    @Override
    public int getMin() {
        return this.min;
    }

    @Override
    public int getMax() {
        return this.max;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<String> {

        private Optional<CharSequence> alphabet = Optional.empty();
        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();
        private boolean repeats = false;

        private Builder() {
        }

        public Builder setAlphabet(CharSequence alphabet) {
            this.alphabet = Optional.of(alphabet);
            return this;
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        public Builder setRepeats() {
            this.repeats = true;
            return this;
        }

        @Override
        public VariableStringKeyType create() {
            VariableStringKeyType handler = new VariableStringKeyType(this.alphabet.orElse(Alphabet.ALL_26_CHARS), this.min.orElse(2), this.max.orElse(6), this.repeats);
            return handler;
        }

    }
}
