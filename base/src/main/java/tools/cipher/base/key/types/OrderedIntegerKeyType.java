/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import tools.cipher.base.key.IRangedKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import tools.cipher.base.key.KeyManipulation;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.RandomUtil;
import com.alexbarter.lib.util.MathUtil;

public class OrderedIntegerKeyType implements IRangedKeyType<Integer[]> {

    // Both inclusive
    private final int min, max;
    private final Optional<Integer> range;
    private final boolean repeats;
    private final Function<Integer[], String> displayFunc;

    private OrderedIntegerKeyType(int min, int max, Optional<Integer> range, boolean repeats, Function<Integer[], String> displayFunc) {
        this.min = min;
        this.max = max;
        this.range = range;
        this.repeats = repeats;
        this.displayFunc = displayFunc;
    }

    @Override
    public Integer[] randomise() {
        BiFunction<Integer, Integer, Integer[]> func = this.repeats ? KeyGeneration::createRepeatingShortOrderKey : KeyGeneration::createShortOrderKey;

        int length = RandomUtil.pickRandomInt(this.min, this.max);
        return func.apply(this.range.orElse(length), length);
    }

    @Override
    public boolean isValid(Integer[] key) {
        for (int i = 0; i < key.length; i++) {
            if (key[i] >= this.range.orElse(key.length) || key[i] < 0) {
                return false;
            } else if (!this.repeats && i < key.length - 1) {
                if (ArrayUtil.contains(key, i + 1, key.length, key[i])) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<Integer[], Boolean> consumer) {
        for (int length = this.min; length <= this.max; length++) {
            if (!KeyIterator.iterateIntegerArray(consumer, this.range.orElse(length), length, this.repeats)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Integer[] alterKey(Integer[] key) {
        return KeyManipulation.modifyOrder(key);
    }

    @Override
    public String prettifyKey(Integer[] key) {
        return this.displayFunc == null ? Arrays.toString(key) : this.displayFunc.apply(key);
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ZERO;

        for (int length = this.min; length <= this.max; length++) {
            if(this.repeats) {
                total = total.add(BigInteger.valueOf(this.range.orElse(length)).pow(length));
            } else {
                total = total.add(MathUtil.factorialLength(BigInteger.valueOf(this.range.orElse(length)), BigInteger.valueOf(length)));
            }
        }

        return total;
    }

    @Override
    public Integer[] parse(String input) throws ParseException {
        if (input.startsWith("[") && input.endsWith("]")) {
            int offset = 1;
            String[] elements = input.substring(1, input.length() - 1).split(",");
            Integer[] key = new Integer[elements.length];
            for (int i = 0; i < elements.length; i++) {
                try {
                    key[i] = Integer.valueOf(elements[i]);
                } catch (NumberFormatException e) {
                    throw new ParseException(input, offset);
                } finally {
                    offset += elements[i].length() + 1;
                }
            }
            return key;
        }
        throw new ParseException(input, 0);
    }

    @Override
    public String getHelp() {
        return "array";
    }

    @Override
    public int getMin() {
        return this.min;
    }

    @Override
    public int getMax() {
        return this.max;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<Integer[]> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();
        private Optional<Integer> entryRange = Optional.empty();
        private boolean repeats = false;
        private Function<Integer[], String> displayFunc;

        private Builder() {
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        public Builder setEntryRange(int range) {
            this.entryRange = Optional.of(range);
            return this;
        }

        public Builder setRepeats() {
            this.repeats = true;
            return this;
        }

        @Override
        public Builder setDisplay(Function<Integer[], String> displayFunc) {
            this.displayFunc = displayFunc;
            return this;
        }

        @Override
        public OrderedIntegerKeyType create() {
            OrderedIntegerKeyType handler = new OrderedIntegerKeyType(this.min.orElse(2), this.max.orElse(6), this.entryRange, this.repeats, this.displayFunc);
            return handler;
        }
    }
}
