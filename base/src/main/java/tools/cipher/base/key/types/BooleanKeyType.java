/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.RandomUtil;

public class BooleanKeyType implements IKeyType<Boolean> {

    private Boolean[] universe;
    private final boolean alterable;

    private BooleanKeyType(Boolean[] universe, boolean alterable) {
        this.universe = universe;
        this.alterable = alterable;
    }

    @Override
    public Boolean randomise() {
        return RandomUtil.pickRandomElement(this.universe);
    }

    @Override
    public boolean isValid(Boolean key) {
        return ArrayUtil.contains(this.universe, key);
    }

    @Override
    public boolean iterateKeys(Function<Boolean, Boolean> consumer) {
        for (Boolean value : this.universe) {
            if (!consumer.apply(value)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Boolean alterKey(Boolean key) {
        return this.alterable ? !key : key;
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.valueOf(this.universe.length);
    }

    @Override
    public Boolean parse(String input) throws ParseException {
        if (input.equalsIgnoreCase("true")) {
            return true;
        } else if (input.equalsIgnoreCase("false")) {
            return false;
        }

        try {
            int i = Integer.parseInt(input);
            if (i == 0) {
                return false;
            } else if (i == 1) {
                return true;
            }
        } catch (NumberFormatException e) {}

        throw new ParseException(input, 0);
    }

    @Override
    public String getHelp() {
        StringJoiner joiner = new StringJoiner("|");
        for (Boolean value : this.universe) {
            joiner.add(Boolean.toString(value));
        }
        return joiner.toString();
    }

    @Override
    public String[] getExamples() {
        String[] examples = new String[this.universe.length];
        for (int i = 0; i < this.universe.length; i++) {
            examples[i] = Boolean.toString(this.universe[i]);
        }

        return examples;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IKeyBuilder<Boolean> {

        private boolean alterable = false;
        private Optional<Boolean[]> universe = Optional.empty();

        private Builder() {
        }

        public Builder setAlterable() {
            this.alterable = true;
            return this;
        }

        @Override
        public BooleanKeyType create() {
            BooleanKeyType handler = new BooleanKeyType(this.universe.orElse(new Boolean[] {true, false}), this.alterable);
            return handler;
        }

        public Builder setUniverse(Boolean... universe) {
            this.universe = Optional.of(universe);
            return this;
        }

    }
}
