/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.RandomUtil;

public class ObjectKeyType<T> implements IKeyType<T> {

    private final T[] universe;
    private final boolean alterable;

    private ObjectKeyType(T[] universe, boolean alterable) {
        this.universe = universe;
        this.alterable = alterable;
    }

    @Override
    public T randomise() {
        return RandomUtil.pickRandomElement(this.universe);
    }

    @Override
    public boolean isValid(T key) {
        return ArrayUtil.contains(this.universe, key);
    }

    @Override
    public boolean iterateKeys(Function<T, Boolean> consumer) {
        for (T atom : this.universe) {
            if (!consumer.apply(atom)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.valueOf(this.universe.length);
    }

    @Override
    public T alterKey(T key) {
        return this.alterable ? RandomUtil.pickRandomElement(this.universe) : key;
    }

    @Override
    public T parse(String input) throws ParseException {
        for (T atom : this.universe) {
            if (atom instanceof Character) {
                if (((Character) atom) == input.charAt(0)) {
                    return atom;
                }
            } else if (atom instanceof String) {
                if (((String) atom).equalsIgnoreCase(input)) {
                    return atom;
                }
            } else if (atom instanceof Integer) {
                try {
                    if ((Integer) atom == Integer.parseInt(input)) {
                        return atom;
                    }
                } catch (NumberFormatException e) {}
            }
        }
        throw new ParseException(input, 0);
    }

    @Override
    public String getHelp() {
        StringBuilder builder = new StringBuilder();
        for (T atom : this.universe) {
            if (atom instanceof Character) {
                builder.append(atom);
            }
        }
        return builder.toString();
    }

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static class Builder<T> implements IKeyBuilder<T> {

        private Optional<T[]> universe = Optional.empty();
        private boolean alterable = false;

        private Builder() {
        }

        public Builder<T> setUniverse(T... universe) {
            this.universe = Optional.of(universe);
            return this;
        }

        public Builder<T> setAlterable() {
            this.alterable = true;
            return this;
        }

        @Override
        public ObjectKeyType<T> create() {
            ObjectKeyType<T> handler = new ObjectKeyType<>(this.universe.orElse(null), this.alterable);
            return handler;
        }

    }
}
