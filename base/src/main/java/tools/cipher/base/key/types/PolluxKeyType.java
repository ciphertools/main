/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.base.key.KeyIterator;
import com.alexbarter.lib.util.ArrayUtil;
import com.alexbarter.lib.util.RandomUtil;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.characters.CharacterArrayUtils;
import tools.cipher.lib.constants.Alphabet;

public class PolluxKeyType implements IKeyType<Character[]> {

    // Both inclusive
    private final CharSequence alphabet;

    private PolluxKeyType(CharSequence alphabet) {
        this.alphabet = alphabet;
    }

    @Override
    public Character[] randomise() {
        return KeyGeneration.createKeyGuaranteed(this.alphabet, 10);
    }

    @Override
    public boolean isValid(Character[] key) {
        // Contains at least one of each character
        for (int i = 0; i < this.alphabet.length(); i++) {
            if (!ArrayUtil.contains(key, this.alphabet.charAt(i))) {
                return false;
            }
        }

        // Contains only characters from alphabet
        for (int i = 0; i < key.length; i++) {
            if (!CharSequenceUtils.contains(this.alphabet, key[i])) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<Character[], Boolean> consumer) {
        return KeyIterator.iterateObject(consumer, 10, CharSequenceUtils.toArray(this.alphabet));
    }

    @Override
    public Character[] alterKey(Character[] key) {
        int pos = RandomUtil.pickRandomInt(key.length);
        key[pos] = RandomUtil.pickRandomChar(this.alphabet);
        return key;
    }

    @Override
    public String prettifyKey(Character[] key) {
        return CharacterArrayUtils.toString(key);
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.valueOf(59049); // Calculate number
    }

    @Override
    public Character[] parse(String input) throws ParseException {
        if (input.length() != 10) {
            throw new ParseException(input, input.length());
        }

        input = input.toUpperCase(Locale.ROOT);

        Character[] key = new Character[10];
        for (int i = 0; i < 10; i++) {
            key[i] = input.charAt(i);

            if (!CharSequenceUtils.contains(this.alphabet, key[i])) {
                throw new ParseException("Invalid character", i);
            }
        }

        return key;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IKeyBuilder<Character[]> {

        private Optional<CharSequence> alphabet = Optional.empty();

        private Builder() {
        }

        public Builder setAlphabet(CharSequence alphabet) {
            this.alphabet = Optional.of(alphabet);
            return this;
        }

        @Override
        public PolluxKeyType create() {
            PolluxKeyType handler = new PolluxKeyType(this.alphabet.orElse(Alphabet.ALL_POLLUX_CHARS));
            return handler;
        }

    }
}
