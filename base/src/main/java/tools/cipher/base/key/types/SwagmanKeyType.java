/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;
import tools.cipher.base.key.IRangedKeyType.IRangedKeyBuilder;
import tools.cipher.base.key.KeyGeneration;
import tools.cipher.lib.ListUtil;
import com.alexbarter.lib.util.RandomUtil;

public class SwagmanKeyType implements IKeyType<int[]> {

    // Both inclusive
    private final int min, max;

    private SwagmanKeyType(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public int[] randomise() {
        return KeyGeneration.createSwagmanKey(RandomUtil.pickRandomInt(this.min, this.max));
    }

    @Override
    public boolean isValid(int[] key) {
        double sizeD = Math.sqrt(key.length);
        // Is square
        if (sizeD != Math.floor(sizeD)) {
            return false;
        }

        int size = (int) sizeD;

        for (int row = 0; row < size; row++) {
            List<Integer> test = ListUtil.randomRange(0, size - 1);
            for (int col = 0; col < size; col++) {
                if (!test.remove((Integer) key[row * size + col])) {
                    return false;
                }
            }
        }

        for (int col = 0; col < size; col++) {
            List<Integer> test = ListUtil.randomRange(0, size - 1);
            for (int row = 0; row < size; row++) {
                if (!test.remove((Integer) key[row * size + col])) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean iterateKeys(Function<int[], Boolean> consumer) {
        for (int length = this.min; length <= this.max; length++) {
            // TODO
        }

        return true;
    }

    @Override
    public int[] alterKey(int[] key) {
        return key;
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.ZERO;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<int[]> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();

        private Builder() {
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        @Override
        public SwagmanKeyType create() {
            SwagmanKeyType handler = new SwagmanKeyType(this.min.orElse(3), this.max.orElse(5));
            return handler;
        }

    }
}
