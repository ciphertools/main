/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import tools.cipher.base.interfaces.IKeyType;

public class ConstantKeyType<T> implements IKeyType<T> {

    private final T constant;

    private ConstantKeyType(T constant) {
        this.constant = constant;
    }

    @Override
    public T randomise() {
        return this.constant;
    }

    @Override
    public boolean iterateKeys(Function<T, Boolean> consumer) {
        return consumer.apply(this.constant);
    }

    @Override
    public boolean isValid(T key) {
        return key == this.constant ? true : key.equals(this.constant);
    }

    @Override
    public BigInteger getNumOfKeys() {
        return BigInteger.ONE;
    }

    @Override
    public T alterKey(T key) {
        return key;
    }

    @Override
    public T parse(String input) throws ParseException {
        return this.constant;
    }

    @Override
    public String getHelp() {
        return this.constant.toString();
    }

    @Override
    public String[] getExamples() {
        return new String[] {this.constant.toString()};
    }

    public static <T> Builder<T> builder(T constant) {
        return new Builder<>(constant);
    }

    public static class Builder<T> implements IKeyBuilder<T> {

        private final T constant;

        private Builder(T constant) {
            this.constant = constant;
        }

        @Override
        public ConstantKeyType<T> create() {
            return new ConstantKeyType<>(this.constant);
        }
    }
}
