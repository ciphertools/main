/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key.types;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;

import tools.cipher.base.key.IRangedKeyType;
import tools.cipher.lib.characters.CharSequenceUtils;
import tools.cipher.lib.constants.Alphabet;
import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.ListUtil;
import com.alexbarter.lib.util.RandomUtil;
import com.alexbarter.lib.util.MathUtil;

public class PlugboardKeyType implements IRangedKeyType<Integer[]> {

    private int plugboardSize;
    private int minPlugs;
    private int maxPlugs;

    private PlugboardKeyType(int plugboardSize, int minPlugs, int maxPlugs) {
        this.plugboardSize = plugboardSize;
        this.minPlugs = minPlugs;
        this.maxPlugs = maxPlugs;
    }

    @Override
    public Integer[] randomise() {
        int numPlugs = RandomUtil.pickRandomInt(this.minPlugs, this.maxPlugs);
        Integer[] key = ArrayUtil.createRangeInteger(0, this.plugboardSize);
        List<Integer> plugs = ListUtil.range(0, this.plugboardSize - 1);
        for (int i = 0; i < numPlugs; i++) {
            int plug1 = RandomUtil.removeRandomElement(plugs);
            int plug2 = RandomUtil.removeRandomElement(plugs);
            key[plug1] = plug2;
            key[plug2] = plug1;
        }

        return key;
    }

    @Override
    public boolean iterateKeys(Function<Integer[], Boolean> consumer) {
        for (int n = this.minPlugs; n <= this.maxPlugs; n++) {
            Integer[] key = ArrayUtil.createRangeInteger(0, this.plugboardSize);

            for (int i = 0; i < n; i++) {

            }
        }
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Integer[] alterKey(Integer[] key) {
        return key;
    }

    @Override
    public boolean isValid(Integer[] key) {
        if (key.length != this.plugboardSize) {
            return false;
        }

        for (int i = 0; i < this.plugboardSize; i++) {
            if (key[i] < 0 || key[i] >= this.plugboardSize) {
                return false;
            }

            if (key[key[i]] != i) {
                return false;
            }
        }

        return true;
    }

    @Override
    public Integer[] parse(String input) throws ParseException {
        String[] elements = input.split("-");

        if (elements.length > 13) {
            throw new ParseException("Plugboard has too many plugs - max 13", 0);
        }

        for (int i = 0; i < elements.length; i++) {
            if (elements[i].length() > 2) {
                throw new ParseException("Plug has too many letters", i * 3);
            }

            if (elements[i].length() < 2) {
                throw new ParseException("Plug has too many letters", i * 3);
            }

            // Normalise capitalisation
            elements[i] = elements[i].toUpperCase(Locale.ROOT);

            if (!CharSequenceUtils.isCharSubset(elements[i], Alphabet.ALL_26_CHARS)) {
                throw new ParseException("Plug has invalid letters", i * 3);
            }

            if (elements[i].charAt(0) == elements[i].charAt(1)) {
                throw new ParseException("Plug has duplicate letters", i * 3);
            }
        }

        Integer[] key = ArrayUtil.createRangeInteger(0, 26);

        for (int i = 0; i < elements.length; i++) {
            int val1 = elements[i].charAt(0) - 'A';
            int val2 = elements[i].charAt(1) - 'A';

            if (key[val1] != val1 || key[val2] != val2) {
                throw new ParseException("Plug uses letters that have already been used", i * 3);
            }

            key[val1] = val2;
            key[val2] = val1;
        }

        return key;
    }

    @Override
    public String getHelp() {
        return "Plugs:AB-UJ-DH";
    }

    @Override
    public String prettifyKey(Integer[] key) {
        StringJoiner joiner = new StringJoiner(" ", "[", "]");
        List<Integer> plugsUsed = new ArrayList<>(key.length / 2);

        for (int i = 0; i < key.length; i++) {
            if (!plugsUsed.contains(i)) {
                int other = ArrayUtil.indexOf(key, i);
                if (other == i) continue;

                joiner.add(String.format("%c%c", (char) (i + 'A'), (char) (other + 'A')));
                plugsUsed.add(i);
                plugsUsed.add(other);
            }
        }

        return joiner.toString();
    }

    @Override
    public BigInteger getNumOfKeys() {
        BigInteger total = BigInteger.ONE; // 0 Plugs

        BigInteger numTotal = BigInteger.valueOf(this.plugboardSize);

        for (int i = 1; i <= this.plugboardSize / 2; i++) {
            total = total.add(MathUtil.chooseNumPairs(BigInteger.valueOf(i), numTotal));
        }

        return total;
    }

    @Override
    public int getMin() {
        return this.minPlugs;
    }

    @Override
    public int getMax() {
        return this.maxPlugs;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder implements IRangedKeyBuilder<Integer[]> {

        private Optional<Integer> min = Optional.empty();
        private Optional<Integer> max = Optional.empty();

        private Builder() {
        }

        @Override
        public Builder setMin(int min) {
            this.min = Optional.of(min);
            return this;
        }

        @Override
        public Builder setMax(int max) {
            this.max = Optional.of(max);
            return this;
        }

        @Override
        public Builder setRange(int min, int max) {
            return this.setMin(min).setMax(max);
        }

        @Override
        public Builder setSize(int size) {
            return this.setRange(size, size);
        }

        @Override
        public PlugboardKeyType create() {
            PlugboardKeyType handler = new PlugboardKeyType(26, this.min.orElse(0), this.max.orElse(13));
            return handler;
        }
    }
}
