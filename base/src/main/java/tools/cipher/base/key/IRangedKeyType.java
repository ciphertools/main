/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.key;

import tools.cipher.base.interfaces.IKeyType;

public interface IRangedKeyType<K> extends IKeyType<K> {

    public int getMin();

    public int getMax();

    public interface IRangedKeyBuilder<K> extends IKeyBuilder<K> {

        public IRangedKeyBuilder<K> setMin(int min);

        public IRangedKeyBuilder<K> setMax(int max);

        public IRangedKeyBuilder<K> setRange(int min, int max);

        default IRangedKeyBuilder<K> setRange(int[] range) {
            return this.setRange(range[0], range[1]);
        }

        public IRangedKeyBuilder<K> setSize(int size);
    }

}
