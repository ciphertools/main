/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

@FunctionalInterface
public interface IFormat {

    CharSequence format(CharSequence text);

    /**
     * Applies this then the given format
     */
    default IFormat then(IFormat formatIn) {
        return (text) -> formatIn.format(this.format(text));
    }

    /**
     * Applies this after the given format
     */
    default IFormat after(IFormat formatIn) {
        return (text) -> this.format(formatIn.format(text));
    }
}
