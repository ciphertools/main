/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.math.BigDecimal;
import java.math.BigInteger;

public interface IProgress {

    /**
     * Increases the current value by 1, this could be as simple
     * as getting the current value and adding 1 or an optimised version
     *
     * @return Whether the percentage was recalculated
     */
    public boolean increment();

    /**
     * Sets the current value, this should not be smaller than 0
     * or greater than {@link #getMax()}.
     *
     * @param value The new value
     * @return Whether the percentage was recalculated
     */
    public boolean set(BigInteger value);

    /**
     * Sets the current value, this should not be smaller than 0
     * or greater than {@link #getMax()}.
     * By default wraps the long in BigInteger and passes to {@link #set(BigInteger)}
     *
     * @param max The new value
     * @return Whether the percentage was recalculated
     */
    default boolean set(long max) {
        return this.set(BigInteger.valueOf(max));
    }

    /**
     * The current value
     *
     * @return The current value
     */
    public BigInteger get();

    /**
     * The value at which indicates the task is done
     * i.e 100% complete
     *
     * @return The max value
     */
    public BigInteger getMax();

    /**
     * Sets the max value to specified value
     *
     * @param max The new max value
     * @return Whether the percentage was recalculated
     */
    public boolean setMax(BigInteger max);

    /**
     * Adds the specified value to the max value
     *
     * @param max The value to add
     * @return Whether the percentage was recalculated
     */
    public boolean addMax(BigInteger max);

    /**
     * Adds the specified value to the max value
     * By default wraps the long in BigInteger and passes to {@link #addMax(BigInteger)}
     *
     * @param max The value to add
     */
    default boolean addMax(long max) {
        return this.addMax(BigInteger.valueOf(max));
    }

    /**
     * In practice this is likely {@link #get()} / {@link #getMax()}
     * but depending on implementation value may be cached or only
     * recalculated on occasion to reduce computation cost
     *
     * @return The percentage done, a value from 0-1
     */
    public BigDecimal getPercentage();

    /**
     * Gets a rough estimated for the amount of time remaining
     *
     * @return The amount of time estimated till the task is complete
     */
    public int getEstimatedTimeRemaining();

    /**
     * Indicates when this object has started being used
     */
    public void start();

    /**
     * Indicates when this object has finished
     */
    public void finish();

    /**
     * Resets the state of this progress. Should be in same visible
     * state when the object was first created however the internal
     * state may be different.
     *
     * This includes current value, max value and percentage
     */
    public void reset();

    /**
     * Sets the progress bar to indeterminate status, in other words
     * the max value is unknown but are still progressing towards the
     * end.
     *
     * @param indeterminate The indeterminate status
     */
    public void setIndeterminate(boolean indeterminate);

    /**
     * Adds callback for when the progress changes
     * In the default implementation this is not called every time
     * the value is updated but only when the actually progress
     * percentage is updated.
     *
     * @param callback The callback
     */
    public void addCallback(IProgressCallback callback);

    @FunctionalInterface
    public interface IProgressCallback {

        public void onChange(IProgress prog);
    }
}
