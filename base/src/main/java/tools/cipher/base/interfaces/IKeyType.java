/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Function;

import javax.annotation.Nullable;

public interface IKeyType<K> {

    /**
     * Generates a randomised key
     *
     * @return A randomised key from the set of possible keys
     */
    K randomise();

    /**
     * Iterates through all the possible keys
     *
     * @param consumer Keys are directed here, if the iteration process
     *                 needs to be terminated false can be returned,
     *                 otherwise to continue normally return true
     * @return Returns true if all the keys where iterated and false
     *         if the <code>consumer</code> returned false at some point
     */
    public boolean iterateKeys(Function<K, Boolean> consumer);

    /**
     * Given a key alters the key is some way to another that is somehow
     * similar. Used in simulated annealing.
     *
     * @param key The key
     * @return A key "close" to the given key
     */
    K alterKey(K key);

    /**
     * Checks if the key is in the domain of this key type.
     * E.g the key could be an integer, but this key type may
     * only be valid on non-negative integers. So <code>isValid(-1)</code>
     * returns false, <code>isValid(x) = true</code>, x positive
     *
     * @param key The key
     * @return If the key is in the domain of this key type
     */
    boolean isValid(K key);

    /**
     * Gets the number of keys in the domain of this key type
     *
     * @return The total number of possible keys
     *         null indicates infinite keys.
     */
    @Nullable
    BigInteger getNumOfKeys();

    default K inverseKey(K key) {
        return key;
    }

    /**
     * Converts the key into something readable.
     * E.g An integer key 12 returns "12"
     *
     * @param key The key
     * @return A readable version of the key
     */
    default String prettifyKey(K key) {
        return key.toString();
    }

    /**
     * Tries to parse a string and convert to a key
     *
     * @param input A string version of the key
     * @return A key representing the input
     * @throws ParseException If the input could not be converted to a key
     */
    default K parse(String input) throws ParseException {
        throw new UnsupportedOperationException();
    }

    /**
     * Gets a example of how to represent a key as a string
     *
     * @return A helpful example
     */
    default String getHelp() {
        return this.getClass().getSimpleName();
    }

    /**
     * Gets a example of how to represent a key as a string
     *
     * @return A helpful example
     */
    default String[] getExamples() {
        throw new UnsupportedOperationException();
    }

    public interface IKeyBuilder<K> {

        /**
         * Creates the Key type from the given builder params
         */
        public IKeyType<K> create();

        /**
         * Sets the function which converts a key to its string version.
         * This must be implemented by the super class otherwise this
         * feature is unsupported
         *
         * @param displayFunc Then conversion function
         * @return This {@link IKeyBuilder}
         */
        default IKeyBuilder<K> setDisplay(Function<K, String> displayFunc) {
            throw new UnsupportedOperationException();
        }
    }
}
