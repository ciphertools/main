/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

public interface ISettings {

    public ISettings put(String key, Object value);

    public Object getRaw(String key, Object default_);

    @SuppressWarnings("unchecked")
    default <T> T get(String key, T default_) {
        return get(key, x -> x, (Class<T>)default_.getClass(), default_);
    }

    @SuppressWarnings("unchecked")
    default <T> List<T> getList(String key, List<T> default_) {
        return get(key, x -> x, List.class, default_);
    }

    @SuppressWarnings("unchecked")
    default <T> Map<String, T> getMap(String key, Map<String, T> default_) {
        return get(key, x -> x, Map.class, default_);
    }

    @SuppressWarnings("unchecked")
    default <I, T> T get(String key, Function<I, T> inputTransformer, Class<I> type, T default_) {
        Object raw = this.getRaw(key, null);
        if (raw != null && type.isAssignableFrom(raw.getClass())) {
            return inputTransformer.apply((I) raw);
        } else {
            return default_;
        }
    }

    /**
     * Perform the save action. Saving to file etc
     */
    public void save();

    /**
     * Perform the load action. Loading from file etc
     */
    public void load();

    public void addHook(ILoadHook hook);

    /**
     * Marks that some settings have been changed and the
     * contents should be saved again.
     */
    public void markDirty();

    /**
     * @return If settings have changed and needs saving
     */
    public boolean isDirty();

}
