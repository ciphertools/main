/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import javax.annotation.Nullable;

import tools.cipher.base.solve.DecryptionMethod;

public interface ICipherAttack {

    public IDecryptionTracker startAttack(CharSequence text, DecryptionMethod method, ICipherProgram app);

    /**
     * Called once the attack has taken place
     * @param tracker Could be null if the attack errors
     * @param forced Was the attack terminated or did it finish
     */
    public void endAttack(@Nullable IDecryptionTracker tracker, boolean forced);

    public ICipherAttack addCallback(IDecryptionTracker.ISolutionCallback callback);
}
