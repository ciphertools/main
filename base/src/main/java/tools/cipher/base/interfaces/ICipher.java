/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.alexbarter.lib.Pair;
import tools.cipher.lib.characters.CharArrayWrapper;

public interface ICipher<K> {

    /**
     * Alters the plaintext so that the cipher can be applied to it E.g. Padding the
     * text so it is a multiple of N (Hill Cipher) Making sure there is no double
     * letters (Playfair Cipher)
     */
    default CharSequence normaliseText(CharSequence plainText, K key) {
        return plainText;
    }

    CharSequence encode(CharSequence plainText, K key);

    default String encode(String plainText, K key) {
        return this.encode(this.normaliseText(plainText, key), key).toString();
    }

    default CharSequence decode(CharSequence cipherText, K key) {
        return new CharArrayWrapper(this.decodeEfficiently(cipherText, key));
    }

    default String decode(String cipherText, K key) {
        return new String(this.decodeEfficiently(cipherText, key));
    }

    /**
     * Used in cipher solvers for the most memory and speed efficiency code
     */
    char[] decodeEfficiently(CharSequence cipherText, @Nullable char[] plainText, K key);

    default char[] decodeEfficiently(CharSequence cipherText, K key) {
        return decodeEfficiently(cipherText, new char[cipherText.length()], key);
    }

    @Deprecated // Use corrected spelling version decodeEfficiently
    default char[] decodeEfficently(CharSequence cipherText, @Nullable char[] plainText, K key) {
        return decodeEfficiently(cipherText, plainText, key);
    }

    @Deprecated // Use corrected spelling version decodeEfficiently
    default char[] decodeEfficently(CharSequence cipherText, K key) {
        return decodeEfficiently(cipherText, new char[cipherText.length()], key);
    }

    /**
     * Will the same ciphertext be produced each run for the same PT and key
     */
    default boolean deterministic() {
        return true;
    }

    /**
     * Generates a random key which may vary on the ciphertext length
     *
     * @param length The length of the ciphertext
     * @return A randomised key
     */
    default K randomiseKey(int length) {
        return this.randomiseKey();
    }

    /**
     * This function can and should be overridden but should avoid
     * invoking it. Instead invoke the text length aware {@link #randomiseKey(int)}.
     * If you know the cipher algorithm being used does not have any correlation
     * between text length and key then it should be save to call.
     *
     * Returns a random key
     *
     * @return A randomised key
     */
    @Deprecated
    K randomiseKey();

    /**
     * Iterates thought all the ciphers keys, the key object K is not necessarily
     * immutable so if access is required after a copy is needed
     */
    boolean iterateKeys(Function<K, Boolean> consumer);

    default void iterateKeys(Consumer<K> consumer) {
        iterateKeys(key -> { consumer.accept(key); return true; });
    }

    /**
     * Used for simulated annealing, returns a key that is similar to the given key
     * @param key
     * @param temp
     * @param count
     */
    default K alterKey(K key, double temp, int count) {
        return key;
    }

    /**
     * Converts the key into something readable, often with some labelling.
     * E.g The Caesar Cipher for a key of 12 returns "12"
     *
     * @param key The key
     * @return A readable version of the key
     */
    default String prettifyKey(K key) {
        return key.toString();
    }

    default K parseKey(String input) throws ParseException {
        throw new UnsupportedOperationException();
    }

    @Nullable
    default String getHelp() {
        return null;
    }

    boolean isValid(K key);

    BigInteger getNumOfKeys();

    /**
     * @return Total number of keys for the whole domain
     *         null indicates infinite keys.
     */
    @Nullable
    default BigInteger getTotalNumOfKeys() {
        return null;
    }

    /**
     * Encrypts using a random key which is not returned.
     * @see #randomEncodePair(String) or
     * @see #randomEncodeKeyStr(String) if you would like the random key used
     * to generate the ciphertext
     *
     * @param plainText The plaintext
     * @return The ciphertext
     */
    default String randomEncode(String plainText) {
        return this.encode(plainText, this.randomiseKey(plainText.length()));
    }

    default Pair<String, K> randomEncodePair(String plainText) {
        K key = this.randomiseKey(plainText.length());
        return new Pair<>(this.encode(plainText, key), key);
    }

    default Pair<String, String> randomEncodeKeyStr(String plainText) {
        K key = this.randomiseKey(plainText.length());
        return new Pair<>(this.encode(plainText, key), this.prettifyKey(key));
    }

    default Difficulty getDifficulty() {
        return Difficulty.EASY;
    }

    enum Difficulty {
        EASY(1), NORMAL(5), HARD(10);

        int level;

        Difficulty(int level) {
            this.level = level;
        }

        public int getLevel() {
            return this.level;
        }

        @Deprecated
        public boolean isEasierThan(int levelIn) {
            return this.level <= levelIn;
        }

        public boolean isEasierThan(Difficulty levelIn) {
            return this.level <= levelIn.level;
        }

        public boolean isHarderThan(Difficulty levelIn) {
            return this.level >= levelIn.level;
        }
    }

}
