/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.io.PrintStream;

import tools.cipher.lib.language.ILanguage;

public interface ICipherProgram {

    /**
     * Gets the settings
     *
     * @return The programs settings
     */
    public ISettings getSettings();

    /**
     * Gets the current language
     *
     * @return The language
     */
    public ILanguage getLanguage();

    /**
     * Gets the {@link PrintStream} which all program outputs are directed to
     * This could be {@link System#out} or some custom output
     *
     * @return The output
     */
    public PrintStream out();

    /**
     * Gets the current progress in the task being run
     * @return The current task's progress
     */
    public IProgress getProgress();

    /**
     * @return Whether the decryption process should be stopped
     */
    public boolean shouldStop();
}
