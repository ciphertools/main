/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.alexbarter.lib.Pair;
import tools.cipher.lib.result.Solution;

public interface IAttackMethod<K> {

    ICipher<K> getCipher();

    /**
     * Decrypts the cipher using the given key checks if it is better than the
     * the best solution and if so updates it.
     * @param tracker The results tracker
     * @param key The key to decrypt using
     */
    default void decryptAndUpdate(IDecryptionTracker tracker, K key) {
        Solution solution = this.toSolution(tracker, key);

        synchronized (tracker) {
            this.updateIfBetterThanBest(tracker, solution, key);
        }
    }

    default Solution toSolution(IDecryptionTracker tracker, K key) {
        Pair<char[], Boolean> holder = tracker.getVariableHolder();
        return new Solution(this.getCipher().decodeEfficiently(tracker.getCipherText(), holder.getLeft(), key), tracker.getLanguage(), holder.getRight());
    }

    default boolean updateIfBetterThanBest(IDecryptionTracker tracker, Solution solution) {
        return this.updateIfBetterThanBest(tracker, solution, (K) null);
    }

    default boolean updateIfBetterThanBest(IDecryptionTracker tracker, Solution solution, K key) {
        if (this.isBetterThanBest(tracker, solution)) {
            this.updateBestSolution(tracker, solution, key);
            return true;
        }

        return false;
    }

    default boolean updateIfBetterThanBest(IDecryptionTracker tracker, Solution solution, Supplier<K> key) {
        if (this.isBetterThanBest(tracker, solution)) {
            this.updateBestSolution(tracker, solution, key.get());
            return true;
        }

        return false;
    }

    default boolean isBetterThanBest(IDecryptionTracker tracker, Solution solution) {
        return solution.compareTo(tracker.getBestSolution()) < 0;
    }

    /**
     * Updates the best solution. Converts the key to a readable form, copies the
     * plaintext array, prints out the solution and updates the solution panel UI.
     *
     * @param tracker  The {@link IDecryptionTracker} instance that tracks best solutions
     * @param solution The solution to become the best solution
     * @param key      The key to generate this solution, can be null which indicates
     *                 the keystring was already set at some point
     */
    default void updateBestSolution(IDecryptionTracker tracker, Solution solution, @Nullable K key) {

        solution.bake();
        if (key != null) {
            solution.setKeyString(this.getCipher().prettifyKey(key));
        }

        this.output(tracker, solution.toString());
        tracker.setBestSolution(solution);
    }


    /**
     * @return If the cipher attack is muted
     */
    default boolean isMuted() {
        return false;
    }

    default boolean output(IDecryptionTracker tracker, String text, Object... format) {
        if(!this.isMuted() && !tracker.shouldStop()) {
            tracker.out().println(format.length > 0 ? String.format(text, format) : text);
            return true;
        }

        return false;
    }
}
