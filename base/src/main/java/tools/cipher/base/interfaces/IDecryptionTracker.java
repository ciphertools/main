/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.base.interfaces;

import java.io.PrintStream;
import java.util.function.Function;

import tools.cipher.base.solve.DecryptionTracker;
import com.alexbarter.lib.Pair;
import tools.cipher.lib.language.ILanguage;
import tools.cipher.lib.result.Solution;

public interface IDecryptionTracker {

    public CharSequence getCipherText();

    default int getLength() {
        return this.getCipherText().length();
    }

    public Solution getBestSolution();
    public Solution getLastSolution();

    public IDecryptionTracker setBestSolution(Solution solutionIn);
    public IDecryptionTracker setLastSolution(Solution solutionIn);

    public boolean addSolution(Solution solutionIn);

    public boolean resetSolution();

    public boolean shouldStop();

    public DecryptionTracker setOutputLength(Function<Integer, Integer> outputLength);

    /**
     * Gets the progress through the decryption process
     * @return The progress
     */
    public IProgress getProgress();

    /**
     * Gets the language the output/plaintext should be in
     * and as such access to the language statistics so the
     * fitness of an output can be quantified.
     * @return The plaintext's Language object
     */
    public ILanguage getLanguage();

    /**
     * Returns an char array of length {@link #setOutputLength(Function)}
     * That is guaranteed to not be used anywhere else so is fine to be used
     * when decryption is taking place in parallel
     *
     * @return A new array
     */
    public char[] getNewHolder();

    /**
     *
     * @return
     */
    public char[] getHolder();

    /**
     * Returns an char array of length {@link #setOutputLength(Function)}
     * and a boolean indicating if it is new or a reusable array
     * @return
     */
    public Pair<char[], Boolean> getVariableHolder();

    public ICipherProgram getApp();

    public PrintStream out();

    public void addCallback(ISolutionCallback callback);

    @FunctionalInterface
    public static interface ISolutionCallback {

        /**
         * Called when a new best solution is found.
         * The {@link IDecryptionTracker#getBestSolution()}
         * @param tracker The tracker
         */
        public void onChange(IDecryptionTracker tracker);

        /**
         * Receives possible solutions, note that not all solutions may be pass here
         * due to possible filtering for performance reasons.
         *  Global solution collecting disabled
         *  Fitness below a certain threshold
         *
         * @param tracker The tracker
         * @param solutionIn The solution
         * @return If the solution was stored and the text array should be baked
         */
        default boolean onSolution(DecryptionTracker tracker, Solution solutionIn) { return false; }
    }
}
