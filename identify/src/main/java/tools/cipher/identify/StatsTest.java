/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.identify;

import tools.cipher.lib.language.ILanguage;
import tools.cipher.util.VigenereType;

public class StatsTest {

    public static double calculateSubTypeLDI(String text, VigenereType type, ILanguage language) {
        if (StatCalculator.containsDigit(text) || StatCalculator.containsHash(text))
            return 0.0D;

        double largestSum = 0.0D;

        for (int period = 2; period <= 15; period++) {
            double sum = 0.0D;
            for (int col = 0; col < period; col++)
                sum += best_di(text, period, col, type, language);

            sum /= period;
            largestSum = Math.min(largestSum, sum);
        }

        return largestSum;
    }

    public static double best_di(String text, int period, int col, VigenereType type, ILanguage language) {
        double bestScore = 0.0D;

        int rows = (int) Math.ceil(text.length() / period);
        for (char keyLeft = 'A'; keyLeft <= 'Z'; keyLeft++) {
            for (char keyRight = 'A'; keyRight <= 'Z'; keyRight++) {
                double score = 0.0D;
                int ct = 0;
                char kl1 = keyLeft;
                char kr1 = keyRight;
                for (int r = 0; r < rows; r++) {
                    if (col + r * period + 1 >= text.length())
                        break;

                    char pl = type.decode(text.charAt(col + r * period), kl1);
                    char pr = type.decode(text.charAt(col + r * period + 1), kr1);
                    score += language.getDiagramData().getMaxValue() - language.getDiagramData().getValue(new char[] { pl, pr }, 0);
                    ct++;
                }
                if (ct == 0)
                    return 0.0D;

                score *= 100;
                score /= ct;
                if (score < bestScore)
                    bestScore = score;
            }
        }
        return bestScore;
    }
}
