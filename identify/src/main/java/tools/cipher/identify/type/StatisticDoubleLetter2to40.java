/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.identify.type;

import tools.cipher.identify.StatCalculator;
import tools.cipher.identify.TextStatistic;

public class StatisticDoubleLetter2to40 extends TextStatistic<Boolean> {

    public StatisticDoubleLetter2to40(String text) {
        super(text);
    }

    @Override
    public TextStatistic<Boolean> calculateStatistic() {
        this.value = !StatCalculator.calculateSeriatedPlayfair(this.text, 2, 40);
        return this;
    }
}
