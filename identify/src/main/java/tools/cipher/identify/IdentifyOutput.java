/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.identify;

import java.util.ArrayList;
import java.util.List;

import tools.cipher.lib.result.ResultPositive;

public class IdentifyOutput extends ResultPositive {

    public String id;
    public List<IdentifyOutput> subOutput;
    public Pos pos;

    public IdentifyOutput(String id, double score) {
        this(id, Pos.LEAF, score);
    }

    public IdentifyOutput(String id, Pos pos, double score) {
        super(score);
        this.id = id;
        this.pos = pos;
        this.subOutput = new ArrayList<IdentifyOutput>();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String)
            return ((String) obj).equals(this.id);
        return false;
    }

    @Override
    public String toString() {
        return String.format("IdentiftyOutput = [id:%s, s:%f, o:%s]", this.id, this.score, this.subOutput);
    }

    public static enum Pos {
        BRANCH, LEAF;
    }
}
