/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.identify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import javax.swing.JProgressBar;

import tools.cipher.base.interfaces.ICipher;
import tools.cipher.identify.holder.DataHolder;
import tools.cipher.identify.type.StatisticBifid0;
import tools.cipher.identify.type.StatisticDiagrahpicICx10000;
import tools.cipher.identify.type.StatisticDoubleLetter;
import tools.cipher.identify.type.StatisticDoubleLetter2to40;
import tools.cipher.identify.type.StatisticEvenDiagrahpicICx10000;
import tools.cipher.identify.type.StatisticICx1000;
import tools.cipher.identify.type.StatisticKappaICx1000;
import tools.cipher.identify.type.StatisticLogDigraph;
import tools.cipher.identify.type.StatisticLogDigraphAffine;
import tools.cipher.identify.type.StatisticLogDigraphAutokeyBeaufort;
import tools.cipher.identify.type.StatisticLogDigraphAutokeyPorta;
import tools.cipher.identify.type.StatisticLogDigraphAutokeyVariant;
import tools.cipher.identify.type.StatisticLogDigraphAutokeyVigenere;
import tools.cipher.identify.type.StatisticLogDigraphBeaufort;
import tools.cipher.identify.type.StatisticLogDigraphCaesar;
import tools.cipher.identify.type.StatisticLogDigraphPorta;
import tools.cipher.identify.type.StatisticLogDigraphPortax;
import tools.cipher.identify.type.StatisticLogDigraphReversed;
import tools.cipher.identify.type.StatisticLogDigraphSlidefairBeaufort;
import tools.cipher.identify.type.StatisticLogDigraphSlidefairVariant;
import tools.cipher.identify.type.StatisticLogDigraphSlidefairVigenere;
import tools.cipher.identify.type.StatisticLogDigraphVariant;
import tools.cipher.identify.type.StatisticLogDigraphVigenere;
import tools.cipher.identify.type.StatisticLongRepeat;
import tools.cipher.identify.type.StatisticMaxBifid3to15;
import tools.cipher.identify.type.StatisticMaxICx1000;
import tools.cipher.identify.type.StatisticMaxNicodemus3to15;
import tools.cipher.identify.type.StatisticMaxTrifid3to15;
import tools.cipher.identify.type.StatisticMaxUniqueCharacters;
import tools.cipher.identify.type.StatisticNormalOrder;
import tools.cipher.identify.type.StatisticPercentageOddRepeats;
import tools.cipher.identify.type.StatisticTextLengthMultiple;
import tools.cipher.identify.type.StatisticTrigraphNoOverlapICx100000;
import tools.cipher.lib.constants.StatisticsLib;
import tools.cipher.lib.file.FileReader;
import tools.cipher.lib.math.Statistics;
import tools.cipher.lib.registry.IRegistry;
import tools.cipher.lib.registry.Registry;

public class StatisticRegistry {

    public static final IRegistry<String, Function<String, TextStatistic<?>>> TEXT_STATISTIC_MAP = Registry.<Function<String, TextStatistic<?>>>builder().setRegistryName("TextStatistic").build();
    public static final IRegistry<String, String>                             DISPLAY_NAME_MAP   = Registry.builder(String.class).setRegistryName("StatisticDisplayName").build();

    public static boolean registerStatistic(String id, Function<String, TextStatistic<?>> textStatistic) {
        return registerStatistic(id, textStatistic, id);
    }

    public static boolean registerStatistic(String id, Function<String, TextStatistic<?>> textStatistic, String shortName) {
        if (TEXT_STATISTIC_MAP.register(id, textStatistic)) {
            if (DISPLAY_NAME_MAP.register(id, shortName)) {
                return true;
            } else {
                TEXT_STATISTIC_MAP.remove(id);
            }
        }

        return false;
    }

    public static List<IdentifyOutput> orderCipherProbability(String text) {
        return orderCipherProbability(createTextStatistics(text));
    }

    public static List<IdentifyOutput> orderCipherProbability(HashMap<String, TextStatistic<?>> stats) {
        return orderCipherProbability(stats, TEXT_STATISTIC_MAP.getKeys());
    }

    public static List<IdentifyOutput> orderCipherProbability(HashMap<String, TextStatistic<?>> stats, Collection<String> doOnly) {
        List<IdentifyOutput> computedResult = new ArrayList<IdentifyOutput>();

        Map<String, Object> statistic = CipherStatistics.getOtherCipherStatistics();
        traverseDataTree(stats, doOnly, computedResult, new ArrayList<String>(), statistic);

        return computedResult;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void traverseDataTree(Map<String, TextStatistic<?>> stats, Collection<String> doOnly, List<IdentifyOutput> computedResult, Collection<String> keyBefore, Map<String, Object> toCheck) {
        for (String key : toCheck.keySet()) {
            Map<String, ?> nextBranch = (Map<String, ?>) toCheck.get(key);
            List<String> copy = new ArrayList<String>(keyBefore);
            copy.add(key);

            // Determines which tree is next (true = end of tree, false = recursive
            // function)
            boolean type = false;

            // Checks the first value to see if is DataHolder instance, if it is we have
            // reached the end of the true
            for (Object value : nextBranch.values()) {
                type = value instanceof DataHolder;
                break;
            }

            if (type) {
                double value = scoreCipher(stats, (Map<String, DataHolder>) nextBranch, doOnly);

                IdentifyOutput identifyOutput;
                List<IdentifyOutput> last = computedResult;
                for (int i = 0; i < copy.size(); i++) {
                    int index = indexOf(copy.get(i), last);
                    if (index != -1) {
                        IdentifyOutput old = last.get(index);
                        identifyOutput = new IdentifyOutput(old.id, Math.min(old.score, value));
                        identifyOutput.subOutput.addAll(old.subOutput);
                        old.subOutput.clear(); // Not sure it this is necessary, it may free up some memory but since
                                               // all the object referenced in the list are still in use I assume this
                                               // list just points to memory locations that are still in use
                        last.set(index, identifyOutput);
                    } else {
                        identifyOutput = new IdentifyOutput(copy.get(i), value);
                        last.add(identifyOutput);
                    }

                    last = identifyOutput.subOutput;
                }
            } else
                traverseDataTree(stats, doOnly, computedResult, copy, (Map<String, Object>) nextBranch);
        }
    }

    public static int indexOf(String key, List<IdentifyOutput> computedResult) {
        for (int i = 0; i < computedResult.size(); i++)
            if (computedResult.get(i).id.equals(key))
                return i;
        return -1;
    }

    /**
     * Return a number larger than 0, the closer to zero the better
     *
     * @param stats  Pre-calculated statistics about a particular piece of text
     * @param data   Map of statistics IDs and their expected values for a specific
     *               cipher
     * @param doOnly A list of statistics IDs to test @see
     *               nationalcipher.lib.StatisticsLib
     * @return A quantised idea of how likely this text is to this cipher attributes
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static double scoreCipher(Map<String, TextStatistic<?>> stats, Map<String, DataHolder> data, Collection<String> doOnly) {
        double value = 0.0D;

        for (String id : data.keySet()) {
            if (doOnly.contains(id)) {
                value += stats.get(id).quantify(data.get(id));
            }
        }

        return value;
    }

    public static HashMap<String, TextStatistic<?>> createTextStatistics(String text) {
        return createTextStatistics(text, null);
    }

    public static HashMap<String, TextStatistic<?>> createTextStatistics(String text, JProgressBar value) {
        HashMap<String, TextStatistic<?>> stats = new HashMap<String, TextStatistic<?>>();
        if (value != null) { value.setMaximum(TEXT_STATISTIC_MAP.size()); };
        for (Entry<String, Function<String, TextStatistic<?>>> entry : TEXT_STATISTIC_MAP.getEntries()) {
            if (value != null) { value.setString(DISPLAY_NAME_MAP.get(entry.getKey())); }
            TextStatistic<?> stat = entry.getValue().apply(text);
            stats.put(entry.getKey(), stat.calculateStatistic());
            if (value != null) { value.setValue(value.getValue() + 1); }
        }

        return stats;
    }

    public static void calculateStatPrint(ICipher<?> cipher, Function<String, TextStatistic<?>> textStatistic, int times) {
        List<Double> values = new ArrayList<Double>();

        try {
            FileReader.read("/plainText.txt", (line) -> {
                String plainText = line;

                for (int i = 0; i < times; i++) {
                    TextStatistic<?> test = textStatistic.apply(cipher.randomEncode(plainText));
                    // System.out.println(test.text);
                    test.calculateStatistic();
                    try {
                        if (test.value instanceof Number)
                            values.add(((Number) test.value).doubleValue());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Statistics<Double> stats = new Statistics<>(values);

        String name = cipher.getClass().getSimpleName();
        String variableName = "";
        if (!Character.isJavaIdentifierStart(name.charAt(0))) {
            variableName += "_";
        }
        for (char c : name.toCharArray()) {
            if (!Character.isJavaIdentifierPart(c)) {
                variableName += "_";
            } else {
                variableName += c;
            }
        }

        System.out.println(variableName + ".put(" + textStatistic.getClass().getSimpleName() + ", new DataHolder(" + String.format("%.2f", stats.getMean()) + ", " + String.format("%.2f", stats.getStandardDeviation()) + ")); //Min: " + String.format("%.2f", stats.getMin()) + " Max: " + String.format("%.2f", stats.getMax()));
    }

    public static void registerStatistics() {
        // Default all ciphers
        registerStatistic(StatisticsLib.IC_x1000, StatisticICx1000::new, "IC");
        registerStatistic(StatisticsLib.IC_MAX_1to15_x1000, StatisticMaxICx1000::new, "MIC");
        registerStatistic(StatisticsLib.IC_2_TRUE_x10000, StatisticDiagrahpicICx10000::new, "DIC");
        registerStatistic(StatisticsLib.IC_2_FALSE_x10000, StatisticEvenDiagrahpicICx10000::new, "DIC_E");
        registerStatistic(StatisticsLib.IC_3_FALSE_x100000, StatisticTrigraphNoOverlapICx100000::new, "TIC_E");
        registerStatistic(StatisticsLib.IC_KAPPA_x1000, StatisticKappaICx1000::new, "MKA");
        registerStatistic(StatisticsLib.LOG_DIGRAPH, StatisticLogDigraph::new, "LDI");
        registerStatistic(StatisticsLib.LOG_DIGRAPH_REVERSED, StatisticLogDigraphReversed::new, "LDI_R");
        registerStatistic(StatisticsLib.LONG_REPEAT, StatisticLongRepeat::new, "LR");
        registerStatistic(StatisticsLib.LONG_REPEAT_ODD_PERCENTAGE, StatisticPercentageOddRepeats::new, "LR_OP");
        registerStatistic(StatisticsLib.NORMAL_ORDER, StatisticNormalOrder::new, "NOR");

        // Boolean statistics
        registerStatistic(StatisticsLib.DOUBLE_LETTER_EVEN, StatisticDoubleLetter::new, "DBL_PLAY");
        registerStatistic(StatisticsLib.DOUBLE_LETTER_EVEN_2to40, StatisticDoubleLetter2to40::new, "DBL_SERP");
        registerStatistic(StatisticsLib.MAX_UNIQUE_CHARACTERS, StatisticMaxUniqueCharacters::new, "MAX_UNIQUE");
        registerStatistic(StatisticsLib.TEXT_LENGTH_MULTIPLE, StatisticTextLengthMultiple::new, "DIV_N");

        StatisticRegistry.registerStatistic(StatisticsLib.BIFID_MAX_3to15, StatisticMaxBifid3to15::new, "BIC");
        StatisticRegistry.registerStatistic(StatisticsLib.BIFID_0, StatisticBifid0::new, "BIC_Z");
        StatisticRegistry.registerStatistic(StatisticsLib.NICODEMUS_MAX_3to15, StatisticMaxNicodemus3to15::new, "NIC");
        StatisticRegistry.registerStatistic(StatisticsLib.TRIFID_MAX_3to15, StatisticMaxTrifid3to15::new, "TIC");

        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_CAESAR, StatisticLogDigraphCaesar::new, "LDI_CAE");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_AFFINE, StatisticLogDigraphAffine::new, "LDI_AFF");

        // Vigenere Family
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_BEAUFORT, StatisticLogDigraphBeaufort::new, "LDI_BEU");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_PORTA, StatisticLogDigraphPorta::new, "LDI_POR");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_PORTAX, StatisticLogDigraphPortax::new, "LDI_PTX");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_VARIANT, StatisticLogDigraphVariant::new, "LDI_VAR");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_VIGENERE, StatisticLogDigraphVigenere::new, "LDI_VIG");

        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_AUTOKEY_BEAUFORT, StatisticLogDigraphAutokeyBeaufort::new, "LDI_A_BEU");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_AUTOKEY_PORTA, StatisticLogDigraphAutokeyPorta::new, "LDI_A_POR");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_AUTOKEY_VARIANT, StatisticLogDigraphAutokeyVariant::new, "LDI_A_VAR");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_AUTOKEY_VIGENERE, StatisticLogDigraphAutokeyVigenere::new, "LDI_A_VIG");

        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_SLIDEFAIR_BEAUFORT, StatisticLogDigraphSlidefairBeaufort::new, "LDI_SLI_BEU");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_SLIDEFAIR_VARIANT, StatisticLogDigraphSlidefairVariant::new, "LDI_SLI_VAR");
        StatisticRegistry.registerStatistic(StatisticsLib.LOG_DIGRAPH_SLIDEFAIR_VIGENERE, StatisticLogDigraphSlidefairVigenere::new, "LDI_SLI_VIG");
    }
}
