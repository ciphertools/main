/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.identify.holder;

public abstract class DataHolder<T> {

    public T value;

    public DataHolder(T value) {
        this.value = value;
    }

    // Default calculator see TextStatistic.quantify(DataHolder);
    public abstract double quantify(T value, double weight);

    public double quantify(T value) {
        return this.quantify(value, 1.0D);
    }
}
