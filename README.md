[![](https://img.shields.io/maven-central/v/tools.cipher/core.svg?label=Maven%20Central)](https://search.maven.org/search?q=tools.cipher)
[![](https://img.shields.io/badge/License-GNU-blue.svg)](https://opensource.org/licenses/GPL-3.0)

# Implementing a Cipher
To create a cipher you need extend `ICipher` or better one of the abstract
cipher implementions `UniKeyCipher`, `BiKeyCipher` .... or `QuinKeyCipher`
which just requires you to specify the key types, encryption and decryption
algorithm and then gives you easy access to encryption and decryption
functions, the ability to generate a randomised key and iterate through all
the keys.

For examples or to use the implementations see [ciphertools-core](https://gitlab.com/ciphertools/main/tree/master/src/core/java/tools/cipher/ciphers).  
As an example the caesar cipher `extends UniKeyCipher<Integer, IntegerKeyType.Builder>`
specifies the key in the constructor

```
public CaesarCipher() {
    super(IntegerKeyType.builder().setRange(1, 25));
}
```
and then the encryption and decryption algorithms in the functions  
`CharSequence encode(CharSequence plainText, Integer key);`  
`char[] decodeEfficently(CharSequence cipherText, char[] plainText, Integer key);`

It can then be used as so...

```
CaesarCipher cipher = new CaesarCipher();

String cipherText = cipher.encode("PLAINTEXT", 4);
String plainText  = cipher.decode("CIPHETEXT", 23);
Integer randShift = cipher.randomiseKey();
cipher.iterateKeys(shift -> {
    // Do something with 'shift'
});
```

# The libraries

# lib
ciphertool-lib contains lots of useful functions for text analysis...

# base
ciphertool-base provides the framework in which cipher encryption and
decryption algorithms can be implemented. Provides generic cipher
interfaces, key generation/iteration tools and general utilities.

# core
ciphertool-core provides implementations of most classical ciphers

# identify
ciphertool-identify can analysis text and attempt to identify the cipher used.

# Additional Files

Calculating text fitness requires additional frequency files... otherwise certain aspects will fail to execute.  
I have provided links to the original source as I did not generate them. All the files should be placed at the root of the project, in a folder called `files`.

The essential files are:  
`/files/english_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/english_quadgrams.txt.zip>  
`/files/english_words.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/english_words.txt.zip>  
`/files/english_bigrams_1.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/english_bigrams_1.txt>  
`/files/english_trigrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/english_trigrams.txt.zip>  
`/files/english_wordlist.txt` from <https://web.archive.org/web/20150219162314/http://www-01.sil.org/linguistics/wordlists/english/wordlist/wordsEn.txt>

For splitting text into sentences with spaces between words:  
`/files/count_1w.txt` <http://practicalcryptography.com/media/cryptanalysis/files/count_1w.txt>  
`/files/count_2w.txt` <http://practicalcryptography.com/media/cryptanalysis/files/count_2w.txt>

If you want to use other languages (not required):  
`/files/danish_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/danish_quadgrams.txt.zip>  
`/files/finnish_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/finnish_quadgrams.txt.zip>  
`/files/french_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/french_quadgrams.txt.zip>  
`/files/german_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/german_quadgrams.txt.zip>  
`/files/icelandic_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/icelandic_quadgrams.txt.zip>  
`/files/polish_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/polish_quadgrams.txt.zip>  
`/files/spanish_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/spanish_quadgrams.txt.zip>  
`/files/spanish_quadgrams.txt` from <http://practicalcryptography.com/media/cryptanalysis/files/swedish_quadgrams.txt.zip>

# Add as a dependency

Pre-compiled jars are hosted on [Maven Central](https://search.maven.org/) so can be easily added to a build script,
for a gradle project following blocks

```
repositories {
    mavenCentral()
}
```
```
dependencies {
    implementation 'tools.cipher:core:0.2.0'
}
```
