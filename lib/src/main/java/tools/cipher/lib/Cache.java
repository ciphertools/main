/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.util.function.Supplier;

public class Cache<T> {

    protected T value;

    protected final Supplier<T> load;

    /**
     * @param loadIn Cannot supply null as this is reserved to indicate no value cached
     */
    protected Cache(Supplier<T> loadIn) {
        if (loadIn == null) {
            throw new IllegalArgumentException();
        }

        this.load = loadIn;
    }

    public T get() {
        if (this.value == null) {
            this.value = this.load.get();
        }

        return this.value;
    }

    public void release() {
        this.value = null;
    }

    public static <T> Cache<T> create(Supplier<T> loadIn) {
        return new Cache<>(loadIn);
    }

    public static <T> Cache<T> make(Supplier<T> loadIn) {
        Cache<T> cache = create(loadIn);
        cache.get();
        return cache;
    }
}
