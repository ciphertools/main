/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Consumer;

import tools.cipher.lib.fitness.TextFitness;
import com.alexbarter.lib.Utils;

public class FileReader {

    public static void read(File file, Consumer<String> action) throws FileNotFoundException, IOException {
        if(!file.exists())  {
            throw new FileNotFoundException(file.getName() + " (No such file)");
        }

        FileInputStream fIn = new FileInputStream(file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fIn));

        read(reader, action);
    }

    public static void read(String resourcePath, Consumer<String> action) throws FileNotFoundException, IOException {
        InputStream inputStream = TextFitness.class.getResourceAsStream(resourcePath);

        if (inputStream == null) {
            throw new FileNotFoundException(resourcePath + " (No such resource)");
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        read(reader, action);
    }

    public static void read(BufferedReader reader, Consumer<String> action) throws IOException {
        try  {
            String line = null;
            while((line = reader.readLine()) != null) {
                action.accept(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        } finally {
            Utils.tryClose(reader);
        }
    }


    @Deprecated /** Use FileReader.read(File, Consumer<String>), **/
    public static void compileTextFromFile(String filePath, Consumer<String> action) {
        try {
            read(new File(filePath), action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Deprecated /** Use FileReader.read(File, Consumer<String>), **/
    public static void readLinesFromFile(File file, Consumer<String> action) {
        try {
            read(file, action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Deprecated /** Use FileReader.read(String, Consumer<String>), **/
    public static void compileTextFromResource(String resourcePath, Consumer<String> action) {
        try {
            read(resourcePath, action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Deprecated /** Use FileReader.read(BufferedReader, Consumer<String>), **/
    public static void readLinesFromBufferedReader(BufferedReader reader, Consumer<String> action) {
        try {
            read(reader, action);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
