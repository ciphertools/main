/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.fitness;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import tools.cipher.lib.file.FileReader;
import tools.cipher.lib.language.ILanguage;

/**
 * @author Alex Barter (10AS)
 */
public class TextFitness {

    public static double scoreFitnessQuadgrams(String text, ILanguage language) {
        return scoreFitnessQuadgrams(text.toCharArray(), language);
    }

    public static double scoreFitnessQuadgrams(char[] text, ILanguage language) {
        NGramData quadgramData = language.getQuadgramData();

        double fitness = 0.0D;
        for(int k = 0; k < (text.length - 4 + 1); k++) {
            fitness += scoreWord(text, k, quadgramData);
        }

        return fitness;
    }

    public static double scoreFitness(char[] text, NGramData ngramData) {
        double fitness = 0.0D;
        for(int k = 0; k < (text.length - ngramData.getLength() + 1); k++) {
            fitness += scoreWord(text, k, ngramData);
        }

        return fitness;
    }

    /* Dynamicly checking if it better score
    public static double scoreFitnessQuadgrams(char[] text, ILanguage language, double bestScore) {
        NGramData quadgramData = language.getQuadgramData();

        double fitness = 0.0D;
        for(int k = 0; k < (text.length - 4 + 1); k++) {
            fitness += scoreWord(text, k, quadgramData);
            if(fitness < bestScore)
                return bestScore - 1D;
        }

        return fitness;
    }*/


    public static double scoreWord(char[] s, int startIndex, NGramData nGramData) {
        return nGramData.getValue(s, startIndex);
    }

    public static double getEstimatedFitness(String text, ILanguage language) {
        return getEstimatedFitness(text.length(), language.getQuadgramData());
    }

    public static double getEstimatedFitness(int length, NGramData nGramData) {
        return nGramData.getAverageFitness() * Math.max(0, length - nGramData.getLength() + 1);
    }

    public static NGramData loadFile(String resourcePath) {
        NGramReader reader = new NGramReader();

        try {
            FileReader.read(resourcePath, reader::readLine);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        Map<String, Double> mapping = new HashMap<>();
        BigDecimal fitnessPerChar = BigDecimal.ZERO;

        for(Entry<String, BigDecimal> rawEntry : reader.entries()) {
            BigDecimal doubleD = rawEntry.getValue().divide(reader.getTotal(), 20, RoundingMode.HALF_DOWN);
            double log = Math.log10(doubleD.doubleValue());

            mapping.put(rawEntry.getKey(), Math.log10(doubleD.doubleValue()));
            fitnessPerChar = fitnessPerChar.add(doubleD.multiply(BigDecimal.valueOf(log)));
        }

        // The fitness given to an ngram that is not in the file
        double floor = Math.log10(BigDecimal.valueOf(0.01D).divide(reader.getTotal(), 20, RoundingMode.HALF_DOWN).doubleValue());

        System.out.println("Sucessfully loaded ngrams from " + resourcePath);
        return new NGramData(mapping, floor, fitnessPerChar.doubleValue(), reader.getGramSize());
    }

    private static class NGramReader {

        private final HashMap<String, BigDecimal> mapping = new HashMap<>();
        private BigDecimal total = BigDecimal.ZERO;
        private int length = -1;

        public void readLine(String line) {
            String[] str = line.split(" ");
            // Ignore lines with less than 2 entries
            if(str.length < 2) { return; }

            String ngram = str[0];
            String countStr = str[1];

            BigDecimal count = new BigDecimal(new BigInteger(countStr));
            this.total = total.add(count);
            this.mapping.put(ngram, count);

            if(this.length == -1) {
                this.length = ngram.length();
            } else if (this.length != ngram.length()) {
                throw new IllegalArgumentException("Error loading line: " + line);
            }
        }

        public Set<Entry<String, BigDecimal>> entries() {
            return this.mapping.entrySet();
        }

        public BigDecimal getTotal() {
            return this.total;
        }

        public int getGramSize() {
            return this.length;
        }
    }
}
