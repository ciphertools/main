/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.fitness;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import com.alexbarter.lib.util.MathUtil;

/**
 * @author Alex Barter
 */
public class NGramData {

    private final Double[] valueMapping; // Using Double instead of double so we can have null to detect when value not set
    private final int nGramLength;
    private final int[] powValues;
    private final double floor;
    private final double fitnessPerChar;
    private final double maxValue;

    /**
     * Assumes that ngrams are all of the same length
     */
    public NGramData(Map<String, Double> mapping, double floor, double fitnessPerChar, int nGram) {
        this.floor = floor;
        this.fitnessPerChar = fitnessPerChar;
        this.nGramLength = nGram;
        this.powValues = new int[nGram];
        for(int i = 0; i < this.powValues.length; i++) {
            this.powValues[this.powValues.length - i - 1] = (int)Math.pow(26, i);
        }
        this.maxValue = MathUtil.getSmallest(mapping.values());

        this.valueMapping = new Double[(int)Math.pow(26, nGram)];
        Arrays.fill(this.valueMapping, floor);

        for(Entry<String, Double> entry : mapping.entrySet()) {
            String ngram = entry.getKey();
            for(int i = 0; i < this.nGramLength; i++) {
                if(ngram.charAt(i) < 'A' || ngram.charAt(i) > 'Z') {
                    break;
                }
            }

            int value = 0;
            for(int i = 0; i < this.nGramLength; i++) {
                value += (ngram.charAt(i) - 'A') * this.powValues[i];
            }

            this.valueMapping[value] = entry.getValue();
        }

        // Fill in the null with floor value
        for (int i = 0; i < this.valueMapping.length; i++) {
            if (this.valueMapping[i] == null) {
                this.valueMapping[i] = this.floor;
            }
        }
    }

    public double getValue(char[] gram, int startIndex) {
        int intConversion = 0;
        for(int i = startIndex; i < startIndex + this.nGramLength; i++) {
            if(gram[i] < 'A' || gram[i] > 'Z') {
                return this.floor;
            }
            intConversion += (gram[i] - 'A') * this.powValues[i - startIndex];
        }

        return this.valueMapping[intConversion];
    }

    public double getValue(CharSequence gram, int startIndex) {
        int intConversion = 0;
        for(int i = startIndex; i < startIndex + this.nGramLength; i++) {
            if(gram.charAt(i) < 'A' || gram.charAt(i) > 'Z') {
                return this.floor;
            }
            intConversion += (gram.charAt(i) - 'A') * this.powValues[i - startIndex];
        }

        return this.valueMapping[intConversion];
    }

    public double getAverageFitness() {
        return this.fitnessPerChar;
    }

    public int getLength() {
        return this.nGramLength;
    }

    public double getMaxValue() {
        return this.maxValue;
    }
}
