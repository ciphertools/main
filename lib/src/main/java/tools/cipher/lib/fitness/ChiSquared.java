/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.fitness;

import java.util.Map;

import tools.cipher.lib.characters.CharacterCount;
import tools.cipher.lib.language.ILanguage;
import com.alexbarter.lib.util.MathUtil;

/**
 * @author Alex Barter
 */
public class ChiSquared {

    public static double calculate(CharSequence text, ILanguage language) {
        return calculate(CharacterCount.getCount(text), text.length(), language);
    }

    public static double calculate(char[] text, ILanguage language) {
        return calculate(CharacterCount.getCount(text), text.length, language);
    }

    public static double calculate(Map<Character, Integer> letters, int length, ILanguage language) {
        // If length is not given calculate from counts in map
        if (length == -1) {
            length = MathUtil.sumAsInt(letters.values());
        }

        double total = 0.0D;

        for(char letter : language.getLetterFrequency().keySet()) {
            double count = letters.getOrDefault(letter, 0);
            double expectedCount = language.getFrequencyOfLetter(letter) * length / 100;

            double sum = Math.pow(count - expectedCount, 2) / expectedCount;
            total += sum;
        }

        return total;
    }
}
