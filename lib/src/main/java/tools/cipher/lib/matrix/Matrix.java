/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.matrix;

import java.math.BigInteger;
import java.util.StringJoiner;
import java.util.function.BiFunction;

import tools.cipher.lib.characters.StringUtils;
import com.alexbarter.lib.util.MathUtil;

public class Matrix {

    /** Matrix data held in row to row order **/
    public Integer[] data;
    /** Order of matrix rows x column **/
    public int rows, columns;

    public Matrix(Integer[] data, int size) {
        this(data, size, size);
    }

    public Matrix(Integer[] data, int rows, int columns) {
        this.data = data;
        this.rows = rows;
        this.columns = columns;

        //Some error checking
        if(rows * columns != data.length)
            throw new IllegalArgumentException("Matrix data array len doesn't match specified args.");
    }

    public Matrix(BiFunction<Integer, Integer, Integer> dataFunc, int rows, int columns) {
        this.data = new Integer[rows * columns];
        this.rows = rows;
        this.columns = columns;

        //Some error checking
        if(rows * columns != this.data.length)
            throw new IllegalArgumentException("Matrix data array len doesn't match specified args.");

        for (int r = 0; r < this.rows; r++) {
            for (int c = 0; c < this.columns; c++) {
                this.data[r * this.columns + c] = dataFunc.apply(r, c);
            }
        }
    }

    /**
     * Creates a blank matrix -- MUST BE POPULATED --
     */
    public Matrix(int rows, int columns) {
        this(new Integer[rows * columns], rows, columns);
    }

    public Matrix(int size) {
        this(size, size);
    }

    public void print() {
        System.out.println("Matrix ");
        for(int i = 0; i < this.rows; ++i) {
            StringJoiner str = new StringJoiner(","); // At least this in size
            for(int k = 0; k < this.columns; ++k) {
                str.add(Integer.toString(this.data[i * this.columns + k]));
            }
            System.out.println(str.toString());
        }
        System.out.println(" Y Order: " + this.rows);
        System.out.println(" X Order: " + this.columns);
        System.out.println("");
    }

    public Matrix add(Matrix parent) {
        if(this.rows != parent.rows || this.columns != parent.columns) {
            throw new MatrixMutiplyException("Matrices dimensions are not the same.");
        }

        Matrix result = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                result.data[r * result.columns + c] = this.data[r * this.columns + c] + parent.data[r * this.columns + c];

        return result;
    }

    public Matrix subtract(Matrix parent) {
        if(this.rows != parent.rows || this.columns != parent.columns) {
            throw new MatrixMutiplyException("Matrices dimensions are not the same.");
        }

        Matrix result = new Matrix((r, c) -> 0, this.rows, this.columns);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                result.data[r * result.columns + c] = this.data[r * this.columns + c] - parent.data[r * this.columns + c];

        return result;
    }

    public Matrix multiply(int scalar) {
        Matrix product = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                product.data[r * product.columns + c] = this.data[r * this.columns + c] * scalar;

        return product;
    }

    public Matrix divide(int scalar) {
        Matrix product = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                product.data[r * product.columns + c] = (int) Math.floor((double) this.data[r * this.columns + c] / scalar);

        return product;
    }

    public Matrix multiply(Matrix parent) {
        if(this.columns != parent.rows)
            throw new MatrixMutiplyException("Matrices are invalid size for multiplication");

        Matrix product = new Matrix((r, c) -> 0, this.rows, parent.columns);

        //Dot product each row from this matrix with each column from parent matrix
        for(int rT = 0; rT < this.rows; ++rT) {
            for(int cP = 0; cP < parent.columns; ++cP) {
                for(int pos = 0; pos < this.columns; ++pos) {
                    product.data[rT * product.columns + cP] +=
                            this.data[rT * this.columns + pos] *
                            parent.data[pos * parent.columns + cP];
                }
            }
        }

        return product;
    }

    /**
     * A crude modular function adds or subtracts the given mod till each cell is within 0 and mod.
     */
    public Matrix modular(int mod) {
        Matrix result = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; r++)
            for(int c = 0; c < this.columns; c++)
                result.data[r * result.columns + c] = MathUtil.wrap(this.data[r * result.columns + c], 0, mod);

        return result;
    }

    /**
     * Returns the matrix which is the equivalent of dividing by said matrix
     */
    public Matrix inverse() throws MatrixNoInverse {
        int determinant = this.determinant();

        if(determinant == 0)
            throw new MatrixNoInverse("Matrix has no inverse, determinant is 0.");

        return this.minors().cofactor().adjugate().divide(determinant);
    }

    public boolean hasInverseMod(int mod) {
        int determinant = (int) this.determinant();

        //Other Option: return MathUtil.hasMultiplicativeInverse(determinant, mod);
        try {
            BigInteger.valueOf(determinant).modInverse(BigInteger.valueOf(mod)).intValue();
            return true;
        }
        catch(ArithmeticException e) {
            return false;
        }
    }

    /**
     * Returns the matrix which is the equivalent of multiplying by the multiplicative inverse matrix
     */
    public Matrix inverseMod(int mod) throws MatrixNoInverse {
        double determinant = this.determinant();

        int multiplicativeInverse = 0;
        try {
            multiplicativeInverse = BigInteger.valueOf((int)determinant).modInverse(BigInteger.valueOf(mod)).intValue();
        }
        catch(ArithmeticException e) {
            throw new MatrixNoInverse("Matrix no inverse in mod " + mod + ", De: " + determinant  + ", MuIn of De: " + multiplicativeInverse);
        }

        return this.minors().cofactor().adjugate().modular(mod).multiply(multiplicativeInverse).modular(mod);
    }

    /**
     * Reflects the matrix in the main diagonal (which runs from top-left to bottom-right)
     */
    public Matrix adjugate() {
        Matrix result = new Matrix(this.columns, this.rows);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                result.data[c * result.columns + r] = this.data[r * this.columns + c];

        return result;
    }

    /**
     * 1x1 and 2x2 are special otherwise the in minor matrix each term is the determinant
     * of the matrix excluding the row and column of which the term is in
     * Minor matrix is only defined for square matrices
     */
    public Matrix minors() {
        if(this.rows != this.columns)
            throw new MatrixNotSquareException("The determinant can't be found for non-square matrices");

        Matrix result = new Matrix(this.rows, this.columns);

        switch(this.rows) {
        case 1: //Simply the only number in the matrix
            result.data[0] = this.data[0];
            break;
        case 2: //2x2 matrices
            result.data[0] = this.data[3];
            result.data[1] = this.data[2];
            result.data[2] = this.data[1];
            result.data[3] = this.data[0];
            break;
        default: //Matrices greater than 2x2
            for(int r = 0; r < this.rows; r++)
                for(int c = 0; c < this.columns; c++)
                    result.data[r * result.columns + c] = this.createMatrixExcluding(r, c).determinant();
        }

        return result;
    }

    /**
     * Then the cofactor matrix C has each element of M multiplied by its sign
     * -1^{r+c} where r,c are the row and column number of each position.
     */
    public Matrix cofactor() {
        Matrix result = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; r++) {
            for(int c = 0; c < this.columns; c++) {
                result.data[r * result.columns + c] = this.changeSign(r) * this.changeSign(c) * this.data[r * this.columns + c];
            }
        }

        return result;
    }

    public int determinant() {
        if(this.rows != this.columns) {
            throw new MatrixNotSquareException("The determinant can't be found for non-square matrices");
        }

        switch(this.rows) {
        case 1: //Simply the only number in the matrix
            return this.data[0];
        case 2: //2x2 matrix ad - bc
            return this.data[0] * this.data[3] - this.data[1] * this.data[2];
        default: //Matrices greater than 2x2
            int sum = 0;
            for(int i = 0; i < this.columns; i++) {
                sum += this.changeSign(i) * this.data[i] * this.createMatrixExcluding(0, i).determinant();
            }
            return sum;
        }
    }

    public Matrix createMatrixExcluding(int rowT, int columnT) {
        Matrix minorCopy = new Matrix(this.columns - 1, this.rows - 1);

        int rowN = 0;
        for(int rowO = 0; rowO < this.rows; rowO++) {
            if(rowO == rowT) continue;

            int columnN = 0;
            for(int columnO = 0; columnO < this.columns; columnO++) {
                if(columnO == columnT) continue;
                minorCopy.data[rowN * minorCopy.columns + columnN] = this.data[rowO * this.columns + columnO];
                columnN += 1;
            }
            rowN += 1;
        }

        return minorCopy;
    }

    public int size() {
        if(this.rows != this.columns) {
            throw new MatrixNotSquareException("Non-square matrices do not have a square size!");
        }

        return this.rows;
    }

    public Matrix copy() {
        Matrix copy = new Matrix(this.rows, this.columns);

        for(int r = 0; r < this.rows; ++r)
            for(int c = 0; c < this.columns; ++c)
                copy.data[r * copy.columns + c] = this.data[r * this.columns + c];

        return copy;
    }

    @Override
    public String toString() {
        return String.format("%dx%d [%s]", this.rows, this.columns, StringUtils.join(", ", this.data));
    }

    //Small internally used function that returns 1 when i is even and -1 when odd
    private int changeSign(int i) {
        return i % 2 == 0 ? 1 : -1;
    }
}
