/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.parallel;

import java.io.PrintStream;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.alexbarter.lib.CollectionUtil;

// Creates Jobs and assigns them to worker threads
public class MasterThread extends Thread {

    public static final IShutdownCondition NONE = () -> false;

    public final LinkedBlockingQueue<Runnable> jobs = new LinkedBlockingQueue<>(100000);
    private final ThreadGroup threadGroup = new ThreadGroup("job-queue");
    private final Set<WorkerThread> workers = new LinkedHashSet<WorkerThread>();
    private final Consumer<MasterThread> jobProvider;
    private IErrorHandler errorHandler;
    private volatile boolean finishedAddingJobs = false;
    private volatile boolean terminated = false;

    public MasterThread(Consumer<MasterThread> jobProvider) {
        this(Runtime.getRuntime().availableProcessors(), jobProvider);
    }

    public MasterThread(int numThreads, Consumer<MasterThread> jobProvider) {
        if (numThreads < 1 || jobProvider == null) {
            throw new IllegalArgumentException();
        }

        this.jobProvider = jobProvider;
        for (int i = 0; i < numThreads; i++) {
            this.workers.add(new WorkerThread(this, this.threadGroup));
        }
    }

    public MasterThread setErrorHandler(IErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
        return this;
    }

    @Override
    public void run() {
        // Start all the workers
        this.workers.forEach(Thread::start);

        // Generate jobs, this may sleep while it waits for there to be space in the job queue
        this.jobProvider.accept(this);

        // Mark as finished to stop more jobs being added
        this.finishedAddingJobs = true;
    }

    /**
     *
     * @param job
     * @return
     */
    public Status tryAddJob(Runnable job, long sleep) {
        Status status = null;

        while((status = this.addJob(job)).retry()) {
            try {
                TimeUnit.MILLISECONDS.sleep(sleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return status;
    }

    // Not thread safe
    public Status addJob(Runnable job) {
        if (this.finishedAddingJobs || this.terminated) {
            return Status.END;
        } else if (this.jobs.remainingCapacity() == 0) {
            return Status.RETRY;
        } else if (!this.jobs.add(job)) {
            return Status.FAIL;
        } else {
            return Status.SUCCESS;
        }
    }

    @Nullable
    public Runnable getJob() {
        return this.jobs.poll();
    }

    public boolean hasFinishedAddedJobs() {
        return this.finishedAddingJobs;
    }

    public boolean terminated() {
        return this.terminated;
    }

    public boolean completed() {
        return (this.terminated || this.finishedAddingJobs) && this.jobs.isEmpty() && CollectionUtil.noneMatch(this.workers, WorkerThread::hasJobRunning);
    }

    /**
     * Stops any more jobs being added and removes all the current jobs
     * There may still be jobs being executed by the threads
     */
    public void shutdown() {
        this.terminated = true;
        this.finishedAddingJobs = true;
        this.jobs.clear();
    }

    public boolean waitTillCompleted(IShutdownCondition condition) {
        while (!this.completed()) {
            if (this.hasError() || condition.shouldShutdown()) {
                this.shutdown();
            }

            try {
                TimeUnit.MILLISECONDS.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (this.hasError()) {
            if (this.errorHandler != null) {
                this.errorHandler.onError(this);
            }
            return true;
        }

        return false;
    }

    private boolean hasError() {
        return CollectionUtil.anyMatch(this.workers, WorkerThread::hasError);
    }

    public List<Exception> getErrors() {
        return this.workers.stream().filter(WorkerThread::hasError).map(WorkerThread::getError).collect(Collectors.toList());
    }

    public static IErrorHandler defaultErrorHandler(PrintStream out) {
        return (control) -> {
            List<Exception> errors = control.getErrors();
            out.println(String.format("Child thread errored, num errors: %d", errors.size()));
            errors.get(0).printStackTrace(out);
        };
    }

    @FunctionalInterface
    public interface IErrorHandler {

        public void onError(MasterThread thread);
    }

    @FunctionalInterface
    public interface IShutdownCondition {

        public boolean shouldShutdown();
    }

    public enum Status {
        RETRY,
        END,
        FAIL,
        SUCCESS;

        /**
         * Should the job attempted to be added again
         */
        public boolean retry() {
            return this == RETRY;
        }

        public boolean end() {
            return this == END;
        }
    }
}
