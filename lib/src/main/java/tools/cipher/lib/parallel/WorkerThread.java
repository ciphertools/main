/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.parallel;

public class WorkerThread extends Thread {

    private MasterThread master;
    private Runnable currentJob;
    private Exception error;

    public WorkerThread(MasterThread master, ThreadGroup threadGroup) {
        super(threadGroup, "worker-thread");
        this.master = master;
        this.error = null;
    }

    @Override
    public void run() {
        while(true) {
            this.currentJob = this.master.getJob();

            if (this.currentJob != null) {
                // Catch error and mark worker as errored and stop processing
                try {
                    this.currentJob.run();
                } catch (Exception e) {
                    this.error = e;
                } finally {
                    if (this.hasError()) {
                        this.currentJob = null;
                        break;
                    }
                }
            } else {
                if (this.master.hasFinishedAddedJobs()) {
                    break;
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean hasError() {
        return this.error != null;
    }

    public Exception getError() {
        return this.error;
    }

    public boolean hasJobRunning() {
        return this.currentJob != null;
    }
}
