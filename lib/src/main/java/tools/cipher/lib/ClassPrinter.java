/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.lang.reflect.Field;
import java.util.Objects;
import java.util.StringJoiner;

public class ClassPrinter {

    public static String toString(Object obj) {
        try {
            StringBuilder builder = new StringBuilder();
            builder.append(obj.getClass().getSimpleName());
            builder.append(' ');
            StringJoiner joiner = new StringJoiner(", ", "[", "]");

            for(Field field : obj.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                joiner.add(String.format("%s=%s", field.getName(), Objects.toString(field.get(obj))));
            }

            builder.append(joiner);

            return builder.toString();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return "ERROR";
    }
}
