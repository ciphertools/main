/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import tools.cipher.lib.characters.CharSequenceUtils;

public class CipherUtils {

    private static final NumberFormat numFormatter = NumberFormat.getNumberInstance(Locale.UK);

    // TODO Use AlphabetMap
    public static Map<Character, Integer> createCharacterIndexMapping(CharSequence key) {
        Map<Character, Integer> keyIndex = new HashMap<Character, Integer>();

        for (int i = 0; i < key.length(); i++) {
            keyIndex.put(key.charAt(i), i);
        }

        return keyIndex;
    }

    public static byte getAlphaIndex(char alphaChar) {
        if ('A' <= alphaChar && alphaChar <= 'Z') {
            return (byte) (alphaChar - 'A');
        } else if ('a' <= alphaChar && alphaChar <= 'z') {
            return (byte) (alphaChar - 'a');
        } else {
            return -1;
        }
    }

    public static byte[] charSeqToByteArray(CharSequence input) {
        byte[] output = new byte[input.length()];
        for (int i = 0; i < input.length(); i++)
            output[i] = (byte) input.charAt(i);
        return output;
    }

    public static String byteArrayToCharSeq(byte[] input) {
        return new String(input, Charset.forName("UTF-8"));
    }

    public static String formatBigInteger(BigInteger value) {
        return numFormatter.format(value);
    }

    public static String displayAsLetters(Integer[] array) {
        StringBuilder builder = new StringBuilder(array.length);
        for (int i = 0; i < array.length; i++) {
            builder.append((char)(array[i] + 'A'));
        }
        return builder.toString();
    }

    //TODO
    public static <K> void recussivelyIterate(Object key, Consumer<List<Object>> accept, BiConsumer<Object, Consumer>... lists) {
        if (lists.length == 0) return;
        List<Object> list =  new ArrayList<>();
        Consumer<Consumer<K>> consumer = (func) -> {};
        BiConsumer<Object, Consumer> consumer3 = (o, c) -> {list.add(c); accept.accept(list);list.clear();};
        for (int i = lists.length - 2; i >=0; i--) {
            final BiConsumer<Object, Consumer> consumer2 = consumer3;
            int j = i;
            consumer3 = (o, c) -> lists[j].accept(key, n -> {list.add(n); consumer2.accept(key, null);});
        }
        consumer3.accept(null, null);
    }

    public static String genKeySquare(String keyword, CharSequence alphabet) {
        char[] builder = new char[alphabet.length()];
        int index = 0;
        for (int i = 0; i < keyword.length(); i++) {
            if (CharSequenceUtils.contains(alphabet, keyword.charAt(i)) && !ArrayUtil.contains(builder, 0, index, keyword.charAt(i))) {
                builder[index++] = keyword.charAt(i);
            }
        }

        for (int i = 0; i < alphabet.length(); i++) {
            char ch = alphabet.charAt(i);
            int keyindex = keyword.indexOf(ch);
            if (keyindex == -1) {
                builder[index++] = ch;
            }
        }
        return new String(builder);
    }
}
