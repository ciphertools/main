/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.stats;

import java.util.Map;

import tools.cipher.lib.characters.CharacterCount;

public class StatIC {

    public static double calculate(CharSequence text, int length, boolean overlap) {
        Map<String, Integer> letters = CharacterCount.getCount(text, length, length, overlap);

        double sum = 0.0D;
        for (int value : letters.values()) {
            sum += value * (value - 1);
        }

        int n = overlap ? text.length() - (length - 1) : text.length() / length;
        return sum / (n * (n - 1));
    }

    public static double calculateMonoIC(char[] text) {
        Map<Character, Integer> letters = CharacterCount.getCount(text);

        double sum = 0.0D;
        for (int value : letters.values()) {
            sum += value * (value - 1);
        }

        int n = text.length;
        return sum / (n * (n - 1));
    }
}
