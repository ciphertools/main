/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tools.cipher.lib.Cache;

public class Statistics<T extends Number & Comparable<T>> {

    private List<T> data = null;

    private Cache<T> max = Cache.create(() -> MathUtil.getBiggest(this.data));
    private Cache<T> min = Cache.create(() -> MathUtil.getSmallest(this.data));
    private Cache<T> sum = Cache.create(() -> MathUtil.sum(this.data));
    private Cache<Double> mean = Cache.create(() -> this.sum.get().doubleValue() / this.data.size());
    private Cache<Double> variance = Cache.create(() -> {
        double mean = this.mean.get();
        double variance = 0.0D;

        for(T n : this.data) {
            variance += Math.pow(mean - n.doubleValue(), 2);
        }

        return variance / this.data.size();
    });
    private Cache<Double> standardDeviation = Cache.create(() -> Math.sqrt(this.variance.get()));

    public Statistics(Iterable<T> data) {
        this.data = new ArrayList<>();
        for(T n : data) {
            this.data.add(n);
        }

        this.data = Collections.unmodifiableList(this.data);
    }

    public double getMean() {
        return this.mean.get();
    }

    public double getVariance() {
        return this.variance.get();
    }

    public double getStandardDeviation() {
        return this.standardDeviation.get();
    }

    public T getMax() {
        return this.max.get();
    }

    public T getMin() {
        return this.min.get();
    }

    @Override
    public String toString() {
        return String.format("Mean: %f, SD: %f ", this.getMean(), this.getStandardDeviation());
    }
}
