/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.math;

import tools.cipher.lib.matrix.Matrix;
import tools.cipher.lib.matrix.MatrixNoInverse;

public class SimultaneousEquations {

    public static Integer[] solve(Integer[][] simEquations, int mod) throws SimultaneousEquationExpection  {
        int UNKNOWNS = simEquations.length;

        Matrix matrix = new Matrix((r, c) -> simEquations[r][c], UNKNOWNS, UNKNOWNS);

        Matrix invMatrix = null;
        try {
            invMatrix = matrix.inverseMod(mod);
        } catch(MatrixNoInverse e) {
            throw new SimultaneousEquationExpection("No exact solution to simulation equations");
        }

        Matrix equalsMatrix = new Matrix((r, c) -> simEquations[r][UNKNOWNS], UNKNOWNS, 1);

        return invMatrix.multiply(equalsMatrix).modular(mod).data;
    }

    // Matrix does not support double value so this will return an approximate solution
    @SuppressWarnings("unused")
    private static Integer[] solve(Integer[][] simEquations) throws SimultaneousEquationExpection  {
        int UNKNOWNS = simEquations.length;

        Matrix matrix = new Matrix((r, c) -> simEquations[r][c], UNKNOWNS, UNKNOWNS);

        Matrix invMatrix = null;
        try {
            invMatrix = matrix.inverse();
        } catch(MatrixNoInverse e) {
            throw new SimultaneousEquationExpection("No exact solution to simulation equations");
        }

        Matrix equalsMatrix = new Matrix((r, c) -> simEquations[r][UNKNOWNS], UNKNOWNS, 1);

        return invMatrix.multiply(equalsMatrix).data;
    }
}
