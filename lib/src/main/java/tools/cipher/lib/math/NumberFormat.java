/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.math;

public class NumberFormat {

    /**
     * Returns {@link Integer#toString()} if the number given is an integer
     * otherwise {@link Double#toString()}
     * @param value The decimal (possibly int value) in
     * @return The number in string form without trailing .0
     */
    public static String getNumber(double value) {
        return (long) value == value
                ? Long.toString((long) value)
                : Double.toString(value);
    }
}
