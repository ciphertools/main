/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.swing;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@FunctionalInterface
public interface MouseClickedListener extends MouseListener {

    @Override
    public void mouseClicked(MouseEvent event);

    @Override
    default void mouseEntered(MouseEvent event) {}

    @Override
    default void mouseExited(MouseEvent event) {}

    @Override
    default void mousePressed(MouseEvent event) {}

    @Override
    default void mouseReleased(MouseEvent event) {}

    /**
     * Adds a {@link java.awt.event.MouseListener} that only requires the
     * {@link #mouseClicked(MouseEvent)} event to be specified. Therefore
     * the listener can be a lambda expression for.
     * @param component The component to add the mouse listener to
     * @param listener The mouse clicked listener
     * @return The component given
     */
    public static <T extends Component> T addTo(T component, MouseClickedListener listener) {
        component.addMouseListener(listener);
        return component;
    }
}
