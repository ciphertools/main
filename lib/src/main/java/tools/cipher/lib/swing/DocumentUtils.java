/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.swing;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.JTextComponent;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

public class DocumentUtils {

    public static DocumentFilter createTextFilter(String filterRegex) {
        return new DocumentTextFilter(filterRegex);
    }

    public static class DocumentTextFilter extends DocumentFilter {

        private String filterRegex;

        private DocumentTextFilter(String filterRegex) {
            this.filterRegex = filterRegex;
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            text = text.replaceAll(this.filterRegex, "");

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static class DocumentUpperCaseInput extends DocumentFilter {

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            text = text.toUpperCase();

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static class DocumentCardInput extends DocumentFilter {

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            text = text.replaceAll("[^0-9ATJQKcdhs,]", "");

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static class DocumentIntegerInput extends DocumentFilter {

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            text = text.replaceAll("[^0-9]", "");

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static class DocumentDoubleInput extends DocumentFilter {

        private JTextComponent textComponent;

        public DocumentDoubleInput(JTextComponent textComponent) {
            this.textComponent = textComponent;
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            String textFull = this.textComponent.getText();
            String textLeft = textFull.substring(0, offset) + textFull.substring(offset + length, textFull.length());

            if(textLeft.isEmpty() && text.startsWith(".")) {
                text = "0" + text;
            }
            if(textLeft.indexOf('.') != -1 && text.contains("."))
                text = text.replaceAll(".", "");


            text = text.replaceAll("[^0-9.]", "");

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static class DocumentIntegerRangeInput extends DocumentFilter {

        private JTextComponent textComponent;

        public DocumentIntegerRangeInput(JTextComponent textComponent) {
            this.textComponent = textComponent;
        }

        @Override
        public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
            String textFull = this.textComponent.getText();
            String textLeft = textFull.substring(0, offset) + textFull.substring(offset + length, textFull.length());

            if(textLeft.indexOf('-') != -1 && text.contains("-"))
                text = text.replaceAll("-", "");
            text = text.replaceAll("[^0-9-]", "");

            super.replace(fb, offset, length, text, attrs);
        }
    }

    public static abstract class DocumentChangeAdapter implements DocumentListener {

        @Override
        public void changedUpdate(DocumentEvent event) {
            this.onUpdate(event);
        }

        @Override
        public void insertUpdate(DocumentEvent event) {
            this.onUpdate(event);
        }

        @Override
        public void removeUpdate(DocumentEvent event) {
            this.onUpdate(event);
        }

        public abstract void onUpdate(DocumentEvent event);

    }

    @SuppressWarnings("serial")
    public static void addUndoManager(JTextArea textArea) {
        final UndoManager undoManager = new UndoManager();

        textArea.getDocument().addUndoableEditListener(undoManager);

        InputMap im = textArea.getInputMap(JComponent.WHEN_FOCUSED);
        ActionMap am = textArea.getActionMap();

        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Undo");
        im.put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()), "Redo");

        am.put("Undo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canUndo()) {
                        undoManager.undo();
                    }
                } catch (CannotUndoException exp) {
                    exp.printStackTrace();
                }
            }
        });
        am.put("Redo", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (undoManager.canRedo()) {
                        undoManager.redo();
                    }
                } catch (CannotUndoException exp) {
                    exp.printStackTrace();
                }
            }
        });
    }
}
