/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.swing;

import javax.swing.JSpinner;
import javax.swing.SpinnerListModel;
import javax.swing.SpinnerNumberModel;

public class JSpinnerUtil {

    /**
     * Returns an array of the two spinners that are interlinked
     */
    public static JSpinner[] createRangeSpinners(int minStart, int maxStart, int min, int max, int step) {
        SpinnerNumberModel modelMin = new SpinnerNumberModel(minStart, min, max, step);
        JSpinner spinnerMin = new JSpinner(modelMin);
        SpinnerNumberModel modelMax = new SpinnerNumberModel(maxStart, min, max, step) {
            private static final long serialVersionUID = 1L;

            @Override
            public Comparable<Integer> getMinimum() {
                Integer minVariable = (Integer)modelMin.getNumber().intValue();
                Integer minConstant = (Integer)super.getMinimum();
                return minVariable.compareTo(minConstant) > 0 ? minVariable : minConstant;
            }
        };

        JSpinner spinnerMax = new JSpinner(modelMax);
        spinnerMin.addChangeListener((event) -> {
            if(modelMin.getNumber().intValue() > modelMax.getNumber().intValue()) {
                spinnerMax.setValue(spinnerMin.getValue());
            }
        });

        spinnerMax.addChangeListener((event) -> {
            if(modelMin.getNumber().intValue() > modelMax.getNumber().intValue()) {
                spinnerMin.setValue(spinnerMax.getValue());
            }
        });

        ((JSpinner.DefaultEditor)spinnerMin.getEditor()).getTextField().setEditable(false);
        ((JSpinner.DefaultEditor)spinnerMax.getEditor()).getTextField().setEditable(false);

        return new JSpinner[] {spinnerMin, spinnerMax};
    }

    public static JSpinner createSpinner(int start, int min, int max, int step) {
        SpinnerNumberModel model = new SpinnerNumberModel(start, min, max, step);
        JSpinner spinner = new JSpinner(model);
        ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
        return spinner;
    }

    public static JSpinner createSpinner(Object[] data) {
        SpinnerListModel monthModel = new SpinnerListModel(data);
        JSpinner spinner = new JSpinner(monthModel);
        ((JSpinner.DefaultEditor)spinner.getEditor()).getTextField().setEditable(false);
        return spinner;
    }
}
