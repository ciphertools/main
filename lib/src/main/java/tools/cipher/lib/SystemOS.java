/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.io.File;

public class SystemOS {

    public static enum OS {
        LINUX,
        SOLARIS,
        WINDOWS,
        MACOS,
        UNKNOWN;
    }

    public static OS getPlatform() {
        String systemOs = System.getProperty("os.name").toLowerCase();
        if(systemOs.contains("win")) return OS.WINDOWS;
        if(systemOs.contains("mac")) return OS.MACOS;
        if(systemOs.contains("solaris") || systemOs.contains("sunos")) return OS.SOLARIS;
        if(systemOs.contains("linux") || systemOs.contains("unix")) return OS.LINUX;
        return OS.UNKNOWN;
    }

    public static File getMyDataFolder(String module) {
        File appdataFolder = SystemOS.getAppdataFolder();

        File localFile = new File(appdataFolder, "percivalalb/" + module + "/");

        if(!localFile.exists() && !localFile.mkdirs())
            throw new RuntimeException("The module directory could not be created: " + localFile);

        return localFile;
    }

    public static File getAppdataFolder() {
        String userHome = System.getProperty("user.home", ".");
        File localFile;
        switch(SystemOS.getPlatform()) {
            case LINUX:
            case SOLARIS:
                localFile = new File(userHome);
                break;
            case WINDOWS:
                String appdata = System.getenv("APPDATA");
                String finalPath = appdata != null ? appdata : userHome;
                localFile = new File(finalPath);
                break;
            case MACOS:
                localFile = new File(userHome, "Library/Application Support");
                break;
            default:
                localFile = new File(userHome);
        }

        if(!localFile.exists() && !localFile.mkdirs()) {
            throw new RuntimeException("The appdata directory could not be created: " + localFile);
        }

        return localFile;
    }
}
