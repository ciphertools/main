/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tools.cipher.lib.file.FileReader;

public class ReadableText {

    public static Map<String, Double>     MAPPING_MONOGRAM     = new HashMap<String, Double>();
    public static Map<String, Double>     MAPPING_DIGRAM         = new HashMap<String, Double>();
    public static List<Double>            LIST_UNSEEN         = new ArrayList<Double>();

    public static String parseText(String text) {
        return ReadableText.parseText(text.toCharArray());
    }

    public static String parseText(char[] textArray) {
        int textLength = textArray.length;
        int maxWordLength = Math.min(textLength, 20);

        double[][] scores = new double[textLength][maxWordLength];
        String[][] texts = new String[textLength][maxWordLength];
        String[][] words = new String[textLength][maxWordLength];

        for(int j = 0; j < maxWordLength; j++) {
            String s = new String(textArray, 0, j + 1);
            scores[0][j] = ReadableText.getMonogramLogProbability(s);
            texts[0][j] = s;
            words[0][j] = s;
        }

        for(int i = 1; i < textLength; i++) {
            for(int j = 0; j < maxWordLength; j++) {
                if(i + j + 1 > textLength) { break; }

                String bestTempWord = "";
                String bestTempText = "";
                double bestTempScore = Double.NEGATIVE_INFINITY;

                String s = new String(textArray, i, j + 1);

                for(int k = 0; k < Math.min(i, maxWordLength); k++) {
                    String lastS = texts[i - k - 1][k];
                    String lastWord = words[i - k - 1][k];

                    double score = scores[i - k - 1][k] + ReadableText.getDigramLogProbability(s, lastWord);

                    if(bestTempScore < score) {
                        bestTempWord = s;
                        bestTempScore = score;
                        bestTempText = lastS + " " + s;
                    }
                }

                scores[i][j] = bestTempScore;
                texts[i][j] = bestTempText;
                words[i][j] = bestTempWord;
            }
        }

        String bestTempText = "";
        double bestTempScore = Double.NEGATIVE_INFINITY;
        for(int i = 0; i < maxWordLength; i++) {
            double score = scores[scores.length - i - 1][i];
            if(bestTempScore < score) {
                bestTempScore = score;
                bestTempText = texts[texts.length - i - 1][i];
            }
        }

        return bestTempText;
    }

    public static double getMonogramLogProbability(String word) {
        return getDigramLogProbability(word, null);
    }

    public static double getDigramLogProbability(String word, String prevWord) {
        if(!ReadableText.MAPPING_MONOGRAM.containsKey(word)) {
            return ReadableText.LIST_UNSEEN.get(word.length());
        } else if(prevWord == null || prevWord.isEmpty()) {
            return ReadableText.MAPPING_MONOGRAM.get(word);
        } else {
            String combined = prevWord + " " + word;

            if(!ReadableText.MAPPING_DIGRAM.containsKey(combined))
                return ReadableText.MAPPING_MONOGRAM.get(word);

            return ReadableText.MAPPING_DIGRAM.get(combined);
        }
    }

    /**
     * Loads the predefined files from googles trillion word list and calculates the log probability each word
     */
    public static void loadFile() {
        double NO_TOKENS = 1024908267229.0D;
        double LOG_NO_TOKENS = Math.log10(NO_TOKENS);

        try {
            FileReader.read("/files/count_1w.txt", (line) -> {
                String[] parts = line.toUpperCase().split("\t");

                if(parts.length == 2)  {
                    double count = Double.valueOf(parts[1]);
                    ReadableText.MAPPING_MONOGRAM.put(parts[0], Math.log10(count) - LOG_NO_TOKENS);
                }
                else {
                    System.out.println(String.format("%s Format Error: %s", ReadableText.class.getSimpleName(), line));
                }
            });

            FileReader.read("/files/count_2w.txt", (line) -> {
                String[] parts = line.toUpperCase().split("\t");

                if(parts.length == 2) {
                    String[] bigram = parts[0].split(" ");
                    double count = Double.valueOf(parts[1]);
                    double log = Math.log10(count) - LOG_NO_TOKENS;

                    if(ReadableText.MAPPING_MONOGRAM.containsKey(bigram[0]))
                        log -= ReadableText.MAPPING_MONOGRAM.get(bigram[0]);

                    ReadableText.MAPPING_DIGRAM.put(parts[0], log);
                }
                else {
                    System.out.println(String.format("%s Format Error: %s", ReadableText.class.getSimpleName(), line));
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //Calculates the score given to words not in the trillion word list for various lengths
        for(int i = 0; i < 50; i++) {
            ReadableText.LIST_UNSEEN.add(1 - i - LOG_NO_TOKENS);
        }
    }
}
