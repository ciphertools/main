/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Danish extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 6.025D);
        freqMap.put('B', 2.000D);
        freqMap.put('C', 0.565D);
        freqMap.put('D', 5.858D);
        freqMap.put('E', 15.453D);
        freqMap.put('F', 2.406D);
        freqMap.put('G', 4.077D);
        freqMap.put('H', 1.621D);
        freqMap.put('I', 6.000D);
        freqMap.put('J', 0.730D);
        freqMap.put('K', 3.395D);
        freqMap.put('L', 5.229D);
        freqMap.put('M', 3.237D);
        freqMap.put('N', 7.240D);
        freqMap.put('O', 4.636D);
        freqMap.put('P', 1.756D);
        freqMap.put('Q', 0.007D);
        freqMap.put('R', 8.956D);
        freqMap.put('S', 5.805D);
        freqMap.put('T', 6.862D);
        freqMap.put('U', 1.979D);
        freqMap.put('V', 2.332D);
        freqMap.put('W', 0.069D);
        freqMap.put('X', 0.028D);
        freqMap.put('Y', 0.698D);
        freqMap.put('Z', 0.034D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/danish_quadgrams.txt");
        //trigramData = TextFitness.loadFile("/files/danish_trigrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Danish";
    }

    @Override
    public String getImagePath() {
        return "/image/danish_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0672D;
    }
}
