/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Swedish extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 9.383D);
        freqMap.put('B', 1.535D);
        freqMap.put('C', 1.486D);
        freqMap.put('D', 4.702D);
        freqMap.put('E', 10.149D);
        freqMap.put('F', 2.027D);
        freqMap.put('G', 2.862D);
        freqMap.put('H', 2.090D);
        freqMap.put('I', 5.817D);
        freqMap.put('J', 0.614D);
        freqMap.put('K', 3.140D);
        freqMap.put('L', 5.275D);
        freqMap.put('M', 3.471D);
        freqMap.put('N', 8.542D);
        freqMap.put('O', 4.482D);
        freqMap.put('P', 1.839D);
        freqMap.put('Q', 0.020D);
        freqMap.put('R', 8.431D);
        freqMap.put('S', 6.590D);
        freqMap.put('T', 7.691D);
        freqMap.put('U', 1.919D);
        freqMap.put('V', 2.415D);
        freqMap.put('W', 0.142D);
        freqMap.put('X', 0.159D);
        freqMap.put('Y', 0.708D);
        freqMap.put('Z', 0.070D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/swedish_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Swedish";
    }

    @Override
    public String getImagePath() {
        return "/image/swedish_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0655D;
    }
}
