/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Polish extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;
    public static Map<Character, Double> freqMap;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 10.503D);
        freqMap.put('B', 1.740D);
        freqMap.put('C', 3.895D);
        freqMap.put('D', 3.725D);
        freqMap.put('E', 7.352D);
        freqMap.put('F', 0.143D);
        freqMap.put('G', 1.731D);
        freqMap.put('H', 1.015D);
        freqMap.put('I', 8.328D);
        freqMap.put('J', 1.836D);
        freqMap.put('K', 2.753D);
        freqMap.put('L', 2.564D);
        freqMap.put('M', 2.515D);
        freqMap.put('N', 6.237D);
        freqMap.put('O', 6.667D);
        freqMap.put('P', 2.445D);
        freqMap.put('Q', 0.0D);
        freqMap.put('R', 5.243D);
        freqMap.put('S', 5.224D);
        freqMap.put('T', 2.475D);
        freqMap.put('U', 2.062D);
        freqMap.put('V', 0.012D);
        freqMap.put('W', 5.813D);
        freqMap.put('X', 0.004D);
        freqMap.put('Y', 3.206D);
        freqMap.put('Z', 4.852D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/polish_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Polish";
    }

    @Override
    public String getImagePath() {
        return "/image/polish_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0584D;
    }
}
