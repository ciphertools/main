/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import tools.cipher.lib.Cache;
import tools.cipher.lib.fitness.NGramData;
import com.alexbarter.lib.util.MathUtil;

/**
 * @author Alex Barter
 */
public abstract class ILanguage {

    private final Cache<Double> minFrequency =
            Cache.create(() -> MathUtil.getSmallest(this.getLetterFrequency().values()));
    private final Cache<Double> maxFrequency =
            Cache.create(() -> MathUtil.getBiggest(this.getLetterFrequency().values()));

    private final Cache<List<Character>> sortedCharacters = Cache.create(() -> this.getLetterFrequency()
            .entrySet()
            .stream()
            .sorted(new LetterFrequencyComparator())
            .map(Entry::getKey).collect(Collectors.toList()));
    private final Cache<List<Double>> sortedFrequency = Cache.create(() -> this.getLetterFrequency()
            .entrySet()
            .stream()
            .sorted(new LetterFrequencyComparator())
            .map(Entry::getValue).collect(Collectors.toList()));
    private final Cache<List<Character>> sortedCharactersReverse = Cache.create(() -> this.getLetterFrequency()
            .entrySet()
            .stream()
            .sorted(new LetterFrequencyComparator().reversed())
            .map(Entry::getKey).collect(Collectors.toList()));
    private final Cache<List<Double>> sortedFrequencyReverse = Cache.create(() -> this.getLetterFrequency()
            .entrySet()
            .stream()
            .sorted(new LetterFrequencyComparator().reversed())
            .map(Entry::getValue).collect(Collectors.toList()));
    private final Cache<Double> normalisedIC = Cache.create(() -> LanguageUtils.calculateNormalisedIC(this.getLetterFrequency().values()));

    private final Cache<Map<Character, Double>> frequencyMap = Cache.create(() -> {
        return Collections.unmodifiableMap(this.addLetterFrequencies(new HashMap<>()));
    });

    public abstract Map<Character, Double> addLetterFrequencies(Map<Character, Double> temp);

    /**
     * Entries should be static
     */
    public Map<Character, Double> getLetterFrequency() {
        return this.frequencyMap.get();
    }

    public double getFrequencyOfLetter(char character) {
        return this.getLetterFrequency().get(character);
    }

    public List<Character> getLetterLargestFirst() {
        return this.sortedCharactersReverse.get();
    }

    public List<Character> getLetterSmallestFirst() {
        return this.sortedCharacters.get();
    }

    public List<Double> getFrequencyLargestFirst() {
        return this.sortedFrequencyReverse.get();
    }

    public List<Double> getFrequencySmallestFirst() {
        return this.sortedFrequency.get();
    }

    public double getMaxFrequency() {
        return this.maxFrequency.get();
    }

    public double getMinFrequency() {
        return this.minFrequency.get();
    }

    public void clearCache() {
        this.frequencyMap.release();
        this.minFrequency.release();
        this.maxFrequency.release();
        this.sortedCharacters.release();
        this.sortedFrequency.release();
        this.sortedCharactersReverse.release();
        this.sortedFrequencyReverse.release();
        this.normalisedIC.release();
    }

    public abstract void loadNGramData();

    public abstract NGramData getQuadgramData();

    public abstract NGramData getDiagramData();

    public NGramData getTrigramData() {
        return null;
    }

    // TODO
    public Dictionary getDictionary() {
        return null;
    }

    public double getNormalisedIC() {
        return this.normalisedIC.get();
    }

    public abstract String getName();

    public abstract String getImagePath();

    private class LetterFrequencyComparator implements Comparator<Entry<Character, Double>> {

        @Override
        public int compare(Entry<Character, Double> o1, Entry<Character, Double> o2) {
            return Double.compare(o1.getValue(), o2.getValue());
        }

    }
}
