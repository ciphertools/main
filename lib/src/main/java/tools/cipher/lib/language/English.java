/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

public class English extends ILanguage {

    private NGramData quadgramData;
    private NGramData trigramData;
    private NGramData bigramData;

    protected English() {}

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 8.167D);
        freqMap.put('B', 1.492D);
        freqMap.put('C', 2.782D);
        freqMap.put('D', 4.253D);
        freqMap.put('E', 12.702D);
        freqMap.put('F', 2.228D);
        freqMap.put('G', 2.015D);
        freqMap.put('H', 6.094D);
        freqMap.put('I', 6.996D);
        freqMap.put('J', 0.153D);
        freqMap.put('K', 0.772D);
        freqMap.put('L', 4.025D);
        freqMap.put('M', 2.406D);
        freqMap.put('N', 6.749D);
        freqMap.put('O', 7.507D);
        freqMap.put('P', 1.929D);
        freqMap.put('Q', 0.095D);
        freqMap.put('R', 5.987D);
        freqMap.put('S', 6.327D);
        freqMap.put('T', 9.056D);
        freqMap.put('U', 2.758D);
        freqMap.put('V', 0.978D);
        freqMap.put('W', 2.360D);
        freqMap.put('X', 0.150D);
        freqMap.put('Y', 1.974D);
        freqMap.put('Z', 0.074D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/english_quadgrams.txt");
        trigramData = TextFitness.loadFile("/files/english_trigrams.txt");
        bigramData = TextFitness.loadFile("/files/english_bigrams_1.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getTrigramData() {
        return trigramData;
    }

    @Override
    public NGramData getDiagramData() {
        return bigramData;
    }

    @Override
    public String getName() {
        return "English";
    }

    @Override
    public String getImagePath() {
        return "/image/english_flag.png";
    }

    //@Override
    //public double getNormalCoincidence() {
    //    return 0.0667D;
    //}
}
