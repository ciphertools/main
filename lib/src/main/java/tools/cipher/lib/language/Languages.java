/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alex Barter (10AS)
 */
public class Languages {

    public static Danish DANISH = new Danish();
    public static English ENGLISH = new English();
    public static Finnish FINNISH = new Finnish();
    public static French FRENCH = new French();
    public static German GERMAN = new German();
    public static Icelandic ICELANDIC = new Icelandic();
    public static Polish POLISH = new Polish();
    public static Spanish SPANISH = new Spanish();
    public static Swedish SWEDISH = new Swedish();

    public static List<ILanguage> languages = new ArrayList<ILanguage>();

    public static ILanguage byName(String name) {
        for(int i = 0; i < languages.size(); ++i) {
            ILanguage language = languages.get(i);
            if (language.getName().equalsIgnoreCase(name)) {
                return language;
            }
        }
        return null;
    }

    public static String[] getNames() {
        String[] names = new String[languages.size()];
        for(int i = 0; i < languages.size(); ++i)
            names[i] = languages.get(i).getName();
        return names;
    }

    public static void loadNGgramData() {
        for(ILanguage language : languages)
            language.loadNGramData();
    }

    static {
        //languages.add(danish);
        languages.add(ENGLISH);
        //languages.add(finnish);
        //languages.add(FRENCH);
        //languages.add(german);
        //languages.add(icelandic);
        //languages.add(polish);
        //languages.add(spanish);
        //languages.add(swedish);
    }
}
