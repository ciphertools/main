/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Icelandic extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 10.110D);
        freqMap.put('B', 1.043D);
        freqMap.put('C', 0.0D);
        freqMap.put('D', 1.575D);
        freqMap.put('E', 6.418D);
        freqMap.put('F', 3.013D);
        freqMap.put('G', 4.241D);
        freqMap.put('H', 1.871D);
        freqMap.put('I', 7.578D);
        freqMap.put('J', 1.144D);
        freqMap.put('K', 3.314D);
        freqMap.put('L', 4.532D);
        freqMap.put('M', 4.041D);
        freqMap.put('N', 7.711D);
        freqMap.put('O', 2.166D);
        freqMap.put('P', 0.789D);
        freqMap.put('Q', 0.0D);
        freqMap.put('R', 8.581D);
        freqMap.put('S', 5.630D);
        freqMap.put('T', 4.953D);
        freqMap.put('U', 4.562D);
        freqMap.put('V', 2.437D);
        freqMap.put('W', 0.0D);
        freqMap.put('X', 0.046D);
        freqMap.put('Y', 0.900D);
        freqMap.put('Z', 0.0D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/icelandic_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Icelandic";
    }

    @Override
    public String getImagePath() {
        return "/image/icelandic_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0674D;
    }
}
