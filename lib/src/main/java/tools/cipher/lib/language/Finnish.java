/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Finnish extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;
    public static Map<Character, Double> freqMap;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 12.217D);
        freqMap.put('B', 0.281D);
        freqMap.put('C', 0.281D);
        freqMap.put('D', 1.043D);
        freqMap.put('E', 7.968D);
        freqMap.put('F', 0.194D);
        freqMap.put('G', 0.392D);
        freqMap.put('H', 1.851D);
        freqMap.put('I', 10.817D);
        freqMap.put('J', 2.042D);
        freqMap.put('K', 4.973D);
        freqMap.put('L', 5.761D);
        freqMap.put('M', 3.202D);
        freqMap.put('N', 8.826D);
        freqMap.put('O', 5.614D);
        freqMap.put('P', 1.842D);
        freqMap.put('Q', 0.013D);
        freqMap.put('R', 2.872D);
        freqMap.put('S', 7.862D);
        freqMap.put('T', 8.750D);
        freqMap.put('U', 5.008D);
        freqMap.put('V', 2.250D);
        freqMap.put('W', 0.094D);
        freqMap.put('X', 0.031D);
        freqMap.put('Y', 1.745D);
        freqMap.put('Z', 0.051D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/finnish_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Finnish";
    }

    @Override
    public String getImagePath() {
        return "/image/finnish_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0742D;
    }
}
