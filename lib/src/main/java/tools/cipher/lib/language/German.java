/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class German extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;
    public static Map<Character, Double> freqMap;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 6.516D);
        freqMap.put('B', 1.886D);
        freqMap.put('C', 2.732D);
        freqMap.put('D', 5.076D);
        freqMap.put('E', 16.396D);
        freqMap.put('F', 1.656D);
        freqMap.put('G', 3.009D);
        freqMap.put('H', 4.577D);
        freqMap.put('I', 6.550D);
        freqMap.put('J', 0.268D);
        freqMap.put('K', 1.417D);
        freqMap.put('L', 3.437D);
        freqMap.put('M', 2.534D);
        freqMap.put('N', 9.776D);
        freqMap.put('O', 2.594D);
        freqMap.put('P', 0.670D);
        freqMap.put('Q', 0.018D);
        freqMap.put('R', 7.003D);
        freqMap.put('S', 7.270D);
        freqMap.put('T', 6.154D);
        freqMap.put('U', 4.166D);
        freqMap.put('V', 0.846D);
        freqMap.put('W', 1.921D);
        freqMap.put('X', 0.034D);
        freqMap.put('Y', 0.039D);
        freqMap.put('Z', 1.134D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/german_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "German";
    }

    @Override
    public String getImagePath() {
        return "/image/german_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0762D;
    }
}
