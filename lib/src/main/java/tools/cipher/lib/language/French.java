/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class French extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;
    public static Map<Character, Double> freqMap;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 7.636D);
        freqMap.put('B', 0.901D);
        freqMap.put('C', 3.260D);
        freqMap.put('D', 3.669D);
        freqMap.put('E', 14.715D);
        freqMap.put('F', 1.066D);
        freqMap.put('G', 0.866D);
        freqMap.put('H', 0.737D);
        freqMap.put('I', 7.529D);
        freqMap.put('J', 0.613D);
        freqMap.put('K', 0.049D);
        freqMap.put('L', 5.456D);
        freqMap.put('M', 2.968D);
        freqMap.put('N', 7.095D);
        freqMap.put('O', 5.796D);
        freqMap.put('P', 2.521D);
        freqMap.put('Q', 1.362D);
        freqMap.put('R', 6.693D);
        freqMap.put('S', 7.948D);
        freqMap.put('T', 7.244D);
        freqMap.put('U', 6.311D);
        freqMap.put('V', 1.838D);
        freqMap.put('W', 0.074D);
        freqMap.put('X', 0.427D);
        freqMap.put('Y', 0.128D);
        freqMap.put('Z', 0.326D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/french_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "French";
    }

    @Override
    public String getImagePath() {
        return "/image/french_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0778D;
    }
}
