/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.language;

import java.util.Map;

import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;

/**
 * @author Alex Barter
 */
public class Spanish extends ILanguage {

    public static NGramData quadgramData;
    public static NGramData trigramData;

    @Override
    public Map<Character, Double> addLetterFrequencies(Map<Character, Double> freqMap) {
        freqMap.put('A', 11.525D);
        freqMap.put('B', 2.215D);
        freqMap.put('C', 4.019D);
        freqMap.put('D', 5.010D);
        freqMap.put('E', 12.181D);
        freqMap.put('F', 0.692D);
        freqMap.put('G', 1.768D);
        freqMap.put('H', 0.703D);
        freqMap.put('I', 6.247D);
        freqMap.put('J', 0.493D);
        freqMap.put('K', 0.011D);
        freqMap.put('L', 4.967D);
        freqMap.put('M', 3.157D);
        freqMap.put('N', 6.712D);
        freqMap.put('O', 8.683D);
        freqMap.put('P', 2.510D);
        freqMap.put('Q', 0.877D);
        freqMap.put('R', 6.871D);
        freqMap.put('S', 7.977D);
        freqMap.put('T', 4.632D);
        freqMap.put('U', 2.927D);
        freqMap.put('V', 1.138D);
        freqMap.put('W', 0.017D);
        freqMap.put('X', 0.215D);
        freqMap.put('Y', 1.008D);
        freqMap.put('Z', 0.467D);
        return freqMap;
    }

    @Override
    public void loadNGramData() {
        quadgramData = TextFitness.loadFile("/files/spanish_quadgrams.txt");
    }

    @Override
    public NGramData getQuadgramData() {
        return quadgramData;
    }

    @Override
    public NGramData getDiagramData() {
        return trigramData;
    }

    @Override
    public String getName() {
        return "Spanish";
    }

    @Override
    public String getImagePath() {
        return "/image/spanish_flag.png";
    }

    @Override
    public double getNormalisedIC() {
        return 0.0770D;
    }
}
