/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class CharacterCount {

    public static Map<String, Integer> getCount(CharSequence text, int minLength, int maxLength) {
        return getCount(text, minLength, maxLength, true);
    }

    public static Map<String, Integer> getCount(CharSequence text, int minLength, int maxLength, boolean overlap) {
        Map<String, Integer> map = new HashMap<>();

        if(text.length() >= minLength) {
            for(int length = minLength; length <= maxLength; ++length) {
                for(int k = 0; k < (text.length() - length + 1); k += (overlap ? 1 : length)) {
                    String s = text.subSequence(k, k + length).toString();
                    map.put(s, map.getOrDefault(s, 0) + 1);
                }
            }
        }

        return map;
    }

    /**
     * Returns a map that takes each character of characters set of {@code text} to
     * is the corresponding times it appeared in {@code text}
     *
     * This should be used over {@link #getCount(CharSequence, int, int, boolean)}
     * if you are just looking at single character counts.
     *
     * @param text The text to generate the map
     * @return A map that takes character to count appeared in text
     */
    public static Map<Character, Integer> getCount(CharSequence text) {
        HashMap<Character, Integer> map = new HashMap<Character, Integer>();
        for(int k = 0; k < text.length(); k++) {
            map.put(text.charAt(k), map.getOrDefault(text.charAt(k), 0) + 1);
        }

        return map;
    }

    /**
     * Returns a map that takes each character of characters set of {@code text} to
     * is the corresponding times it appeared in {@code text}.
     *
     * This should be used over {@link #getCount(CharSequence, int, int, boolean)}
     * if you are just looking at single character counts.
     *
     * @param text The text to generate the map
     * @return A map that takes character to count appeared in text
     */
    public static Map<Character, Integer> getCount(char[] text) {
        HashMap<Character, Integer> map = new HashMap<>();
        for(int k = 0; k < text.length; k++) {
            map.put(text[k], map.getOrDefault(text[k], 0) + 1);
        }

        return map;
    }

    /**
     * Returns the characters set of {@code text} in descending order count.
     * Equal counts are ordered in ascending letter ASCII value.
     *
     * @param text The text to generate the ordered character list
     * @return The list of ordered characters generated from the given test
     */
    public static List<Character> getOrderedCharacters(char[] text) {
        return getOrderedCharacter(getOrderedEntries(getCount(text)));
    }

    /**
     * Returns the characters set from the {@code text} in descending order count.
     * Equal counts are ordered in ascending letter ASCII value.
     *
     * @param text The text to generate the ordered character list
     * @return The list of ordered characters generated from the given test
     */
    public static List<Character> getOrderedCharacters(CharSequence text) {
        return getOrderedCharacter(getOrderedEntries(getCount(text)));
    }

    private static List<Entry<Character, Integer>> getOrderedEntries(Map<Character, Integer> map) {
        List<Entry<Character, Integer>> list = new ArrayList<>(map.entrySet());
        Collections.sort(list, LETTER_COUNT_COMPARATOR);
        return list;
    }

    private static List<Character> getOrderedCharacter(List<Entry<Character, Integer>> orderedEntries) {
        List<Character> list = new ArrayList<>(orderedEntries.size());
        for (int i = 0; i < orderedEntries.size(); i++) {
            list.add(orderedEntries.get(i).getKey());
        }
        return list;
    }

    private static Comparator<Entry<Character, Integer>> LETTER_COUNT_COMPARATOR = (o1, o2) -> {
        int comp = Integer.compare(o2.getValue(), o1.getValue());
        return comp == 0 ? Character.compare(o1.getKey(), o2.getKey()) : comp;
    };
}
