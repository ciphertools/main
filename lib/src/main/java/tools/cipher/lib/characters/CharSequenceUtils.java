/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

public class CharSequenceUtils {

    public static int indexOf(CharSequence seq, int a, int b, char t) {
        for(int i = a; i < b; i++) {
            if(seq.charAt(i) == t) {
                return i;
            }
        }
        return -1;
    }

    public static int indexOf(CharSequence seq, char t) {
        return indexOf(seq, 0, seq.length(), t);
    }

    public static boolean contains(CharSequence seq, int a, int b, char t) {
        return indexOf(seq, a, b, t) != -1;
    }

    public static boolean contains(CharSequence seq, char t) {
        return indexOf(seq, 0, seq.length(), t) != -1;
    }

    public static <T> Character[] toArray(CharSequence seq) {
        Character[] array = new Character[seq.length()];

        for(int i = 0; i < array.length; i++) {
            array[i] = seq.charAt(i);
        }

        return array;
    }

    public static <T> char[] toPrimitiveArray(CharSequence seq) {
        char[] array = new char[seq.length()];

        for(int i = 0; i < array.length; i++) {
            array[i] = seq.charAt(i);
        }

        return array;
    }

    public static boolean isCharSubset(CharSequence test, CharSequence universe) {
        for (int i = 0; i < test.length(); i++) {
            if (!CharSequenceUtils.contains(universe, test.charAt(i))) {
                return false;
            }
        }

        return true;
    }
}
