/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

public class CharacterArrayWrapper implements CharSequence {

    private final Character[] array;
    private int start, length;

    public CharacterArrayWrapper(Character[] array) {
        this(array, 0, array.length);
    }

    public CharacterArrayWrapper(Character[] array, int start, int length) {
        this.array = array;
        this.start = start;
        this.length = length;
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public char charAt(int index) {
        if (index < 0 || index >= this.length) {
            throw new IndexOutOfBoundsException();
        }

        return this.array[index + this.start];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0 || end < 0 || start > end || end > this.length) {
            throw new IndexOutOfBoundsException();
        }

        return new CharacterArrayWrapper(this.array, start, end - start);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(this.length);
        for (int i = this.start; i < this.start + this.length; i++) {
            builder.append((char) this.array[i]);
        }
        return builder.toString();
    }
}
