/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Alex Barter
 */
public class StringUtils {

    public static final Pattern DIGIT_PATTERN  = Pattern.compile("[0-9]");
    public static final Pattern LETTER_PATTERN = Pattern.compile("[a-zA-Z]");
    public static final Pattern SPACE_PATTERN  = Pattern.compile("\\s");
    public static final Pattern OTHER_PATTERN  = Pattern.compile("[^0-9a-zA-Z\\s]");

    public static String join(String delimiter, Object[] parts) {
        return join(delimiter, Objects::toString, parts);
    }

    public static <T> String join(String delimiter, Function<T, String> toString, T[] parts) {
        StringJoiner joiner = new StringJoiner(delimiter);
        for (T part : parts) {
            joiner.add(toString.apply(part));
        }
        return joiner.toString();
    }

    /**
     * Reverses the string, last character becomes first character etc
     * @param target The string to be reversed
     * @return The reverse of the given string
     */
    public static String reverseString(CharSequence target) {

        StringBuilder newString = new StringBuilder(target.length());
        // Runs through all the characters in reverse
        for(int i = target.length() - 1; i >= 0; i--) {
            newString.append(target.charAt(i));
        }

        return newString.toString();
    }

    /**
     * Creates a string of the <i>nth</i> characters with offset <i>o</i>
     * @param text The text to extract the nth character from
     * @param o The column to start Should be non-negative and
     * @param n The number of columns
     * @return The nth characters
     */
    public static String getEveryNthChar(CharSequence text, int o, int n) {
        StringBuilder accumulator = new StringBuilder((int) Math.floor((double) text.length() / n) + (text.length() % n > o ? 1 : 0));

        for(int i = o; i < text.length(); i += n) {
            accumulator.append(text.charAt(i));
        }

        return accumulator.toString();
    }

    public static String getEveryNthChar(char[] text, int start, int n) {
        StringBuilder accumulator = new StringBuilder((int) Math.floor((double) text.length / n) + (text.length % n > start ? 1 : 0));

        for(int i = start; i < text.length; i += n) {
            accumulator.append(text[i]);
        }

        return accumulator.toString();
    }


    /**
     * Similar to {@link #getEveryNthChar(CharSequence, int, int)} however treats every
     * every character as a block of entrySize characters
     */
    public static String getEveryNthBlock(String text, int entrySize, int start, int n) {
        int length = text.length() / entrySize;
        StringBuilder accumulator = new StringBuilder((int) Math.floor((double) length / n) + (length % n > start ? 1 : 0));

        for(int i = start; i < text.length() / entrySize; i += n) {
            accumulator.append(text, i * entrySize, (i + 1) * entrySize);
        }

        return accumulator.toString();
    }

    /**
     * Translates the string n places to the right wrapping characters to the
     * front of the string.
     * @param text The text in to rotate
     * @param n The amount to rotate to the right
     * @return The text rotated n places to the right
     */
    public static String rotateRight(CharSequence text, int n) {
        StringBuilder newString = new StringBuilder(text.length());
        int cuttingPoint = text.length() - (n % text.length());
        newString.append(text, cuttingPoint, text.length());
        newString.append(text, 0, cuttingPoint);
        return newString.toString();
    }

    /**
     * Translates the string n places to the left wrapping characters to the
     * back of the string.
     * @param text The text in to rotate
     * @param n The amount to rotate to the left
     * @return The text rotated n places to the left
     */
    public static String rotateLeft(CharSequence text, int n) {
        StringBuilder newString = new StringBuilder(text.length());
        int cuttingPoint = n % text.length();
        newString.append(text, cuttingPoint, text.length());
        newString.append(text, 0, cuttingPoint);
        return newString.toString();
    }

    /**
     * Creates a string by appending the given string <i>n</i> times
     * @param repeat The string to repeat
     * @param n The number of times to repeat the string
     * @return A string consisting of repetitions of the same string
     */
    public static String repeat(CharSequence repeat, int n) {
        StringBuilder total = new StringBuilder(repeat.length() * n);
        for(int i = 0; i < n; i++) {
            total.append(repeat);
        }
        return total.toString();
    }

    public static String[] splitInto(String text, int size) {
        String[] list = new String[(int) Math.ceil((double) text.length() / size)];
        for(int i = 0, j = 0; i < text.length(); i += size) {
            list[j++] = text.subSequence(i, Math.min(i + size, text.length())).toString();
        }
        return list;
    }

    public static int countMatchingCharacters(CharSequence text, String regex) {
        return countMatchingCharacters(text, Pattern.compile(regex));
    }

    public static int countMatchingCharacters(CharSequence text, Pattern pattern) {
        Matcher matcher = pattern.matcher(text);
        int count = 0;
        while(matcher.find()) { count++; }
        return count;
    }

    public static int countDigitChars(String text) {
        return countMatchingCharacters(text, DIGIT_PATTERN);
    }

    public static int countLetterChars(String text) {
        return countMatchingCharacters(text, LETTER_PATTERN);
    }

    public static int countSpacesChars(String text) {
        return countMatchingCharacters(text, SPACE_PATTERN);
    }

    public static int countOtherChars(String text) {
        return countMatchingCharacters(text, OTHER_PATTERN);
    }

    /**
     * Returns a set of characters, which by the specification of a set only
     * contains each character once, from the given text
     * @param inputText The text to create the char set from
     * @return A set of characters in the given text
     */
    public static Set<Character> getUniqueCharSet(CharSequence inputText) {
        Set<Character> set = new HashSet<>();

        for(int i = 0; i < inputText.length(); i++) {
            set.add(inputText.charAt(i));
        }

        return set;
    }

    public static boolean isContentEqual(CharSequence expected, CharSequence actual) {
        // Lengths must be equal otherwise they cannot be equal
        if(expected.length() != actual.length()) {
            return false;
        }

        // Check whether any characters diff at the same position
        for(int i = 0; i < expected.length(); i++) {
            if(expected.charAt(i) != actual.charAt(i)) {
                return false;
            }
        }

        // If no characters diff then content must be equal
        return true;
    }
}
