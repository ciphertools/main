/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

public class CharArrayWrapper implements CharSequence {

    private final char[] array;
    private int start, length;

    public CharArrayWrapper(char[] array) {
        this(array, 0, array.length);
    }

    public CharArrayWrapper(char[] array, int start, int length) {
        this.array = array;
        this.start = start;
        this.length = length;
    }

    @Override
    public int length() {
        return this.length;
    }

    @Override
    public char charAt(int index) {
        if (index < 0 || index >= this.length) {
            throw new IndexOutOfBoundsException();
        }

        return this.array[index + this.start];
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0 || end < 0 || start > end || end > this.length) {
            throw new IndexOutOfBoundsException();
        }

        return new CharArrayWrapper(this.array, start, end - start);
    }

    @Override
    public String toString() {
        return new String(this.array, this.start, this.length);
    }
}
