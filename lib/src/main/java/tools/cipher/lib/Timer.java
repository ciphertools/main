/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import tools.cipher.lib.math.Statistics;

/**
 * @author Alex Barter
 */
public class Timer {

    private long startTime = 0;
    public List<Long> recordedTimes;

    public Timer() {
        this.startTime = System.nanoTime();
        this.recordedTimes = new ArrayList<>();
    }

    public void restart() {
        this.startTime = System.nanoTime();
    }

    public void pause() {

    }

    public void runLoop() {

    }

    /**
     * Adds the time since last reset t
     * @return
     */
    public long recordTime() {
        long currentTime = this.getTimeRunning(TimeUnit.MILLISECONDS);
        this.recordedTimes.add(currentTime);
        return currentTime;
    }

    public Statistics<Long> getRecordedTimesStats() {
        return new Statistics<>(this.recordedTimes);

    }

    public void displayTime() {
        System.out.println("Milliseconds: " + this.getTimeRunning(TimeUnit.MILLISECONDS));
    }

    public void call() {
        this.displayTime();
        this.restart();
    }

    /**
     * @return The time in milliseconds since the object was created or startTime reset
     */
    public long getTimeRunning() {
        return System.nanoTime() - this.startTime;
    }

    public long getTimeRunning(TimeUnit unit) {
        long timeInNanoseconds = System.nanoTime() - this.startTime;
        return unit.convert(timeInNanoseconds, TimeUnit.NANOSECONDS);
    }
}
