/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.result;

import java.util.Arrays;

import com.alexbarter.lib.util.ArrayUtil;
import tools.cipher.lib.fitness.NGramData;
import tools.cipher.lib.fitness.TextFitness;
import tools.cipher.lib.language.ILanguage;

public class Solution extends ResultNegative {

    public static final Solution WORST_SOLUTION = new Solution(new char[0], Double.NEGATIVE_INFINITY);
    private static final String UNKNOWN_KEY = "UNKNOWN";

    private char[] text;
    /**
     * Indicates whether {@link Solution#text} is a copy and so should be immutable
     */
    private boolean beenBaked;
    public String keyString;

    public Solution(char[] text, double score) {
        this(text, score, false);
    }

    public Solution(char[] text, double score, boolean immutable) {
        super(score);
        this.text = text;
        this.beenBaked = immutable;
        this.keyString = Solution.UNKNOWN_KEY;
    }

    public Solution(char[] text, ILanguage language, boolean immutable) {
        this(text, TextFitness.scoreFitnessQuadgrams(text, language), immutable);
    }

    public Solution(char[] text, NGramData nGramData, boolean immutable) {
        this(text, TextFitness.scoreFitness(text, nGramData), immutable);
    }

    public Solution(char[] text, ILanguage language) {
        this(text, TextFitness.scoreFitnessQuadgrams(text, language), false);
    }

    public Solution(char[] text, NGramData nGramData) {
        this(text, TextFitness.scoreFitness(text, nGramData), false);
    }

    public Solution setKeyString(String keyString) {
        this.keyString = keyString;
        return this;
    }

    public Solution setKeyString(String keyString, Object... args) {
        return this.setKeyString(String.format(keyString, args));
    }

    /**
     * Makes a copy of the text array as quite often this array reference is reused
     * so it can change
     */
    public Solution bake() {
        if (!this.beenBaked) {
            this.beenBaked = true;
            this.text = ArrayUtil.copy(this.text);
        }
        return this;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(this.score);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (this.getClass() != obj.getClass())
            return false;
        Solution other = (Solution) obj;
        // The score must be close
        if (Math.abs(this.score - other.score) > 0.4)
            return false;
        return Arrays.equals(this.text, other.text);

    }

    @Override
    public String toString() {
        return String.format("Fitness: %f, Key: %s, Plaintext: %s", this.score, this.keyString, new String(this.text));
    }

    public char[] getRaw() {
        return this.text;
    }

    public String getText() {
        return new String(this.text);
    }
}
