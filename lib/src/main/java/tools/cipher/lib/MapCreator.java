/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

@Deprecated
public class MapCreator {

    private final Map<String, Object> map;

    private MapCreator(Supplier<Map<String, Object>> map) {
        this.map = map.get();
    }

    public MapCreator put(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

    public Map<String, Object> get() {
        return this.map;
    }

    public static MapCreator create() {
        return new MapCreator(HashMap::new);
    }

    public static MapCreator create(Supplier<Map<String, Object>> map) {
        return new MapCreator(map);
    }
}
