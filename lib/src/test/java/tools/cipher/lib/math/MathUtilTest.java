/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * @author Alex
 *
 */
public class MathUtilTest {


    @Test
    public void testSum() {
        assertEquals(588, MathUtil.sum(Arrays.asList(89, 28, 112, 5, 231, 123)), () -> "MathUtil#sum(Iterable<Integer>) failed");
        assertEquals(7.55D, MathUtil.sum(Arrays.asList(2.45D, 5D, 0.1D)), () -> "MathUtil#sum(Iterable<Double>) failed");
        assertEquals(16.222F, MathUtil.sum(Arrays.asList(12.222F, -0.2F, 4.2F)), () -> "MathUtil#sum(Iterable<Float>) failed");
        assertEquals(1502030595L, MathUtil.sum(Arrays.asList(13L, 1502030568L, 2L, 12L)), () -> "MathUtil#sum(Iterable<Long>) failed");

        assertEquals(145, MathUtil.sumByte(Arrays.asList((byte) 4, (byte) 17, (byte) 1, (byte) 123)), () -> "MathUtil#sumByte failed");
    }
}
