/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.math;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * @author Alex
 *
 */
public class SimultaneousEquationsTest {

    @Test
    public void test2x2SolveMod26() {
        Integer[] equation1 = {2,   3,  16};
        Integer[] equation2 = {15, -2,  2};
        Integer[] result = {22, 8};

        assertArrayEquals(result, SimultaneousEquations.solve(new Integer[][] {equation1, equation2}, 26), "Unable to solve 2x2 sim equations mod 26");
    }

    @Test
    public void test3x3UnsolvableMod26() {
        Integer[] equation1 = {1, 2, 3, 16};
        Integer[] equation2 = {0, 4, 2, 2};
        Integer[] equation3 = {0, 2, 2, 2}; //det = 1 * (4 * 2 - 2 * 2) = 4. which is not coprime to 26

        assertThrows(SimultaneousEquationExpection.class, () -> SimultaneousEquations.solve(new Integer[][] {equation1, equation2, equation3}, 26));
    }

    @Test
    public void test3x3SolveMod26() {
        Integer[] equation1 = {3,  1, 3, 1};
        Integer[] equation2 = {7,  4, 7, 23};
        Integer[] equation3 = {31, 9, 2, -2};
        Integer[] result = {8, 2, 9};

        assertArrayEquals(result, SimultaneousEquations.solve(new Integer[][] {equation1, equation2, equation3}, 26));
    }
}
