/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.registry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class RegistryTest {

    @Test
    public void testRegistry() {
        IRegistry<String, String> REGISTRY = Registry.builder(String.class).setNamingScheme(x -> x).setMaxSize(1).build();
        assertEquals(0, REGISTRY.size(), () -> "Intial registry size wrong");
        assertTrue(REGISTRY.register("1"));
        assertEquals(1, REGISTRY.size(), () -> "Mismatching registry size");
        assertFalse(REGISTRY.register("2"));
        assertEquals(1, REGISTRY.size(), () -> "Mismatching registry size");
    }
}
