/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

/**
 * @author Alex
 *
 */
public class StringUtilsTest {

    @Test
    public void reverseString() {
        assertEquals("EMESREVER",   StringUtils.reverseString("REVERSEME"));
        assertEquals("7!DAWE$)OFK", StringUtils.reverseString("KFO)$EWAD!7"));
    }

    @Test
    public void getEveryNthChar() {
        assertEquals("TQCRNXMOREZO", StringUtils.getEveryNthChar("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG", 0, 3));
        assertEquals("HLS",          StringUtils.getEveryNthChar("HELPS", 0, 2));
        assertEquals("EP",           StringUtils.getEveryNthChar("HELPS", 1, 2));
    }

    @Test
    public void getEveryNthBlock() {
        assertEquals("THCKNFMPRTZY", StringUtils.getEveryNthBlock("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG", 2, 0, 3));
        assertEquals("HEL",          StringUtils.getEveryNthBlock("HELPS", 3, 0, 2));
        assertEquals("LP",           StringUtils.getEveryNthBlock("HELPS", 2, 1, 2));
    }

    @Test
    public void repeat() {
        assertEquals("AAAAAAAAAA", StringUtils.repeat("A", 10));
        assertEquals("CATCATCAT", StringUtils.repeat("CAT", 3));
        assertEquals("==================================================", StringUtils.repeat("=", 50));
    }

    @Test
    public void rotate() {
        assertEquals("ZYDOGTHEQUICKBROWNFOXJUMPSOVERTHELA", StringUtils.rotateRight("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG", 5));
        assertEquals("ICKBROWNFOXJUMPSOVERTHELAZYDOGTHEQU", StringUtils.rotateLeft("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG", 5));
        assertEquals("TTES", StringUtils.rotateLeft("TEST", 15));
    }

    @Test
    public void splitInto() {
        String[] result = {"THEQUICK", "BROWNFOX", "JUMPSOVE", "RTHELAZY", "DOG"};
        assertArrayEquals(result, StringUtils.splitInto("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG", 8));
    }

    @Test
    public void countCharacters() {
        assertEquals(0, StringUtils.countDigitChars("THEQUICKBROWNFOXJUMPSOVERTHELAZYDOG"));
        assertEquals(6, StringUtils.countDigitChars("HE  LP2673*@~23"));
        assertEquals(2, StringUtils.countSpacesChars("HE  LP2673*@~23"));
        assertEquals(3, StringUtils.countOtherChars("HE  LP2673*@~23"));
    }

    @Test
    public void getUniqueCharSet() {
        assertEquals(Stream.of('A', 'B', 'C', 'D', 'E', 'F').collect(Collectors.toSet()), StringUtils.getUniqueCharSet("FAABDEC"));
    }
}
