/**
 * CipherTools - Tools for en/decoding and solving classical ciphers
 * Copyright (C) 2019-2021  Alex Barter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package tools.cipher.lib.characters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class WrapperTest {

    @Test
    public void testWrappers() {
        CharSequence wrapper = new CharArrayWrapper("HELP".toCharArray());
        assertEquals("HELP", wrapper.toString());
        assertTrue(StringUtils.isContentEqual("EL", wrapper.subSequence(1, 3)));

        wrapper = new CharacterArrayWrapper(new Character[] { 'L', 'O', 'O', 'K', 'U', 'P', 'N', 'O', 'W' });
        assertEquals("LOOKUPNOW", wrapper.toString());
        assertTrue(StringUtils.isContentEqual("UPNOW", wrapper.subSequence(4, 9)));
        assertTrue(StringUtils.isContentEqual("LOOK", wrapper.subSequence(0, 4)));
    }
}
